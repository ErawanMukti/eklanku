<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Application extends CI_Controller
{
  protected $data        = array();
  protected $tax_rate    = 0.1;
  protected $total_price = 0;
  protected $total_cart  = 0;

  public function __construct()
  {
    parent::__construct();
	
   /* $this->load->model('m_company');
    $this->load->model('m_cart');
    $this->load->model('m_province');*/
    $this->load->model('m_product_category');
    $this->load->model('m_product_category_sub');
    $this->load->model('m_product_category_last');
    /**/
//	$this->load->model('m_user_address');
	$this->load->model('m_billing_account');
	
	$this->_recheckSaldo();
	
    $member       = $this->session->member; //print_r($member);
	$explode_name = explode(' ', $member['name']);

    if (count($explode_name) > 1) {
      $member['short_name'] = $explode_name[0];
    }

    if (!empty($member)) {
      $this->data['member']    = $member;
      $this->data['is_member'] = true;
    } else {
      $this->data['member']    = array();
      $this->data['is_member'] = false;
    }
/*	$arrCart=array();
    if ($this->data['is_member']) {
      $carts = $this->m_cart->get_all_items($this->data['member']['id']);
	
      foreach ($carts as $key => $cart) {
        switch ($cart ['discount_type']) {
          case '1':
            $discount_price = $cart['price'] - ($cart['price'] * $cart['discount'] / 100);
          break;
          case '2':
            $discount_price = $cart['price'] - $cart['discount'];
          break;
        }

        if (!empty($discount_price)) {
          $cart[$key]['discount_price'] = $discount_price;
          $this->total_price += $discount_price * $cart['quantity'];
        } else {
          $this->total_price += $cart['price'] * $cart['quantity'];
        }
		$arrCart[]=array(
			'cart_id'=>$cart['cart_id'],
			'product_id'=>$cart['id'],
			'qty'=>$cart['quantity'],
		);
		
        $this->total_cart ++;//= $cart['quantity'];
      }
    } else {
      $carts = array();
    }
*/
    $categories      = $this->m_product_category->get_all_items();
    $sub_categories  = $this->m_product_category_sub->get_all_items();
    $last_categories = $this->m_product_category_last->get_all_items();
    foreach ($categories as $key => $category) {
      foreach ($sub_categories as $key2 => $sub_category) {
        if ($category['id'] === $sub_category['category_id']) {
          $categories[$key]['sub_categories'][$key2] = $sub_category;

          foreach ($last_categories as $last_category) {
            if ($sub_category['id'] === $last_category['category_id']) {
              $categories[$key]['sub_categories'][$key2]['last_categories'][] = $last_category;
            }
          }
        }
      }
    }

	//account pembayaran bank
	$this->data['billing_account'] = $this->m_billing_account->get_all_items();
	
	//modify by Aep
/*	$this->data['user_address'] = isset($this->data['member']['id'])?$this->m_user_address->get_all_items($this->data['member']['id']):'';
	//die(json_encode($this->data['user_address']).$this->db->last_query());
	if (!empty($this->data['user_address'])){
		foreach ($this->data['user_address'] as $address){
			$this->data['id']         = $address['id'];
			$this->data['name']       = $address['name'];
			$this->data['phone']      = $address['phone'];
			$this->data['address']    = $address['address'];
			$this->data['status']     = $address['status'];
			$this->data['user_id']    = $address['user_id'];
			$this->data['is_active']  = $address['is_active'];
			$this->data['province_id']= $address['province_id'];
			$this->data['regency_id'] = $address['regency_id'];
			$delivery_desc='';
			$delivery_rate=0;
			$this->load->model('m_user_kurir');
    		foreach($arrCart as $_cart){
				$_cart['user_addr_id']=$address['id'];
				$get_all_items =$this->m_user_kurir->get_all_items($_cart);
						if(!empty($get_all_items)){
							foreach ($get_all_items as $user_address){
								$delivery_rate+= $user_address['delivery_rate']*$_cart['qty'];
								$delivery_desc.= $user_address['delivery_desc'];
							}
						}
			}
			//$this->data['delivery_code'] = $address['delivery_code'];
			$this->data['delivery_desc'] = $delivery_desc;
			$this->data['delivery_rate'] = $delivery_rate;
		} 
	}else{
		$this->data['id']         	 = "";
		$this->data['name']       	 = "";
		$this->data['phone']      	 = "";
		$this->data['address']    	 = "";
		$this->data['status']        = "";
		$this->data['user_id']       = "";
		$this->data['is_active']     = "";
		$this->data['province_id']   = "";
		$this->data['regency_id']    = "";
		//$this->data['delivery_code'] = "";
		$this->data['delivery_desc'] = "";
		$this->data['delivery_rate'] = 0;
	} 
	*/	
    $this->data['top_categories'] = $categories;
   /* $this->data['carts']          = $carts;
    $this->data['total_price']    = $this->total_price;
    $this->data['total_cart']     = $this->total_cart;
    $this->data['tax_rate']       = $this->tax_rate;
    $this->data['total_tax']      = $this->total_price * $this->tax_rate;
    $this->data['total']          = $this->total_price + $this->data['total_tax'] + $this->data['delivery_rate'];
    $this->data['company']        = $this->m_company->get_item();
*/
    $this->data['upload_dir_temp'] = '/upload/temp';
      $this->_check_csrf();
    }

    private function _check_csrf()
    {
        // Manually handle CSRF errors
        if ($this->security->csrf_error) {
            // Your logic to handle the error
            redirect(current_url());
        }
    }
 
	
  
  public function redirect_not_login($withPopup=false)
  {
    if (!$this->data['is_member']) {
      $this->session->set_flashdata('errors', 'Login Anda sudah kadaluarsa. Silahkan login kembali.');

      if ($_GET) {
        $query = '?' . http_build_query($_GET);
      } else {
        $query = '';
      }
      if($withPopup)
      	$login_path='home/htmlLogin';
      else
        $login_path='login';
      redirect(base_url($login_path) . '?callback_url=' . urlencode(base_url(uri_string()) . $query));
    }
  }

  
  
  protected function _renew_session_data($id = null)
  {
    $this->load->model('m_user');

    if (empty($id)) {
      $id = $this->session->member['id'];
    }

    $user = $this->m_user->get_item_by_id($id);

    if ($user) {
      $user['time'] = date('Y-m-d H:i:s');

      $this->session->set_userdata('is_login', true);
      $this->session->set_userdata('member', $user);
    }
  }

  protected function _get_discount_price($products)
  {
    foreach ($products as $key => $product) {
      switch ($product['discount_type']) {
        case '1':
          $products[$key]['discount_price'] = $product['price'] - ($product['price'] * $product['discount'] / 100);
        break;
        case '2':
          $products[$key]['discount_price']  = $product['price'] - $product['discount'];
        break;
		default:
			$products[$key]['discount_price'] = $product['price'];
      }
    }

    return $products;
  }

	protected function _recheckSaldo(){
		$this->load->model('m_product');
		$this->load->model('m_user');
		//$this->load->model('mbrlst_model');
		$user=$this->session->userdata('member');
		$iResult = $this->m_product->getSaldo();
		//die("$iResult!=$user[wallet]");
		if($iResult!=$user['wallet']){
				$this->m_user->update_wallet($user['id'], $iResult);
				$this->_renew_session_data();
				//$user['wallet']=$iResult;
				//$this->session->set_userdata('member', $user);
			}	
		return $iResult;
	}
	
}

class Application_Admin extends CI_Controller
{
  protected $data = array();

  public function __construct()
  {
    parent::__construct();

    $admin = $this->session->admin;

    if (!empty($admin)) {
      $this->data['admin']    = $admin;
      $this->data['is_admin'] = true;
    } else {
      $this->data['admin']    = array();
      $this->data['is_admin'] = false;
    }

    $this->data['upload_dir_temp'] = '/upload/temp';
   $this->_check_csrf();
    }

    private function _check_csrf()
    {
        // Manually handle CSRF errors
        if ($this->security->csrf_error) {
            // Your logic to handle the error
            redirect(current_url());
        }
    }

  public function redirect_not_admin()
  {
    $controller_name = $this->uri->segment(2);
    $need_to_check   = array('login');

    if (!$this->data['is_admin'] && !in_array($controller_name, $need_to_check)) {
      $this->session->set_flashdata('errors', 'Login Anda sudah kadaluarsa. Silahkan login kembali.');
      redirect(base_url('cms_admin/login') . '?callback_url=' . urlencode(base_url(uri_string())));
    } else if ($this->data['is_admin'] && in_array($controller_name, $need_to_check)) {
      redirect(base_url('cms_admin'));
    }
  }
}