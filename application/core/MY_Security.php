<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* MY_Security Class
*
* Extended base security class for better CSRF handling.
*/
class MY_Security extends CI_Security {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        $this->csrf_error = FALSE;
    }

    // --------------------------------------------------------------------

    /**
     * Verify Cross Site Request Forgery Protection
     *
     * @return    object
     */
    public function csrf_verify()
    {
        // If no POST data exists we will set the CSRF cookie
        if (count($_POST) == 0)
        {
            return $this->csrf_set_cookie();
        }

        // Check if URI has been whitelisted from CSRF checks
        if ($exclude_uris = config_item('csrf_exclude_uris'))
        {
            $uri = load_class('URI', 'core');
            if (in_array($uri->uri_string(), $exclude_uris))
            {
                return $this;
            }
        }

        // Do the tokens exist in both the _POST and _COOKIE arrays?
        if ( ! isset($_POST[$this->_csrf_token_name]) OR
             ! isset($_COOKIE[$this->_csrf_cookie_name]))
        {
            return $this->csrf_set_error();
        }

        // Do the tokens match?
        if ($_POST[$this->_csrf_token_name] != $_COOKIE[$this->_csrf_cookie_name])
        {
            return $this->csrf_set_error();
        }

        // We kill this since we're done and we don't want to
        // polute the _POST array
        unset($_POST[$this->_csrf_token_name]);

        // Nothing should last forever
        unset($_COOKIE[$this->_csrf_cookie_name]);
        $this->_csrf_set_hash();
        $this->csrf_set_cookie();

        log_message('debug', "CSRF token verified ");

        return $this;
    }

    // --------------------------------------------------------------------

    /**
     * Set CSRF Error
     *
     * @return    void
     */
    public function csrf_set_error()
    {
        $this->csrf_error = TRUE;
    }

}
?>