<?php defined('BASEPATH') OR exit('No direct script access allowed');

$config['root']   = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http") . '://' . $_SERVER['HTTP_HOST'] . '/eklanku';
$config['asset']  = $config['root'] . '/assets';
$config['vendor'] = $config['root'] . '/vendor';

$config['max_product_images']  = 5;
$config['max_upload_size']     = 2;
$config['image_upload_format'] = array('jpeg', 'gif', 'png');
