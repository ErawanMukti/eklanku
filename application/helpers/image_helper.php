<?php defined('BASEPATH') OR exit('No direct script access allowed');

function generate_image_name($uploaded_dir, $extension)
{
  $CI =& get_instance();
  $CI->load->helper('string');

  $image_name = date('YmdHis') . random_string('alnum', 35) . '.' . $extension;

  if (file_exists($uploaded_dir . $image_name)) {
    generate_image_name($uploaded_dir, $extension);
  }

  return $image_name;
}

function generate_image_folder()
{
  $folder_path = func_get_args();

  if (!empty($folder_path)) {
    foreach ($folder_path as $path) {
      if (!file_exists($path)) {
        if (!mkdir($path, 0777, true)) {
          return false;
        }
      }
    }
  }

  return true;
}

function validate_image($file, $extensions = array('jpeg', 'gif', 'png'))
{
  if ($file['error'] > 0) {
    switch ($file['error']) {
      case UPLOAD_ERR_INI_SIZE:
      case UPLOAD_ERR_FORM_SIZE:
        return "Ukuran file terlalu besar.";
      default:
        return "File gagal diupload.";
    }
  }

  if (!in_array(get_extension($file), $extensions)) {
    return "Ekstensi salah, hanya boleh " . implode(', ', $extensions);
  }
}

function get_extension($file)
{
  if (isset($file['data'])) {
    $data = $file['data'];
  } elseif (isset($file['tmp_name'])) {
    $data = file_get_contents($file['tmp_name']);
  }

  if (strncmp("\xff\xd8", $data, 2) === 0) {
    return 'jpeg';
  } elseif (preg_match('/^GIF8[79]a/', $data) === 1) {
    return 'gif';
  } elseif (strncmp("\x89PNG\x0d\x0a\x1a\x0a", $data, 8) === 0) {
    return 'png';
  } elseif (strncmp("BM", $data, 2) === 0) {
    return 'bitmap';
  } elseif (strncmp("\x00\x00\x01\x00", $data, 4) === 0) {
    return 'ico';
  } elseif (strncmp("\x49\x49\x2a\x00\x08\x00\x00\x00", $data, 8) === 0 ||
            strncmp("\x4d\x4d\x00\x2a\x00\x00\x00\x08", $data, 8) === 0) {
    return 'tiff';
  } else {
    return false;
  }
}
