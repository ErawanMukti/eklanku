<?php defined('BASEPATH') OR exit('No direct script access allowed');

function asset_uri($url = '')
{
  $CI =& get_instance();
  return $CI->config->item('asset') . '/' . ltrim($url, '/');
}

function vendor_uri($url = '')
{
  $CI =& get_instance();
  return $CI->config->item('vendor') . '/' . ltrim($url, '/');
}

function h($var)
{
  return html_escape($var);
}

function http_error($data, $error_id, $is_exit = true)
{
  switch ($error_id) {
    case '404':
      $error_text = 'Not Found';
    break;
    case '400':
      $error_text = 'Bad Request';
    break;
    case '500':
      $error_text = 'Internal Server Error';
    break;
    default:
      exit($error_id . ' error id not found.');
  }

  header("{$_SERVER['SERVER_PROTOCOL']} {$error_id} {$error_text}");

  $CI =& get_instance();
  $data['main_html'] = $CI->load->view('error_' . $error_id, $data, true);
  $view = $CI->load->view('layout/main', $data, true); 

  echo $view;

  if ($is_exit) {
    exit;
  }
}

function number_format_id($number) {
  return number_format($number, 0, ',', '.');
}

function clear_html($des, $no_space = false, $no_upper_case = false) {
  $clear = strip_tags($des); // Strip HTML Tags
  $clear = html_entity_decode($clear); // Clean up things like &amp;
  $clear = urldecode($clear); // Strip out any url-encoded stuff
  $clear = preg_replace('/[^A-Za-z0-9]/', ' ', $clear); // Replace non-AlNum characters with space

  if ($no_space) {
    $clear = preg_replace('/ +/', '', $clear); // Replace Multiple spaces with no space
  } else {
    $clear = preg_replace('/ +/', ' ', $clear); // Replace Multiple spaces with single space
    $clear = trim($clear); // Trim the string of leading/trailing space
  }


  if ($no_upper_case) {
    $clear = strtolower($clear);
  }

  return $clear;
}

function convertDate($date){
	$date = explode("-",$date);
		$mt = array();
		$mt['01'] = "Jan";
		$mt['02'] = "Feb";
		$mt['03'] = "Mar";
		$mt['04'] = "Apr";
		$mt['05'] = "May";
		$mt['06'] = "Jun";
		$mt['07'] = "Jul";
		$mt['08'] = "Aug";
		$mt['09'] = "Sep";
		$mt['10'] = "Oct";
		$mt['11'] = "Nov";
		$mt['12'] = "Dec";
	return $date[2]."-".$mt[$date[1]]."-".$date[0];
}

function format_hari_id($tanggal){
	//$tanggal = '2017-06-13';
	$day = date('D', strtotime($tanggal));
	$dayList = array(
		'Sun' => 'Minggu',
		'Mon' => 'Senin',
		'Tue' => 'Selasa',
		'Wed' => 'Rabu',
		'Thu' => 'Kamis',
		'Fri' => 'Jumat',
		'Sat' => 'Sabtu'
	);
	return $dayList[$day];
}