<?php defined('BASEPATH') OR exit('No direct script access allowed');

function dump()
{
  if (ENVIRONMENT === 'development') {
    $vars = func_get_args();

    echo '<pre style="padding: 10px; border: 1px solid #ddd; background-color: #eee;">';

    foreach ($vars as $var) {
      var_dump($var);
    }

    echo '</pre>';
  }
}

function dump_die()
{
  if (ENVIRONMENT === 'development') {
    $vars = func_get_args();

    echo '<pre style="padding: 10px; border: 1px solid #ddd; background-color: #eee;">';

    foreach ($vars as $var) {
      var_dump($var);
    }

    echo '</pre>';

    die;
  }
}

