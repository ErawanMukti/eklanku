<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Egc
{
  	protected $ci;

	var $APP_TITLE = "eklanku.com";
	var $COPYRIGHT = '&copy; 2016 <a href="#" target="_blank">eklanku.com</a> All rights reserved';

	var $STATUS_CHECKBOX_TRUE = "t";
	var $STATUS_CHECKBOX_FALSE = "f";

	var $LEVEL_USER_ADMIN = "Admin";
	var $LEVEL_USER_SUPPORT = "Support";
	var $LEVEL_USER_PROPERTY = "Property";

	var $TPL_MEMBER 	= "Member";
	var $TPL_ACCOUNT 	= "Account";
	var $API_URL 	= array(
		"url"=>"",
		"salt"=>""
	);
	var $newday=70;//status new produk in days

	public function AuthorityCan($action='', $resource='')
	{
		return TRUE;
		
		/*	
		$USER_TYPE = $this->ci->session->userdata("usertype");
		$this->ci->db->from('adm_app_user_role as role');
		$this->ci->db->join('adm_app_user_rule as rule', 'rule.id_role=role.id_role', 'INNER');
		$this->ci->db->where('role.role_code', $USER_TYPE);
		$this->ci->db->where('rule.resource', $resource);
		$data = $this->ci->db->get();
		if($data->num_rows() > 0)
		{
			$row = $data->row();
			$arrActions = explode(',', $row->actions);
			foreach($arrActions as $value)
			{
				if($value == $action)
				{
					$authority = TRUE;
					break;
				}
				$authority = FALSE;
			}

			if($authority == TRUE)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}*/
	}	

	public function ddLevelUser()
	{
		$dataArr = array(	$this->LEVEL_USER_ADMIN			=> $this->LEVEL_USER_ADMIN,
							$this->LEVEL_USER_SUPPORT		=> $this->LEVEL_USER_SUPPORT
						);
		$data=array();
		foreach($dataArr as $key=>$value){
			$data['levelUser'][$key] = $value;
		}
		return $data['levelUser'];
	}

	public function __construct()
	{
        	$this->ci =& get_instance();
	}

	public function accounting_format($number) 
	{
	    $amount = number_format($number);
	    if ($amount < 0) 
	    {
	    	$amount = str_replace("-","", $amount);
	    	return '(' . $amount . ')';
	    }
	    else
	    {
	    	return $amount;
	    }
	}

	public function date_format($obj, $format="d M Y"){
		if(!empty($obj))
			$data = date($format, strtotime($obj));
		else
			$data='--/--/----';
		return $data;
	}
	
	
	public function encrypt_password($str)
	{
		return md5($str);
	}

	public function encrypt($str)
	{
		return md5($str);
	}

	public function date_now($format="d M Y"){
		$data = date($format, strtotime(date($format)));
		return $data;
	}

	public function plus_day($day, $plusDay, $format="d M Y")
	{
		$newdate = strtotime('+'.$plusDay.' day',strtotime($day));
		$newdate = date($format, $newdate);
		return $newdate;
	}

	public function min_day($day, $minDay, $format="d M Y")
	{
		$newdate = strtotime('-'.$minDay.' day',strtotime($day));
		$newdate = date($format, $newdate);
		return $newdate;
	}

	function addLeadingZero($num, $length = 5) 
	{
    		return sprintf("%0" . $length . "d", $num);
	}	

	public function stringReplace($search, $subject, $raplace=array())
	{
		$len = strlen($raplace[0]);
		$strReplace='';
		foreach($raplace as $value)
		{
			$strReplace.= $value;
		}
		$result = substr(str_replace($search, $strReplace, $subject), $len);
		return $result;
	}

	function genRandomString($length = 10) 
	{
    		return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
	}

	function genRandomNumber($length = 6) 
	{
    		return substr(str_shuffle("0123456789"), 0, $length);
	}

     

    public function sendEmail($userEmail, $bodyEmail, $subjectEmail)
    {
        $user_email = $userEmail;
        
        $this->load->library('email');              

        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';

        $this->email->initialize($config);
        $this->email->from('admin@isc.com', 'ISC Robotic System');
        $this->email->to($user_email);
        $this->email->bcc('agus.guna50@gmail.com');
        $this->email->subject($subjectEmail);
        $this->email->message($bodyEmail);
        $this->email->send();
    }

	function isMultiplyHundred($num) 
	{
		if ($num % 1 == 0)
    		return true;
    	else
    		return false;	
	}
	   function isMultiplyCek($num) 
	{
		if ($num % 1 == 0)
    		return true;
    	else
    		return false;	
	}  

	//$src = 'user_data/company_2/T1/';
	//$dst = 'user_data/company_2/T2/T1/';

	//rcopy($src, $dst);  // Call function
	// Function to Copy folders and files
	function rcopy($src, $dst) {
		if (file_exists ( $dst ))
			$this->rrmdir ( $dst );
		/*if (is_dir ( $src )) {
			mkdir ( $dst );
			$files = scandir ( $src );
			foreach ( $files as $file )
			if ($file != "." && $file != "..")
			$this->rcopy ( "$src/$file", "$dst/$file" );

		} else */
		if (file_exists ( $src ))
			copy ( $src, $dst );
		$this->rrmdir ( $src );
	}

	// Function to remove folders and files
	function rrmdir($dir) {
		/*if (is_dir($dir)) {
			$files = scandir($dir);
			foreach ($files as $file)
			if ($file != "." && $file != "..") $this->rrmdir("$dir/$file");
			rmdir($dir);
		}
		else */
			if (file_exists($dir)) unlink($dir);
	}
	
	
	function sendmail($subject,$to_email,$data) { 
         $from_email = "admin@eklanku.com"; 
      //   $to_email = 'suprianto@pancabuana.co.id';//$this->input->post('email');    
         
         $config = Array(
	        'protocol' => 'smtp',
	        'smtp_host' => 'ssl://smtp.gmail.com',
	        'smtp_port' => 465,
	        'smtp_user' => 'kingazis@gmail.com',
	        'smtp_pass' => 'itkreco@08',
	        'smtp_timeout' => '4',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1'
        );

	    $ci =&get_instance();
   		$ci->load->library('email', $config); 
	   /* $this->email->from($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
	    $this->email->reply_to($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
*/
	    $ci->email->from($from_email, 'Eklanku'); 
        $ci->email->to($to_email);
        $ci->email->subject($subject); 
	//    $this->email->subject(sprintf($this->lang->line('auth_subject_'.$type), $this->config->item('website_name', 'tank_auth')));
       // $bill=array();
    /**/    
        
        for($i=0,$cid=array();$i<count($data['cid']);$i++){
        	$ci->email->attach($data['cid'][$i]);
        	$cid[]=$ci->email->attachment_cid($data['cid'][$i]);
        }
        //die(json_encode($data['cid']).json_encode($cid));
        $data['cid']=$cid;
        $html=$ci->load->view($data['view'], $data, TRUE);
	   // $cid = $this->email->attachment_cid($filename);
       // $this->save_download($html);
	    //die($html);
        $ci->email->message($html);
	  //  $this->email->set_alt_message($this->load->view('email/'.$type.'-txt', $data, TRUE));
	
	    $ci->email->set_newline("\r\n"); // require this, otherwise sending via gmail times out
	
	    $ci->email->send();
      } 
      
	function sendmail_bid($member,$bid,$price){      	
        	$ci =&get_instance();
   			$ci->load->library('egc');
        	$data=array(
	        	'title'=>'Bid',
	            'no_order'=>'no_order',
	            'date_order'=>$ci->egc->date_now(),
	            'name_order'=>$member['name'],
	        	'email_order'=>$member['email'],
	        	'phone_order'=>$member['phone'],
	            'participant'=>array(
	        		array('name'=>$bid['name'],'eklanku_id'=>'bid:'.$price),
	        	),
	        	'headlist'=>'',
	        	'list'=>''/*array(
	        		array('kode'=>'11','date'=>'12','service'=>'13','price'=>'14'),
	        	)*/,
	        	'total'=>'total',
	        	'status'=>'status',
	         	'notes'=>array('catatan1','catatan2'),
	        	'cancelation'=>array('cancel1','cancel2'),
	        	'view'=>'mailformat/bid_sendmail'
	        );
	        $this->sendmail('Eklanku - Bid Super Lelang Anda - '.$bid['name'].'['. $ci->egc->accounting_format($bid['price']).']',$member['email'],$data);
      } 
      
      function sendmail_order($member,$order){      	
      		$total_ctrf=$this->accounting_format($order['total']+$order['key_transfer']);
			$ci =&get_instance();
   			$ci->load->model('m_order');
      		$ci->load->model('m_billing_account');
      		$data['view']='mailformat/order_sendmail';
	        $info = array('id'=>$order['order_id'], 'user_id'=>$member['id']);
			$data['list_order_data'] = $ci->m_order->get_review_order_items($info);
		    $data['billing_account'] = $ci->m_billing_account->get_all_items();
	
      		$data['cid'][]='./assets/images/logo.png';
	      	
		    if(!empty($data['billing_account'])&&$order['payment_method']!='wallet'){
			  	foreach ($data['billing_account'] as $bill_acc){
					$data['cid'][]='./assets/images/'.$bill_acc['bill_logo'];						
				}
	        }
	        
        	$this->sendmail('Eklanku - Order Anda dgn No inv: '.$order['invoice_code'].' - Total: Rp. '.$total_ctrf.']',$member['email'],$data);
      } 
      
      function sendmail_promo($member,$order){      	
      		$total_ctrf=$this->accounting_format($order['total']+$order['key_transfer']);
			$ci =&get_instance();
   			$ci->load->model('m_promosi_user');
      		$ci->load->model('m_billing_account');
      		$data['view']='mailformat/promo_sendmail';
	        $info = array('promo'=>$order['promo_id'], 'member'=>$member);
	        
			$data['list_order_data'] = $ci->m_promosi_user->get_review_order_items($info);
		    $data['billing_account'] = $ci->m_billing_account->get_all_items();
	
      		$data['cid'][]='./assets/images/logo.png';
	      	
		    if(!empty($data['billing_account'])&&$order['payment_method']!='wallet'){
			  	foreach ($data['billing_account'] as $bill_acc){
					$data['cid'][]='./assets/images/'.$bill_acc['bill_logo'];						
				}
	        }
	        
        	$this->sendmail('Eklanku - Order Anda dgn No inv: '.$order['invoice_code'].' - Total: Rp. '.$total_ctrf.']',$member['email'],$data);
      } 
      
	
      
	function isnew($date1,$date2){
		//global $newday;
		$datetime1 = new DateTime($date1);
$datetime2 = new DateTime($date2.' 00:00:00');
$interval = $datetime1->diff($datetime2);
    
		$nday=$interval->format('%a');
    if($nday>0&&$nday<$this->newday){
			return '<div class="sticker sticker-new">&nbsp;</div>';
		}else{
			return "";//.date($date2);
		}
	}
	
	function isdiscount($hrg1,$hrg2){
		$disc=round(100-round(($hrg1/$hrg2)*100));
		if($disc>0)
			return '
			<div class="sticker sticker-all top-diskon">
				<div style="width: 100px;">'.$disc.'</div>
			</div>';
		else return '';
	}
	
	function randomRGB()
	{
		 // add limit
		$id_length = 7;
		
		// add any character / digit
		$alfa = "ddeeefffff";
		$token = "";
		for($i = 1; $i < $id_length; $i ++) {
		
		  // generate randomly within given character/digits
		  $token .= $alfa[rand(0, strlen($alfa)-1)];
		
		}    
		return $token;
	}
/*	function save_download($html)
  	{ 
		//load mPDF library
		$ci =&get_instance();
   		$ci->load->library('pdf');
		//load mPDF library


		

		
	//	$html=$ci->load->view('pdf_output',$this->data, true); //load the pdf_output.php by passing our data and get all data in $html varriable.
 	 
		//this the the PDF filename that user will get to download
		$pdfFilePath ="mypdfName-".time()."-download.pdf";
       // $pdfFilePath = getcwd()."/assets/other_uploads/pdf/".$pdfFilePath;
		
		//actually, you can pass mPDF parameter on this load() function
		$pdf = $ci->pdf->load();
		//generate the PDF!
		$pdf->WriteHTML($html,2);
		//offer it to user via browser download! (The PDF won't be saved on your server HDD)
		$pdf->Output($pdfFilePath, "D");
		 	
  	}*/
}




/* End of file isc.php */
/* Location: ../application/libraries/isc.php */
?>
