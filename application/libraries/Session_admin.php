<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Session_admin
{
  protected $login_url = 'login';
  protected $CI        = null;

  public function __construct()
  {
    $this->CI =& get_instance();

    $this->CI->load->library('session');
    $this->CI->load->helper('url');
  }

  public function check_auth($url = '')
  {
    if (empty($url)) {
      $url = $this->login_url;
    }

    $is_login        = $this->CI->session->is_login;
    $controller_name = $this->CI->uri->segment(1);

    if (!$is_login && $controller_name !== $url) {
      redirect(base_url('login'));
    }
  }
}
