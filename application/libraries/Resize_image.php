<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Resize_image
{
  protected $image_name    = '';
  protected $image_src     = '';
  protected $image_dst     = '';
  protected $image_ext     = '';
  protected $resized_image = null;

  public function __construct() {}

  /**
   * @param string $image_src  path of image source 
   * @param string $image_dst  path of image destination
   */
  public function start($image_src, $image_name)
  {
    $this->image_name = $image_name;

    if (!is_readable($image_src . '/' . $this->image_name)) {
      trigger_error(__METHOD__ . '() Invalid image source: ' . $image_src, E_USER_ERROR);
    } else {
      $this->image_src = $image_src . '/' . $this->image_name;
    }

    $this->image_ext = pathinfo($this->image_src, PATHINFO_EXTENSION);

    switch ($this->image_ext) {
      case 'jpeg':
      case 'jpg':
        $this->image = imagecreatefromjpeg($this->image_src);
      break;
      case 'gif':
        $this->image = imagecreatefromgif($this->image_src);
      break;
      case 'png':
        $this->image = imagecreatefrompng($this->image_src);
      break;
    }
  }

  /**
   * this method is used to create a square thumbnail
   * @param int     $newSize        width and height for thumbnail image  
   * @param boolean $delete_src     condition to delete image source 
   * @param int     $image_quality  image quality for create jpeg image 
   */
  public function create_thumbnail($image_dst, $new_width = 300, $new_height = 300, $delete_src = true, $image_quality = 100)
  {
    if (!empty($image_dst)) {
      $this->set_image_destination($image_dst);
    }

    list($width, $height) = getImageSize($this->image_src);

    if ($new_width !== $new_height) {
      $_scale = $width / $new_width;

      if (($new_height * $_scale) > $height) {
        $_scale = $height / $new_height;
      }

      $cropWidth  = $new_width * $_scale;
      $cropHeight = $new_height * $_scale;

      $x = ($width - $cropWidth) / 2;
      $y = ($height - $cropHeight) / 2;
    } else {
      if ($width > $height) {
        $x = ($width - $height) / 2;
        $y = 0.0;
        $cropWidth  = $height;
        $cropHeight = $height;
      } else {
        $x = 0.0;
        $y = ($height - $width) / 2;
        $cropWidth  = $width;
        $cropHeight = $width;
      }
    }
    
    $this->resized_image = imagecreatetruecolor(round($new_width), round($new_height));

    if($this->image_ext === "gif" || $this->image_ext === "png"){
      imagecolortransparent($this->resized_image, imagecolorallocatealpha($this->resized_image, 0, 0, 0, 127));
      imagealphablending($this->resized_image, false);
      imagesavealpha($this->resized_image, true);
    }
    
    imagecopyresampled(
      $this->resized_image, 
      $this->image, 
      0, 
      0, 
      $x, 
      $y,
      $new_width, 
      $new_height, 
      $cropWidth, 
      $cropHeight
    ); 

    return $this->_save_image($delete_src, $image_quality);
  }

  public function resize_image($image_dst, $max_width, $delete_src = true, $width = null, $height = null, $image_quality = 100) 
  {
    if (!empty($image_dst)) {
      $this->set_image_destination($image_dst);
    }

    if (empty($width) || empty($height)) {
      list($width, $height) = getImageSize($this->image_src);
    }

    /* check if width is bigger that maximum width settings */
    if ($max_width < $width) {
      $new_width  = $max_width;
      $new_height = $height / $width * $new_width;
    } else {
      $new_width  = $width;
      $new_height = $height;
    }
    
    $this->resized_image = imagecreatetruecolor($new_width, $new_height);

    if($this->image_ext === "gif" || $this->image_ext === "png"){
      imagecolortransparent($this->resized_image, imagecolorallocatealpha($this->resized_image, 0, 0, 0, 127));
      imagealphablending($this->resized_image, false);
      imagesavealpha($this->resized_image, true);
    }

    imagecopyresampled($this->resized_image, $this->image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

    return $this->_save_image($delete_src, $image_quality);
  }

  public function set_image_destination($image_dst)
  {
    if (!file_exists($image_dst)) {
      mkdir($image_dst);
    }

    $this->image_dst = $image_dst . '/' . $this->image_name;
  }

  protected function _save_image($delete_src, $image_quality)
  {  
    if (is_readable($this->image_src)) {
      switch ($this->image_ext) {
        case 'jpeg':
        case 'jpg':
          $result = imagejpeg($this->resized_image, $this->image_dst, $image_quality);
        break;
        case 'gif':
          $result = imagegif($this->resized_image, $this->image_dst);
        break;
        case 'png':
          $result = imagepng($this->resized_image, $this->image_dst);
        break;
      }

      if ($delete_src) {
        unlink($this->image_src);
      }
    }
    
    imagedestroy($this->resized_image);

    return $result;
  }
}