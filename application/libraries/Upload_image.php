<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Upload_image 
{
  protected $uploaded_dir = null;
  protected $allowedTypes = array('jpeg', 'gif', 'png');

  public function __construct() {}

  public function set_dir($uploaded_dir)
  {
    $this->uploaded_dir = $uploaded_dir;
  }

  public function setAllowedTypes($types)
  {
    if (!is_array($types)) {
      $types = array($types);
    }

    $this->allowedTypes = array_map('strtolower', $types);
  }

  public function getAllowedTypes()
  {
    return $this->allowedTypes;
  }

  public function isTypeValid($data)
  {
    if (empty($this->allowedTypes)) {
      return true;
    } else {
      return in_array(self::getType($data), $this->allowedTypes);
    }
  }

  public function upload_image($file)
  {
    $image = null;

    if (isset($file['data'])) {
      $data = $file['data'];
    } elseif (isset($file['tmp_name'])) {
      $data = file_get_contents($file['tmp_name']);
    }

    $ext = $this->get_type($data);

    if ($ext === 'jpeg') {    
      $image = imagecreatefromstring($data);
      
      if (!empty($image)) {
        ob_start();
        imagejpeg($image);
        $data = ob_get_contents(); // read from buffer
        ob_end_clean(); // delete buffer
      }
    }

    if ($ext === false) {
      throw new Exception(__METHOD__ . "() Can't upload. Wrong image extension.");
    }

    $image_name = $this->generate_image_name($ext);
    $this->upload($data, $image_name);

    return $image_name;
  }

  public function generate_image_name($extension)
  {
    $CI =& get_instance();
    $CI->load->helper('string');

    $image_name = date('YmdHis') . random_string('alnum', 35) . '.' . $extension;

    if (file_exists($this->uploaded_dir . $image_name)) {
      generate_image_name($extension);
    }

    return $image_name;
  }


  public function validate($image)
  {
    $errors = array();

    if (empty($this->allowedExtensions)) {
      return true;
    } else {
      return in_array($this->getExtension($fileName), $this->allowedExtensions);
    }
    
    if (isset($file['name']) && !$this->isExtensionValid($file['name'])) {
      $extensions = implode(', ', $this->allowedExtensions);
      $errors[]   = "Ekstensi salah, hanya boleh {$extensions}.";
    }

    if (isset($file['error']) && $file['error'] > 0) {
      $errors[] = $this->errorMessage($file['error']);
    }

    if (isset($file['size']) && !($file['size'] > $max_size)) {
      $errors[] = "Image size must {$this->maxSize}MB or less";
    }

    if ($image['error'] === 0) {
      $data   = $this->getData($image);

      if (empty($data)) {
        trigger_error(__METHOD__ . "() Image data not found.", E_USER_WARNING);
      } elseif(!$this->isTypeValid($data)) {
        $types = implode(', ', $this->allowedTypes);
        $errors[] = "Wrong image type.";
        $errors[] = "Only can accepts {$types}.";
      }
    }
    return $errors;
  }

  public static function get_type($data)
  {
    if (strncmp("\xff\xd8", $data, 2) === 0) {
      return 'jpeg';
    } elseif (preg_match('/^GIF8[79]a/', $data) === 1) {
      return 'gif';
    } elseif (strncmp("\x89PNG\x0d\x0a\x1a\x0a", $data, 8) === 0) {
      return 'png';
    } elseif (strncmp("BM", $data, 2) === 0) {
      return 'bitmap';
    } elseif (strncmp("\x00\x00\x01\x00", $data, 4) === 0) {
      return 'ico';
    } elseif (strncmp("\x49\x49\x2a\x00\x08\x00\x00\x00", $data, 8) === 0 ||
              strncmp("\x4d\x4d\x00\x2a\x00\x00\x00\x08", $data, 8) === 0) {
      return 'tiff';
    } else {
      return false;
    }
  }
}