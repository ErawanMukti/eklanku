<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends Application
{
  public function index()
  {
    $login_data = array('is_member', 'member');
    $this->session->unset_userdata($login_data);
    redirect(base_url(''));
  }
}
