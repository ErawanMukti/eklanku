<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Application
{
  protected $max_product_images = 5;

  public function __construct()
  {
    parent::__construct();

    $this->redirect_not_login();
	$this->load->model('m_messaging');
    //$this->data['sidebar_html'] = $this->load->view('user-sidebar', $this->data, true);

    $this->data['upload_dir_product']       = '/upload/product';
    $this->data['upload_dir_product_thumb'] = '/upload/product_thumb';
    $this->data['upload_dir_product_temp']  = '/upload/product_temp';
    $this->data['upload_dir_shop']          = '/upload/shop';
  }

  public function index()
  { 
	$this->load->model('m_user');
//	$this->load->model('m_order');
//	$this->load->model('m_product_image');
//	$this->load->model('m_payment_status');
	$this->load->model('m_contact_center');
	$this->load->model('m_eklanpay_order');
//	$this->load->model('m_promosi_user'); 
	
	$contact_center = $this->m_contact_center->get_all_items();
	foreach ($contact_center as $cnt){
		$this->data['cntctr_name']   = $cnt['cntctr_name'];
		$this->data['cntctr_phone1'] = ($cnt['cntctr_phone1'] !="")?"<i class=\"fa fa-fw fa-phone\"></i><span class='cart-title'>".$cnt['cntctr_phone1']."</span>":"";
		$this->data['cntctr_phone2'] = ($cnt['cntctr_phone2'] !="")?"<i class=\"fa fa-fw fa-phone\"></i><span class='cart-title'>".$cnt['cntctr_phone2']."</span>":"";
		$this->data['cntctr_phone3'] = ($cnt['cntctr_phone3'] !="")?"<i class=\"fa fa-fw fa-whatsapp\"></i><span class='cart-title'>".$cnt['cntctr_phone3']."</span>":"";
		$this->data['cntctr_email1'] = ($cnt['cntctr_email1'] !="")?"<i class=\"fa fa-fw fa-envelope\"></i><span class='cart-title'>".$cnt['cntctr_email1']."</span>":"";
	}	
		$this->data['msg']			   = $this->session->flashdata('message');
		$this->data['user_data']       = $this->m_user->get_item_by_id($this->data['member']['id']);
//		$this->data['transaction_list']= $this->m_order->_query_review_order(array('user_id'=>$this->data['member']['id'],'payment_method'=>'transfer','payment_status'=>'1'));
//		$this->data['cofirm_promotion']= $this->m_promosi_user->get_all_items(array('user_id'=>$this->data['member']['id'],'payment_status'=>'Belum dibayar'));
		$this->data['cofirm_eklanpay']= $this->m_eklanpay_order->get_all_items(array('user_id'=>$this->data['member']['id'],'payment_status'=>1,'invoice_date'=>'2 HOUR'));
		
	
	
	$this->data['main_html']      = $this->load->view('user-dashboard', $this->data, true);
    $this->load->view('layout/main', $this->data); 
  }
  
  public function belanja()
  { 
	$this->load->model('m_user');
	$this->load->model('m_order');
	$this->load->model('m_product_image');
	$this->load->model('m_payment_status');
	$this->load->model('m_contact_center');
	
	$contact_center = $this->m_contact_center->get_all_items();
	foreach ($contact_center as $cnt){
		$this->data['cntctr_name']   = $cnt['cntctr_name'];
		$this->data['cntctr_phone1'] = ($cnt['cntctr_phone1'] !="")?"<i class=\"fa fa-fw fa-phone\"></i><span class='cart-title'>".$cnt['cntctr_phone1']."</span>":"";
		$this->data['cntctr_phone2'] = ($cnt['cntctr_phone2'] !="")?"<i class=\"fa fa-fw fa-phone\"></i><span class='cart-title'>".$cnt['cntctr_phone2']."</span>":"";
		$this->data['cntctr_phone3'] = ($cnt['cntctr_phone3'] !="")?"<i class=\"fa fa-fw fa-whatsapp\"></i><span class='cart-title'>".$cnt['cntctr_phone3']."</span>":"";
		$this->data['cntctr_email1'] = ($cnt['cntctr_email1'] !="")?"<i class=\"fa fa-fw fa-envelope\"></i><span class='cart-title'>".$cnt['cntctr_email1']."</span>":"";
	}
	$this->data['member_transaction_list']=$this->m_order->_query_review_order(array('user_id'=>$this->data['member']['id']));
    $this->data['main_html']      = $this->load->view('user-list-pembelian', $this->data, true);
    $this->load->view('layout/main', $this->data); 
  }
  
	public function bayar()
	  { 
		$this->load->model('m_user');
		$this->load->model('m_order');
		$this->load->model('m_product_image');
		$this->load->model('m_payment_status');
		$this->load->model('m_contact_center');
		
		$contact_center = $this->m_contact_center->get_all_items();
		foreach ($contact_center as $cnt){
			$this->data['cntctr_name']   = $cnt['cntctr_name'];
			$this->data['cntctr_phone1'] = ($cnt['cntctr_phone1'] !="")?"<i class=\"fa fa-fw fa-phone\"></i><span class='cart-title'>".$cnt['cntctr_phone1']."</span>":"";
			$this->data['cntctr_phone2'] = ($cnt['cntctr_phone2'] !="")?"<i class=\"fa fa-fw fa-phone\"></i><span class='cart-title'>".$cnt['cntctr_phone2']."</span>":"";
			$this->data['cntctr_phone3'] = ($cnt['cntctr_phone3'] !="")?"<i class=\"fa fa-fw fa-whatsapp\"></i><span class='cart-title'>".$cnt['cntctr_phone3']."</span>":"";
			$this->data['cntctr_email1'] = ($cnt['cntctr_email1'] !="")?"<i class=\"fa fa-fw fa-envelope\"></i><span class='cart-title'>".$cnt['cntctr_email1']."</span>":"";
		}
		$this->data['member_transaction_list']=$this->m_order->_query_review_order(array('user_id'=>$this->data['member']['id']));
	    $this->data['main_html']      = $this->load->view('user-bayar', $this->data, true);
	    $this->load->view('layout/main', $this->data); 
	  }

  public function edit($panel_id = '1')
  {
    if ($panel_id > 2) {
      http_error($this->data, 404);
    }
	
    $this->data['panel_id']  = $panel_id;
    $this->data['main_html'] = '<center><h2>Dalam Perbaikan</h2></center>';//$this->load->view('user-edit-information', $this->data, true);
    $this->load->view('layout/main', $this->data); 
  }

  public function discussion()
  {
    $this->data['main_html'] = $this->load->view('user-discussion', $this->data, true);
    $this->load->view('layout/main', $this->data); 
  }

	public function review(){
		$this->load->library('pagination');
		$this->load->model('m_user_review');
		
		if ($_POST) {
			$product_id      = $this->input->post('product_id');
			$product_creatby = $this->input->post('product_creatby');
			$review_date     = date('Y-m-d');
			$review_time     = date('H:i:s');
			$review_comment  = $this->input->post('review_comment');
			$review_userid   = $this->data['member']['id'];
			$review_status   = 1;
			$data_info = array(
				 'product_id'     => $product_id,		
				 'product_creatby'=> $product_creatby,		
				 'review_date'    => $review_date,		
				 'review_time'    => $review_time,		
				 'review_comment' => $review_comment,		
				 'review_userid'  => $review_userid,		
				 'review_status'  => $review_status	
			);
				
			if ($this->m_user_review->save($data_info)) {
				$this->session->set_flashdata('message', 'Insert Comment Success!.');
				redirect(base_url('user/review/0/0/'.$product_creatby.'/'.$product_id));
			}
		}
		
		
		$userCreateBy = $this->uri->segment(5);
		if(empty($userCreateBy))$userCreateBy=$this->data['member']['id'];
		$this->data['product_creatby'] = $userCreateBy;
		$this->data['product_id']      = $this->uri->segment(6);
		$this->data['review_comment']  = ($this->input->post('review_comment') !="")?$this->input->post('review_comment'):"";
		
		$criteria			  = array('product_creatby'=>$userCreateBy,'product_id'=>$this->data['product_id']);
		$offset 			  = $this->uri->segment(4,0);
		$limit  			  = 10;
		$count_product		  = $this->m_user_review->count_items_byvalue($criteria);
		$config['base_url']   = site_url('user/review/0');
		$config['per_page']   = $limit;
		$config['total_rows'] = $count_product[0]['jumlah'];
		$this->pagination->initialize($config);
		$extra_uri = array('id_createby'=>$userCreateBy,'id_product'=>$this->uri->segment(6));
		$this->data['pagination']  = $this->pagination->create_links($extra_uri);	
		$this->data['comment_list']= $this->m_user_review->get_items_byvalue($criteria,$offset,$limit);
		
		$this->data['msg']       = $this->session->flashdata('message');
		$this->data['main_html'] = $this->load->view('user-review', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}
  
  public function order()
  {
    $this->data['main_html'] = $this->load->view('user-order', $this->data, true);
    $this->load->view('layout/main', $this->data); 
  }

	public function confirm_transfer(){
		$this->load->model('m_user');
	//	$this->load->model('m_order');
		$this->load->model('m_eklanpay_order');
	//	$this->load->model('m_product_image');
	//	$this->load->model('m_payment_status');
	//	$this->load->model('m_promosi_user'); 
		
		$this->data['msg']			   = $this->session->flashdata('message');
		$this->data['user_data']       = $this->m_user->get_item_by_id($this->data['member']['id']);
	//	$this->data['transaction_list']= $this->m_order->_query_review_order(array('user_id'=>$this->data['member']['id'],'payment_method'=>'transfer','payment_status'=>'1'));
	//	$this->data['cofirm_promotion']= $this->m_promosi_user->get_all_items(array('user_id'=>$this->data['member']['id'],'payment_status'=>'Belum dibayar'));
		$this->data['cofirm_eklanpay']= $this->m_eklanpay_order->get_all_items(array('user_id'=>$this->data['member']['id'],'payment_status'=>1,'invoice_date'=>'2 HOUR'));
		$this->data['main_html']       = $this->load->view('user-confirm-transfer', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}
	
	public function ubah_status($id=''){
		$this->load->model('m_user');
		$this->load->model('m_order');
		$this->load->model('m_product_image');
		$this->load->model('m_payment_status');
		$this->load->model('m_billing_account');
		$this->load->model('m_cart_order_status');
		$this->load->library('egc');
		
		if(empty($id))$id =$this->input->post('order_id');
		if($id ==""){
			$text_message = "<span class='text-muted'>
							<i class='fa fa-fw fa-exclamation-circle'></i>
							<span class='cart-title'>Product belum dipilih!</span>
						</span>";
			$this->session->set_flashdata('message', $text_message);
			redirect(base_url('user/confirm_transfer'));
			exit; 
		}
		else
			$transaction_list = $this->m_order->_query_review_order(array('id'=>$id,'user_id'=>$this->data['member']['id'],'payment_method'=>'transfer','payment_status'=>'1'));
		if(!empty($transaction_list)){
			foreach($transaction_list->result_array() as $listdata){
				$this->data['id_order']     = $listdata['id_order'];
				$this->data['invoice_code'] = $listdata['invoice_code'];
				$this->data['key_transfer'] = $listdata['key_transfer'];
				$this->data['total'] = $listdata['total'];
			}
		}else{
			$this->data['id_order'] = $id;
			$this->data['invoice_code'] = "";
			$this->data['key_transfer'] = "";
			$this->data['total'] = "";
		
		} 
		
		if ($_POST) {
				$errors = 0;
			    $all_post = $this->input->post();
			    $all_post['norek_asal']=trim(str_replace('-','',$all_post['norek_asal']));
				$all_post['nominal_transfer']=trim(str_replace(',','',$all_post['nominal_transfer']));
				$this->data['errors'] = $this->m_cart_order_status->validate_cart_order_status($all_post);
				$id_order=$id;
			    $nama_nasabah= $this->input->post('nama_nasabah');
				$norek_asal  = $all_post['norek_asal'];
				$bank_id    = $this->input->post('bank_id');
				$bank_tujuan_id    = $this->input->post('bank_tujuan_id');
				$bank_tujuan_norek = $this->m_billing_account->getBillingOneField($bank_tujuan_id,'bill_number');
				$nominal_transfer  = $all_post['nominal_transfer'];
				$validasi_transfer = '';//substr($nominal_transfer,-3);//echo "x";die();
				$tgl_transfer      = $this->input->post('tgl_transfer');
				
				$result=$this->m_order->get_one_items($id);
			//	die(str_replace('.','',str_replace('Rp ','',$nominal_transfer)).$norek_asal.':'.$nominal_transfer);
		 		
				if (!empty($nominal_transfer)&&$nominal_transfer<$result->total) {
				  $this->data['errors']['nominal_transfer'] = 'Nilai tagihan nominal Transfer Rp. '.$this->egc->accounting_format($result->total+$result->key_transfer).'.';
				}
				
			    if(!empty($this->data['errors']['nama_nasabah']))$this->session->set_flashdata('msg_nama_nasabah', $this->data['errors']['nama_nasabah']);
				if(!empty($this->data['errors']['norek_asal']))$this->session->set_flashdata('msg_norek_asal', $this->data['errors']['norek_asal']);
				if(!empty($this->data['errors']['bank_id']))$this->session->set_flashdata('msg_bank_id', $this->data['errors']['bank_id']);
				if(!empty($this->data['errors']['bank_tujuan_id']))$this->session->set_flashdata('msg_bank_tujuan_id', $this->data['errors']['bank_tujuan_id']);
				if(!empty($this->data['errors']['nominal_transfer']))$this->session->set_flashdata('msg_nominal_transfer', $this->data['errors']['nominal_transfer']);
				if(!empty($this->data['errors']['tgl_transfer']))$this->session->set_flashdata('msg_tgl_transfer', $this->data['errors']['tgl_transfer']);
			/*	if($nominal_transfer <= $total){
					$errors++;
					$this->session->set_flashdata('msg_nominal_transfer_tdk_sama', 'Nominal transfer kurang dari total bayar!');
				}
				if($key_transfer != $validasi_transfer){
					$errors++;
					$this->session->set_flashdata('msg_tiga_digit_transfer', 'Tiga digit terakhir transfer tidak sama dengan nilai tagihan!');
				} */
				$this->data['order_id']=$id_order;
				$this->data['norek_asal']=$norek_asal;
				$this->data['nama_nasabah']=$nama_nasabah;
				$this->data['bank_id']=$bank_id;
				$this->data['bank_tujuan_id']=$bank_tujuan_id;
				$this->data['norek_tujuan']=$bank_tujuan_norek;
				$this->data['nominal_transfer']=$nominal_transfer;
				$this->data['tgl_transfer']=$tgl_transfer;
				
				if(empty($this->data['errors']) && $errors <1){ 
					$this->db->trans_begin(); 
						$order_status = array(
							'cart_order_id'=>$id_order,
							'user_id'=>$this->data['member']['id'],
							'norek_asal'=>$norek_asal,
							'nama_nasabah'=>$nama_nasabah,
							'bank_id'=>$bank_id,
							'bank_tujuan_id'=>$bank_tujuan_id,
							'norek_tujuan'=>$bank_tujuan_norek,
							'nominal_transfer'=>$nominal_transfer,
							'key_transfer'=>$validasi_transfer,
							'tgl_transfer'=>$tgl_transfer,
							'confirm_date'=>date('Y-m-d'),
							'confirm_time'=>date('H:i:s'),
							'datetime_created'=>date('YmdHis'),
							'status'=>1
						);
						$res = $this->m_cart_order_status->save_order_status($order_status);
						
						$cart_order = array(
							'id'=>$id_order,
							'user_id'=>$this->data['member']['id'],
							'status'=>1,
							'payment_status'=>2
						);
						$res =!$res?$res: $this->m_order->update_cart_order($cart_order);
						
						if(!$res || $this->db->trans_status() === false) {
							$this->db->trans_rollback();
							$this->session->set_flashdata('message', 'Status belanja gagal dikonfirmasi.');
							
						}else{
							$this->db->trans_commit();
							$this->data['id_order'] = "";
							//$this->data['invoice_code'] = "";
							$this->data['key_transfer'] = "";
							$this->data['total'] = "";
							$this->_renew_session_data();
							$this->session->set_flashdata('message', '<div class=\"alert alert-success\">Status belanja berhasil dikonfirmasi.</div>');
						}
				}
				
			}
		else	
			$this->data['nominal_transfer']=$this->data['total']+$this->data['key_transfer'];
		
		$this->data['form_url']=base_url('user/ubah_status');
		$this->data['msg'] = $this->session->flashdata('message');
		$this->data['msg_nama_nasabah'] = $this->session->flashdata('msg_nama_nasabah');
		$this->data['msg_norek_asal'] = $this->session->flashdata('msg_norek_asal');
		$this->data['msg_bank_id'] = $this->session->flashdata('msg_bank_id');
		$this->data['msg_bank_tujuan_id'] = $this->session->flashdata('msg_bank_tujuan_id');
		$this->data['msg_nominal_transfer'] = $this->session->flashdata('msg_nominal_transfer');
		$this->data['msg_nominal_transfer_tdk_sama'] = $this->session->flashdata('msg_nominal_transfer_tdk_sama');
		$this->data['msg_tiga_digit_transfer'] = $this->session->flashdata('msg_tiga_digit_transfer');
		$this->data['msg_tgl_transfer'] = $this->session->flashdata('msg_tgl_transfer'); 
		$this->data['main_html'] = $this->load->view('user-ubah-status', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}
	
	/*public function do_update_status(){
		$this->load->model('m_billing_account');
		$this->load->model('m_cart_order_status');
		$this->load->model('m_order');
		
		$id_order    = $this->input->post('order_id');
		$transaction_list = $this->m_order->_query_review_order(array('id'=>$id_order,'user_id'=>$this->data['member']['id'],'payment_method'=>'transfer','payment_status'=>'1'));
		foreach($transaction_list->result_array() as $listdata){
				$this->data['id_order']     = $listdata['id_order'];
				$this->data['invoice_code'] = $listdata['invoice_code'];
				$key_transfer = $listdata['key_transfer'];
				$total = $listdata['total'];
		}
		//$total       = $this->input->post('total');
		//$key_transfer= $this->input->post('key_transfer');
		$nama_nasabah= $this->input->post('nama_nasabah');
		$norek_asal  = $this->input->post('norek_asal');
		$bank_id    = $this->input->post('bank_id');
		$bank_tujuan_id    = $this->input->post('bank_tujuan_id');
		$bank_tujuan_norek = $this->m_billing_account->getBillingOneField($bank_tujuan_id,'bill_number');
		$nominal_transfer  = $this->input->post('nominal_transfer');
		$validasi_transfer = '';//substr($nominal_transfer,-3);//echo "x";die();
		$tgl_transfer      = $this->input->post('tgl_transfer');
		
			if ($_POST) {
				$errors = 0;
			    $all_post = $this->input->post();
			    $this->data['errors'] = $this->m_cart_order_status->validate_cart_order_status($all_post);
				if(!empty($this->data['errors']['nama_nasabah']))$this->session->set_flashdata('msg_nama_nasabah', $this->data['errors']['nama_nasabah']);
				if(!empty($this->data['errors']['norek_asal']))$this->session->set_flashdata('msg_norek_asal', $this->data['errors']['norek_asal']);
				if(!empty($this->data['errors']['bank_id']))$this->session->set_flashdata('msg_bank_id', $this->data['errors']['bank_id']);
				if(!empty($this->data['errors']['bank_tujuan_id']))$this->session->set_flashdata('msg_bank_tujuan_id', $this->data['errors']['bank_tujuan_id']);
				if(!empty($this->data['errors']['nominal_transfer']))$this->session->set_flashdata('msg_nominal_transfer', $this->data['errors']['nominal_transfer']);
				if(!empty($this->data['errors']['tgl_transfer']))$this->session->set_flashdata('msg_tgl_transfer', $this->data['errors']['tgl_transfer']);
			
				
				if(empty($this->data['errors']) && $errors <1){ 
					$this->db->trans_begin(); 
						$order_status = array(
							'cart_order_id'=>$id_order,
							'user_id'=>$this->data['member']['id'],
							'norek_asal'=>$norek_asal,
							'nama_nasabah'=>$nama_nasabah,
							'bank_id'=>$bank_id,
							'bank_tujuan_id'=>$bank_tujuan_id,
							'norek_tujuan'=>$bank_tujuan_norek,
							'nominal_transfer'=>$nominal_transfer,
							'key_transfer'=>$validasi_transfer,
							'tgl_transfer'=>$tgl_transfer,
							'confirm_date'=>date('Y-m-d'),
							'confirm_time'=>date('H:i:s'),
							'datetime_created'=>date('YmdHis'),
							'status'=>1
						);
						$res = $this->m_cart_order_status->save_order_status($order_status);
						
						$cart_order = array(
							'id'=>$id_order,
							'user_id'=>$this->data['member']['id'],
							'status'=>1,
							'payment_status'=>2
						);
						$res =!$res?$res: $this->m_order->update_cart_order($cart_order);
						
						if(!$res || $this->db->trans_status() === false) {
							$this->db->trans_rollback();
							$this->session->set_flashdata('message', 'Status belanja gagal dikonfirmasi.');
							
						}else{
							$this->db->trans_commit();
							$this->_renew_session_data();
							$this->session->set_flashdata('message', '<div class=\"alert alert-success\">Status belanja berhasil dikonfirmasi.</div>');
						}
				}
				
			}
		redirect(base_url('user/ubah_status/'.$id_order));
		exit;
	}
	*/
	public function do_updatepromo_status(){
		$this->load->model('m_billing_account');
		$this->load->model('m_promo_order_status');
		$this->load->model('m_promosi_user');
		
		$id_order    = $this->input->post('promo_order_id');
		$transaction_list = $this->m_promosi_user->_query_review_order(array('id'=>$id_order,'user_id'=>$this->data['member']['id'],'payment_method'=>'transfer','payment_status'=>'1'));
		foreach($transaction_list->result_array() as $listdata){
				$this->data['id_order']     = $listdata['id_order'];
				$this->data['invoice_code'] = $listdata['invoice_code'];
				$key_transfer = $listdata['key_transfer'];
				$total = $listdata['total'];
		}
		//$total       = $this->input->post('total');
		//$key_transfer= $this->input->post('key_transfer');
		$nama_nasabah= $this->input->post('nama_nasabah');
		$norek_asal  = $this->input->post('norek_asal');
		$bank_tujuan_id    = $this->input->post('bank_tujuan_id');
		$bank_tujuan_norek = $this->m_billing_account->getBillingOneField($bank_tujuan_id,'bill_number');
		$nominal_transfer  = $this->input->post('nominal_transfer');
		$validasi_transfer = '';//substr($nominal_transfer,-3);//echo "x";die();
		$tgl_transfer      = $this->input->post('tgl_transfer');
		
			if ($_POST) {
				$errors = 0;
			    $all_post = $this->input->post();
			    $this->data['errors'] = $this->m_cart_order_status->validate_cart_order_status($all_post);
				if($this->data['errors']['nama_nasabah'] !="")$this->session->set_flashdata('msg_nama_nasabah', $this->data['errors']['nama_nasabah']);
				if($this->data['errors']['norek_asal'] !="")$this->session->set_flashdata('msg_norek_asal', $this->data['errors']['norek_asal']);
				if($this->data['errors']['bank_tujuan_id'] !="")$this->session->set_flashdata('msg_bank_tujuan_id', $this->data['errors']['bank_tujuan_id']);
				if($this->data['errors']['nominal_transfer'] !="")$this->session->set_flashdata('msg_nominal_transfer', $this->data['errors']['nominal_transfer']);
				if($this->data['errors']['tgl_transfer'] !="")$this->session->set_flashdata('msg_tgl_transfer', $this->data['errors']['tgl_transfer']);
			/*	if($nominal_transfer <= $total){
					$errors++;
					$this->session->set_flashdata('msg_nominal_transfer_tdk_sama', 'Nominal transfer kurang dari total bayar!');
				}
				if($key_transfer != $validasi_transfer){
					$errors++;
					$this->session->set_flashdata('msg_tiga_digit_transfer', 'Tiga digit terakhir transfer tidak sama dengan nilai tagihan!');
				} */
				
				if(empty($this->data['errors']) && $errors <1){ 
					$this->db->trans_begin(); 
						$order_status = array(
							'cart_order_id'=>$id_order,
							'user_id'=>$this->data['member']['id'],
							'norek_asal'=>$norek_asal,
							'nama_nasabah'=>$nama_nasabah,
							'bank_tujuan_id'=>$bank_tujuan_id,
							'norek_tujuan'=>$bank_tujuan_norek,
							'nominal_transfer'=>$nominal_transfer,
							'key_transfer'=>$validasi_transfer,
							'tgl_transfer'=>$tgl_transfer,
							'confirm_date'=>date('Y-m-d'),
							'confirm_time'=>date('H:i:s'),
							'datetime_created'=>date('YmdHis'),
							'status'=>1
						);
						$res = $this->m_cart_order_status->save_order_status($order_status);
						
						$cart_order = array(
							'id'=>$id_order,
							'user_id'=>$this->data['member']['id'],
							'status'=>1,
							'payment_status'=>2
						);
						$res =!$res?$res: $this->m_order->update_cart_order($cart_order);
						
						if(!$res || $this->db->trans_status() === false) {
							$this->db->trans_rollback();
							$this->session->set_flashdata('message', 'Status belanja gagal dikonfirmasi.');
							
						}else{
							$this->db->trans_commit();
							$this->_renew_session_data();
							$this->session->set_flashdata('message', '<div class=\"alert alert-success\">Status belanja berhasil dikonfirmasi.</div>');
						}
				}
				
			}
		redirect(base_url('user/ubah_status/'.$id_order));
		exit;
	}
	
	public function sale()
	{
		$this->data['main_html'] = $this->load->view('user-sale', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}

	public function help_center(){
		//$this->load->model('m_user');
		//$this->load->model('m_order');
		//$this->load->model('m_product_image');
		//$this->load->model('m_payment_status');
		$this->load->model('m_contact_center');
		
		$contact_center = $this->m_contact_center->get_all_items();
		foreach ($contact_center as $cnt){
			$this->data['cntctr_name']   = $cnt['cntctr_name'];
			$this->data['cntctr_phone1'] = ($cnt['cntctr_phone1'] !="")?"<i class=\"fa fa-fw fa-phone\"></i><span class='cart-title'>".$cnt['cntctr_phone1']."</span>":"";
			$this->data['cntctr_phone2'] = ($cnt['cntctr_phone2'] !="")?"<i class=\"fa fa-fw fa-phone\"></i><span class='cart-title'>".$cnt['cntctr_phone2']."</span>":"";
			$this->data['cntctr_phone3'] = ($cnt['cntctr_phone3'] !="")?"<i class=\"fa fa-fw fa-whatsapp\"></i><span class='cart-title'>".$cnt['cntctr_phone3']."</span>":"";
			$this->data['cntctr_email1'] = ($cnt['cntctr_email1'] !="")?"<i class=\"fa fa-fw fa-envelope\"></i><span class='cart-title'>".$cnt['cntctr_email1']."</span>":"";
		}
		
		if($_POST){
			$msg_to = $this->input->post('msg_to');
		}else{ $msg_to =$cnt['cntctr_email1']; } 	
		$this->data['is_create']=1;
		$this->data['readonly_subject']="";
		$this->data['text_message']="Create New Message";
		$this->data['msg_to']    = $msg_to;
		$this->data['msg_date']  = date('Y-m-d'); 
		$this->data['msg_from']  = $this->data['member']['email']; 
		
		$this->data['main_html'] = $this->load->view('help-center', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}
	
	public function sale_detail($id='')
	{
		$this->load->model('m_order');
		$this->load->model('m_payment_status');
		
		$info = array('id'=>$id, 'user_id'=>$this->data['member']['id']);
		$this->data['list_order_data'] = $this->m_order->get_review_order_items($info);
		$this->data['id_review'] = ($id !="")?$id:"";
		
		$this->data['main_html'] = $this->load->view('user-sale-detail', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}

  public function open_shop()
  {
    if (empty($this->data['is_edit_shop'])) {
      $this->data['is_edit_shop'] = false;
    }

    $this->load->model('m_province');
    $this->load->model('m_regency');
    $this->load->model('m_delivery');
    $this->load->model('m_user');
    $this->load->model('m_user_delivery');

    if ($_POST) {
      $shop_name         = $this->input->post('name');
      $shop_description  = $this->input->post('description');
      $shop_province_id  = $this->input->post('province_id');
      $shop_regency_id   = $this->input->post('regency_id');
      $shop_delivery_id  = $this->input->post('delivery_id');
      $shop_id           = clear_html($shop_name, true, true);
      $shop_date_created = date('Y-m-d H:i:s');
      $shop_startactivedate = date('Y-m-d');
      $shop_status       = 1;

      $this->db->trans_begin();

      $user = $this->m_user->get_item_by_id($this->data['member']['id']);

      $this->data['errors'] = $this->m_user->update_shop($this->data['member']['id'], compact('shop_name', 'shop_description', 'shop_province_id', 'shop_regency_id', 'shop_id', 'shop_date_created','shop_status'), $this->data['is_edit_shop']);
      $this->data['errors'] = array_merge($this->data['errors'], $this->m_user_delivery->save($this->data['member']['id'], $shop_delivery_id));

      if (empty($user['shop_image'])) {
        $this->data['errors']['image'] = 'Pilih gambar toko.';
      }

      if (!empty($this->data['errors']) || $this->db->trans_status() === false) {
        $this->db->trans_rollback();
      } else {
        $this->db->trans_commit();
        $this->_renew_session_data();
      }

      if (empty($this->data['errors'])) {
        $this->session->set_flashdata('shop_update', 'Berhasil membuka toko.');
        redirect(base_url('user/shop-dashboard'));
      }
    }

    $user_deliveries  = $this->m_user_delivery->get_all_items($this->data['member']['id']);
    $user_delivery_id = array();

    foreach ($user_deliveries as $user_delivery) {
      $user_delivery_id[] = $user_delivery['delivery_id'];
    }

    $this->data['user_delivery_id'] = $user_delivery_id;
    $this->data['provinces']        = $this->m_province->get_all_items();
    $this->data['regencies']        = $this->m_regency->get_all_items();
    $this->data['deliveries']       = $this->m_delivery->get_all_items();
    
    $this->data['main_html'] = $this->load->view('user-open-shop', $this->data, true);
    $this->load->view('layout/main', $this->data); 
  }

  public function edit_shop()
  {
    $this->data['is_edit_shop'] = true;
    $this->open_shop();
  }

	function view_shop($id){
		if($id !=""){
			$this->load->model('m_product');
			$this->load->model('m_user_delivery');
			$this->load->model('m_user');
			$this->load->model('m_cart'); 
		 
			$this->data['shop_transactions'] = $this->m_cart->get_items_terjual($id);
			$this->data['products']          = $this->m_product->get_all_items($id);
			$this->data['user_deliveries']   = $this->m_user_delivery->get_all_items($id);
			$this->data['user_data']         = $this->m_user->get_item_by_id($id);
		} 

		$this->load->model('m_contact_center');
		$contact_center = $this->m_contact_center->get_all_items();
		foreach ($contact_center as $cnt){
			$this->data['cntctr_name']   = $cnt['cntctr_name'];
			$this->data['cntctr_phone1'] = ($cnt['cntctr_phone1'] !="")?"<i class=\"fa fa-fw fa-phone\"></i><span class=''>".$cnt['cntctr_phone1']."</span>":"";
			$this->data['cntctr_phone2'] = ($cnt['cntctr_phone2'] !="")?"<i class=\"fa fa-fw fa-phone\"></i><span class=''>".$cnt['cntctr_phone2']."</span>":"";
			$this->data['cntctr_phone3'] = ($cnt['cntctr_phone3'] !="")?"<i class=\"fa fa-fw fa-whatsapp\"></i><span class=''>".$cnt['cntctr_phone3']."</span>":"";
			$this->data['cntctr_email1'] = ($cnt['cntctr_email1'] !="")?"<br><i class=\"fa fa-fw fa-envelope\"></i><span class=''>".$cnt['cntctr_email1']."</span>":"";
		}
	
		$this->data['text_payment'] = ""; 
		$this->data['email_address'] = ($cnt['cntctr_email1'] !="")?$cnt['cntctr_email1']:""; 
		$this->data['title_status'] = "Update Status Penjualan";							   
		$this->data['link_url']  = "'user/'";							   
		$this->data['main_html'] = $this->load->view('user-view-shop', $this->data, true);
		$this->load->view('layout/main', $this->data);
    }
	
  public function validate_shop_name()
  {
    $this->load->model('m_user');

    $name   = $this->input->get('name');
    $result = $this->m_user->validate_shop_name($name, clear_html($name, true, true));

    echo json_encode($result);
  }

  public function upload_shop_image()
  {
    $this->load->model('m_user');
    $this->load->library('resize_image');
    $this->load->helper('image');

    $uploaded_image = (!empty($_FILES['image'])) ? $_FILES['image'] : null;
    $has_image      = !empty($uploaded_image['name']);

    if ($has_image && empty($this->data['errors'])) {
      $upload_dir_temp = $_SERVER['DOCUMENT_ROOT'] . $this->data['upload_dir_temp'];
      $upload_dir_shop = $_SERVER['DOCUMENT_ROOT'] . $this->data['upload_dir_shop'];

      $result = generate_image_folder($upload_dir_temp, $upload_dir_shop);

      if (!$result) {
        $this->data['errors'][] = 'Ada masalah dengan folder upload.';
      }

      if (empty($this->data['errors'])) {
        $image_extension = pathinfo($uploaded_image['name'], PATHINFO_EXTENSION);
        $image_name      = generate_image_name($upload_dir_shop, $image_extension);
        $image_upload    = $upload_dir_temp . '/' . $image_name; 

        if (move_uploaded_file($uploaded_image["tmp_name"], $image_upload)) {
          $this->resize_image->start($upload_dir_temp, $image_name);
          $result = $this->resize_image->create_thumbnail($upload_dir_shop);
        }

        if ($result) {
          $result = $this->m_user->update_shop_image($this->data['member']['id'], $image_name);

          if ($result) {
            $message['success'] = 'Gambar toko berhasil diupload.';
            $message['image']   = base_url($this->data['upload_dir_shop'] . '/' . $image_name);

            if (!empty($this->data['member']['shop_image']) && file_exists($upload_dir_shop . '/' . $this->data['member']['shop_image'])) {
              unlink($upload_dir_shop . '/' . $this->data['member']['shop_image']);
            }

            $this->_renew_session_data();
          }
        } else {
          $message['errors'] = 'Gambar toko gagal diupload.';
        }
      }
    }

    echo json_encode($message);
  }

	public function shop_dashboard()
	{
		$this->load->model('m_product');
		$this->load->model('m_user_delivery');
		$this->load->model('m_user');
		$this->load->model('m_cart');
		$this->load->model('m_payment_status');
		
		$this->data['product_update']    = $this->session->flashdata('product_update');
		$this->data['shop_transactions'] = $this->m_cart->get_items_terjual($this->data['member']['id']);
		$this->data['products']          = $this->m_product->get_all_items($this->data['member']['id']);
		$this->data['user_deliveries']   = $this->m_user_delivery->get_all_items($this->data['member']['id']);
		$this->data['user_data']         = $this->m_user->get_item_by_id($this->data['member']['id']);
	 
		$this->data['main_html'] = $this->load->view('user-shop-dashboard', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}
	
	public function create_message(){
		if($_POST){
			$msg_to = $this->input->post('msg_to');
		}else{ $msg_to =""; } 	
		$this->data['is_create']=1;
		$this->data['readonly_subject']="";
		$this->data['text_message']="Create New Message";
		$this->data['msg_to']    = $msg_to;
		$this->data['msg_date']  = date('Y-m-d'); 
		$this->data['msg_from']  = $this->data['member']['email']; 
		
		$this->data['msg'] = $this->session->flashdata('message');
		$this->data['main_html'] = $this->load->view('user-create-message', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}
	
	public function reply_message($id){
		$this->load->model('m_messaging');
		
		$this->data['msg_row']     = $this->m_messaging->get_one_items($id);
		$this->data['msg_to']      = $this->data['msg_row']->msg_to; 
		$this->data['msg_subject'] = $this->data['msg_row']->msg_subject;
		$this->data['msg_refid']   = $this->data['msg_row']->msg_refid;
		$this->data['msg_parent']  = $this->data['msg_row']->id;
		$this->data['is_create']   =0;
		$this->data['readonly_subject'] = "readOnly='true'";
		$this->data['text_message']= "Reply Message";
		$this->data['msg_date']    = date('Y-m-d'); 
		$this->data['msg_from']    = $this->data['member']['email']; 
		
		$this->data['msg'] = $this->session->flashdata('message');
		$this->data['main_html'] = $this->load->view('user-create-message', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}
		
	function save_message(){
		if ($_POST){
			$this->load->model('m_messaging');
			
			$msg_date    = $this->input->post('msg_date');
			$msg_from    = $this->input->post('msg_from');
			$msg_to      = $this->input->post('msg_to');
			$msg_subject = $this->input->post('msg_subject');
			$msg_body    = $this->input->post('msg_body'); 
			$msg_refid   = $this->input->post('msg_refid');
			$msg_parent  = $this->input->post('msg_parent');
			$is_create   = $this->input->post('is_create');
			$data_message = array(
				'msg_date'=>$msg_date,
				'msg_from'=>$msg_from,
				'msg_to'=>$msg_to,
				'msg_subject'=>$msg_subject,
				'msg_body'=>$msg_body,
				'msg_read'=>0
			);
			$isSave = $this->m_messaging->save_messaging($data_message);
			$msg_id = $this->db->insert_id();
			
			if($is_create==1){
				//Is New
				$ref_msg = array(
					'msg_refid'=>$msg_id
				);	
				$this->m_messaging->update_messaging($msg_id, $ref_msg);
				
			}else{
				//Is Reply
				$parent_msg = array(
					'msg_parent'=>$msg_parent,
					'msg_refid'=>$msg_refid
				);
				$this->m_messaging->update_messaging($msg_id, $parent_msg);	
			} 
			
			$this->m_messaging->update_messaging($msg_id, $parent_msg);	
			
			if($isSave){
				$this->session->set_flashdata('message', '<span class=\"text-muted\">
																		<i class=\"fa fa-fw fa-exclamation-circle\"></i>
																		Send message success.
															  </span>');
				
			}else{
				$this->session->set_flashdata('message', '<span class=\"text-muted\">
																		<i class=\"fa fa-fw fa-exclamation-circle\"></i>
																		Send message failed!.
															  </span>');	
			}
		}	
			
		redirect(base_url('user/inbox_message/'));
		exit;
	}
	
	public function inbox_message(){
		$this->load->model('m_messaging');
		
		$this->data['messaging_list'] = $this->m_messaging->get_inbox_message($this->data['member']['email']);
		//$this->data['messaging_list'] = $this->m_messaging->get_inbox_message_group_by_refid($this->data['member']['email']);
		
	
		$this->data['msg'] = $this->session->flashdata('message');
		$this->data['main_html'] = $this->load->view('user-list-message', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}
		
	public function view_message($id){
		$this->load->model('m_messaging');
		
		$this->data['messaging_view'] = $this->m_messaging->get_view_items($id);
	    $this->data['open_msg'] = $id;
		$this->data['msg'] = $this->session->flashdata('message');
		
		$this->data['main_html'] = $this->load->view('user-view-message', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}

	function update_status_order($id){
		$this->load->model('m_contact_center');
		$this->load->model('m_payment_status');
		$this->load->model('m_order');
		if($id !=""){
			$cart_order = $this->m_order->get_payment_status(array('id'=>$id));
			
			foreach ($cart_order as $key => $cartdata) {
				$this->data['id']      		    = $cartdata['id'];
				$this->data['user_id']		    = $cartdata['user_id'];
				$this->data['receive_name']     = $cartdata['receive_name'];
				$this->data['receive_address1'] = $cartdata['receive_address'];
				$this->data['receive_address2'] = $cartdata['province_name'].' '.$cartdata['type'].' '.$cartdata['name'].' - '.$cartdata['postal_code'];
				$this->data['receive_phone']    = $cartdata['receive_phone'];
				$this->data['payment_status']   = $cartdata['payment_status'];
				$this->data['payment_method']   = $cartdata['payment_method'];
				$this->data['key_transfer']     = $cartdata['key_transfer'];
				$this->data['total_belanja']    = $cartdata['total_belanja'];
				$this->data['pay_name']  		= $cartdata['pay_name'];
				$this->data['nominal_transfer'] = (!empty($cartdata['nominal_transfer']))?$cartdata['nominal_transfer']:$cartdata['total_belanja'];
				$this->data['tgl_transfer']     = $cartdata['tgl_transfer'];
				$this->data['nama_nasabah']     = $cartdata['nama_nasabah'];
				$this->data['norek_asal']       = $cartdata['norek_asal'];
				$this->data['invoice_code']     = $cartdata['invoice_code'];
				$this->data['invoice_date']     = $cartdata['invoice_date'];
		//		$this->data['delivery_desc']    = $cartdata['delivery_desc'];
		//		$this->data['delivery_rate']    = $cartdata['delivery_rate'];
			}
			
			if($this->data['payment_method']=="transfer" && $this->data['key_transfer'] !=""){
				$subtotal_rp = substr($this->data['total_belanja'],0,-3);
				$this->data['total_tagihan'] = $subtotal_rp.$this->data['key_transfer'];
			
			}elseif($payment_method=="wallet"){
				$this->data['total_tagihan'] = $this->data['total_belanja'];
			}
			
			$this->data['background_color'] = ($this->data['nominal_transfer']==$this->data['total_tagihan'])?"success":"warning";
		}

		$contact_center = $this->m_contact_center->get_all_items();
		foreach ($contact_center as $cnt){
			$this->data['cntctr_name']   = $cnt['cntctr_name'];
			$this->data['cntctr_phone1'] = ($cnt['cntctr_phone1'] !="")?"<i class=\"fa fa-fw fa-phone\"></i><span class='cart-title'>".$cnt['cntctr_phone1']."</span>":"";
			$this->data['cntctr_phone2'] = ($cnt['cntctr_phone2'] !="")?"<i class=\"fa fa-fw fa-phone\"></i><span class='cart-title'>".$cnt['cntctr_phone2']."</span>":"";
			$this->data['cntctr_phone3'] = ($cnt['cntctr_phone3'] !="")?"<i class=\"fa fa-fw fa-whatsapp\"></i><span class='cart-title'>".$cnt['cntctr_phone3']."</span>":"";
			$this->data['cntctr_email1'] = ($cnt['cntctr_email1'] !="")?"<i class=\"fa fa-fw fa-envelope\"></i><span class='cart-title'>".$cnt['cntctr_email1']."</span>":"";
		}
		
		$arrPayStatusId=$this->m_payment_status->get_paystatus_field($this->data['payment_status'],'showlist_in_seller');
		$arrPayStatusId=explode(",",$arrPayStatusId);
		$this->data['payment_status_list'] = $this->m_payment_status->get_paystatus_by_id(array('id'=>$arrPayStatusId));
		//die($this->db->last_query());
		//$this->data['payment_status_field'] = $this->m_payment_status->get_all_items(array('showlist_in_seller'=>'Y'));
		$this->data['text_payment'] = "Telah ditransfer dari nomor rekening <b>".$this->data['norek_asal']."</b> atas nama <b>".$this->data['nama_nasabah']."</b>
									   pada tanggal <b>".convertDate($this->data['tgl_transfer']).".</b> sebesar Rp <b>".number_format_id($this->data['nominal_transfer'])."</b>
									   dan di <b>".$this->data['pay_name']."</b> oleh Admin."; 
		$this->data['title_status'] = "Update Status Penjualan";
		$this->data['link_url'] = "user/shop_dashboard/";			
		$this->data['main_html'] = $this->load->view('user_confstatus', $this->data, true);
		$this->load->view('layout/main', $this->data);
    }
	
	function update_status_pembelian($id){
		$this->load->model('m_contact_center');
		$this->load->model('m_payment_status');
		$this->load->model('m_order');
		if($id !=""){
			$cart_order = $this->m_order->get_payment_status(array('id'=>$id));
			$this->load->model('m_cart');
			$this->load->model('m_user_kurir');
		    	
			foreach ($cart_order as $key => $cartdata) {
				$this->data['id']      		    = $cartdata['id'];
				$this->data['user_id']		    = $cartdata['user_id'];
				$this->data['receive_name']     = $cartdata['receive_name'];
				$this->data['receive_address1'] = $cartdata['receive_address'];
				$this->data['receive_address2'] = $cartdata['province_name'].' '.$cartdata['type'].' '.$cartdata['name'].' - '.$cartdata['postal_code'];
				$this->data['receive_phone']    = $cartdata['receive_phone'];
				$this->data['payment_status']   = $cartdata['payment_status'];
				$this->data['payment_method']   = $cartdata['payment_method'];
				$this->data['key_transfer']     = $cartdata['key_transfer'];
				$this->data['total_belanja']    = $cartdata['total_belanja'];
				$this->data['pay_name']  		= $cartdata['pay_name'];
				$this->data['nominal_transfer'] = (!empty($cartdata['nominal_transfer']))?$cartdata['nominal_transfer']:$cartdata['total_belanja'];
				$this->data['tgl_transfer']     = $cartdata['tgl_transfer'];
				$this->data['nama_nasabah']     = $cartdata['nama_nasabah'];
				$this->data['norek_asal']       = $cartdata['norek_asal'];
				$this->data['invoice_code']     = $cartdata['invoice_code'];
				$this->data['invoice_date']     = $cartdata['invoice_date'];
				$this->data['user_addr_id']     = $cartdata['user_addr_id'];
				
						$arrCart=array();
						$carts = $this->m_cart->get_all_items($cartdata['user_id'],$cartdata['id']);
						foreach ($carts as $key => $cart) {
							$arrCart[]=array(
								'cart_id'=>$cart['cart_id'],
								'product_id'=>$cart['id'],
								'qty'=>$cart['quantity'],
							);				
						}
											
						$delivery_desc='';
						$delivery_rate=0;
						foreach($arrCart as $_cart){
							$_cart['user_addr_id']=$cartdata['user_addr_id'];
							$get_all_items =$this->m_user_kurir->get_all_items($_cart);
									if(!empty($get_all_items)){
										foreach ($get_all_items as $user_address){
											
											$delivery_rate+= $user_address['delivery_rate']*$_cart['qty'];
											$delivery_desc.= $user_address['delivery_desc'];
										}
									}
						}
				$this->data['delivery_desc']    = $delivery_desc;
				$this->data['delivery_rate']    = $delivery_rate;
			}
			
			if($this->data['payment_method']=="transfer" && $this->data['key_transfer'] !=""){
				$subtotal_rp = substr($this->data['total_belanja'],0,-3);
				$this->data['total_tagihan'] = $subtotal_rp.$this->data['key_transfer'];
			
			}elseif($this->data['payment_method']=="wallet"){
				$this->data['total_tagihan'] = $this->data['total_belanja'];
			}
			
			$this->data['background_color'] = (empty($this->data['total_tagihan'])||$this->data['nominal_transfer']>=$this->data['total_tagihan'])?"success":"warning";
		} 
		
		$contact_center = $this->m_contact_center->get_all_items();
		foreach ($contact_center as $cnt){
			$this->data['cntctr_name']   = $cnt['cntctr_name'];
			$this->data['cntctr_phone1'] = ($cnt['cntctr_phone1'] !="")?"<i class=\"fa fa-fw fa-phone\"></i><span class='cart-title'>".$cnt['cntctr_phone1']."</span>":"";
			$this->data['cntctr_phone2'] = ($cnt['cntctr_phone2'] !="")?"<i class=\"fa fa-fw fa-phone\"></i><span class='cart-title'>".$cnt['cntctr_phone2']."</span>":"";
			$this->data['cntctr_phone3'] = ($cnt['cntctr_phone3'] !="")?"<i class=\"fa fa-fw fa-whatsapp\"></i><span class='cart-title'>".$cnt['cntctr_phone3']."</span>":"";
			$this->data['cntctr_email1'] = ($cnt['cntctr_email1'] !="")?"<i class=\"fa fa-fw fa-envelope\"></i><span class='cart-title'>".$cnt['cntctr_email1']."</span>":"";
		}
		
		$arrPayStatusId=$this->m_payment_status->get_paystatus_field($this->data['payment_status'],'showlist_in_buyer');
		$arrPayStatusId=explode(",",$arrPayStatusId);
		$this->data['payment_status_list'] = $this->m_payment_status->get_paystatus_by_id(array('id'=>$arrPayStatusId));
		//$this->data['payment_status_list'] = $this->m_payment_status->get_all_items(array('showlist_in_buyer'=>'Y'));
		$this->data['text_payment'] = ""; 
		$this->data['title_status'] = "Update Status Pembelian";							   
		$this->data['link_url'] = "'user/'";							   
		$this->data['main_html'] = $this->load->view('user_confstatus', $this->data, true);
		$this->load->view('layout/main', $this->data);
    }
	
	function do_update_status_order(){
		if ($_POST){
			$this->load->model('m_order');
			$id_order       = $this->input->post('id_order');
			$user_id        = $this->input->post('user_id');
			$payment_status = $this->input->post('payment_status');
			$link_url       = $this->input->post('link_url');
			if($id_order !="" && $payment_status !=""){
				$acton = $this->m_order->update_cart_order(
											array('id'=>$id_order,
												  'user_id'=>$user_id,
												  'user_id'=>$user_id,
												  'payment_status'=>$payment_status,
												  'date_updated'=>date('Y-m-d H:i:s'),
												  'status'=>1)
												);
				if($acton){
					$this->session->set_flashdata('message', '<span class=\"text-muted\">
																		<i class=\"fa fa-fw fa-exclamation-circle\"></i>
																		Status belanja berhasil diupdate.
															  </span>');
				}else{
					$this->session->set_flashdata('message', '<span class=\"text-muted\">
																		<i class=\"fa fa-fw fa-exclamation-circle\"></i>
																		Status belanja gagal diupdate.
															  </span>');
				}								
			}
			
		}
		redirect(base_url($link_url));
		//redirect(base_url('user/shop_dashboard/'));
		exit;
	}
	
	public function promo_detail()
	{
		$this->load->model('m_promosi');
		
		$this->data['promosi_list'] = $this->m_promosi->get_all_items(array());
		
		$this->data['main_html'] = $this->load->view('user-promo-detail', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}
	
	public function promo_billing()
	{
		$this->load->model('m_promosi');
				
		$id = $this->input->get('promosi_id');
		if($id !=""){
			$this->data['promotion_row'] = $this->m_promosi->get_one_items($id);
		}
		
		$this->data['msg']       = $this->session->flashdata('message');
		$this->data['main_html'] = $this->load->view('user-promo-billing', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}
	 
	function do_promo_billing($payment_metod,$promosi_id){
		$this->load->model('m_promosi_user');
		if($_POST){
			//$promosi_id 	 = $this->input->post('promosi_id');
			//$member 		 = $this->session->userdata('member');
			//$user_id=$member['id'];
			//$payment_metod   = $this->input->post('payment_metod');
			$nextact 		 = 3;//$this->input->post('nextact');
			//$passwordeklanku = $this->input->post('passwordeklanku');
			$data_promosi = array(
				//'id'=>'DEFAULT',
				'promosi_id'=>$promosi_id,
				'user_id'=>$this->data['member']['id'],
				'payment_metod'=>$payment_metod,
				'payment_status'=>'Belum dibayar',
				'invoice_code'=>'',
				'invoice_date'=>date('Y-m-d'),
				'status'=>'1'
			);
			
			$insertid = $this->m_promosi_user->do_save($data_promosi);
			if($insertid){
				$text_message ="<div class=\"alert alert-success\">
									<center>
									Pembelian Paket Promo Berhasil, silahkan anda melakaukan transfer nominal paket ke rekening dibawah ini.<br>
									Lalukan <a  href=\"javascript:void($insertid);\" onclick=\"javascript:validate();\">Konfirmasi Promosi</a> untuk menyelesaikan proses paket promosi. 
									</center>
								</div>";
			}
			$this->session->set_flashdata('message', $text_message);
			redirect(base_url('user/promo_billing?promosi_id='.$promosi_id));
			exit; 
		}
	}
	
	protected function _do_payment($metode_bayar,$promo_id,$id='')
	{
		
		
		$this->load->model('m_promosi');
		$member=$this->session->userdata('member');
		$nextact 		 = 3;//$this->input->post('nextact');
		if(!empty($promo_id)){
			$resRow = $this->m_promosi->get_one_items($promo_id);
			$total_bayar =$resRow->harga_paket;
		}
					
		$this->db->trans_begin();
			if(empty($id)){
				//update wallet
				$key_transfer='';
				if($metode_bayar=="wallet"){
					$this->load->model('m_product');
					$saldo=$this->m_product->getSaldo();
					$wallet = $saldo - $total_bayar;
					$is_save=false;
					if(!empty($member['eklanku_id'])){
						$this->load->model('m_user');
						$is_save=$this->m_user->update_db2wallet($member['eklanku_id'], $wallet);
						
						$this->m_user->update_wallet($member['id'], $wallet);
					}
						
					if(!$is_save){
						return array(
							"head"=>'Penyimpanan Data Transaksi Gagal (eklanku_id)!',
							"form"=>"",
							"status"=>"4"
						);
						exit;
					}
					else{
					  $payment_status=3;
					  $inv_code='W'.date('ymdHms');
					  //alert system info postgres
					  	$_paydata['hargajual']=$total_bayar;
				    	$_paydata['produk_desc']="Nama Paket ".$resRow->nama_paket." Jumlah: ".$resRow->jumlah_produk." durasi: ".$resRow->durasi_kontrak; 
				    	$_paydata['invoice']=$inv_code;
						$_ret=$this->doPay($_paydata);
						if(!empty($_ret)&&$_ret['status']>0){
							$this->db->trans_rollback();
							return $_ret;
							exit;
						}else if(empty($_ret)){
							return array(
								"head"=>'Penyimpanan Data Transaksi Gagal (Payment)!',
								"form"=>"",
								"status"=>"4"
							);
							exit;
						}	
					}
				}elseif($metode_bayar=="transfer"){
					$key_transfer = $this->acak_key_transfer();
					$payment_status=1;
					$inv_code='T'.date('ymdHms');					  
				}
				
			$data_promosi = array(
				//'id'=>'DEFAULT',
				'promosi_id'=>$promo_id,
				'user_id'=>$member['id'],
				'payment_metod'=>$metode_bayar,
				'payment_status'=>$payment_status,
				'nominal'=>$total_bayar,
				'key_transfer'=>$key_transfer,
				'invoice_code'=>$inv_code,
				'invoice_date'=>date('Y-m-d'),
				'status'=>'1'
			);
			$this->load->model('m_promosi_user');
			$inser = $this->m_promosi_user->do_save($data_promosi);
			$insertid = $this->db->insert_id();
			
				
				if($this->db->trans_status() === false){
					$this->db->trans_rollback();
					return array(
						"head"=>'Penyimpanan Data Transaksi Gagal!',
						"form"=>"",
						"status"=>"4"
					);
					exit;
				}else{
					$this->db->trans_commit();
					$this->_renew_session_data();
					
					/*
						if(!empty($member['email'])){
				        	$this->load->library('egc');
				        	$data_order['id']=$order_id;
				        	$this->egc->sendmail_promo($member,$data_order);
				        }//redirect(base_url('cart/payment/' . $order_id));*/
				        return array(
							"head"=>"Informasi eklanku",
							"form"=>"Invoice pembelian PROMO telah dikirim email Anda, No Invoice: ".$data_promosi['invoice_code']." berhasil. <a href = '".base_url('user/promo_confirmation?id=' . $insertid)."'>Detail</a>",
							"status"=>"0",
				        	"id"=>$insertid
						);			        
				}
			}else{
				redirect(base_url('user/promo_confirmation?id=' . $insertid));
				exit;
			}	
    }
    
    private function doPay($data){
    	$this->load->library('egc');
		$pem_code		= 'SHOP' . $this->egc->genRandomString(10).date("His");
		//callPayment($jml, $idmember, $invoice, $idtrx, $kodetrx, $tujuan)
	    $resp = $this->m_product->callPayment(
	    	$_data['hargajual'], 
	    	$this->member['eklanku_id'], 
	    	$pem_code, 
	    	'SHOPING', 
	    	$_data['produk_desc'], 
	    	$_data['invoice']);
		$iResult = '';
		if ($resp->num_rows() > 0)
			{
				$resp = $resp->row();
				$iResult = $resp->f_cal_payment;
				if($iResult=="Proses payment selesai.")
				{
					//$this->session->set_flashdata('message_info', "Proses pembelian token listrik berhasil. <a href = 'laporan'>Detail</a>");
					return array(
						"head"=>"Informasi eklanku",
						"form"=>"Proses pembelian ".$_data['text']." berhasil.",
						"status"=>"0"
					);
				}
				else
				{
					//$this->session->set_flashdata('message_error', ''.$iResult);
					return array(
						"head"=>$iResul,
						"form"=>"",
						"status"=>"4"
					);							
				}
																													
			}
			else
			{
				//$this->session->set_flashdata('message_error', 'Sever sedang sibuk silahkan diulang beberapa saat lagi.');
				return (json_encode(array(
						"head"=>"Sever sedang sibuk silahkan diulang beberapa saat lagi.",
						"form"=>"",
						"status"=>"4"
					)));								
			}
    }
	 
	public function promo_manage()
	{	
		$this->data['main_html'] = $this->load->view('user-promo-manage', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}
  
	public function promo_confirmation()
	{ 
		$id = $this->input->get('id');
		$this->load->model('m_promosi_user');
		$this->load->model('m_promosi');
		$promosiuser=$this->m_promosi_user->get_one_items($id);
		$member=$this->session->userdata('member');
		if($promosiuser){
			if($promosiuser->user_id==$member['id']){
				$this->data['promosiuser']=$promosiuser;
				$this->data['member']=$member;
				$this->data['promosi']=$this->m_promosi->get_one_items($promosiuser->promosi_id);		
		//die(json_encode($this->data['promosi'])."$promosiuser->user_id==$member[id]");
				$this->data['main_html'] = $this->load->view('user-promo-confirmation', $this->data, true);
			}
		}
		$this->load->view('layout/main', $this->data); 
	}

	public function promo_status()
	{
		$this->data['main_html'] = $this->load->view('user-promo-status', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}

  public function add_product()
  {
    $this->load->model('m_user');
    $this->load->model('m_product_category');
    $this->load->model('m_product');
    $this->load->model('m_product_image');
    $this->load->model('m_product_category');
    $this->load->model('m_product_category_sub');
    $this->load->model('m_product_category_last');

    $this->load->library('resize_image');

    $product_code = $this->data['member']['product_code'];
    $created_by   = $this->data['member']['id'];

    if ($_POST) {
      $name             = $this->input->post('name');
      $type             = $this->input->post('type');
      $weight_type      = $this->input->post('weight_type');
      $weight           = $this->input->post('weight');
      $price            = $this->input->post('price');
      $discount_type    = $this->input->post('discount_type');
      $discount         = $this->input->post('discount');
      $description      = $this->input->post('description');
      $category_id      = $this->input->post('category_id');
      $sub_category_id  = $this->input->post('sub_category_id');
      $last_category_id = $this->input->post('last_category_id');
      $stock            = $this->input->post('stock');
      $is_published     = $this->input->post('is_published');

      $this->db->trans_begin();

		$data = compact(
			'created_by', 'name', 'type', 'weight_type', 'weight', 'price',
			'discount_type', 'discount', 'description', 'product_code',
			'category_id', 'sub_category_id', 'last_category_id', 'stock', 'is_published'
		);

		$this->data['errors'] = $this->m_product->validate($data);
		$product_images       = $this->m_product_image->get_all_items_by_product_code($created_by, $product_code);
		$has_primary_image    = false;

		foreach ($product_images as $product_image) {
			if ($product_image['is_show'] === '1') {
			  $has_primary_image = true;
			  break;
			}
		}

		if(empty($product_images)){
			$this->data['errors']['image'] = 'Masukkan gambar untuk produk.';
		} elseif (!$has_primary_image) {
			$this->data['errors']['image'] = 'Pilih gambar utama untuk produk.';
		}

		if (empty($this->data['errors'])) {
			$this->m_product->save($data);
			$product_id = $this->db->insert_id();

			$upload_dir_product_temp  = $_SERVER['DOCUMENT_ROOT'] . $this->data['upload_dir_product_temp'];
			$upload_dir_product_thumb = $_SERVER['DOCUMENT_ROOT'] . $this->data['upload_dir_product_thumb'];
			$upload_dir_product       = $_SERVER['DOCUMENT_ROOT'] . $this->data['upload_dir_product'];

			try {
			  foreach ($product_images as $product_image) {
				$this->resize_image->start($upload_dir_product_temp, $product_image['image']);
				$this->resize_image->create_thumbnail($upload_dir_product_thumb, 300, 300, false);
				$this->resize_image->resize_image($upload_dir_product, 800);
			  }
			} catch (Exception $e) {
			  $this->db->trans_rollback();
			}

		    $this->m_user->add_product_code($this->data['member']['id']);

			if ($this->db->trans_status() === true) {
			  $this->db->trans_commit();

			  $this->_renew_session_data();
			  $this->session->set_flashdata('product_update', 'Product telah ditambahkan.');
			  redirect(base_url('user/shop-dashboard'));
			}
		}

      $this->db->trans_rollback();
    }

    $product_images    = $this->m_product_image->get_all_items_by_product_code($created_by, $product_code);
    $product_deficient = $this->max_product_images - count($product_images);

    if ($product_deficient > 0) {
      for ($i = 0; $i < $product_deficient; $i++) {
        $product_images[] = null;
      }
    }  

	$this->data['products']        = $this->m_product->get_all_items($this->data['member']['id']);
    $this->data['product_images']  = $product_images;
    $this->data['categories']      = $this->m_product_category->get_all_items();
    $this->data['sub_categories']  = $this->m_product_category_sub->get_all_items();
    $this->data['last_categories'] = $this->m_product_category_last->get_all_items();
    $this->data['main_html']       = $this->load->view('user-add-product', $this->data, true);
    $this->load->view('layout/main', $this->data); 
  }

  public function edit_product($id)
  {
    $this->load->model('m_user');
    $this->load->model('m_product_category');
    $this->load->model('m_product');
    $this->load->model('m_product_image');
    $this->load->model('m_product_category');
    $this->load->model('m_product_category_sub');
    $this->load->model('m_product_category_last');

    $user_id = $this->data['member']['id'];

    $this->data['product']        = $this->m_product->get_item_by_id($id);
    $this->data['product_images'] = $this->m_product_image->get_all_items_by_product_code($user_id, $this->data['product']['product_code']);

    if ($_POST) {
      $name             = $this->input->post('name');
      $type             = $this->input->post('type');
      $weight_type      = $this->input->post('weight_type');
      $weight           = $this->input->post('weight');
      $price            = $this->input->post('price');
      $discount_type    = $this->input->post('discount_type');
      $discount         = $this->input->post('discount');
      $description      = $this->input->post('description');
      $category_id      = $this->input->post('category_id');
      $sub_category_id  = $this->input->post('sub_category_id');
      $last_category_id = $this->input->post('last_category_id');
      $stock            = $this->input->post('stock');
      $is_published     = $this->input->post('is_published');

      $this->db->trans_begin();

      $data = compact(
        'name', 'type', 'weight_type', 'weight', 'price',
        'discount_type', 'discount', 'description',
        'category_id', 'sub_category_id', 'last_category_id', 'stock', 'is_published'
      );

      $this->data['errors'] = $this->m_product->validate($data);
      $product_images       = $this->m_product_image->get_all_items_by_product_code($user_id, $this->data['product']['product_code']);
      $has_primary_image    = false;

      foreach ($this->data['product_images'] as $product_image) {
        if ($product_image['is_show'] === '1') {
          $has_primary_image = true;
          break;
        }
      }

      if (empty($this->data['product_images'])) {
        $this->data['errors']['image'] = 'Masukkan gambar untuk produk.';
      } elseif (!$has_primary_image) {
        $this->data['errors']['image'] = 'Pilih gambar utama untuk produk.';
      }

      if (empty($this->data['errors'])) {
        $this->m_product->update($id, $user_id, $data);

        if ($this->db->trans_status() === true) {
          $this->db->trans_commit();

          $this->session->set_flashdata('product_update', 'Product telah diubah.');
          redirect(base_url('user/shop-dashboard'));
        }
      }

      $this->db->trans_rollback();
    }

    $total_images = count($this->data['product_images']);
    $empty_images = $this->config->item('max_product_images') - $total_images;

    for ($i = 0; $i < $empty_images; $i++) {
      $this->data['product_images'][] = null;
    }

    $this->data['categories']      = $this->m_product_category->get_all_items();
    $this->data['sub_categories']  = $this->m_product_category_sub->get_all_items();
    $this->data['last_categories'] = $this->m_product_category_last->get_all_items();
    $this->data['main_html']       = $this->load->view('user-edit-product', $this->data, true);
    $this->load->view('layout/main', $this->data); 
  }

  public function upload_product_image()
  {
    $this->load->model('m_product_image');
    $this->load->helper('image');

    $upload_dir_product_temp = $_SERVER['DOCUMENT_ROOT'] . $this->data['upload_dir_product_temp'];

    if (!file_exists($upload_dir_product_temp)) {
      mkdir($upload_dir_product_temp);
    }

    $image     = $_FILES['image'];
    $has_image = !empty($image['name']);

    if ($has_image) {
      $errors = validate_image($image);
      $result = generate_image_folder($upload_dir_product_temp);

      if (!$result) {
        $errors = 'Ada masalah dengan folder upload.';
      }

      if (empty($errors)) {
        $image_extension = get_extension($image);
        $image_name      = generate_image_name($upload_dir_product_temp, $image_extension);
        $image_upload    = $upload_dir_product_temp . '/' . $image_name; 

        if (move_uploaded_file($image["tmp_name"], $image_upload)) {
          $product_code = $this->input->post('product_code');
          $this->m_product_image->save($image_name, $product_code, $this->data['member']['id']);
          $image_id = $this->db->insert_id();
          echo json_encode(array('message' => 'success', 'image_name' => $image_name, 'image_id' => $image_id));
        }
      } else {
        echo json_encode(array('message' => 'failed', 'info' => $errors));
      }
    }

    exit;
  }

  public function upload_edit_product_image()
  {
    $this->load->model('m_product_image');
    $this->load->helper('image');
    $this->load->library('resize_image');
    
    $upload_dir_product_temp  = $_SERVER['DOCUMENT_ROOT'] . $this->data['upload_dir_product_temp'];
    $upload_dir_product_thumb = $_SERVER['DOCUMENT_ROOT'] . $this->data['upload_dir_product_thumb'];
    $upload_dir_product       = $_SERVER['DOCUMENT_ROOT'] . $this->data['upload_dir_product'];

    if (!file_exists($upload_dir_product_temp)) {
      mkdir($upload_dir_product_temp);
    }

    $image     = $_FILES['image'];
    $has_image = !empty($image['name']);

    if ($has_image) {
      $errors = validate_image($image);
      $result = generate_image_folder($upload_dir_product_temp);

      if (!$result) {
        $errors = 'Ada masalah dengan folder upload.';
      }

      if (empty($errors)) {
        $image_extension = get_extension($image);
        $image_name      = generate_image_name($upload_dir_product_temp, $image_extension);
        $image_upload    = $upload_dir_product_temp . '/' . $image_name; 

        if (move_uploaded_file($image["tmp_name"], $image_upload)) {
          $product_code = $this->input->post('product_code');
          $this->m_product_image->save($image_name, $product_code, $this->data['member']['id']);
          $image_id = $this->db->insert_id();

          $this->resize_image->start($upload_dir_product_temp, $image_name);
          $this->resize_image->create_thumbnail($upload_dir_product_thumb, 300, 300, false);
          $this->resize_image->resize_image($upload_dir_product, 800);

          echo json_encode(array('message' => 'success', 'image_name' => $image_name, 'image_id' => $image_id));
        }
      } else {
        echo json_encode(array('message' => 'failed', 'info' => $errors));
      }
    }

    exit;
  }

  public function delete_product_image()
  {
    $this->load->model('m_product_image');

    $image_id = $this->input->post('id');
    $user_id  = $this->input->post('user_id');

    $result = $this->m_product_image->delete($image_id, $user_id);

    if ($result) {
      echo json_encode(array('message' => 'success'));
    } else {
      echo json_encode(array('message' => 'failed'));
    }

    exit;
  }

	public function update_primary_product_image()
	{
		$this->load->model('m_product_image');

		$image_id     = $this->input->post('id');
		$user_id      = $this->input->post('user_id');
		$product_code = $this->input->post('product_code');

		$this->db->trans_begin();

		$this->m_product_image->reset_primary_image($user_id, $product_code);
		$this->m_product_image->update_primary_image($image_id, $user_id, $product_code);

		if ($this->db->trans_status() === true) {
		  $this->db->trans_commit();
		  echo json_encode(array('message' => 'success'));
		} else {
		  $this->db->trans_rollback();
		  echo json_encode(array('message' => 'failed'));
		}

		exit;
	}

  private function _check_product_image_name($image_extension)
  {
    $this->load->helper('string');
    $this->load->model('m_product_image');

    $image_name = date('YmdHis') . random_string('alnum', 35) . '.' . $image_extension;
    $has_name   = $this->m_product_image->check_image_name($image_name);

    if ($has_name) {
      $this->_check_product_image_name($image_name);
    }

    return $image_name;
  }
  
  

   private function acak_key_transfer(){
		$this->load->library('egc');
		for($i=0; $i <=299; $i++){$rand = $this->egc->genRandomNumber(3)."<br>";
			if($rand<=299)break;
		}
		return $rand;
   }

	public function verifyPin(){
		$this->redirect_not_login(true);
		$metode_bayar = $this->input->post('metode_bayar');
		$id = $this->input->post('promotion_id');
		if($id !=""){
			$this->load->model('m_promosi');
			$total_bayar = $this->m_promosi->get_one_items($id)->harga_paket;
			
		}
		
	  	header('Content-type: text/plain'); 
	  	// set json non IE
	  	header('Content-type: application/json'); 
		
		if(!preg_match('/^wallet|transfer/',$metode_bayar))
		{
			die(json_encode(array(
			"head"=>"Informasi Kesalahan",
			"form"=>"<h4>Silahkan pilih metode bayar</h4>",
			"status"=>"1"
			)));
		}
		elseif(preg_match('/^transfer/',$metode_bayar)){
			$ret=$this->_do_payment($metode_bayar,$id,'');
			if(!empty($ret))die(json_encode($ret));	
			else die(json_encode(array(
				"head"=>'Gagal pembayaran...!',
				"form"=>"",
				"status"=>"4"
			)));
		}
		//$this->load->model('mbrlst_model');
		$saldoSisa = $this->_recheckSaldo();
		if($saldoSisa<$total_bayar)
		{
			//$this->session->set_flashdata('message_error', 'Saldo Eklanku Anda tidak mencukupi untuk pembelian silahkan melakukan topup Saldo eklanku terlebih dahulu.');
			die(json_encode(array(
				"head"=>"Saldo Eklanku Anda tidak mencukupi untuk pembelian silahkan melakukan topup Saldo eklanku terlebih dahulu.",
				"form"=>"",
				"status"=>"4"
			)));				
		}
		$this->load->model('m_product');
		/*$otp = $this->m_product->doCreateOtp();
		if(!$otp['sts'])
		{
			die(json_encode(array(
				"head"=>"Otp gagal terkirim",
				"form"=>"<h4>".$otp['msg']."</h4>",
				"status"=>"4"
			)));			
		}
		*/
		echo json_encode(array(
			"status"=>"0",
			"head"=>"Silahkan masukan kode PIN Anda...",
			"form"=>"
			  <div class='col-sm-12'>
				  <div class='input-group form-group'>
						<input  id='otp' type='text' class='form-control' placeholder='Kode PIN'/>
						  <span class='input-group-btn'>
			              	 <button type=button onclick='doPin()' class='btn btn-primary'>Verifikasi</button>
						  </span>
					</div>
				  <script type=text/javascript>
				     function doPin(){
				            otp = $('#otp').val();
				            metode_bayar= $('#metode_bayar').val();
                      	   // alert(otp+'|'+jenis+'||'+id_pel);
                      	    vrf='#verify_',
            	    	  	head=$(vrf+'head'),
            	    	  	form=$(vrf+'form'),
            	    	  	load=$(vrf+'loader'),
            	    	  	sts=$(vrf+'status');
            	    	  	head.html('Dalam Process');
	            		    form.hide();
	            		    load.show();
	            		    sts.html('');
            				$.ajax({
		            		    type: 'POST',
		            		    url: '/user/doToken',
		            		    async: true,
  								data: {
  									metode_bayar:metode_bayar,
  									pin:otp,
					    			promotion_id: $('#promosi_id').val()},
		            		    success: function(msg) {
			            		    /*if(window.console){
									  console.log(msg);
									 }*/
		            		    	if(msg.status=='0'){
				            		    //sts.hide();
			            		    	head.html(msg.head);
				            		    form.html(msg.form);
				            		    if(!(msg.id == null || msg.id.length === 0))
	            		    				window.location.href = '".base_url('user/promo_confirmation')."/'+msg.id;
			            		    }
			            		    else{
			            		      	//sts.show();
			            		      	head.html('Info Gagal');
			                    		sts.html('<h4>'+msg.head+'</h4>');
			                    	}
				            		form.show();
			                    	load.hide();
                      	        },
                      	      /*  complete: function (response){
								 if(window.console){
								  console.log(response.responseText);
								 }
								}*/
	            			});
						}
				  </script>
			  </div>
			  "
			 
		));
	}
	
	public function doToken()
	{	
		$metode_bayar = $this->input->post('metode_bayar');
		$pin   		    = $this->input->post('pin');	
		$id = $this->input->post('promotion_id');
		if($id !=""){
			$this->load->model('m_promosi');
			$total_bayar = $this->m_promosi->get_one_items($id)->harga_paket;
			
		}
		
		header('Content-type: text/plain'); 
	  	header('Content-type: application/json'); 
		
        if(empty($pin))
		{
			die(json_encode(array(
			"head"=>"silahkan input kode PIN",
			"form"=>"",
			"status"=>"3"
			)));
			
		}
		
		if(!preg_match('/^wallet|transfer/',$metode_bayar))
		{
			die(json_encode(array(
			"head"=>"Silahkan pilih metode bayar",
			"form"=>"",
			"status"=>"1"
			)));
		}

		$saldoSisa =$this->_recheckSaldo();
		if($saldoSisa<$total_bayar)
		{
			//$this->session->set_flashdata('message_error', 'Saldo Eklanku Anda tidak mencukupi untuk pembelian silahkan melakukan topup Saldo eklanku terlebih dahulu.');
			die(json_encode(array(
				"head"=>"Saldo Eklanku Anda tidak mencukupi untuk pembelian silahkan melakukan topup Saldo eklanku terlebih dahulu.",
				"form"=>"",
				"status"=>"4"
			)));				
		}
		//$this->load->model('belanja_model');
		/*
		if($this->m_product->isValidOTP($pin) == false)
		{
			//$this->session->set_flashdata('message_error', 'PIN yang Anda masukkan salah!');
			die(json_encode(array(
				"head"=>'no OTP yang Anda masukkan salah!',
				"form"=>"",
				"status"=>"4"
			)));
		}
		*/
		$this->load->model('m_deposit');
		if($this->m_deposit->isValidPIN($pin) == false)
		{
			die(json_encode(array(
				"head"=>'PIN yang Anda masukkan salah!',
				"form"=>"",
				"status"=>"4"
			)));
		}
		
		$ret=$this->_do_payment($metode_bayar,$id,'');
		if(!empty($ret))die(json_encode($ret));	
		else die(json_encode(array(
				"head"=>'Gagal pembayaran...!',
				"form"=>"",
				"status"=>"4"
			)));
	}
	
	
}
