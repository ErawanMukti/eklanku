<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Iklan extends Application {

	public function __construct()
	{		
		parent::__construct();		
		$this->redirect_not_login();
		$this->load->model('m_messaging');
    	/*if ($this->session->userdata("mbr_code") == false)
		{
			redirect("homepage");
		}*/
		
		
	}

	public function index()
	{
		$this->data['page_title'] = 'Dashboard';	
		$this->data['page_map'] = 'Dashboard';	
		$this->data['site_map'] = 'Pasang Iklan';
		$this->data['view_title'] = 'Pasang Iklan ';	
		$this->data['jualbeli'] = str_replace('://','://jual-beli.',str_replace('://www.','://',base_url()));
		$this->load->model('m_iklan');
		$this->load->model('m_category');
		$this->data["jmlData"] = $this->m_iklan->record_countSaya();
		$this->data['content_data_catagory'] = $this->m_category->doReadMember();
		/*$this->data['view_link'] = base_url().'iklan';
        $this->data['menu'] = '1';		
        $this->load->model('belanja_model');
		$this->load->model('mbrlst_model');
		$this->data['content_saldo'] = $this->mbrlst_model->getSaldo();
		$this->data['content_data_ref'] = $this->belanja_model->doReadRef();
		$this->data['content_trx'] = $this->belanja_model->getTotalTrx();
		$this->data['content_wd'] = $this->belanja_model->getTotalWD();
		$this->data['content_wdantre'] = $this->belanja_model->getTotalAntreanWD();
		$this->data['content_view'] = 'iklan/iklan_add';
		//$this->data['data_view'] = '';
		$this->data['content_header'] = 'template/globalheader';
		$this->data['content_footer'] = 'template/globalfooter';
		$this->load->view('template/tplmenu', $this->data);		*/
		$this->load->model('m_user');
		$this->load->model('m_order');
		$this->load->model('m_eklanpay_order');
		$this->load->model('m_product_image');
		$this->load->model('m_payment_status');
		$this->load->model('m_promosi_user'); 
		
		$this->data['msg']			   = $this->session->flashdata('message');
		$this->data['user_data']       = $this->m_user->get_item_by_id($this->data['member']['id']);
		$this->data['transaction_list']= $this->m_order->_query_review_order(array('user_id'=>$this->data['member']['id'],'payment_method'=>'transfer','payment_status'=>'1'));
		$this->data['cofirm_promotion']= $this->m_promosi_user->get_all_items(array('user_id'=>$this->data['member']['id'],'payment_status'=>'Belum dibayar'));
		$this->data['cofirm_eklanpay']= $this->m_eklanpay_order->get_all_items(array('user_id'=>$this->data['member']['id'],'payment_status'=>1,'invoice_date'=>'2 HOUR'));
		
		
		$this->data['main_html'] = $this->load->view('iklan/iklan_add', $this->data, true);
		$this->load->view('layout/main', $this->data); 

	}
	public function detail()
	{
		$this->load->library("pagination");
		$this->data['page_title'] = 'Dashboard';	
		$this->data['page_map'] = 'Dashboard';	
		$this->data['site_map'] = 'Iklan Saya';
		$this->data['view_title'] = 'Iklan Saya ';	
		$this->data['jualbeli'] = //base_url();
		str_replace('://','://jual-beli.',str_replace('://www.','://',base_url()));
		/*$this->data['view_link'] = base_url().'dasbord';	
		$this->data['menu'] = '2';
		$this->load->model('mbrlst_model');
		$this->data['content_saldo'] = $this->mbrlst_model->getSaldo();
		$this->load->model('belanja_model');
		$this->data['content_data_ref'] = $this->belanja_model->doReadRef();
		$this->data['content_trx'] = $this->belanja_model->getTotalTrx();
		$this->data['content_wd'] = $this->belanja_model->getTotalWD();
		$this->data['content_wdantre'] = $this->belanja_model->getTotalAntreanWD();
		*/
		$this->load->model('m_iklan');
		$config = array();
        $config["base_url"] = base_url() . "iklan/detail/";
        $config["total_rows"] = $this->m_iklan->record_countSaya();
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;
		$choice = $config["total_rows"];
        $config["num_links"] = floor($choice);
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $this->data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->data['total'] = $config["num_links"];
		$this->data['tls'] =  floor($config["num_links"]/5)+1;
		$this->data['jmlData'] =  $config["total_rows"];
		$this->data['deptlist'] = $this->m_iklan->get_department_listSaya($config["per_page"], $this->data['page']);           

        $this->data['pagination'] = $this->pagination->create_links();
		/*$this->data['content_view'] = 'iklan/iklandetail';
		//$this->data['data_view'] = '';
		$this->data['content_header'] = 'template/globalheader';
		$this->data['content_footer'] = 'template/globalfooter';
		$this->load->view('template/tplmenu', $this->data);		*/
        $this->load->model('m_user');
		$this->load->model('m_order');
		$this->load->model('m_eklanpay_order');
		$this->load->model('m_product_image');
		$this->load->model('m_payment_status');
		$this->load->model('m_promosi_user'); 
		
		$this->data['msg']			   = $this->session->flashdata('message');
		$this->data['user_data']       = $this->m_user->get_item_by_id($this->data['member']['id']);
		$this->data['transaction_list']= $this->m_order->_query_review_order(array('user_id'=>$this->data['member']['id'],'payment_method'=>'transfer','payment_status'=>'1'));
		$this->data['cofirm_promotion']= $this->m_promosi_user->get_all_items(array('user_id'=>$this->data['member']['id'],'payment_status'=>'Belum dibayar'));
		$this->data['cofirm_eklanpay']= $this->m_eklanpay_order->get_all_items(array('user_id'=>$this->data['member']['id'],'payment_status'=>1,'invoice_date'=>'2 HOUR'));
		
        $this->data['main_html'] = $this->load->view('iklan/iklandetail', $this->data, true);
		$this->load->view('layout/main', $this->data); 

	}
	public function doCreate()
	{
		
		$this->load->model('m_iklan');
		$title = $this->input->post("title");	
		$kategory = $this->input->post("kategory");	
		$harga = $this->input->post("harga");	
		$daerah = $this->input->post("daerah");	
		$deskripsi = $this->input->post("deskripsi");	
		if($title == "")
		{
			$this->session->set_flashdata('message_error', 'Masukan Judul Iklan Anda!');
			redirect("iklan");
		}
        if($kategory == "")
		{
			$this->session->set_flashdata('message_error', 'Pilih Kategory!');
			redirect("iklan");
		}
        if($harga == "")
		{
			$this->session->set_flashdata('message_error', 'Masukan harga!');
			redirect("iklan");
		}
		if($daerah == "")
		{
			$this->session->set_flashdata('message_error', 'Masukan daerah!');
			redirect("iklan");
		}
		if($deskripsi == "")
		{
			$this->session->set_flashdata('message_error', 'Masukan deskripsi!');
			redirect("iklan");
		}
        if($harga <= 0)
		{
			$this->session->set_flashdata('message_error', 'Harga harus angka, jangan gunakan karakter!');
			redirect("iklan");
		}
		if ($this->m_iklan->doReadIklanCek($title,$kategory) == true)
		{				
			$cmd_save = $this->input->post('cmd_save');		
			if ($cmd_save == 'T')
			{
			/*	$nama = $this->input->post("userfile");
				$newNama = md5($nama.date('Y-m-d H:i:s'));
				$config['image_library'] = 'gd2';
				$original_path = './assets/uploads/activity_images/original';
				$resized_path = './assets/receipt';
				$thumbs_path = './assets/uploads/activity_images/thumb';
				$this->load->library('image_lib');
		   
				$config = array(
					'allowed_types' => 'jpg|jpeg|gif|png', //only accept these file types
					'max_size' => 2048, //2MB max
					'file_name'=>$newNama,
					'upload_path' => $resized_path //upload directory    
				);
				$this->load->library('upload', $config);
				$this->upload->do_upload();
				$image_data = $this->upload->data(); //upload the image
				$image1 = $image_data['file_name'];
				if (!$this->upload->do_upload()){
					$resise_eror =  $this->upload->display_errors('', ''); 
					$this->session->set_flashdata('message_error', 'Error: '.$resise_eror);
					redirect('iklan');
				}else{
				
				

				//your desired config for the resize() function
				/*$config = array(
					'source_image' => $image_data['full_path'], //path to the uploaded image
					'new_image' => $resized_path,
					'maintain_ratio' => true,
					'width' => 870,
					'height' => 500
				);
				$this->image_lib->initialize($config);
				$this->image_lib->resize();

				// for the Thumbnail image
				$config = array(
					'source_image' => $image_data['full_path'],
					'new_image' => $thumbs_path,
					'maintain_ratio' => true,
					'width' => 435,
					'height' => 250
				);
				//here is the second thumbnail, notice the call for the initialize() function again
				$this->image_lib->initialize($config);

				$this->image_lib->resize();
				 if ( !$this->image_lib->resize()){
					$resise_eror =  $this->image_lib->display_errors('', ''); 
					$this->session->set_flashdata('message_error', 'Error: '.$resise_eror);
					redirect('iklan');
				 }
				 else{
			        $cnfFil 		= '';
					
					foreach ($this->upload->data() as $item => $value)
					{
						if ($item == 'file_name')
						{
							$cnfFil = $value;	
						}				
					}*/
					if ($this->m_iklan->doCreate()//($cnfFil) 
					== true)
					{
							redirect('iklan/detail');	
					}
					else
					{
						$this->session->set_flashdata('message_error', 'Register failed. ');
						redirect('iklan');	
					}	
				// }
				
			}
		}
		else
		{
			$this->session->set_flashdata('message_error', 'Gunakan judul lain, judul tersebut sudah terdaftar di sistem kami!');
			redirect('iklan');	
		}				
	}
	

    public function doDelete($code)
	{
		$this->load->model('m_iklan');				
		if($this->m_iklan->doDelete($code) == true)
		{
			$this->session->set_flashdata('message_info', 'Iklan sudah berhasil dihapus');
			redirect('iklan/detail');	
		}else{
			$this->session->set_flashdata('message_error', 'Kode iklan tidak terdaftar atau sever sedang sibuk');
			redirect('iklan/detail');
		}
	}   
	 

	
	
}

/* End of file member.php */
/* Location: ./application/controllers/member.php */