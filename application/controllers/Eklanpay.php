<?php
class Eklanpay extends Application
{
	public function __construct()
	{
	    parent::__construct();
		
	    $this->redirect_not_login();
	    $this->load->model('m_messaging');
	    $this->data['nav']=array(
		    'data'=>array(
			    'Daftar Paket Deposit',
	            'Detail Penagihan',
	            'Konfirmasi',
	            'Status')
	    );
	}
	
  	public function order()
	{
		$this->load->model('m_eklanpay');
		$this->data['nav']['idx']=0;
		$this->data['eklanpay_list'] = $this->m_eklanpay->get_all_items(array());
		
		$this->data['main_html'] = $this->load->view('eklanpay/order', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}
	
	public function billing()
	{
		$this->load->model('m_eklanpay');
		$this->data['nav']['idx']=1;
				
		$id = $this->input->get('eklanpay_id');
		if($id !=""){
			$this->data['promotion_row'] = $this->m_eklanpay->get_one_items($id);
		}
		
		$this->data['msg']       = $this->session->flashdata('message');
		$this->data['main_html'] = $this->load->view('eklanpay/billing', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}
	
	public function detail($id)
	{
		$this->load->model('m_eklanpay_order');
		$this->load->model('m_eklanpay');
		$this->data['nav']['idx']=2;
		//$id = $this->input->get('order_id');
		if(!empty($id)){
			$order = $this->m_eklanpay_order->get_one_items($id);
			$payid =$order->eklanpay_id;
		}
		if(!empty($payid)){
			$this->data['pay_row'] = $this->m_eklanpay->get_one_items($payid);
		}
		$this->data['order_row']=$order;
		$this->data['main_html'] = $this->load->view('eklanpay/detail', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}
	
	public function tarik(){
		if ($_POST) {
				$errors = 0;
			    $all_post = $this->input->post();
			    $this->data['jml_ditarik']=$all_post['jml_ditarik'];
			   	//$this->load->model('m_cart_order_status');
		 		$this->data['errors'] = $this->validate_tarik_status($all_post);
		 		//die(json_encode($this->data['errors']));
				if(!empty($this->data['errors']['jml_ditarik']))$this->session->set_flashdata('msg_jml_ditarik', $this->data['errors']['jml_ditarik']);
				if(!empty($this->data['errors']['pin_trx']))$this->session->set_flashdata('msg_pin_trx', $this->data['errors']['pin_trx']);
				if(empty($this->data['errors']) && $errors <1){ 
					$this->doWd($all_post['jml_ditarik']);
				}
		}
		$this->data['form_url']=base_url('eklanpay/tarik');
		$this->data['main_html'] = $this->load->view('eklanpay/tarik', $this->data,true);
		$this->load->view('layout/main', $this->data); 
	}
	
	public function confirm($id=''){
		$this->load->model('m_user');
		$this->load->model('m_eklanpay_order');
		$this->load->model('m_billing_account');
		$this->load->library('egc');
		if(empty($id))$id =$this->input->post('order_id');
			
		if($id ==""){
			$text_message = "<span class='text-muted'>
							<i class='fa fa-fw fa-exclamation-circle'></i>
							<span class='cart-title'>Product belum dipilih!</span>
						</span>";
			$this->session->set_flashdata('message', $text_message);
		//	redirect(base_url('eklanpay'));
		//	exit; 
		}else
			$transaction_list = $this->m_eklanpay_order->get_all_items(array('id'=>$id,'user_id'=>$this->data['member']['id'],'payment_status'=>'1'));
		
		if(!empty($transaction_list)){
			foreach($transaction_list as $listdata){
				$this->data['id_order']     = $listdata['id'];
				$this->data['invoice_code'] = $listdata['invoice_code'];
				$this->data['key_transfer'] = $listdata['key_transfer'];
				$this->data['total'] = $listdata['nominal'];
			}
		}else{
			$this->data['id_order'] = $id;
			$this->data['invoice_code'] = "";
			$this->data['key_transfer'] = "";
			$this->data['total'] = "";
		
		} 
		
		if ($_POST) {
				$errors = 0;
			    $all_post = $this->input->post();
			   	$this->load->model('m_cart_order_status');
			   	$all_post['norek_asal']=trim(str_replace('-','',$all_post['norek_asal']));
				$all_post['nominal_transfer']=trim(str_replace(',','',$all_post['nominal_transfer']));
		
		 		$this->data['errors'] = $this->m_cart_order_status->validate_cart_order_status($all_post);
				
		 		$nominal_transfer  = $all_post['nominal_transfer'];
				$nama_nasabah= $this->input->post('nama_nasabah');
				$norek_asal  = $all_post['norek_asal'];
				$eklanpay_id  = $id;//this->input->post('order_id');
				//$id=$eklanpay_id;
				$bank_id    = $this->input->post('bank_id');
				$bank_tujuan_id    = $this->input->post('bank_tujuan_id');
				$this->load->model('m_billing_account');
		 		$bank_tujuan_norek = $this->m_billing_account->getBillingOneField($bank_tujuan_id,'bill_number');
				$validasi_transfer = '';//substr($nominal_transfer,-3);//echo "x";die();
				$tgl_transfer      = $this->input->post('tgl_transfer');
				
				
				$result=$this->m_eklanpay_order->get_one_items($id);
				//die(str_replace('.','',str_replace('Rp ','',$nominal_transfer)).$result->nominal.':'.$nominal_transfer);
		 		
				if (!empty($nominal_transfer)&&$nominal_transfer<$result->nominal) {
				  $this->data['errors']['nominal_transfer'] = 'Nilai tagihan nominal Transfer Rp. '.$this->egc->accounting_format($result->nominal+$result->key_transfer).'.';
				}/**/
		 		//die(json_encode($this->data['errors']));
		 		if(!empty($this->data['errors']['nama_nasabah']))$this->session->set_flashdata('msg_nama_nasabah', $this->data['errors']['nama_nasabah']);
				if(!empty($this->data['errors']['norek_asal']))$this->session->set_flashdata('msg_norek_asal', $this->data['errors']['norek_asal']);
				if(!empty($this->data['errors']['bank_id']))$this->session->set_flashdata('msg_bank_id', $this->data['errors']['bank_id']);
				if(!empty($this->data['errors']['bank_tujuan_id']))$this->session->set_flashdata('msg_bank_tujuan_id', $this->data['errors']['bank_tujuan_id']);
				if(!empty($this->data['errors']['nominal_transfer']))$this->session->set_flashdata('msg_nominal_transfer', $this->data['errors']['nominal_transfer']);
				if(!empty($this->data['errors']['tgl_transfer']))$this->session->set_flashdata('msg_tgl_transfer', $this->data['errors']['tgl_transfer']);
		
				$this->data['order_id']=$eklanpay_id;
				$this->data['norek_asal']=$norek_asal;
				$this->data['nama_nasabah']=$nama_nasabah;
				$this->data['bank_id']=$bank_id;
				$this->data['bank_tujuan_id']=$bank_tujuan_id;
				$this->data['norek_tujuan']=$bank_tujuan_norek;
				$this->data['nominal_transfer']=$nominal_transfer;
				$this->data['tgl_transfer']=$tgl_transfer;
				
				if(empty($this->data['errors']) && $errors <1){ 
					$this->db->trans_begin(); 
					
					
					
						$order_status = array(
							'eklanpay_order_id'=>$eklanpay_id,
							'user_id'=>$this->data['member']['id'],
							'norek_asal'=>$norek_asal,
							'nama_nasabah'=>$nama_nasabah,
							'bank_id'=>$bank_id,
							'bank_tujuan_id'=>$bank_tujuan_id,
							'norek_tujuan'=>$bank_tujuan_norek,
							'nominal_transfer'=>$nominal_transfer,
							'key_transfer'=>$validasi_transfer,
							'tgl_transfer'=>$tgl_transfer,
							'confirm_date'=>date('Y-m-d'),
							'confirm_time'=>date('H:i:s'),
							'datetime_created'=>date('YmdHis'),
							'status'=>1
						);
						$this->load->model('m_eklanpay_confirm');
		 				$res = $this->m_eklanpay_confirm->save_order_status($order_status);
						
						$cart_order = array(
							'status'=>1,
							'payment_status'=>2
						);
						$res =!$res?$res: $this->m_eklanpay_order->do_update($eklanpay_id,$cart_order);
						$rec_id=$this->doTopup($order_status);
						if(!empty($rec_id))
							$this->m_eklanpay_order->do_update($eklanpay_id,array('pem_code'=>$rec_id));
							
						if(!$res || $this->db->trans_status() === false) {
							$this->db->trans_rollback();
							$this->session->set_flashdata('message', 'Status gagal dikonfirmasi.');
							
						}else{
							$this->db->trans_commit();
							$this->data['id_order'] = "";
							//$this->data['invoice_code'] = "";
							$this->data['key_transfer'] = "";
							$this->data['total'] = "";
							$this->_renew_session_data();
							$this->session->set_flashdata('message', '<div class=\"alert alert-success\">Status berhasil dikonfirmasi.</div>');
						}
				}
		}
		else {
			$this->data['nominal_transfer']=$this->data['total']+$this->data['key_transfer'];
		}
		
		
		//$payid =$order->eklanpay_id;
		
		//$transaction_list = $this->m_eklanpay_order->_query_review_order(array('id'=>$id,'user_id'=>$this->data['member']['id'],'payment_method'=>'transfer','payment_status'=>'1'));
		
		$this->data['form_url']=base_url('eklanpay/confirm');
		$this->data['msg'] = $this->session->flashdata('message');
		$this->data['msg_nama_nasabah'] = $this->session->flashdata('msg_nama_nasabah');
		$this->data['msg_norek_asal'] = $this->session->flashdata('msg_norek_asal');
		$this->data['msg_bank_id'] = $this->session->flashdata('msg_bank_id');
		$this->data['msg_bank_tujuan_id'] = $this->session->flashdata('msg_bank_tujuan_id');
		$this->data['msg_nominal_transfer'] = $this->session->flashdata('msg_nominal_transfer');
		$this->data['msg_nominal_transfer_tdk_sama'] = $this->session->flashdata('msg_nominal_transfer_tdk_sama');
		$this->data['msg_tiga_digit_transfer'] = $this->session->flashdata('msg_tiga_digit_transfer');
		$this->data['msg_tgl_transfer'] = $this->session->flashdata('msg_tgl_transfer'); 
		$this->data['main_html'] = $this->load->view('user-ubah-status', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}
	
	public function index(){
		$this->load->model('m_user');
		$this->load->model('m_eklanpay_order');
		//$this->load->model('m_product_image');
		//$this->load->model('m_eklanpay_confirm');
		//$this->load->model('m_eklanpay'); 
		
		$this->data['msg']			   = $this->session->flashdata('message');
		$this->data['user_data']       = $this->m_user->get_item_by_id($this->data['member']['id']);
		//$this->data['transaction_list']= $this->m_order->_query_review_order(array('user_id'=>$this->data['member']['id'],'payment_method'=>'transfer','payment_status'=>'1'));
		$this->data['cofirm_eklanpay']= $this->m_eklanpay_order->get_all_items(array('user_id'=>$this->data['member']['id'],'payment_status'=>1,'invoice_date'=>'2 HOUR'));
		$this->data['main_html']       = $this->load->view('eklanpay-list', $this->data, true);
		$this->load->view('layout/main', $this->data); 
	}
	
	
	public function verifyPin(){
		$this->redirect_not_login(true);
		$metode_bayar = $this->input->post('metode_bayar');
		$id = $this->input->post('promotion_id');
		
	  	header('Content-type: text/plain'); 
	  	// set json non IE
	  	header('Content-type: application/json'); 
		if(!preg_match('/^transfer/',$metode_bayar))
		{
			die(json_encode(array(
			"head"=>"Informasi Kesalahan",
			"form"=>"<h4>Silahkan pilih metode bayar</h4>",
			"status"=>"1"
			)));
		}
		else{//if(preg_match('/^transfer/',$metode_bayar)){
			$ret=$this->_do_payment($metode_bayar,$id,'');
			if(!empty($ret))die(json_encode($ret));	
			else die(json_encode(array(
				"head"=>'Gagal order pembelian...!',
				"form"=>"",
				"status"=>"4"
			)));
		}
	}
	
	protected function _do_payment($metode_bayar,$promo_id,$id='')
	{
		
		$this->load->model('m_eklanpay');
		$member=$this->session->userdata('member');
	//	$nextact 		 = 3;//$this->input->post('nextact');
		if(!empty($promo_id)){
			$total_bayar = $this->m_eklanpay->get_one_items($promo_id)->harga_paket;
		}
					
		$this->db->trans_begin();
			if(empty($id)){
				//update wallet
				$key_transfer='';
				if($metode_bayar=="transfer"){
					$key_transfer = $this->acak_key_transfer();
					$payment_status=1;
				}
				
			$data_promosi = array(
				//'id'=>'DEFAULT',
				'eklanpay_id'=>$promo_id,
				'user_id'=>$member['id'],
			//	'payment_metod'=>$metode_bayar,
				'payment_status'=>$payment_status,
				'nominal'=>$total_bayar,
				'key_transfer'=>$key_transfer,
				'invoice_code'=>date('ymdHms'),
				'invoice_date'=>date('Y-m-d H:m:s'),
				'status'=>'1'
			);
		
			$this->load->model('m_eklanpay_order');
			$inser = $this->m_eklanpay_order->do_save($data_promosi);
			$sql=$this->db->last_query();
			$insertid = $this->db->insert_id();
			
				
				if($this->db->trans_status() === false){
					$this->db->trans_rollback();
					return array(
						"head"=>'Penyimpanan Data Transaksi Gagal!',
						"form"=>"",
						"status"=>"4"
					);
					exit;
				}else{
					$this->db->trans_commit();
					$this->_renew_session_data();
					/*
						if(!empty($member['email'])){
				        	$this->load->library('egc');
				        	$data_order['id']=$order_id;
				        	$this->egc->sendmail_promo($member,$data_order);
				        }//redirect(base_url('cart/payment/' . $order_id));
				        */
				        return array(
							"head"=>"Informasi eklanku",
							"form"=>"Invoice paket eklanpay telah dikirim email Anda, No Invoice: ".$data_promosi['invoice_code']." berhasil. <a href = '".base_url('eklanpay/detail/' . $insertid)."'>Detail</a>",
							"status"=>"0",
				        	"id"=>$insertid
						);			        
				}
			}else{
				redirect(base_url('eklanpay'));
				exit;
			}	
    }
    
	private function acak_key_transfer(){
		$this->load->library('egc');
		for($i=0; $i <=299; $i++){$rand = $this->egc->genRandomNumber(3)."<br>";
			if($rand<=299)break;
		}
		return $rand;
   }
	
	
	private function doWd($jml_ditarik)
	{
		$pin_trx        = $this->input->post("pin_trx");
       
		$this->load->model('m_product');
		$this->load->library('egc');
		$this->load->model('m_deposit');
        $jmlMoney 	    = $this->_recheckSaldo();
		if ($jml_ditarik == ""){
			$this->session->set_flashdata('message_error', 'Masukan jumlah penarikan!');
			redirect('eklanpay/tarik');
		
		}
		if($jml_ditarik < 50000){
			$this->session->set_flashdata('message_error', 'Minimal jumlah penarikan Rp. 50.000!');
			redirect('eklanpay/tarik');
		
		}
		//die("$jml_ditarik > $jmlMoney");
		if($jml_ditarik > $jmlMoney){
			$this->session->set_flashdata('message_error', 'Jumlah penarikan melebihi nominal tersedia!');
			redirect('eklanpay/tarik');
		}
		if($this->egc->isMultiplyHundred($jml_ditarik) == false){
			$this->session->set_flashdata('message_error', 'Jumlah penarikan kelipatan Rp. 1!');
			redirect('eklanpay/tarik');
		}
		//$this->load->model('belanja_model');
		if($this->m_deposit->isValidPIN($pin_trx) == false)
		{
			$this->session->set_flashdata('message_error', 'PIN yang Anda masukkan salah!');
			redirect('eklanpay/tarik');
			
		}
		
		if($this->m_deposit->getDataRekening()!='')
			{
			
				if($this->m_deposit->cekPenarikan($jml_ditarik))
				{
					
                    $memberName = $this->m_deposit->getName();
                    $mnominal = $this->egc->accounting_format($jml_ditarik);
                    $hp2 = "082146165666";
                    $this->m_deposit->doSMSRWD('WDRSMS', $hp2, $mnominal, $memberName,$this->session->userdata("mbr_user"));
					$this->m_deposit->doInsertRecord($jml_ditarik);
					$this->session->set_flashdata('message_info', 'Request penarikan saldo eklanku berhasil dan akan segera kami proses max 1x24 jam. Terimakasih');
					redirect('eklanpay/tarik');
				}else{
					$this->session->set_flashdata('message_error', 'Penarikan dengan nominal yang sama sudah pernah terjadi dan masih dalam proses.!');
					redirect('eklanpay/tarik');
				}
			}else{
				$this->session->set_flashdata('message_error', 'Silahkan update nomer rekening Anda di menu profile');
			    redirect('eklanpay/tarik');
		}	
	}
	
	private function doTopup($data)
	{	
		$this->load->library('egc');
		$this->load->model('m_billing_account');
		$res=$this->m_billing_account->get_all_items(array('id'=>$data['bank_tujuan_id']));
		foreach($res as $row){
			$anbank=$row['bill_account'];
			$bnk=$row['bill_name'];
			$norec=$row['bill_number'];
			$interval_bayar='2 jam';
		}
		$message = "Topup saldo eklanku Rp. ".
			$this->egc->accounting_format($data['nominal_transfer']).
			" dari rek ".$data['norek_asal'].
			": ".$data['bank_id'].
			" an ".$data['nama_nasabah'].
			" ke rek ".$data['norek_tujuan'].
			": $bnk - ".$anbank;
		$this->load->model('m_deposit');
		return $this->m_deposit->doTopup(array(
			'nominal'=>$data['nominal_transfer'],
			'pesan'=>$message,
			'anbank'=>$anbank,
			'bnk'=>$bnk,
			'norec'=>$norec,  
			'interval_bayar'=>$interval_bayar
		));
	}
	
	public function validate_tarik_status($params)
	{
		extract($params);
		$errors = array();
		
		if (empty($jml_ditarik)) {
		  $errors['jml_ditarik'] = 'Jumlah nominal penarikan harus diisi.';
		} 
		
		if (empty($pin_trx)) {
		  $errors['pin_trx'] = 'Input PIN Anda harus diisi.';
		} 
		
		return $errors;
	}
	
	
}
?>