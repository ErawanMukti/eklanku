<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries\REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Advslider extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['transaksi_post']['limit'] = 100;
    }

    public function advimage_get()
    {
        $image = array('banner-1.jpg', 'banner-2.jpg', 'banner-3.jpg',
                       'banner-4.jpg', 'banner-5.jpg', 'banner-6.jpg',
                       'banner-7.jpg', 'banner-8.jpg', 'banner-9.jpg',
                       'banner-10.jpg', 'banner-11.jpg','banner-12.jpg');

        $message = array(
            'status' => 'OK',
            'error'  => '-',
            'data'   => $image);
        $this->response($message, REST_Controller::HTTP_OK);
    }
}
