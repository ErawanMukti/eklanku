<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries\REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Topup extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['transaksi_post']['limit'] = 100;
    }

    public function order_get()
    {
        $this->load->model('m_eklanpay');
        $data = $this->m_eklanpay->get_all_items(array());

        if (empty($data)) {
            $this->response(array(
                'status' => FALSE,
                'error'  => 'Paket Topup Tidak Tersedia'
            ), REST_Controller::HTTP_OK);
        } else {
            $message = array(
                'status' => 'OK',
                'error'  => '-',
                'data'   => $data
            );
            $this->response($message, REST_Controller::HTTP_OK);
        }
    }

    public function billing_get()
    {
        $this->load->model('m_billing_account');
        $data = $this->m_billing_account->get_all_items();

        if (empty($data)) {
            $this->response(array(
                'status' => FALSE,
                'error'  => 'Daftar Bank Tidak Tersedia'
            ), REST_Controller::HTTP_OK);
        } else {
            $message = array(
                'status' => 'OK',
                'error'  => '-',
                'data'   => $data
            );
            $this->response($message, REST_Controller::HTTP_OK);
        }
    }

    public function pay_post()
    {
        $this->load->model('m_eklanpay');
        $metode_bayar = $this->input->post('metode_bayar');
        $promo_id     = $this->input->post('id_paket');
        $member_id    = $this->input->post('id_member');
        $total_bayar  = $this->m_eklanpay->get_one_items($promo_id)->harga_paket;

        if($metode_bayar == "transfer"){
            $key_transfer   = $this->acak_key_transfer();
            $payment_status = 1;
        }

        $data_promosi = array(
            'eklanpay_id'    => $promo_id,
            'user_id'        => $member_id,
            'payment_status' => $payment_status,
            'nominal'        => $total_bayar,
            'key_transfer'   => $key_transfer,
            'invoice_code'   => date('ymdHms'),
            'invoice_date'   => date('Y-m-d H:m:s'),
            'status'         => '1'
        );

        $this->load->model('m_eklanpay_order');
        $inser    = $this->m_eklanpay_order->do_save($data_promosi);
        $sql      = $this->db->last_query();
        $insertid = $this->db->insert_id();

        if ($insertid == "") {
            $this->response(array(
                'status' => FALSE,
                'error'  => 'Terjadi Kesalahan'
            ), REST_Controller::HTTP_OK);
        } else {
            $message = array(
                'status'  => 'OK',
                'error'   => '-',
                'message' => $insertid
            );
            $this->response($message, REST_Controller::HTTP_OK);
        }
    }

    public function detail_get()
    {
        $id = $this->get('id');

        $this->load->model('m_eklanpay_order');
        $data = $this->m_eklanpay_order->get_one_item_array($id);

        if (empty($data)) {
            $this->response(array(
                'status' => FALSE,
                'error'  => 'Data Order Tidak Ditemukan'
            ), REST_Controller::HTTP_OK);
        } else {
            $message = array(
                'status' => 'OK',
                'error'  => '-',
                'data'   => $data
            );
            $this->response($message, REST_Controller::HTTP_OK);
        }
    }

    private function acak_key_transfer(){
        $this->load->library('egc');
        for($i=0; $i <=299; $i++){$rand = $this->egc->genRandomNumber(3)."<br>";
            if($rand<=299)break;
        }
        return $rand;
   }

}
