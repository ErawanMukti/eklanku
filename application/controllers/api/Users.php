<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries\REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Users extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['login_post']['limit'] = 100;
        $this->methods['register_post']['limit'] = 100;        
        $this->methods['account_get']['limit'] = 100;        
    }

    public function login_post()
    {
        $this->load->model('m_user');
        $email    = $this->input->post('email');
        $password = $this->input->post('password');
        $user     = $this->m_user->login($email, $password);

        if (empty($user)) {
            $this->response(array(
                'status' => FALSE,
                'error'  => 'Belum ada data user'
            ), REST_Controller::HTTP_OK);
        } else {
            $message = array(
                'status' => 'OK',
                'error'  => '-',
                'user'   => array($user)
            );
            $this->response($message, REST_Controller::HTTP_OK);
        }
    }

    public function register_post()
    {
        $this->load->model('m_user');

        $data = array(
             'name'        => $this->input->post('name'),
             'phone'       => $this->input->post('phone'),
             'email'       => $this->input->post('email'),
             'password'    => $this->input->post('password'),
             're_password' => $this->input->post('re_password'),
             'agreement'   => '1',
             'referral_id' => $this->input->post('referal'),
             'member_id'   => $this->input->post('email')
        );
        $error = $this->m_user->add($data);

        if (empty($error)) {
            $message  = array(
                'status'  => 'OK',
                'error'   => '',
                'message' => 'Registrasi berhasil'
            );
            $this->response($message, REST_Controller::HTTP_CREATED);            
        } else {
            $message = "";
            foreach ($error as $key => $value) {
                if ($message != "") { $message .= ", "; }
                $message .= $value;
            }
            $this->response(array(
                'status'  => FALSE,
                'error'   => 'Terjadi kesalahan. Gagal mendaftarkan user',
                'message' => $message
            ), REST_Controller::HTTP_OK);
        }
    }

    public function account_get()
    {
        $this->load->model('m_user');
        $id   = $this->get('id');
        $user = $this->m_user->get_item_by_id($id);

        if (empty($user)) {
            $this->response([
                'status' => FALSE,
                'error'  => 'Terjadi kesalahan. User tidak ditemukan'
            ], REST_Controller::HTTP_OK);
        } else {
            $message  = [
                'status' => 'OK',
                'error'  => '-',
                'user'   => array($user)
            ];
            $this->response($message, REST_Controller::HTTP_OK);
        }
    }

}
