<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries\REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Pembayaran extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['loaddata_post']['limit'] = 100;
        $this->methods['transbeli_post']['limit'] = 100;        
        $this->methods['transconfirm_post']['limit'] = 100;
    }

    public function loaddata_post()
    {
        $loadType = $this->input->post('load_type'); //token_nominal
        $loadId   = $this->input->post('load_id'); //PLN

        if ( $loadType == "" || $loadId == "" ) {
            $this->response(array(
                'status' => FALSE,
                'error'  => 'Terjadi kesalahan. Parameter tidak lengkap'
            ), REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        $this->load->model('m_product');
        $this->load->library('egc');

        if($loadType == 'token_nominal') {
            $result = $this->m_product->getDataToken($loadId)->result_array();
        } else if($loadType == 'pulsa_nominal') {
            $result = $this->m_product->getDataPulsa($loadId)->result();
        } else if($loadType == 'game_nominal') {
            $result = $this->m_product->getDataGame($loadId)->result();
        } else if($loadType == 'paket_nominal') {
            $result = $this->m_product->getDataPaket($loadId)->result();
        } else if(preg_match('/tvbyr/',$loadType)) {
            $result = $this->m_product->getDataTV($loadId)->result();
        }

        $message = array(
            'status' => 'OK',
            'error'  => '-',
            'result' => $result
        );
        $this->response($message, REST_Controller::HTTP_OK);
    }

    public function transbeli_post() {
        $id     = $this->input->post('id');
        $jenis  = $this->input->post('jenis'); //PLN / PLNPOST
        $produk = $this->input->post('nominal'); //PLN20
        $idPel  = $this->input->post('id_pel'); //Idpel
        $nmtype = $this->input->post('nm');

        $this->load->model('m_product');
        $this->load->model('m_user');
        $this->load->library('egc');

        $user = $this->m_user->get_item_by_id($id);

        /* BEGIN - BAYAR TAGIHAN */
        if(preg_match('/POST|BPJS|TELKOM|PDAM|TV|VOC/', $jenis)) {
            $_arrdata = array(
                'PLNPOST'=>array(
                    'data'=>array(
                        'text'=>'tagihan listrik',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'TSELPOST'=>array(
                    'data'=>array(
                        'text'=>'tagihan hallo telkomsel',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'ISATPOST'=>array(
                    'data'=>array(
                        'text'=>'tagihan matrix indosat',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'XLPOST'=>array(
                    'data'=>array(
                        'text'=>'tagihan XL pasca',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'THREEPOST'=>array(
                    'data'=>array(
                        'text'=>'tagihan 3 (THREE) pasca',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'SMARTPOST'=>array(
                    'data'=>array(
                        'text'=>'tagihan SMARTFREN pasca',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'FRENPOST'=>array(
                    'data'=>array(
                        'text'=>'tagihan FREN/MOBI/HEPI pasca',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'ESIAPOST'=>array(
                    'data'=>array(
                        'text'=>'tagihan ESIA pasca',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'BPJSKES'=>array(
                    'data'=>array(
                        'text'=>'tagihan BPJS Kesehatan',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'TELKOM'=>array(
                    'data'=>array(
                        'text'=>'tagihan Telkom',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_AETRA' => array ('data'=>array('text'=>'PDAM AETRA JAKARTA',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_BONDO' => array ('data'=>array('text'=>'PDAM BONDOWOSO',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_BALANGAN' => array ('data'=>array('text'=>'PDAM KAB. BALANGAN',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_BNKLN' => array ('data'=>array('text'=>'PDAM KAB BANGKALAN',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_WABATANG' => array ('data'=>array('text'=>'PDAM KAB. BATANG',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_WABJN' => array ('data'=>array('text'=>'PDAM KAB. BOJONEGORO',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_GRBG' => array ('data'=>array('text'=>'PDAM KAB GROBOGAN',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_JMBR' => array ('data'=>array('text'=>'PDAM KAB JEMBER',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_WAKUBURAYA' => array ('data'=>array('text'=>'PDAM KAB. KUBU RAYA',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_MLG' => array ('data'=>array('text'=>'PDAM KAB MALANG',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_MJKRT' => array ('data'=>array('text'=>'PDAM KAB MOJOKERTO',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_WAPASU' => array ('data'=>array('text'=>'PDAM KAB. PASURUAN',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_WASAMPANG' => array ('data'=>array('text'=>'PDAM KAB. SAMPANG',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_SIDO' => array ('data'=>array('text'=>'PDAM KAB SIDOARJO',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_STUBN' => array ('data'=>array('text'=>'PDAM KAB SITUBONDO',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_WATAPIN' => array ('data'=>array('text'=>'PDAM KAB. TAPIN',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_LPG' => array ('data'=>array('text'=>'PDAM KOTA BANDAR LAMPUNG',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_BDG' => array ('data'=>array('text'=>'PDAM KOTA BANDUNG',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_WAIBANJAR' => array ('data'=>array('text'=>'PDAM KOTA BANJARBARU',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_KOBGR' => array ('data'=>array('text'=>'PDAM KOTA BOGOR',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_WAJAMBI' => array ('data'=>array('text'=>'PDAM KOTA JAMBI',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_WAMANADO' => array ('data'=>array('text'=>'PDAM KOTA MANADO',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_WAGIRIMM' => array ('data'=>array('text'=>'PDAM KOTA MATARAM',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_WAPLMBNG' => array ('data'=>array('text'=>'PDAM KOTA PALEMBANG',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_WAKOPASU' => array ('data'=>array('text'=>'PDAM KOTA PASURUAN',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_PURRJ' => array ('data'=>array('text'=>'PDAM KOTA PURWOREJO',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_SURKT' => array ('data'=>array('text'=>'PDAM KOTA SURAKARTA',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_WAGROGOT' => array ('data'=>array('text'=>'PDAM KOTA TANAH GROGOT',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_PLYJA' => array ('data'=>array('text'=>'PDAM PALYJA JAKARTA',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_PON' => array ('data'=>array('text'=>'PDAM PONTIANAK',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'PDAM_SBY' => array ('data'=>array('text'=>'PDAM SURABAYA',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'TVINDVS' => array ('data'=>array('text'=>'Indovision',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'TVTOP' => array ('data'=>array('text'=>'Top TV',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'TVOKE' => array ('data'=>array('text'=>'Okevision',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'TVTLKMV' => array ('data'=>array('text'=>'Telkomvision',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'TVTTNSVIONS' => array ('data'=>array('text'=>'Transvision',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'TVYES' => array ('data'=>array('text'=>'Yes TV',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'TVNEX' => array ('data'=>array('text'=>'Nex Media',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'TVTOPAS' => array ('data'=>array('text'=>'Topas TV',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'TVORANGE' => array ('data'=>array('text'=>'Orange TV',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'TVBIG' => array ('data'=>array('text'=>'BIG TV',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'TVINNOV' => array ('data'=>array('text'=>'INNOVATE TV',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCSKYDEL1' => array ('data'=>array('text'=>'SKYNINDO TV DELUXE 1 BLN (80.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCSKYDEL12' => array ('data'=>array('text'=>'SKYNINDO TV DELUXE 12 BLN (960.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCSKYMAN3' => array ('data'=>array('text'=>'SKYNINDO TV MANDARIN 3 BLN (420.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCSKYDEL3' => array ('data'=>array('text'=>'SKYNINDO TV DELUXE 3 BLN (240.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCSKYDEL6' => array ('data'=>array('text'=>'SKYNINDO TV DELUXE 6 BLN (480.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCSKYFAM3' => array ('data'=>array('text'=>'SKYNINDO TV FAMILY 3 BLN (120.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCSKYMAN12' => array ('data'=>array('text'=>'SKYNINDO TV MANDARIN 12 BLN (1.680.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCSKYMAN6' => array ('data'=>array('text'=>'SKYNINDO TV MANDARIN 6 BLN (840.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCKV100' => array ('data'=>array('text'=>'K VISION (100.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCKV1000' => array ('data'=>array('text'=>'K VISION (1.000.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCKV125' => array ('data'=>array('text'=>'K VISION (125.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCKV150' => array ('data'=>array('text'=>'K VISION (150.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCKV175' => array ('data'=>array('text'=>'K VISION (175.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCKV200' => array ('data'=>array('text'=>'K VISION (200.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCSKYMAN1' => array ('data'=>array('text'=>'SKYNINDO TV MANDARIN 1 BLN (140.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCKV250' => array ('data'=>array('text'=>'K VISION (250.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCKV300' => array ('data'=>array('text'=>'K VISION (300.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCKV50' => array ('data'=>array('text'=>'K VISION (50.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCKV500' => array ('data'=>array('text'=>'K VISION (500.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCKV75' => array ('data'=>array('text'=>'K VISION (75.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCKV750' => array ('data'=>array('text'=>'K VISION (750.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCORG50' => array ('data'=>array('text'=>'Orange TV (50.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCORG80' => array ('data'=>array('text'=>'Orange TV (80.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCORG100' => array ('data'=>array('text'=>'Orange TV (100.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCORG300' => array ('data'=>array('text'=>'Orange TV (300.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCSKYFAM6' => array ('data'=>array('text'=>'SKYNINDO TV FAMILY 6 BLN (240.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCSKYFAM12' => array ('data'=>array('text'=>'SKYNINDO TV FAMILY 12 BLN (480.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCSKYFAM1' => array ('data'=>array('text'=>'SKYNINDO TV FAMILY 1 BLN (40.000)',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCKMVPRE' => array ('data'=>array('text'=>'Voucher Telkomvision Pra Combo',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCTELVISCOS' => array ('data'=>array('text'=>'Voucher Telkomvision Pra Cosmo',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCTELVISFIL' => array ('data'=>array('text'=>'Voucher Telkomvision Pra Film',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCTELVISHEM' => array ('data'=>array('text'=>'Voucher Telkomvision Pra Hemat',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCTELVISOLA' => array ('data'=>array('text'=>'Voucher Telkomvision Pra Olahraga',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCTELVISPEL' => array ('data'=>array('text'=>'Voucher Telkomvision Pra Pelangi',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
                'VOCTTELVISPEN' => array ('data'=>array('text'=>'Voucher Telkomvision Pra Pendidi',
                        'waktu'=>'24 hours',
                        'waktuid'=>'24 jam')
                ),
            );

            $_arr = $_arrdata[$jenis];
            if( $this->m_product->cektransaksiToken($jenis, $idPel, $_arr['data']['waktu']) == false) {
                $message = array(
                    'status' => FALSE,
                    'error'  => "Transaksi Dengan tujuan $idPel sudah pernah terjadi, transaksi $_arr[data][text] dengan tujuan yang sama hanya bisa di proses setiap $_arr[data][waktuid]"
                );           
                $this->response($message, REST_Controller::HTTP_OK);
                return;
            }

            $rs1 = explode("|", $this->m_product->sendInquiry($jenis, $idPel, $user['phone']));

            if($rs1[0] == 0 || $rs1[0] == '0') {
                $tampil = str_replace(',','</div><div>',$rs1[2]);
                $message = array(
                    'status' => 'OK',
                    'error'  => '-',
                    'result' => array(0 => array(
                        'transaksi' => $rs1[2],
                        'harga'     => 0,
                        'jenis'     => $jenis,
                        'id_pel'    => $idPel,
                        'pin'       => base64_encode($rs1[1].'|'.$rs1[3]),
                        'cmd_save'  => 'TAG'
                    ))
                );
            } else {
                $message = array(
                    'status' => FALSE,
                    'error'  => 'Anda Tidak Mempunyai Hak Akses'
                );           
            }
            $this->response($message, REST_Controller::HTTP_OK);
            return;
        }
        /* ENDOF - BAYAR TAGIHAN */

        /* BEGIN - PEMBELIAN */
        $data      = $this->m_product->doReadList($produk);
        $hargajual = $this->m_product->getHarga($produk);

        if( $data == false || $hargajual <= 0 ) {
            $message = array(
                'status' => FALSE,
                'error'  => 'Produk yang Anda request belum terdaftar disistem kami'
            );
            $this->response($message, REST_Controller::HTTP_OK);
            return;
        }

        $saldoSisa = $this->m_product->getSaldo();
        if($saldoSisa < $hargajual) {
            $message = array(
                'status' => FALSE,
                'error'  => 'Saldo Eklanku Anda tidak mencukupi untuk pembelian silahkan melakukan topup Saldo eklanku terlebih dahulu.'
            );           
            $this->response($message, REST_Controller::HTTP_OK);
            return;
        }

        if($nmtype == 'token') { 
            $kd_save = 'TO';
            $_data   = array('text'=>'token listrik','waktu'=>'120 second','waktuid'=>'2 menit');
            $ischeck = $this->m_product->cektransaksiToken($produk, $idPel, $_data['waktu']);
        }
        else if($nmtype=='pulsa') {
            $kd_save = 'PL';
            $_data   = array('text'=>'pulsa','waktu'=>'24 hours','waktuid'=>'24 jam');
            $ischeck = $this->m_product->cektransaksiToken($produk, $idPel, $_data['waktu'], $user['eklanku_id']);
        }
        else if($nmtype=='game') {
            $kd_save = 'PO';
            $_data   = array('text'=>'voucher game','waktu'=>'120 second','waktuid'=>'2 menit');
            $ischeck = $this->m_product->cektransaksiToken($produk, $idPel, $_data['waktu'], $user['eklanku_id']);
        }
        else if($nmtype=='paket') {
            $kd_save = 'PK';
            $_data   = array('text'=>'Paket Data','waktu'=>'24 hours','waktuid'=>'24 jam');
            $ischeck = $this->m_product->cektransaksiToken($produk, $idPel, $_data['waktu'], $user['eklanku_id']);
        }

        if($ischeck == false) {
            $message = array(
                'status' => FALSE,
                'error'  => "Transaksi Dengan tujuan $idPel sudah pernah terjadi, transaksi $_data[text] dengan tujuan dan atau Nominal yang sama hanya bisa di proses setiap $_data[waktuid]"
            );           
            $this->response($message, REST_Controller::HTTP_OK);
            return;
        }

        $message = array(
            'status' => 'OK',
            'error'  => '-',
            'result' => array(0 => array(
                'transaksi' => $_data['text'],
                'harga'     => $hargajual,
                'jenis'     => $produk,
                'id_pel'    => $idPel,
                'pin'       => '',
                'cmd_save'  => $kd_save
            ))
        );
        $this->response($message, REST_Controller::HTTP_OK);
        /* ENDOF - PEMBELIAN */
    }

    public function transconfirm_post() {
        $id       = $this->input->post('id');
        $produk   = $this->input->post('jenis');  
        $idPel    = $this->input->post('id_pel');
        $pin      = $this->input->post('pin');    
        $cmd_save = $this->input->post('cmd_save');   

        $this->load->model('m_product');
        $this->load->model('m_user');
        $this->load->library('egc');

        $user = $this->m_user->get_item_by_id($id);

        if(preg_match('/POST|BPJS|TELKOM|PDAM|TV|VOC/',$produk)){ 
            $rs1 = explode("|",base64_decode($pin));

            $saldo = $this->m_product->getSaldo();
            if($saldo >= $rs1[1]) {
                $idIvoice = 'PM' . $this->egc->genRandomString(13);
                $resp     = $this->m_product->callPayment($rs1[1], $user['eklanku_id'], $idIvoice, $rs1[0], $produk, $idPel);

                $iResult = "";
                if ($resp->num_rows() > 0) {
                    $resp    = $resp->row();
                    $iResult = $resp->f_cal_payment;
                    if($iResult == "Proses payment selesai.") {
                        $message = array(
                            'status' => 'OK',
                            'error'  => "Proses pembayaran tagihan berhasil."
                        );           
                        $this->response($message, REST_Controller::HTTP_OK);
                        return;
                    } else {
                        $message = array(
                            'status' => FALSE,
                            'error'  => $iResult
                        );           
                        $this->response($message, REST_Controller::HTTP_OK);
                        return;
                    }
                } else {
                    $message = array(
                        'status' => FALSE,
                        'error'  => "Server sibuk silahkan mengulang beberapa saat lagi"
                    );           
                    $this->response($message, REST_Controller::HTTP_OK);
                    return;
                }
            } else {
                $message = array(
                    'status' => FALSE,
                    'error'  => "Saldo Eklanku Anda tidak mencukupi untuk pembelian silahkan melakukan topup Saldo eklanku terlebih dahulu."
                );           
                $this->response($message, REST_Controller::HTTP_OK);
                return;
            }
        }
        //===================end PASCA BAYAR =================
        
        $data      = $this->m_product->doReadList($produk);
        $hargajual = $this->m_product->getHarga($produk);
        if( $data== false || $hargajual <= 0) {
            $message = array(
                'status' => FALSE,
                'error'  => "Produk yang Anda request belum terdaftar disistem kami"
            );           
            $this->response($message, REST_Controller::HTTP_OK);
            return;
        }

        $saldoSisa = $this->m_product->getSaldo();
        if($saldoSisa < $hargajual) {
            $message = array(
                'status' => FALSE,
                'error'  => "Saldo Eklanku Anda tidak mencukupi untuk pembelian silahkan melakukan topup Saldo eklanku terlebih dahulu."
            );           
            $this->response($message, REST_Controller::HTTP_OK);
            return;
        }

        if ($cmd_save == 'TO') {
            $_data = array('kode'=>'PLNTOKEN','text'=>'token listrik','waktu'=>'120 second','waktuid'=>'2 menit');
        } elseif ($cmd_save == 'PL') {
            $_data = array('kode'=>'PULSA','text'=>'pulsa','waktu'=>'24 hours','waktuid'=>'24 jam');
        } elseif ($cmd_save == 'PO') {
            $_data = array('kode'=>'GAME','text'=>'voucher game','waktu'=>'120 second','waktuid'=>'2 menit');
        } elseif ($cmd_save == 'PK') {
            $_data = array('kode'=>'KUOTA','text'=>'paket data','waktu'=>'24 hours','waktuid'=>'24 jam');
        }

        $pem_code = 'T' . $this->egc->genRandomString(13).date("His");
        $resp     = $this->m_product->callPulsa($hargajual, $user['eklanku_id'], $pem_code, $_data['kode'], $produk, $idPel);
        
        $iResult = '';
        if ($resp->num_rows() > 0) {
            $resp = $resp->row();
            $iResult = $resp->f_cal_pulsa;
            if($iResult == "Proses payment selesai.") {
                $message = array(
                    'status' => 'OK',
                    'error'  => "Proses pembelian $_data[text] berhasil."
                );           
            } else {
                $message = array(
                    'status' => FALSE,
                    'error'  => $iResul
                );           
            }
        } else {
            $message = array(
                'status' => FALSE,
                'error'  => "Sever sedang sibuk silahkan diulang beberapa saat lagi."
            );
        }
        $this->response($message, REST_Controller::HTTP_OK);
    }
}
