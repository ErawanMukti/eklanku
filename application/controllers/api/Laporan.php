<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries\REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Laporan extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['transaksi_post']['limit'] = 100;
    }

    public function transaksi_post()
    {
        $this->load->model('m_laporan');

        $id_member = $this->input->post('id'); //eklankuid
        $data      = $this->m_laporan->get_department_listpayment_api($id_member, 20, 0);

        if (empty($data)) {
            $this->response(array(
                'status' => FALSE,
                'error'  => 'Anda tidak memiliki riwayat transaksi'
            ), REST_Controller::HTTP_OK);
        } else {
            $message = array(
                'status' => 'OK',
                'error'  => '-',
                'data'   => $data
            );
            $this->response($message, REST_Controller::HTTP_OK);
        }
    }
}
