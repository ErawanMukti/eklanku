<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends Application {

	public function __construct()
	{		
		parent::__construct();
		
    
	//	if ($this->session->userdata("mbr_code") == false)
	//	{
	//		redirect("homepage");
	//	}
	}

	public function index()
	{
		$data['page_title'] = 'Dashboard';	
		$data['page_map'] = 'Dashboard';	
		$data['site_map'] = 'Payment';
		$data['view_title'] = 'Payment ';	
		$data['view_link'] = base_url().'payment';
        $data['menu'] = '8';		
        $this->load->model('belanja_model');
		$this->load->model('category_model');
		$this->load->model('iklan_model');
		$this->load->model('mbrlst_model');
		$data['content_saldo'] = $this->mbrlst_model->getSaldo();
		$data["jmlData"] = $this->iklan_model->record_countSaya();
		$data['content_data_catagory'] = $this->category_model->doReadMember();
		$data['content_data_ref'] = $this->belanja_model->doReadRef();
		$data['content_trx'] = $this->belanja_model->getTotalTrx();
		$data['content_wd'] = $this->belanja_model->getTotalWD();
		$data['content_wdantre'] = $this->belanja_model->getTotalAntreanWD();
		$this->load->model('product_model');
		$data['data_game'] = $this->product_model->getDataGame();
		$data['data_token'] = $this->product_model->getDataToken();
		//die(json_encode(	$data['data_token']));
	/*	$data['data_pulsa'] = $this->product_model->getDataPulsa();*/
		$data['content_view'] = 'belanja/ppob';
		$data['content_header'] = 'template/globalheader';
		$data['content_footer'] = 'template/globalfooter';
		$this->load->view('template/tplmenu', $data);		

	}
	
	public function verifyPin($namatype=null){
		$this->redirect_not_login(true);
		// set text compatible IE7, IE8
	  	$jenis 	    = $this->input->post('jenis');	
		$produk 	    = $this->input->post('nominal');	
	    $idPel   	      = trim(str_replace('-','',$this->input->post('id_pel')));
		header('Content-type: text/plain'); 
	  	// set json non IE
	  	header('Content-type: application/json'); 
		
	  	$this->load->model('m_product');
	    
		if(empty($jenis))
		{
			die(json_encode(array(
			"head"=>"terdapat kesalahan input Jenis Produk",
			"form"=>"<h4>silahkan pilih produk yang sesuai </h4>",
			"status"=>"1"
			)));
			
		}
	
		if(empty($idPel))
		{
			die(json_encode(array(
			"head"=>"terdapat kesalahan input nomor",
			"form"=>"<h4>silahkan nomor yang sesuai</h4>",
			"status"=>"2"
			)));
			
		}elseif(strlen($idPel)<8){
			die(json_encode(array(
			"head"=>"terdapat kesalahan input nomor",
			"form"=>"<h4>Jumlah input nomor minimal 8 digit</h4>",
			"status"=>"2"
			)));
		}
		//if($jenis=='TAG'){$produk='PLNPOST';}
		//=======under constr
		$this->load->library('egc');
			
	    if(preg_match('/POST|BPJS|TELKOM|PDAM|TV|VOC/',$jenis))
		{
		/*	die(json_encode(array(
			"head"=>"$jenis",
			"form"=>"<h4>Sementara belum bisa melakukan transaksi</h4>",
			"status"=>"4"
			)));*/
			$_arrdata=array(
				'PLNPOST'=>array(
					'data'=>array(
						'text'=>'tagihan listrik',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'TSELPOST'=>array(
					'data'=>array(
						'text'=>'tagihan hallo telkomsel',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'ISATPOST'=>array(
					'data'=>array(
						'text'=>'tagihan matrix indosat',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'XLPOST'=>array(
					'data'=>array(
						'text'=>'tagihan XL pasca',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'THREEPOST'=>array(
					'data'=>array(
						'text'=>'tagihan 3 (THREE) pasca',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'SMARTPOST'=>array(
					'data'=>array(
						'text'=>'tagihan SMARTFREN pasca',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'FRENPOST'=>array(
					'data'=>array(
						'text'=>'tagihan FREN/MOBI/HEPI pasca',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'ESIAPOST'=>array(
					'data'=>array(
						'text'=>'tagihan ESIA pasca',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'BPJSKES'=>array(
					'data'=>array(
						'text'=>'tagihan BPJS Kesehatan',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'TELKOM'=>array(
					'data'=>array(
						'text'=>'tagihan Telkom',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_AETRA' => array ('data'=>array('text'=>'PDAM AETRA JAKARTA',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_BONDO' => array ('data'=>array('text'=>'PDAM BONDOWOSO',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_BALANGAN' => array ('data'=>array('text'=>'PDAM KAB. BALANGAN',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_BNKLN' => array ('data'=>array('text'=>'PDAM KAB BANGKALAN',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_WABATANG' => array ('data'=>array('text'=>'PDAM KAB. BATANG',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_WABJN' => array ('data'=>array('text'=>'PDAM KAB. BOJONEGORO',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_GRBG' => array ('data'=>array('text'=>'PDAM KAB GROBOGAN',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_JMBR' => array ('data'=>array('text'=>'PDAM KAB JEMBER',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_WAKUBURAYA' => array ('data'=>array('text'=>'PDAM KAB. KUBU RAYA',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_MLG' => array ('data'=>array('text'=>'PDAM KAB MALANG',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_MJKRT' => array ('data'=>array('text'=>'PDAM KAB MOJOKERTO',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_WAPASU' => array ('data'=>array('text'=>'PDAM KAB. PASURUAN',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_WASAMPANG' => array ('data'=>array('text'=>'PDAM KAB. SAMPANG',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_SIDO' => array ('data'=>array('text'=>'PDAM KAB SIDOARJO',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_STUBN' => array ('data'=>array('text'=>'PDAM KAB SITUBONDO',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_WATAPIN' => array ('data'=>array('text'=>'PDAM KAB. TAPIN',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_LPG' => array ('data'=>array('text'=>'PDAM KOTA BANDAR LAMPUNG',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_BDG' => array ('data'=>array('text'=>'PDAM KOTA BANDUNG',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_WAIBANJAR' => array ('data'=>array('text'=>'PDAM KOTA BANJARBARU',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_KOBGR' => array ('data'=>array('text'=>'PDAM KOTA BOGOR',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_WAJAMBI' => array ('data'=>array('text'=>'PDAM KOTA JAMBI',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_WAMANADO' => array ('data'=>array('text'=>'PDAM KOTA MANADO',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_WAGIRIMM' => array ('data'=>array('text'=>'PDAM KOTA MATARAM',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_WAPLMBNG' => array ('data'=>array('text'=>'PDAM KOTA PALEMBANG',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_WAKOPASU' => array ('data'=>array('text'=>'PDAM KOTA PASURUAN',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_PURRJ' => array ('data'=>array('text'=>'PDAM KOTA PURWOREJO',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_SURKT' => array ('data'=>array('text'=>'PDAM KOTA SURAKARTA',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_WAGROGOT' => array ('data'=>array('text'=>'PDAM KOTA TANAH GROGOT',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_PLYJA' => array ('data'=>array('text'=>'PDAM PALYJA JAKARTA',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_PON' => array ('data'=>array('text'=>'PDAM PONTIANAK',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'PDAM_SBY' => array ('data'=>array('text'=>'PDAM SURABAYA',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'TVINDVS' => array ('data'=>array('text'=>'Indovision',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'TVTOP' => array ('data'=>array('text'=>'Top TV',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'TVOKE' => array ('data'=>array('text'=>'Okevision',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'TVTLKMV' => array ('data'=>array('text'=>'Telkomvision',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'TVTTNSVIONS' => array ('data'=>array('text'=>'Transvision',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'TVYES' => array ('data'=>array('text'=>'Yes TV',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'TVNEX' => array ('data'=>array('text'=>'Nex Media',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'TVTOPAS' => array ('data'=>array('text'=>'Topas TV',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'TVORANGE' => array ('data'=>array('text'=>'Orange TV',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'TVBIG' => array ('data'=>array('text'=>'BIG TV',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'TVINNOV' => array ('data'=>array('text'=>'INNOVATE TV',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCSKYDEL1' => array ('data'=>array('text'=>'SKYNINDO TV DELUXE 1 BLN (80.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCSKYDEL12' => array ('data'=>array('text'=>'SKYNINDO TV DELUXE 12 BLN (960.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCSKYMAN3' => array ('data'=>array('text'=>'SKYNINDO TV MANDARIN 3 BLN (420.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCSKYDEL3' => array ('data'=>array('text'=>'SKYNINDO TV DELUXE 3 BLN (240.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCSKYDEL6' => array ('data'=>array('text'=>'SKYNINDO TV DELUXE 6 BLN (480.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCSKYFAM3' => array ('data'=>array('text'=>'SKYNINDO TV FAMILY 3 BLN (120.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCSKYMAN12' => array ('data'=>array('text'=>'SKYNINDO TV MANDARIN 12 BLN (1.680.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCSKYMAN6' => array ('data'=>array('text'=>'SKYNINDO TV MANDARIN 6 BLN (840.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCKV100' => array ('data'=>array('text'=>'K VISION (100.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCKV1000' => array ('data'=>array('text'=>'K VISION (1.000.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCKV125' => array ('data'=>array('text'=>'K VISION (125.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCKV150' => array ('data'=>array('text'=>'K VISION (150.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCKV175' => array ('data'=>array('text'=>'K VISION (175.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCKV200' => array ('data'=>array('text'=>'K VISION (200.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCSKYMAN1' => array ('data'=>array('text'=>'SKYNINDO TV MANDARIN 1 BLN (140.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCKV250' => array ('data'=>array('text'=>'K VISION (250.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCKV300' => array ('data'=>array('text'=>'K VISION (300.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCKV50' => array ('data'=>array('text'=>'K VISION (50.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCKV500' => array ('data'=>array('text'=>'K VISION (500.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCKV75' => array ('data'=>array('text'=>'K VISION (75.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCKV750' => array ('data'=>array('text'=>'K VISION (750.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCORG50' => array ('data'=>array('text'=>'Orange TV (50.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCORG80' => array ('data'=>array('text'=>'Orange TV (80.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCORG100' => array ('data'=>array('text'=>'Orange TV (100.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCORG300' => array ('data'=>array('text'=>'Orange TV (300.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCSKYFAM6' => array ('data'=>array('text'=>'SKYNINDO TV FAMILY 6 BLN (240.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCSKYFAM12' => array ('data'=>array('text'=>'SKYNINDO TV FAMILY 12 BLN (480.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCSKYFAM1' => array ('data'=>array('text'=>'SKYNINDO TV FAMILY 1 BLN (40.000)',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCKMVPRE' => array ('data'=>array('text'=>'Voucher Telkomvision Pra Combo',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCTELVISCOS' => array ('data'=>array('text'=>'Voucher Telkomvision Pra Cosmo',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCTELVISFIL' => array ('data'=>array('text'=>'Voucher Telkomvision Pra Film',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCTELVISHEM' => array ('data'=>array('text'=>'Voucher Telkomvision Pra Hemat',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCTELVISOLA' => array ('data'=>array('text'=>'Voucher Telkomvision Pra Olahraga',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCTELVISPEL' => array ('data'=>array('text'=>'Voucher Telkomvision Pra Pelangi',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
				'VOCTTELVISPEN' => array ('data'=>array('text'=>'Voucher Telkomvision Pra Pendidi',
						'waktu'=>'24 hours',
						'waktuid'=>'24 jam')
				),
			);
			//$produk=$jenis;//'PLNPOST';
			//$_data=array('text'=>'tagihan listrik','waktu'=>'24 hours','waktuid'=>'24 jam');
			$_arr=$_arrdata[$jenis];
			if($this->m_product->cektransaksiToken($jenis,$idPel,$_arr['data']['waktu']) == false)
			{
				die(json_encode(array(
					"head"=>'Transaksi Dengan tujuan '.$idPel.' sudah pernah terjadi, transaksi '.$$_arr['data']['text'].' dengan tujuan yang sama hanya bisa di proses setiap '.$_arr['data']['waktuid'],
					"form"=>"",
					"status"=>"4"
				)));
			}
			//tagihan
			$mbr=$this->session->userdata('member');
									$rs1 = explode("|",$this->m_product->sendInquiry($jenis,$idPel,$mbr['phone']));
									if($rs1[0] == 0 || $rs1[0] == '0')
									{
								$tampil=str_replace(',','</div><div>',$rs1[2]);
							/*	$res = explode(",",$rs1[2]);
								$jumlah = str_replace("Jml:","",$res[4]);
								$admin = str_replace("Admin:","",$res[6]);
								$total = str_replace("TOTAL Bayar:","",$res[7]);
								$namapemlik = str_replace("Tagihan PLN:","",$res[0]);
								$th = str_replace("bln:","",$res[3]);
								$tg = str_replace("Bln Tag:","",$res[5]);
								$tampil = '<table id="table01" width="100%" cellpadding="0" cellspacing="0" >
								<tr><th colspan="4" align="left"><b>Detail Data Transaksi</b></th></tr>
								<tr>
								    <td>Nama Lengkap</td><td>: '.$namapemlik.'</td>
								</tr>
								<tr>
								    <td>ID Pelanggan</td><td>: '.$res[1].'</td>
								</tr>
								<tr>
								    <td>Tarif/Daya</td><td>: '.$res[2].'</td>
								</tr>
								<tr>
								   <td>Bulan</td><td>: '.$th.'</td>
								</tr>
								<tr>
								    <td>Jumlah</td><td>: Rp. '.$this->egc->accounting_format($jumlah).',-</td>
								</tr>
								<tr>
								    <td>Tagihan Bulan</td><td>: '.$tg.'</td>
								</tr>
								<tr>
								    <td>Admin</td><td>: Rp. '.$this->egc->accounting_format($admin).',-</td>
								</tr>
								<tr>
								    <td>Total Bayar</td><td>:<b> Rp. '.$this->egc->accounting_format($total).',-</b></td></tr></table>';
							
	
	
		*/

								
										die(json_encode(array(
			"status"=>"0",
			"head"=>"Berikut Detail Informasi tagihan Anda...",
			"form"=>"
			  <div class='col-sm-12'>
				  <div class='input-group form-group'>
						<div>
							$tampil
						</div>
						<br><br>
						<div>
						  <span class='input-group-btn'>
			              	 <button type=button onclick='doPin()' class='btn btn-primary'>Bayar Tagihan</button>
						  </span>
						  </div>
					</div>
				  <script type=text/javascript>
				     function doPin(){
				            otp = '".base64_encode($rs1[1].'|'.$rs1[3])."';
				            jenis= '".$jenis."';
                      	    id_pel= $('#".$namatype."_id_pel').val();
                      	   // alert(otp+'|'+jenis+'|".$namatype."|'+id_pel);
                      	    vrf='#verify_',
            	    	  	head=$(vrf+'head'),
            	    	  	form=$(vrf+'form'),
            	    	  	load=$(vrf+'loader'),
            	    	  	sts=$(vrf+'status');
            	    	  	head.html('Dalam Process');
	            		    form.hide();
	            		    load.show();
	            		    sts.html('');
            				$.ajax({
		            		    type: 'POST',
		            		    url: 'payment/doToken',
		            		    async: true,
  								data: {jenis:jenis,id_pel:id_pel,pin:otp,cmd_save:'TAG'},
		            		    success: function(msg) {
			            		    /*if(window.console){
									  console.log(msg);
									 }*/
		            		    	if(msg.status=='0'){
				            		    //sts.hide();
			            		    	head.html(msg.head);
			            		    }
			            		    else{
			            		      	//sts.show();
			            		      	head.html('Info Gagal');
			                    		sts.html('<h4>'+msg.head+'</h4>');
			                    	}
				            		form.html(msg.form);
				            		form.show();
			                    	load.hide();
                      	        },
                      	    /*    complete: function (response){
								 if(window.console){
								  console.log(response.responseText);
								 }
								}*/
	            			});
						}
				  </script>
			  </div>
			  "
			 
		)));
										

									}
									else
									{
										//$user['kode']= $rs1[0];
										//$user['respone'] = $rs1[1];
										die(json_encode(array(
											"head"=>"Error code:".$rs1[0],
											"form"=>"<h4>".$rs1[1]."</h4>",
											"status"=>"4"
										)));	
									}
	
			
		}
		elseif(empty($produk))
		{
			die(json_encode(array(
			"head"=>"terdapat kesalahan pilihan harga",
			"form"=>"<h4>silahkan pilih harga berdasar jenis yang sesuai</h4>",
			"status"=>"3"
			)));
		}
		
		$data = $this->m_product->doReadList($produk);
		$hargajual = $this->m_product->getHarga($produk);
		if( $data== false||$hargajual<= 0)
		{
			//print_r $data;
			//$this->session->set_flashdata('message_error', 'Produk yang Anda request belum terdaftar disistem kami');
			die(json_encode(array(
			"head"=>"Produk yang Anda request belum terdaftar disistem kami",
			"form"=>"",
			"status"=>"4"
			)));
			
		}
		//$this->load->model('mbrlst_model');
		$saldoSisa = $this->m_product->getSaldo();
		if($saldoSisa<$hargajual)
		{
			//$this->session->set_flashdata('message_error', 'Saldo Eklanku Anda tidak mencukupi untuk pembelian silahkan melakukan topup Saldo eklanku terlebih dahulu.');
			die(json_encode(array(
				"head"=>"Saldo Eklanku Anda tidak mencukupi untuk pembelian silahkan melakukan topup Saldo eklanku terlebih dahulu.",
				"form"=>"",
				"status"=>"4"
			)));				
		}
		//$this->load->model('belanja_model');
		$user=$this->session->userdata('member');
	
		if($namatype=='token'){ 
			$kd_save='TO';
			$_data=array('text'=>'token listrik','waktu'=>'120 second','waktuid'=>'2 menit');
			$ischeck=$this->m_product->cektransaksiToken($produk,$idPel,$_data['waktu']);
		}
		else if($namatype=='pulsa') {
			$kd_save='PL';
			$_data=array('text'=>'pulsa','waktu'=>'24 hours','waktuid'=>'24 jam');
			$ischeck=$this->m_product->cektransaksiToken($produk,$idPel,$_data['waktu'],$user['eklanku_id']);
		}
		else if($namatype=='game') {
			$kd_save='PO';
			$_data=array('text'=>'voucher game','waktu'=>'120 second','waktuid'=>'2 menit');
			$ischeck=$this->m_product->cektransaksiToken($produk,$idPel,$_data['waktu'],$user['eklanku_id']);
		}
		else if($namatype=='paket') {
			$kd_save='PK';
			$_data=array('text'=>'Paket Data','waktu'=>'24 hours','waktuid'=>'24 jam');
			$ischeck=$this->m_product->cektransaksiToken($produk,$idPel,$_data['waktu'],$user['eklanku_id']);
		}
	/*	else if($namatype=='tvbyr') {
			$kd_save='TV';
			$_data=array('text'=>'Voucher TV','waktu'=>'24 hours','waktuid'=>'24 jam');
			$ischeck=$this->m_product->cektransaksiToken($produk,$idPel,$_data['waktu'],$user['eklanku_id']);
		}*/
	
    
		if($ischeck == false)
		{
			//$this->session->set_flashdata('message_error', 'Transaksi Dengan ID pelangan '.$idPel.' sudah pernah terjadi, transaksi Token PLN dengan id pelanggan dan Nominal yang sama hanya bisa di proses setiap 12 jam');
			die(json_encode(array(
				"head"=>'Transaksi Dengan tujuan '.$idPel.' sudah pernah terjadi, transaksi '.$_data['text'].' dengan tujuan dan atau Nominal yang sama hanya bisa di proses setiap '.$_data['waktuid'],
				"form"=>"",
				"status"=>"4"
			)));
		}
		/*
		$otp ='1111';// $this->m_product->doCreateOtp();
		if(!$otp['sts'])
		{
			die(json_encode(array(
				"head"=>"Otp gagal terkirim",
				"form"=>"<h4>".$otp['msg']."</h4>",
				"status"=>"4"
			)));			
		}*/
		//$this->load->library('egc');
		$tampil01 = 'Yakin Anda melanjutkan transaksi '.$_data['text'].' senilai Rp. '.$this->egc->accounting_format($hargajual);
		echo json_encode(array(
			"status"=>"0",
			"head"=>"Konfirmasi Transaksi",//"silahkan masukan no OTP yg telah dikirim via sms ke nomor hp Anda,<br>
				//ulangi tekan tombol beli jika dalam 5 menit tdk menerima sms...",
			"form"=>"
			  <div class='col-sm-12'>
				  <div class='input-group form-group'>
				  		<div>
				  			$tampil01
				  		</div>
				  		<div>ke nomer tujuan: $idPel ?<br>
				  		Untuk melanjutkan silahkan tekan proses!</div><br>
				  		<div>
						<input  id='otp' type='hidden' class='form-control' placeholder='no OTP'/>
						  <span class='input-group-btn'>
			              	 <button type=button onclick='doPin()' class='btn btn-primary'>Process</button>
						  </span>
						  </div>
					</div>
				  <script type=text/javascript>
				     function doPin(){
				            otp = $('#otp').val();
				            jenis= $('#".$namatype."_nominal_dd').val();
                      	    id_pel= $('#".$namatype."_id_pel').val();
                      	   // alert(otp+'|'+jenis+'|".$namatype."|'+id_pel);
                      	    vrf='#verify_',
            	    	  	head=$(vrf+'head'),
            	    	  	form=$(vrf+'form'),
            	    	  	load=$(vrf+'loader'),
            	    	  	sts=$(vrf+'status');
            	    	  	head.html('Dalam Process');
	            		    form.hide();
	            		    load.show();
	            		    sts.html('');
            				$.ajax({
		            		    type: 'POST',
		            		    url: 'payment/doToken',
		            		    async: true,
  								data: {jenis:jenis,id_pel:id_pel,pin:otp,cmd_save:'".$kd_save."'},
		            		    success: function(msg) {
			            		    /*if(window.console){
									  console.log(msg);
									 }*/
		            		    	if(msg.status=='0'){
				            		    //sts.hide();
			            		    	head.html(msg.head);			                    	
			            		    }
			            		    else{
			            		      	//sts.show();
			            		      	head.html('Info Gagal');
			                    		sts.html('<h4>'+msg.head+'</h4>');
			                    	}
				            		form.html(msg.form);
				            		form.show();
				            		load.hide();
                      	        },
                      	    /* */   complete: function (response){
								 if(window.console){
								  console.log(response.responseText);
								 }
								}
	            			});
						}
				  </script>
			  </div>
			  "
			 
		));
	}
	
	public function doToken()
	{	
		$produk 	    = $this->input->post('jenis');	
		$idPel   	      = trim(str_replace('-','',$this->input->post('id_pel')));//$this->input->post('id_pel');//)?$this->input->post('id_pel'):'';
		$pin   		    = $this->input->post('pin');	
		$cmd_save   	= $this->input->post('cmd_save');	
		header('Content-type: text/plain'); 
	  	header('Content-type: application/json');
			
	  		if(empty($produk))
			{
				die(json_encode(array(
				"head"=>"silahkan pilih nominal",
				"form"=>"",
				"status"=>"1"
				)));
			}
			if(empty($idPel))
			{
				die(json_encode(array(
				"head"=>"silahkan input nomor pelanggan",
				"form"=>"",
				"status"=>"2"
				)));
				
			}elseif(strlen($idPel)<8){
				die(json_encode(array(
				"head"=>"terdapat kesalahan input nomor",
				"form"=>"<h4>Jumlah input nomor minimal 8 digit</h4>",
				"status"=>"2"
				)));
			}

		$this->load->model('m_product');
		//===================start PASCA BAYAR =================
		if(preg_match('/POST|BPJS|TELKOM|PDAM|TV|VOC/',$produk)){ 
			/*die(json_encode(array(
			"head"=>"$produk",
			"form"=>"<h4>Sementara belum bisa melakukan transaksi</h4>",
			"status"=>"4"
			)));
	        if(empty($pin))
			{
				die(json_encode(array(
				"head"=>"silahkan input nomor OTP",
				"form"=>"",
				"status"=>"3"
				)));
				
			}
	  	}
	  	else{*/
	  		//$_data=array('kode'=>'PLNPOST','text'=>'token listrik','waktu'=>'12 hours','waktuid'=>'12 jam');
		
	  		$rs1 = explode("|",base64_decode($pin));
	//								if($rs1[0] == 0 || $rs1[0] == '0')
	//								{
	  									$saldo = $this->m_product->getSaldo();
										if($saldo>=$rs1[1])
										{
											//$this->load->model('mbrlst_model');
											$mbr=$this->session->userdata('member');
											$this->load->library('egc');
											$idIvoice		= 'PM' . $this->egc->genRandomString(13);
											$resp = $this->m_product->callPayment($rs1[1],$mbr['eklanku_id'],$idIvoice,$rs1[0],$produk,$idPel);
											$iResult = "";
											if ($resp->num_rows() > 0)
											{
												$resp = $resp->row();
												$iResult = $resp->f_cal_payment;
												if($iResult=="Proses payment selesai.")
												{
													die(json_encode(array(
														"head"=>"Informasi eklanku",
														"form"=>"Proses pembelian tagihan berhasil.",
														"status"=>"0"
													)));
												}
												else
												{
													die(json_encode(array(
														"head"=>"Informasi Gagal:$iResult",
														"form"=>"",
														"status"=>"3"
													)));
												}
																													
											}
											else
											{
													die(json_encode(array(
														"head"=>"Informasi Gagal: Server sibuk silahkan mengulang beberapa saat lagi",
														"form"=>"",
														"status"=>"3"
													)));
											}
											
										}
										else
										{
											die(json_encode(array(
												"head"=>"Saldo Eklanku Anda tidak mencukupi untuk pembelian silahkan melakukan topup Saldo eklanku terlebih dahulu.",
												"form"=>"",
												"status"=>"4"
											)));				
										}
	  	}
		//===================end PASCA BAYAR =================
		
		$data = $this->m_product->doReadList($produk);
		$hargajual = $this->m_product->getHarga($produk);
		if( $data== false||$hargajual<= 0)
		{
			//print_r $data;
			//$this->session->set_flashdata('message_error', 'Produk yang Anda request belum terdaftar disistem kami');
			die(json_encode(array(
			"head"=>"Produk yang Anda request belum terdaftar disistem kami",
			"form"=>"",
			"status"=>"4"
			)));
			
		}
		//$this->load->model('mbrlst_model');
		$saldoSisa = $this->m_product->getSaldo();
		if($saldoSisa<$hargajual)
		{
			//$this->session->set_flashdata('message_error', 'Saldo Eklanku Anda tidak mencukupi untuk pembelian silahkan melakukan topup Saldo eklanku terlebih dahulu.');
			die(json_encode(array(
				"head"=>"Saldo Eklanku Anda tidak mencukupi untuk pembelian silahkan melakukan topup Saldo eklanku terlebih dahulu.",
				"form"=>"",
				"status"=>"4"
			)));				
		}
		//$this->load->model('belanja_model');
		$user=$this->session->userdata('member');
	    if ($cmd_save == 'TO')$_data=array('kode'=>'PLNTOKEN','text'=>'token listrik','waktu'=>'120 second','waktuid'=>'2 menit');
		elseif ($cmd_save == 'PL')$_data=array('kode'=>'PULSA','text'=>'pulsa','waktu'=>'24 hours','waktuid'=>'24 jam');
		elseif ($cmd_save == 'PO')$_data=array('kode'=>'GAME','text'=>'voucher game','waktu'=>'120 second','waktuid'=>'2 menit');
		elseif ($cmd_save == 'PK')$_data=array('kode'=>'KUOTA','text'=>'paket data','waktu'=>'24 hours','waktuid'=>'24 jam');
	/*	elseif ($cmd_save == 'TV')$_data=array('kode'=>'VOUCHER TV KABEL','text'=>'paket data','waktu'=>'24 hours','waktuid'=>'24 jam');
		
		if($this->m_product->cektransaksiToken($produk,$idPel,$_data['waktu']) == false)
		{
			//$this->session->set_flashdata('message_error', 'Transaksi Dengan ID pelangan '.$idPel.' sudah pernah terjadi, transaksi Token PLN dengan id pelanggan dan Nominal yang sama hanya bisa di proses setiap 12 jam');
			die(json_encode(array(
				"head"=>'Transaksi Dengan ID pelangan '.$idPel.' sudah pernah terjadi, transaksi Token PLN dengan id pelanggan dan Nominal yang sama hanya bisa di proses setiap '.$_data['waktuid'],
				"form"=>"",
				"status"=>"4"
			)));
		}
		
		if($this->m_product->isValidOTP($pin) == false)
		{
			//$this->session->set_flashdata('message_error', 'PIN yang Anda masukkan salah!');
			die(json_encode(array(
				"head"=>'no OTP yang Anda masukkan salah!',
				"form"=>"",
				"status"=>"4"
			)));
		}
		*/
		$this->load->library('egc');
		$pem_code		= 'T' . $this->egc->genRandomString(13).date("His");
	    $resp = $this->m_product->callPulsa($hargajual, $user['eklanku_id'], $pem_code, $_data['kode'], $produk, $idPel);
		$iResult = '';
		if ($resp->num_rows() > 0)
			{
				$resp = $resp->row();
				$iResult = $resp->f_cal_pulsa;
				if($iResult=="Proses payment selesai.")
				{
					//$this->session->set_flashdata('message_info', "Proses pembelian token listrik berhasil. <a href = 'laporan'>Detail</a>");
					die(json_encode(array(
						"head"=>"Informasi eklanku",
						"form"=>"Proses pembelian ".$_data['text']." berhasil.",
						"status"=>"0"
					)));
				}
				else
				{
					//$this->session->set_flashdata('message_error', ''.$iResult);
					die(json_encode(array(
						"head"=>$iResul,
						"form"=>"",
						"status"=>"4"
					)));							
				}
																													
			}
			else
			{
				//$this->session->set_flashdata('message_error', 'Sever sedang sibuk silahkan diulang beberapa saat lagi.');
				die(json_encode(array(
						"head"=>"Sever sedang sibuk silahkan diulang beberapa saat lagi.",
						"form"=>"",
						"status"=>"4"
					)));								
			}
	
	}
	
}

/* End of file belanja.php */
/* Location: ./application/controllers/belanja.php */