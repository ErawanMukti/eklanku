<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Application
{
  public function __construct()
  {
    parent::__construct();
    $this->data['upload_dir_slider']        = '/upload/slider';
    $this->data['upload_dir_product_thumb'] = '/upload/product_thumb';
    $this->data['upload_dir_bid']           = '/upload/bid';
  }

  public function index()
  {
  ///  $this->load->model('m_slider');
 //   $this->load->model('m_product');
 //   $this->load->model('m_bid');

    $this->data['category_1'] = 'Rumah Tangga';
    $this->data['category_2'] = 'Makanan & Minuman';
    $this->data['category_3'] = 'Kecantikan';
    $this->data['category_4'] = 'Busana Pria';
    $this->data['category_5'] = 'Busana Wanita';
	
/*	
    $this->data['products_1'] = $this->_get_discount_price($this->m_product->get_items_by_category_alias_name('rumahtangga', 5));
    $this->data['products_2'] = $this->_get_discount_price($this->m_product->get_items_by_category_alias_name('makananminuman', 8));
    $this->data['products_3'] = $this->_get_discount_price($this->m_product->get_items_by_category_alias_name('kecantikan'));
    $this->data['products_4'] = $this->_get_discount_price($this->m_product->get_items_by_category_alias_name('busanapria'));
    $this->data['products_5'] = $this->_get_discount_price($this->m_product->get_items_by_category_alias_name('busanawanita'));
*/
    $this->data['adv_slider']       = array(
    	'banner-1.jpg','banner-2.jpg','banner-3.jpg','banner-4.jpg','banner-5.jpg',
    	'banner-6.jpg','banner-7.jpg','banner-8.jpg','banner-9.jpg','banner-10.jpg',
    	'banner-12.jpg','banner-12.jpg');
/*
    $this->data['adv_right_slider'] = array(null, null);
    $this->data['adv_medium']       = array(null, null, null, null);
    $this->data['adv_small']        = array(null, null, null, null);
*/    $idx=rand(1,count($this->data['adv_slider']));
    $og_img=$this->data['adv_slider'][$idx-1];
 
       $this->data['og_facebook']=array(
		'title'=>'Eklanku Shopping, berbelanja murah - cashback menarik - untung beliung',
		'image'=>asset_uri('images/banner/'.$og_img),
		'description'=>'Berbelanja murah, cepat, dan ekonomis merupakan layanan kami eklanku.com. Beragam promo cashback menarik meningkatkan keuntungan anda dalam jual beli di eklanku.com. Eklanku.com sangat menarik buat Anda semua, silahkan bergabung...'
	);
/*	
    // $this->data['bids']      = $this->m_bid->get_available_bid();
    $this->data['bids']      = $this->m_bid->get_items();
    $this->data['sliders']   = $this->m_slider->get_all_items();
    */
    $this->data['main_html'] = $this->load->view('home', $this->data, true);
    $this->load->view('layout/main', $this->data); 
  }
  
	public function htmlLogin(){
		// set text compatible IE7, IE8
	  	header('Content-type: text/plain'); 
	  	// set json non IE
	  	header('Content-type: application/json'); 
		echo json_encode(array(
			"head"=>"silahkan Login terlebih dahulu",
			"form"=>"
			  <div class='col-sm-6'>
				  <div class='input-group form-group'>
				    <input id='email' type='text' class='form-control' placeholder='Email/Username'/>
				  </div>
			  </div>
			  <div class='col-sm-6'>
				  <div class='input-group form-group'>
					<input id='password' type='password' class='form-control' placeholder='Password'/>
					  <span class='input-group-btn'>
		              	 <button type=button onclick='login()' class='btn btn-primary'>Login</button>
					  </span>
				  </div>
				  <script type=text/javascript>
				     function login(){
				            email = $('#email').val();
            				pass = $('#password').val();
            				ceklanku=$('#ceklanku').val();
            				vrf='#verify_',
            	    	  	head=$(vrf+'head'),
            	    	  	form=$(vrf+'form'),
            	    	  	load=$(vrf+'loader'),
            	    	  	sts=$(vrf+'status');
            	    	  	head.html('Dalam Process');
	            		    form.hide();
	            		    load.show();
	            		    sts.html('');
            			    $.ajax({
		            		    type: 'POST',
		            		    url: 'login/htmldoLogin',
		            		    async: true,
  								data: {username:email,password:pass,EKLANKU:ceklanku},
		            		    success: function(msg) {
			            		    if(msg.status=='0'){
				            		 //   sts.hide();
			            		    	head.html(msg.head);
				            		    form.html(msg.form);
			            		    }
			            		    else{
			            		      	head.html('Info Gagal');
			                    		sts.html('<h4>'+msg.head+'</h4>');
			            		      	sts.show();
			                    	}
			                    	form.show();
			                    	load.hide();
                      	        },
                      	       /* complete: function (response){
								 if(window.console){
								  console.log(response.responseText);
								 }
								}*/
	            			});
						}
				  </script>
			  </div>
			  <p>Belum punya akun? <a href='/register'>Daftar Sekarang</a></p>
			  "
		));
	}
	
	
	
  public function loadData(){
  	$loadType=$_POST['loadType'];
   	$loadId=$_POST['loadId'];
  	$this->load->model('m_product');
  	$this->load->library('egc');
  	//die("$loadId");
   	//$this->data['data_token'] = $this->m_product->getDataToken();
   	if($loadType=='token_nominal')
    	$result=$this->m_product->getDataToken($loadId);
    else if($loadType=='pulsa_nominal')
    	$result=$this->m_product->getDataPulsa($loadId);
    else if($loadType=='game_nominal')
    	$result=$this->m_product->getDataGame($loadId);
    else if($loadType=='paket_nominal')
    	$result=$this->m_product->getDataPaket($loadId);
    else if(preg_match('/tvbyr/',$loadType))
    	$result=$this->m_product->getDataTV($loadId);
   // die(json_encode($result));
    $HTML="";

    if($result->num_rows() > 0){
      foreach($result->result() as $list){
      	if(preg_match('/tvbyr/',$loadType))
      		$HTML.="<option value='".$list->product_kode."'>".$list->product_name."</option>";
      	else
      		$HTML.="<option value='".$list->product_kode."'>".$list->product_name." [Rp. ".$this->egc->accounting_format($list->harga_jual)."]</option>";
      }
    }
    echo $HTML;   
   	
  }  
	
}
