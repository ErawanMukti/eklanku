<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends Application {

	public function __construct()
	{		
		parent::__construct();		
		$this->redirect_not_login();
		$this->load->model('m_messaging');
    	/*if ($this->session->userdata("mbr_code") == false)
		{
			redirect("homepage");
		}*/
	}

	public function index()
	{
		redirect("laporan/transaksi");

	}
	 function downloadExelTransaksi() {
		$this->load->model('m_laporan');
		$data['content_data'] = $this->m_laporan->getDatapayment();
		$this->load->view('laporan/laporan_transaksi',$data);
		
	}
	
	 function downloadExel() {
		$this->load->model('m_laporan');
		$data['content_data'] = $this->m_laporan->getData();
		$this->load->view('laporan/laporan_cashback',$data);
		
	}
	public function transaksi()
	{
		$this->data['page_title'] = 'Pembelian';	
		$this->data['page_map'] = 'Pembelian';	
		$this->data['site_map'] = 'Laporan Transaksi';
		$this->data['view_title'] = 'Laporan Transaksi';	
		$this->data['jualbeli'] = base_url();//str_replace('://','://jual-beli.',str_replace('://www.','://',base_url()));
		/*$this->data['view_link'] = base_url().'laporan';
		$this->data['menu'] = '6';
		$this->load->model('iklan_model');
		$this->data["jmlData"] = $this->iklan_model->record_countSaya();
		$this->load->model('mbrlst_model');
		$this->load->model('belanja_model');
		$this->data['content_saldo'] = $this->mbrlst_model->getSaldo();
		$this->data['content_trx'] = $this->belanja_model->getTotalTrx();
		$this->data['content_wd'] = $this->belanja_model->getTotalWD();
		$this->data['content_wdantre'] = $this->belanja_model->getTotalAntreanWD();
		$this->data['content_data_ref'] = $this->belanja_model->doReadRef();
		*/
		$this->load->model('m_laporan');
		$this->data['TotalNum']=$this->m_laporan->record_count_payment();
		$this->data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $this->data['limit'] = 20;
		
		$this->data['FirstNum']=$this->data['page']+1; 
		if($this->data['TotalNum']<$this->data['FirstNum']+$this->data['limit'])
			$this->data['LastNum']=$this->data['TotalNum']; 
		else 
			$this->data['LastNum']=$this->data['FirstNum']+$this->data['limit']-1;
			
		$this->data['LastPageNum'] = ceil($this->data['TotalNum']/$this->data['limit']);
		
		$this->data['clink']=base_url().'laporan/transaksi';
		$this->data['clinkreff']='';
		$cmd_save		= trim($this->input->post('key_pencarian'));
		if($cmd_save != "")
		{
			$this->data['content_data'] = $this->m_laporan->get_department_list_caripayment($cmd_save,$this->data['limit'],$this->data['page']);
			$this->data['proses_pencarian_data'] = true;
		}
		else
		{
			$this->data['content_data'] = $this->m_laporan->get_department_listpayment($this->data['limit'],$this->data['page']);  
			$this->data['proses_pencarian_data'] = false;		
		}
		
		$this->load->model('m_user');
		$this->load->model('m_order');
		$this->load->model('m_eklanpay_order');
		$this->load->model('m_product_image');
		$this->load->model('m_payment_status');
		$this->load->model('m_promosi_user'); 
		
		$this->data['msg']			   = $this->session->flashdata('message');
		$this->data['user_data']       = $this->m_user->get_item_by_id($this->data['member']['id']);
		$this->data['transaction_list']= $this->m_order->_query_review_order(array('user_id'=>$this->data['member']['id'],'payment_method'=>'transfer','payment_status'=>'1'));
		$this->data['cofirm_promotion']= $this->m_promosi_user->get_all_items(array('user_id'=>$this->data['member']['id'],'payment_status'=>'Belum dibayar'));
		$this->data['cofirm_eklanpay']= $this->m_eklanpay_order->get_all_items(array('user_id'=>$this->data['member']['id'],'payment_status'=>1,'invoice_date'=>'2 HOUR'));
		
		
		$this->data['main_html'] = $this->load->view('laporan/lap_transaksi', $this->data, true);
		$this->load->view('layout/main', $this->data); 
		
		/*$this->data['content_view'] = 'laporan/lap_transaksi';
		$this->data['content_header'] = 'template/globalheader';
		$this->data['content_footer'] = 'template/globalfooter';
		$this->load->view('template/tplmenu', $this->data);*/

	}
	public function cashback()
	{  
	   	$data['page_title'] = 'Pembelian';	
		$data['page_map'] = 'Pembelian';	
		$data['site_map'] = 'Laporan Cashback';
		$data['view_title'] = 'Laporan Cashback';	
		$data['view_link'] = base_url().'laporan/cashback';
		$data['menu'] = '7';
		$this->load->model('iklan_model');
		$data["jmlData"] = $this->iklan_model->record_countSaya();
		$this->load->model('mbrlst_model');
		$this->load->model('uang_model');
		$this->load->model('belanja_model');
		$data['content_saldo'] = $this->mbrlst_model->getSaldo();
		$data['content_trx'] = $this->belanja_model->getTotalTrx();
		$data['content_wd'] = $this->belanja_model->getTotalWD();
		$data['content_wdantre'] = $this->belanja_model->getTotalAntreanWD();
		$data['content_data_ref'] = $this->belanja_model->doReadRef();
		$data['content_total'] = $this->uang_model->doReadTotal();
		$data['content_out'] = $this->uang_model->doReadTerbayar();
		$this->load->model('m_laporan');
		$data['TotalNum']=$this->m_laporan->record_count();
		$data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['limit'] = 5;
		
		$data['FirstNum']=$data['page']+1; 
		if($data['TotalNum']<$data['FirstNum']+$data['limit'])
			$data['LastNum']=$data['TotalNum']; 
		else 
			$data['LastNum']=$data['FirstNum']+$data['limit']-1;
			
		$data['LastPageNum'] = ceil($data['TotalNum']/$data['limit']);
		
		$data['clink']=base_url().'laporan/cashback';
		$data['clinkreff']='';
		$cmd_save		= trim($this->input->post('key_pencarian'));
		if($cmd_save != "")
		{
			$data['content_data'] = $this->m_laporan->get_department_list_cari($cmd_save,$data['limit'],$data['page']);
			$data['proses_pencarian_data'] = true;
		}
		else
		{
			$data['content_data'] = $this->m_laporan->get_department_list($data['limit'],$data['page']);  
			$data['proses_pencarian_data'] = false;		
		}
		
		
		$data['content_view'] = 'laporan/lap_cashback';
		$data['content_header'] = 'template/globalheader';
		$data['content_footer'] = 'template/globalfooter';
		$this->load->view('template/tplmenu', $data);
		
	}
	
	function cetak($id='', $vtpe = '') {
		$this->load->model('m_laporan');
		$data['content_data'] = $this->m_laporan->get_datatrx($id);
		if($vtpe=='PULSA')
		{
			$this->load->view('laporan/pulsa',$data);
		}
		else if($vtpe=='PLNTOKEN')
		{
			$this->load->view('laporan/token',$data);
		}
		else if($vtpe=='PLNPOST')
		{
			$this->load->view('laporan/pln',$data);
		}
		
	}
	
	
}

/* End of file laporan.php */
/* Location: ./application/controllers/laporan.php */