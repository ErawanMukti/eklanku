<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Application
{
  public function index()
  {
    if ($_POST) {
      $this->load->model('m_user');

      $email        = $this->input->post('email');
      $password     = $this->input->post('password');
      $callback_url = $this->input->post('callback_url');

  //    $this->load->library('egc');
	
  //    $result = $this->egc->_login_api($email, $password);
//die(json_encode($result));

  /*    if (!empty($result) && $result->kode === 0) {
        $name      = $result->respone[0]->mbr_name;
        $phone     = $result->respone[0]->mbr_hp;
        $member_id = $result->respone[0]->mbr_id;
        $wallet    = $result->respone[0]->mbr_wallet;
        */
        $user = $this->m_user->login($email);
        if (empty($user)) {
        /*  $data = array(
            'name'        => $name,
            'phone'       => $phone,
            'email'       => $email,
            'password'    => $password,
            're_password' => $password,
            'agreement'   => '1',
            'wallet'      => $wallet,
            'eklanku_id'  => $member_id,
          );

          $errors = $this->m_user->add_all_data($data);*/
			$errors ='Belum ada data user';
        } else {
          $data = array(
            /*'name'        => $name,
            'phone'       => $phone,
            */
			'email'       => $email,
            'password'    => $password,
            're_password' => $password,
            //'wallet'      => $wallet,
          );
          

        //  $errors = $this->m_user->update_all_data($user['id'], $data);
          
        }

        if (empty($errors)) {
          $user            = $this->m_user->login($email, $password);
          $user['time']    = date('Y-m-d H:i:s');

          // if (ENVIRONMENT === 'development') {
          //   $user['premium_user'] = true;
          // } else {
         //   $user['premium_user'] = ($result->lelang[0]->status_lelang === 't') ? true : false;
          // }

          $this->session->set_userdata('is_login', true);
          $this->session->set_userdata('member', $user);

          if (!empty($callback_url)) {
            redirect($callback_url);
          } else {
            redirect(base_url('user'));
          }
        }
     /* } elseif (!empty($result) && $result->kode === 7) {
        $this->session->set_flashdata('errors', $result->respone);
        $this->session->set_userdata('for_update_old_email', $email);
        redirect('update_email');
      } else {
        $this->data['errors'] = $result->respone;
      }*/
    }


    $this->data['update_email_message'] = $this->session->flashdata('update_email');
    $this->data['callback_url']         = $this->input->get('callback_url');
    $this->data['main_html']            = $this->load->view('login', $this->data, true);
    $this->load->view('layout/login', $this->data);
  }
  
	public function htmldoLogin()
    {
		header('Content-type: text/plain'); 
	  	// set json non IE
	  	header('Content-type: application/json'); 
		$email = $this->input->post("username");
        $password = $this->input->post("password");
		//$cmd_reff = $this->input->post("cmd_reff");
		if($email == "")
		{
			die(json_encode(array(
			"status"=>'1',
			"head"=>"silahkan isi email/username",
			"form"=>"")));
		}
		if($password == "")
		{
			die(json_encode(array(
			"status"=>'2',
			"head"=>"silahkan isi password",
			"form"=>"")));
		}
		/*$this->load->library('egc');
	
		$result = $this->egc->_login_api($email, $password);
		$_result=false;
    if (!empty($result) && $result->kode === 0) {
        $name      = $result->respone[0]->mbr_name;
        $phone     = $result->respone[0]->mbr_hp;
        $member_id = $result->respone[0]->mbr_id;
        $wallet    = $result->respone[0]->mbr_wallet;
        */
        $this->load->model('m_user');
        $user = $this->m_user->login($email);
        if (empty($user)) {
         /* $data = array(
            'name'        => $name,
            'phone'       => $phone,
            'email'       => $email,
            'password'    => $password,
            're_password' => $password,
            'agreement'   => '1',
            'wallet'      => $wallet,
            'eklanku_id'  => $member_id,
          );

          $errors = $this->m_user->add_all_data($data);*/

			$errors ='Belum ada data user';
        } else {
          $data = array(
          /*  'name'        => $name,
            'phone'       => $phone,*/
            'email'       => $email,
            'password'    => $password,
            're_password' => $password,
         //   'wallet'      => $wallet,
          );
          

        //  $errors = $this->m_user->update_all_data($user['id'], $data);
          
       // }

        if (empty($errors)) {
          $user            = $this->m_user->login($email, $password);
          $user['time']    = date('Y-m-d H:i:s');

        //  $user['premium_user'] = ($result->lelang[0]->status_lelang === 't') ? true : false;
          
          $this->session->set_userdata('is_login', true);
          $this->session->set_userdata('member', $user);
          $_result=true;       
        }
      }
       
		
        if($_result)
        {
            die(json_encode(array(
			"status"=>'0',
			"head"=>"Login Anda telah sukses",
			"form"=>"<h2>silahkan ulangi transaksi...<h2>")));
        }
        else
        {
			die(json_encode(array(
			"status"=>'3',
			"head"=>$result->respone,
			"form"=>"")));
        }
	}
/*
  protected function _login_api($email, $password)
  {
    $this->load->library('egc');
	$apiLink=$this->egc->getApi();
    $token = sha1(md5($email) . $apiLink['salt']);
    $query = array(
      'username' => $email,
      'password' => $password,
      'token'    => $token
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiLink['url']."/login");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec ($ch);
    curl_close ($ch);

    return json_decode($output);
  }*/
  
	public function ceklanku()
	{
	    $csrf['ceklanku'] = $this->security->get_csrf_token_name();
	    $csrf['ceklanku_token'] = $this->security->get_csrf_hash();
	
	    echo json_encode($csrf);
	}
}
