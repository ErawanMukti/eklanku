<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_product extends CI_Model
{
  protected $table_name          = 'product';
  protected $table_product_image = 'product_image';
  protected $table_category      = 'product_category';
  protected $table_category_sub  = 'product_category_sub';
  protected $table_category_last = 'product_category_last';
  protected $table_user          = 'user';

  protected $min_name_length        = 3;
  protected $max_name_length        = 100;
  protected $max_description_length = 2000;
  private $tableproduck;
  protected $db2;
  private static $xml = null;
  private static $encoding = 'UTF-8';
  public function __construct()
  {
    $this->load->database();
	$this->tableproduck = 'vw_product';
  }
 
	
	private static function init($version = '1.0', $encoding = 'UTF-8', $format_output = true) {
        self::$xml = new DomDocument($version, $encoding);
        self::$xml->formatOutput = $format_output;
		self::$encoding = $encoding;
    }
	private static function &createXML($node_name, $arr=array()) {
        $xml = self::getXMLRoot();
        $xml->appendChild(self::convert($node_name, $arr));

        self::$xml = null;    
        return $xml;
    }
	 private static function &convert($node_name, $arr=array()) {

       
        $xml = self::getXMLRoot();
        $node = $xml->createElement($node_name);

        if(is_array($arr)){
           
            if(isset($arr['@attributes'])) {
                foreach($arr['@attributes'] as $key => $value) {
                    if(!self::isValidTagName($key)) {
                        throw new Exception('[Array2XML] Illegal character in attribute name. attribute: '.$key.' in node: '.$node_name);
                    }
                    $node->setAttribute($key, self::bool2str($value));
                }
                unset($arr['@attributes']);
            }

           
            if(isset($arr['@value'])) {
                $node->appendChild($xml->createTextNode(self::bool2str($arr['@value'])));
                unset($arr['@value']);    
                
                return $node;
            } else if(isset($arr['@cdata'])) {
                $node->appendChild($xml->createCDATASection(self::bool2str($arr['@cdata'])));
                unset($arr['@cdata']);   
                
                return $node;
            }
        }

       
        if(is_array($arr)){
          
            foreach($arr as $key=>$value){
                if(!self::isValidTagName($key)) {
                    throw new Exception('[Array2XML] Illegal character in tag name. tag: '.$key.' in node: '.$node_name);
                }
                if(is_array($value) && is_numeric(key($value))) {
                   
                    foreach($value as $k=>$v){
                        $node->appendChild(self::convert($key, $v));
                    }
                } else {
                 
                    $node->appendChild(self::convert($key, $value));
                }
                unset($arr[$key]); 
            }
        }

      
        if(!is_array($arr)) {
            $node->appendChild($xml->createTextNode(self::bool2str($arr)));
        }

        return $node;
    }

   
    private static function getXMLRoot(){
        if(empty(self::$xml)) {
            self::init();
        }
        return self::$xml;
    }

  
    private static function bool2str($v){
       
        $v = $v === true ? 'true' : $v;
        $v = $v === false ? 'false' : $v;
        return $v;
    }

    
    private static function isValidTagName($tag){
        $pattern = '/^[a-z_]+[a-z0-9\:\-\.\_]*[^:]*$/i';
        return preg_match($pattern, $tag, $matches) && $matches[0] == $tag;
    }
    
  public function get_all_items($user_id)
  {
    $this->db->select("{$this->table_name}.*, {$this->table_product_image}.image, {$this->table_category}.name AS category_name");
    $this->db->from($this->table_name);
    $this->db->join($this->table_product_image, "{$this->table_name}.product_code = {$this->table_product_image}.product_code AND {$this->table_product_image}.created_by = {$this->table_name}.created_by");
    $this->db->join($this->table_category, "{$this->table_name}.category_id = {$this->table_category}.id");
    $this->db->where("{$this->table_name}.created_by", $user_id);
    $this->db->where("{$this->table_name}.status", '1');
    $this->db->where("{$this->table_product_image}.status", '1');
    $this->db->where("{$this->table_category}.status", '1');
    $this->db->where("{$this->table_product_image}.is_show", '1');

    $query = $this->db->get(); //print_r($this->db->last_query());
    $array = $query->result_array();

    $query->free_result();
    return $array;
  }

   public function count_items_with_limit($extra="")
  {
    $sql="SELECT COUNT(*) AS jumlah FROM product AS a
			LEFT JOIN user AS b ON b.id=a.created_by
				WHERE a.status = '1' AND is_published = '1' -- AND (CURDATE() >= shop_startactivedate AND CURDATE() <=shop_endactivedate)
					$extra;";
    $query = $this->db->query($sql);
	$array = $query->result_array();
	return $array;
  }
  
  public function get_items_with_limit($offset=0, $limit = 10, $order = 'RAND()', $extra="")
  {
	
    $this->db->select("{$this->table_name}.*, {$this->table_product_image}.image");
    $this->db->from($this->table_name);
    $this->db->join($this->table_product_image, "{$this->table_name}.product_code = {$this->table_product_image}.product_code AND {$this->table_product_image}.created_by = {$this->table_name}.created_by");
    $this->db->join($this->table_user, "{$this->table_name}.created_by = {$this->table_user}.id");
	$this->db->where("{$this->table_name}.status", '1');
    $this->db->where("{$this->table_product_image}.status", '1');
    $this->db->where("{$this->table_product_image}.is_show", '1');
//	$this->db->where("(CURDATE() >= shop_startactivedate AND CURDATE() <=shop_endactivedate)");
    
    $this->db->order_by($order);
	$this->db->limit($limit,$offset);
    $query = $this->db->get(); //print_r($this->db->last_query());
    $array = $query->result_array();

    $query->free_result();
    return $array;
  }

  public function get_all_items_for_super_admin()
  {
    $this->db->select("{$this->table_name}.*, {$this->table_product_image}.image, {$this->table_user}.name AS created_name");
    $this->db->from($this->table_name);
    $this->db->join($this->table_product_image, "{$this->table_name}.product_code = {$this->table_product_image}.product_code AND {$this->table_product_image}.created_by = {$this->table_name}.created_by");
    $this->db->join($this->table_user, "{$this->table_name}.created_by = {$this->table_user}.id");
    $this->db->where("{$this->table_name}.status", '1');
    $this->db->where("{$this->table_product_image}.status", '1');
    $this->db->where("{$this->table_product_image}.is_show", '1');
    $this->db->where("{$this->table_user}.status", '1');
    $this->db->order_by('id', 'DESC');

    $query = $this->db->get();
    $array = $query->result_array();

    $query->free_result();
    return $array;
  }

  public function get_items_by_category_id($category_id, $limit = 10, $order_by = 'RAND()')
  {
    $this->db->select("{$this->table_name}.*, {$this->table_product_image}.image");
    $this->db->from($this->table_name);
    $this->db->join($this->table_product_image, "{$this->table_name}.product_code = {$this->table_product_image}.product_code AND {$this->table_product_image}.created_by = {$this->table_name}.created_by");
    $this->db->where("{$this->table_name}.status", '1');
    $this->db->where("{$this->table_name}.is_published", '1');
    $this->db->where("{$this->table_name}.category_id", $category_id);
    $this->db->where("{$this->table_product_image}.status", '1');
    $this->db->where("{$this->table_product_image}.is_show", '1');
    $this->db->order_by($order_by);
    $this->db->limit($limit);

    $query = $this->db->get();
    $array = $query->result_array();

    $query->free_result();
    return $array;
  }
  
  public function get_items_bestseller($field="", $id="", $limit)
  {
    $where = ($field !="" && $id !="")?" WHERE $field='".$id."' ":"";
	$sql="SELECT DISTINCT(product_id) AS product_id FROM cart AS a
			LEFT JOIN product AS b ON b.id=a.product_id
			$where ORDER BY a.id DESC LIMIT $limit;";
    $query = $this->db->query($sql);
	$array = $query->result_array();
	return $array;
  }
  
  public function count_items_by_categoryId($category_id,$extra="")
  {
    $sql="SELECT COUNT(*) AS jumlah FROM product AS a
			LEFT JOIN user AS b ON b.id=a.created_by
				WHERE a.status = '1' AND is_published = '1' -- AND (CURDATE() >= shop_startactivedate AND CURDATE() <=shop_endactivedate)
					AND category_id = '".$category_id."' $extra;";
    $query = $this->db->query($sql);
	$array = $query->result_array();
	return $array;
  }
  
  public function get_items_by_categoryId($category_id, $offset=0, $limit = 30, $order_by = 'RAND()', $extra="")
  {
    $sql="SELECT a.* FROM product AS a
			LEFT JOIN user AS b ON b.id=a.created_by
				WHERE a.status = '1' AND is_published = '1' -- AND (CURDATE() >= shop_startactivedate AND CURDATE() <=shop_endactivedate)
					AND category_id = '".$category_id."' $extra
					ORDER BY $order_by LIMIT $offset,$limit;";
    $query = $this->db->query($sql);
	$array = $query->result_array();
	return $array;
  }
  
  public function count_items_by_subcategoryId($category_id,$extra="")
  {
    $sql="SELECT COUNT(*) AS jumlah FROM product AS a
			LEFT JOIN user AS b ON b.id=a.created_by
				WHERE a.status = '1' AND is_published = '1' -- AND (CURDATE() >= shop_startactivedate AND CURDATE() <=shop_endactivedate)
					AND sub_category_id = '".$category_id."' $extra;";
    $query = $this->db->query($sql);
	$array = $query->result_array();
	return $array;
  }
  
    public function get_items_by_subcategoryId($category_id, $offset=0, $limit = 30, $order_by = 'RAND()', $extra="")
  {
    $sql="SELECT a.* FROM product AS a
			LEFT JOIN user AS b ON b.id=a.created_by
				WHERE a.status = '1' AND is_published = '1' -- AND (CURDATE() >= shop_startactivedate AND CURDATE() <=shop_endactivedate)
				AND sub_category_id = '".$category_id."' $extra
				ORDER BY $order_by LIMIT $offset,$limit;";
    $query = $this->db->query($sql);
	$array = $query->result_array();
	return $array;
  }
  
  public function count_items_by_lastcategoryId($category_id,$extra="")
  {
    $sql="SELECT COUNT(*) AS jumlah FROM product AS a
			LEFT JOIN user AS b ON b.id=a.created_by
				WHERE a.status = '1' AND is_published = '1' -- AND (CURDATE() >= shop_startactivedate AND CURDATE() <=shop_endactivedate)
					AND last_category_id = '".$category_id."' $extra;";
    $query = $this->db->query($sql);
	$array = $query->result_array();
	return $array;
  }
  
  public function get_items_by_lastcategoryId($category_id, $offset=0, $limit = 30, $order_by = 'RAND()', $extra="")
  {
    $sql="SELECT a.* FROM product AS a
			LEFT JOIN user AS b ON b.id=a.created_by
				WHERE a.status = '1' AND is_published = '1' -- AND (CURDATE() >= shop_startactivedate AND CURDATE() <=shop_endactivedate)
				AND last_category_id = '".$category_id."' $extra 
				ORDER BY $order_by LIMIT $offset,$limit;";
    $query = $this->db->query($sql);
	$array = $query->result_array();
	return $array;
  }

  public function get_items_by_category_alias_name($category_alias, $limit = 10, $order_by = 'RAND()')
  {
    $this->db->select("{$this->table_name}.*, {$this->table_product_image}.image");
    $this->db->from($this->table_name);
    $this->db->join($this->table_product_image, "{$this->table_name}.product_code = {$this->table_product_image}.product_code AND {$this->table_product_image}.created_by = {$this->table_name}.created_by");
    $this->db->join($this->table_category, "{$this->table_name}.category_id = {$this->table_category}.id");
    $this->db->join($this->table_user, "{$this->table_name}.created_by = {$this->table_user}.id");
	$this->db->where("{$this->table_name}.status", '1');
    $this->db->where("{$this->table_name}.is_published", '1');
    $this->db->where("{$this->table_category}.alias_name", $category_alias);
    $this->db->where("{$this->table_product_image}.status", '1');
    $this->db->where("{$this->table_product_image}.is_show", '1');
	//$this->db->where("(CURDATE() >= shop_startactivedate AND CURDATE() <=shop_endactivedate)");
    $this->db->order_by($order_by);
    $this->db->limit($limit);

    $query = $this->db->get();
    $array = $query->result_array();

    $query->free_result();
    return $array;
  }

  public function get_item_by_id_for_user($id)
  {
    $this->db->select("{$this->table_name}.*, {$this->table_product_image}.image, {$this->table_category}.name AS category_name");
    $this->db->from($this->table_name);
    $this->db->join($this->table_product_image, "{$this->table_name}.product_code = {$this->table_product_image}.product_code AND {$this->table_product_image}.created_by = {$this->table_name}.created_by");
    $this->db->join($this->table_category, "{$this->table_name}.category_id = {$this->table_category}.id");
    $this->db->where("{$this->table_name}.id", $id);
    $this->db->where("{$this->table_name}.status", '1');
    $this->db->where("{$this->table_name}.is_published", '1');
    $this->db->where("{$this->table_product_image}.status", '1');
    $this->db->where("{$this->table_product_image}.is_show", '1');
    $this->db->where("{$this->table_category}.status", '1');

    $query = $this->db->get();
    $array = $query->row_array();

    $query->free_result();

    unset($query);
    return $array;
  }

  public function get_item_by_id($id)
  {
    $this->db->select("{$this->table_name}.*, {$this->table_product_image}.image, {$this->table_category}.name AS category_name");
    $this->db->from($this->table_name);
    $this->db->join($this->table_product_image, "{$this->table_name}.product_code = {$this->table_product_image}.product_code AND {$this->table_product_image}.created_by = {$this->table_name}.created_by");
    $this->db->join($this->table_category, "{$this->table_name}.category_id = {$this->table_category}.id");
    $this->db->where("{$this->table_name}.id", $id);
    $this->db->where("{$this->table_name}.status", '1');
    $this->db->where("{$this->table_product_image}.status", '1');
    $this->db->where("{$this->table_product_image}.is_show", '1');
    $this->db->where("{$this->table_category}.status", '1');

    $query = $this->db->get();
    $array = $query->row_array();

    $query->free_result();

    unset($query);
    return $array;
  }

  public function save($data)
  {
    $errors = $this->validate($data);
	
    if (empty($errors)) {
      $data['date_created'] = date('Y-m-d H:i:s');
      $this->db->insert($this->table_name, $data);
    }

    return $errors;
  }

  public function update($id, $user_id, $data)
  {
    $errors = $this->validate($data);

    if (empty($errors)) {
      $data['date_updated'] = date('Y-m-d H:i:s');

      $this->db->where('id', $id);
      $this->db->where('created_by', $user_id);
      $this->db->update($this->table_name, $data);
    }

    return $errors;
  }

  public function validate($data)
  {
    extract($data);

    $errors = array();

    if (empty($name)) {
      $errors['name'] = 'Nama produk tidak boleh kosong.';
    } elseif (strlen($name) < $this->min_name_length) {
      $errors['name'] = 'Panjang minimal nama produk adalah ' . $this->min_name_length . ' karakter.';
    } elseif (strlen($name) > $this->max_name_length) {
      $errors['name'] = 'Panjang maksimal nama produk adalah ' . $this->max_name_length . ' karakter.';
    }

    if (empty($type)) {
      $errors['type'] = 'Pilih salah satu kondisi.';
    }

    if (empty($weight)) {
      $errors['weight'] = 'Berat produk tidak boleh kosong.';
    }

    if (empty($price)) {
      $errors['price'] = 'Harga produk tidak boleh kosong.';
    }

    if (!empty($discount_type) && empty($discount)) {
      $errors['discount'] = 'Jika option diskon dipilih kolom diskon tidak boleh kosong.';
    }

    if (!empty($description) && strlen($description) > $this->max_description_length) {
      $errors['description'] = 'Panjang maksimal deskripsi produk adalah ' . $this->max_description_length . ' karakter.';
    }

    if (empty($category_id)) {
      $errors['category_id'] = 'Pilih kategori yang telah disediakan.';
    } elseif (!$this->validate_category_id($category_id)) {
      $errors['category_id'] = 'Kategori yang anda pilih salah.';
    }

    if (empty($sub_category_id)) {
      $errors['sub_category_id'] = 'Pilih sub kategori yang telah disediakan.';
    } elseif (!$this->validate_sub_category_id($category_id, $sub_category_id)) {
      $errors['sub_category_id'] = 'Kategori yang anda pilih salah.';
    }

    if (empty($last_category_id)) {
      $errors['last_category_id'] = 'Pilih kategori terakhir yang telah disediakan.';
    } elseif (!$this->validate_last_category_id($sub_category_id, $last_category_id)) {
      $errors['last_category_id'] = 'Kategori yang anda pilih salah.';
    }

    if (isset($stock) && $stock <= 0) {
      $errors['stock'] = 'Stok barang tidak boleh kosong atau kurang dari 1';
    }

    return $errors;
  }

  public function validate_category_id($id)
  {
    $this->db->from($this->table_category);
    $this->db->where('id', $id);

    $query = $this->db->get();
    $data  = $query->result_array();

    $query->free_result();

    if (!empty($data)) {
      return true;
    }
  }

  public function validate_sub_category_id($id, $category_id)
  {
    $this->db->from($this->table_category_sub);
    $this->db->where('id', $category_id);
    $this->db->where('category_id', $id);

    $query = $this->db->get();
    $data  = $query->result_array();

    $query->free_result();

    if (!empty($data)) {
      return true;
    }
  }

  public function validate_last_category_id($id, $category_id)
  {
    $this->db->from($this->table_category_last);
    $this->db->where('id', $category_id);
    $this->db->where('category_id', $id);

    $query = $this->db->get();
    $data  = $query->result_array();

    $query->free_result();

    if (!empty($data)) {
      return true;
    }
  }
  //voucher product
    public function getDataGame($id=null)
    {
        $_where=empty($id)?'':"and product_kode like '$id%'";
        $sql = "select * from ".$this->tableproduck." where product_status = 'Active' and type_product = 'GAME' $_where order by harga_jual ASC  ";
        // Loading second db and running query.
        $CI = &get_instance();
        //setting the second parameter to TRUE (Boolean) the function will return the database object.
        $this->db2 = $CI->load->database('pgsql', TRUE);
        //$qry = $this->db2->query("SELECT * FROM cds");
        //print_r($qry->result());
        $query = $this->db2->query($sql);
        return $query;
    }
    
	public function getDataPaket($id=null)
    {
        $_where=empty($id)?'':"and product_kode like '$id%'";
        $sql = "select * from ".$this->tableproduck." where product_status = 'Active' and type_product in ('AXIS DATA','XL COMBO','XL DATA','BOLT DATA','SMARTFREN DATA','TRI DATA','TSEL DATA','ISAT COMBO','ISAT EXTRA','ISAT DATA','TSEL SMS') $_where order by harga_jual ASC  ";
        // Loading second db and running query.
        $CI = &get_instance();
        //setting the second parameter to TRUE (Boolean) the function will return the database object.
        $this->db2 = $CI->load->database('pgsql', TRUE);
        //$qry = $this->db2->query("SELECT * FROM cds");
        //print_r($qry->result());
        $query = $this->db2->query($sql);
        return $query;
    }
	
	public function getDataToken($id=null)
    {
    	$_where=empty($id)?'':"and product_kode like '$id%'";
        $sql = "select * from ".$this->tableproduck." where product_status = 'Active' and type_product = 'PLNTOKEN' $_where order by harga_jual ASC  ";
        $CI = &get_instance();
        //setting the second parameter to TRUE (Boolean) the function will return the database object.
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $query = $this->db2->query($sql);
        return $query;
    }
	
    public function getDataPulsa($id=null)
    {
        $_where=empty($id)?'':"and product_kode like '$id%'";
        $sql = "select * from ".$this->tableproduck." where product_status = 'Active' and type_product = 'PULSA' $_where order by harga_jual ASC  ";
        $CI = &get_instance();
        //setting the second parameter to TRUE (Boolean) the function will return the database object.
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $query = $this->db2->query($sql);
        return $query;
    }
    
	public function getDataTV($id=null)
    {
        $_where=empty($id)?'':"and product_kode like '$id%'";
        $sql = "select * from ".$this->tableproduck." where product_status = 'Active' and type_product like '%TV KABEL' $_where order by harga_jual ASC  ";
        $CI = &get_instance();
        //setting the second parameter to TRUE (Boolean) the function will return the database object.
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $query = $this->db2->query($sql);//die($this->db2->last_query());
        return $query;
    }
    
	public function doReadList($kode)
	{
		$CI = &get_instance();
        //setting the second parameter to TRUE (Boolean) the function will return the database object.
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $this->db2->where('product_kode', $kode);
		$this->db2->where('product_status', 'Active');
		$data = $this->db2->get($this->tableproduck);
		if ($data->num_rows() > 0)
		{
			return TRUE;	
			
		}
		else
		{
			return FALSE;
		}	
	}
	
	public function getHarga($kode)
	{
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $iResult =0;
		$this->db2->where('product_kode', $kode);
		$this->db2->where('product_status', 'Active');
		$data = $this->db2->get($this->tableproduck);
		if ($data->num_rows() > 0)
		{
			$data = $data->row();
			$iResult = $data->harga_jual;
			
		}
		return $iResult;
	}
	
	public function getSaldo()
	{
		/*$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $iResult =0;
		$user=$this->session->userdata('member');
		$this->db2->where('member_code', $user['eklanku_id']);
		$data = $this->db2->get('member_list');
		if ($data->num_rows() > 0)
		{
			$data = $data->row();
			$iResult = $data->member_amount;
			
		}*/
		return 2000000;//$iResult;
	}
	
	public function cektransaksiToken($kode,$nomertujuan,$waktu,$code_mbr='')
	{
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $iresult = true;
        $code_mbr=empty($code_mbr)?"":"and member_code='".$code_mbr."'";
		$sql = "select transaksi_id from transaksi where transaksi_date +  interval '$waktu' > NOW() and opr = '".$kode."' and transaksi_status <> 'Gagal' and ip_addrs = '".$nomertujuan."' ".$code_mbr;
        $query = $this->db2->query($sql);
        if ($query->num_rows() > 0)
		{
			$iresult= false;	
		}
	   return $iresult;
	}
	
	public function isValidOTP($pin)
	{
		$user=$this->session->userdata('member');
		$ipuser=$this->getUserIp();
		$sql = "select * from otp_list where TIMESTAMPDIFF(MINUTE, create_date, now()) <= 5 and otp_code = '".$pin."' and eklanku_id = '".$user['eklanku_id']."' and ip='$ipuser'";
        $data = $this->db->query($sql);
        if ($data->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}	
	}
	
	public function callPulsa($jml, $idmember, $invoice, $idtrx, $kodetrx, $tujuan)
	{
		
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $ssql = 'select * from f_cal_pulsa(';
		$ssql = $ssql . '\'' . $idmember . '\',';
		$ssql = $ssql . '\'' . $jml . '\',';
		$ssql = $ssql . '\'' . $invoice . '\',';
		$ssql = $ssql . '\'' . $idtrx . '\',';
		$ssql = $ssql . '\'' . $kodetrx . '\',';
		$ssql = $ssql . '\'' . $tujuan . '\'';
		$ssql = $ssql . ')';

		$query = $this->db2->query($ssql);
		return $query;
	}
	
	public function callShopping($jml, $idmember, $invoice, $typetrx)
	{
		
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $ssql = 'select * from f_cal_shoping(';
		$ssql = $ssql . '\'' . $idmember . '\',';
		$ssql = $ssql . '\'' . $jml . '\',';
		$ssql = $ssql . '\'' . $invoice . '\',';
		$ssql = $ssql . '\'' . $typetrx . '\'';
		$ssql = $ssql . ')';

		$query = $this->db2->query($ssql);
		return $query;
	}
	
	public function callPayment($jml, $idmember, $invoice, $idtrx, $kodetrx, $tujuan)
	{
		
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $ssql = 'select * from f_cal_payment(';
		$ssql = $ssql . '\'' . $idmember . '\',';
		$ssql = $ssql . '\'' . $jml . '\',';
		$ssql = $ssql . '\'' . $invoice . '\',';
		$ssql = $ssql . '\'' . $idtrx . '\',';
		$ssql = $ssql . '\'' . $kodetrx . '\',';
		$ssql = $ssql . '\'' . $tujuan . '\'';
		$ssql = $ssql . ')';

		$query = $this->db2->query($ssql);
		return $query;
	}
	
	//f_cal_shoping_member_wallet
	//(meber_code character varying, 
	//jml numeric, invoice character varying)
	public function callOrderWallet($jml, $idmember, $invoice)
	{
		
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $ssql = 'select * from f_cal_shoping_member_wallet(';
		$ssql = $ssql . '\'' . $idmember . '\',';
		$ssql = $ssql . '\'' . $jml . '\',';
		$ssql = $ssql . '\'' . $invoice . '\'';
		$ssql = $ssql . ')';

		$query = $this->db2->query($ssql);
		return $query;
	}
	
	public function callOrderTranfer($jml, $idmember, $invoice)
	{
		
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $ssql = 'select * from f_cal_shoping_member_transfer(';
		$ssql = $ssql . '\'' . $idmember . '\',';
		$ssql = $ssql . '\'' . $jml . '\',';
		$ssql = $ssql . '\'' . $invoice . '\'';
		$ssql = $ssql . ')';

		$query = $this->db2->query($ssql);
		return $query;
	}
	/*
	 
	  $rs1 = explode("|",$this->sendInquiry($product,$tujuan,$hp));
									if($rs1[0] == 0 || $rs1[0] == '0')
									{
										$saldo = $this->mbrlst_model->getSaldoWallet($id_member);
										if($saldo>=$rs1[3])
										{
											$this->load->model('mbrlst_model');
											$resp = $this->mbrlst_model->callPayment($rs1[3],$id_member,$idIvoice,$rs1[1],$product,$tujuan);
											$iResult = "";
											if ($resp->num_rows() > 0)
											{
												$resp = $resp->row();
												$iResult = $resp->f_cal_payment;
												if($iResult=="Proses payment selesai.")
												{
													$user['kode']= 0;
													$user['respone'] = $iResult;
													print_r( json_encode($user));
												}
												else
												{
													$user['kode']= 4;
													$user['respone'] = $iResult;
													print_r( json_encode($user));
												}
																													
											}
											else
											{
												$user['kode']= 5;
												$user['respone'] = "Server sibuk silahkan mengulang beberapa saat lagi";
												print_r( json_encode($user));
											}
											
										}
										else
										{
											$user['kode']= 6;
											$user['respone']='Saldo kurang sisa ewallet Anda Rp. '.$this->egc->accounting_format($saldo);
											print_r( json_encode($user));
										}
									}
									else
									{
										$user['kode']= $rs1[0];
										$user['respone'] = $rs1[1];
										print_r( json_encode($user));	
									}
	 
	 */
	
	public function supliyerdoReadList($kode)
	{
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $this->db2->where('product_kode', $kode);
		$this->db2->where('supliyer_status', 'Active');
		$data = $this->db2->get('vw_hpp_supliyer');
		return $data;
	}
	
	public function sendInquiry($product,$idpelangan, $hppelangan)
	{
	
        $tz = 'Asia/Makassar';
		$timestamp = time();
		$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
		$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
		$tgl= $dt->format('Y-m-d H:i:s');
		$trxtime= $dt->format('YmdHis');
		//$this->load->model('product_model');
		$usr = "";
		$pswd = "";
		$url = "";
		$h2hKode = "";
		$spsID = 0;
		if($this->doReadList($product)==true)
		{
			//$this->load->model('supliyer_model');
			$rs = $this->supliyerdoReadList($product);
			if ($rs->num_rows() > 0)
			{
				
				foreach ($rs->result() as $r) 
				{
					$usr = $r->user_url;
					$pswd = $r->paswd_url;
					$url = $r->url_topup;
					$h2hKode = $r->kode_h2h;
					$spsID = $r->supliyer_id;

				}
				if($usr!= "" && $pswd != "" && $url != "" && $h2hKode != "")
				{
				   if ($spsID == 1)
				   { 
				  
    					$url = $url."ppob_inquiry.php?user=".$usr."&pwd=".$pswd."&idpel=".$idpelangan."&prodtype=".$h2hKode."&hpplg=".$hppelangan;
    					$curlHandle= curl_init(); 
    					curl_setopt($curlHandle, CURLOPT_URL, $url); 
    					curl_setopt($curlHandle, CURLOPT_HEADER, 0);
    					curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
    					curl_setopt($curlHandle, CURLOPT_TIMEOUT,300000);
    					curl_setopt($curlHandle, CURLOPT_POST, 0);
    					$content = curl_exec($curlHandle);
    					curl_close($curlHandle);
    
    					$msg= $this->getPesan($content);
    					$kode = $this->getKode($content);
    					$idTrx = $this->getTrxID($content);
    					$tagihan = $this->cekInq($content);
    					if($kode == 0)
    					{
    						$message = $kode."|".$idTrx."|".$msg."|".$tagihan;
    					}else{
    						$pesanEror = $this->getEror($content);
    						$message=$kode."|".$pesanEror;
    					}
				   }
				   elseif ($spsID == 5)
				   {
				       $send_xml = array(
            			'methodName' => array(
            				'@value' => 'rajabiller.inq' 
            			),
            			'params' => array(
            				'param' => array(
            					array(
            						'value' => array(
            							'string' => $h2hKode 
            						)
            					),
            					array(
            						'value' => array(
            							'string' => $idpelangan 
            						)
            					),
            					array(
            						'value' => array(
            							'string' => '' 
            						)
            					),
            					array(
            						'value' => array(
            							'string' => '' 
            						)
            					),
            					array(
            						'value' => array(
            							'string' => $usr 
            						)
            					),
            					array(
            						'value' => array(
            							'string' => $pswd 
            						)
            					), 
            					array(
            						'value' => array(
            							'string' =>  $trxtime
            						)
            					) 
            					
            				)
            			)
            		);
                    $xml = $this->createXML('methodCall', $send_xml);
            		$send = $xml->saveXML(); 
            		
            	//	echo 'Hasil Generate XML<br/><pre>', htmlentities($send), '</pre>';
            		$url = $url; 
            
            		$ch = curl_init();
            		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            		curl_setopt($ch, CURLOPT_VERBOSE, true);
            		curl_setopt($ch, CURLOPT_POST, true);
            		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            		curl_setopt($ch, CURLOPT_POSTFIELDS, $send); //$send ambil dari generate xml diatas
            		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            		curl_setopt($ch, CURLOPT_URL, $url);
            		curl_setopt($ch, CURLOPT_REFERER, $url);
            		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            			'Content-Type: application/text')
            		);
            		$data = curl_exec($ch);
            
            		if ($data === FALSE) {
            		   printf("cUrl error (#%d): %s<br>\n", curl_errno($ch), htmlspecialchars(curl_error($ch)));
            			rewind($verbose);
            			$verboseLog = stream_get_contents($verbose);
            			echo "Verbose information:\n<pre>", htmlspecialchars($verboseLog), "</pre>\n";
            		}
            
            		$errno = curl_errno($ch);
            		$error = curl_error($ch);
            
            		if ($errno > 0)
            			$result = 'null';
            		else
            			$result = $data;
            		//close connection
            		curl_close($ch);
            	//	echo "<hr>Server Response : <br /><br />" . nl2br(htmlentities($result)) . "<br /><br />";
            		//ECHO '<hr>';
            	//	ECHO 'HASIL BREAKDOWN XML<br><br/>';
                print_r($result);
            		$dom = new DOMDocument;
            		$dom->loadXML($result);
            		$arrays = $dom->getElementsByTagName('string');
            		$key = array('KODEPRODUK','WAKTU','IDPELANGGAN1','IDPELANGGAN2','IDPELANGGAN3', 'NAMAPELANGGAN', 'PERIODETAGIHAN','NOMINAL','BIAYAADMIN', 'ID', 'PIN','REF1','REF2','REF3','STATUS','KETERANGAN','SALDOTERPOTONG','SISASALDO','URLSTRUK');
            		$i =0;
            		foreach ($arrays as $array) {
            			$a[$key[$i++]] = $array->nodeValue; 
            		}
            		
            		if(empty($a['ID']) === true){
            			$message='99|Something went wrong!';
            		}else {
            		    if($a['STATUS'] == '00')
    					{
    					    $tagihan = (int) $a['NOMINAL'] + (int) $a['BIAYAADMIN'];
    					    $pesan = 'Tagihan PDAM Atas Nama : '.$a['NAMAPELANGGAN'].',ID Pelanggan :  '.$a['IDPELANGGAN1'].',Periode : '.$a['PERIODETAGIHAN'].',RPTAG : '.$a['NOMINAL'].', Admin : '.$a['BIAYAADMIN'].',RP Bayar : '.$tagihan;
    					    
    					    $message = "0|".$a['REF2']."|".$pesan."|".$tagihan;
            			    
    					}
    					else
    					{
    					   $message = $a['STATUS']."|".$a['KETERANGAN'];
    					    
    					}
            		}
  
				   }
    				return $message;
				}
				else
				{
				    	$kode = 3;
				    	$pesanEror = "Gagal url supliyer belum di set";
						$message=$kode."|".$pesanEror;	
					return $message;
				}
			}
			else
			{
			    	$kode = 4;
			    	$pesanEror = "Gagal supliyer belum di set";
					$message=$kode."|".$pesanEror;	
					return $message;
				
			}
		}
		else
		{
		    	$kode = 5;
		    	$pesanEror = "Product gangguan";
				$message=$kode."|".$pesanEror;	
				return $message;
	
		}
	}
	

	private function getEror($psn)
    {

      $pesan = strpos($psn,'<ErrorMsg>');
      $pesan = substr($psn, strpos($psn,'<ErrorMsg>'));					
      $pesan = substr($pesan, strpos($pesan, "<ErrorMsg>")+10, strpos($pesan, "</ErrorMsg>")-strpos($pesan, "<ErrorMsg>")-10); 
	  return $pesan;

    }
	
	private function getKode($psn)
	{
		$rescode = strpos($psn,'<ResultCode>');
		$rescode = substr($psn, strpos($psn,'<ResultCode>'));					
		$rescode = substr($rescode, strpos($rescode, "<ResultCode>")+12, strpos($rescode, "</ResultCode>")-strpos($rescode, "<ResultCode>")-12);
		return $rescode;
	}
	
	private function getTrxID($psn)
	{
		$rescode = strpos($psn,'<TrxID>');
		$rescode = substr($psn, strpos($psn,'<TrxID>'));					
		$rescode = substr($rescode, strpos($rescode, "<TrxID>")+7, strpos($rescode, "</TrxID>")-strpos($rescode, "<TrxID>")-7);
		return $rescode;
	}
	
	private function getResKode($psn)
	{
		$rescode = strpos($psn,'<Response>');
		$rescode = substr($psn, strpos($psn,'<Response>'));					
		$rescode = substr($rescode, strpos($rescode, "<Response>")+10, strpos($rescode, "</Response>")-strpos($rescode, "<Response>")-10);
		return $rescode;
	}
	
	 private function cekInq($psn)
	{	
      
 		$Tagihan = strpos($psn,'<Tagihan>');
		$Tagihan = substr($psn, strpos($psn,'<Tagihan>'));					
		$Tagihan = substr($Tagihan, strpos($Tagihan, "<Tagihan>")+9, strpos($Tagihan, "</Tagihan>")-strpos($Tagihan, "<Tagihan>")-9);
	    $msg =$Tagihan;
	
		return $msg;
	}
	
	private function getPesan($psn)
    {

      $pesan = strpos($psn,'<Msg>');
      $pesan = substr($psn, strpos($psn,'<Msg>'));					
      $pesan = substr($pesan, strpos($pesan, "<Msg>")+5, strpos($pesan, "</Msg>")-strpos($pesan, "<Msg>")-5); 
	  return $pesan;

    }
	
	public function sendOtp($nohp,$text,$modemid)
	{
		
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsqlsms', TRUE);
        $ssql = 'select sent_sms(';
		$ssql = $ssql . '\'' . $nohp . '\',';
		$ssql = $ssql . '\'' . $text . '\',';
		$ssql = $ssql . '\'' . $modemid . '\'';
		$ssql = $ssql . ')';

		$query = $this->db2->query($ssql);
		return $query;
	}
	
	public function doCreateOtp()
	{
		$otp=rand(1000,9999);
		$user=$this->session->userdata('member');
		//die($this->getUserIp());
		$this->db->set("eklanku_id", $user['eklanku_id']);
		$this->db->set("otp_code", $otp);
        $this->db->set("ip", $this->getUserIp());
        if($this->db->insert('otp_list')){
        	$hp=$this->getHpMember($user['eklanku_id']);
        	if(!empty($hp)){
        	    $prefix = substr((string)$hp,0,4); 
	            $dataPrefix = $this->doGetPrefix($prefix);
	            $jns_phone=$dataPrefix=='Telkomsel'?'asphone':'xlphone';
        		$this->sendOtp($hp,'Eklanku, nomor/kode otp: '.$otp,$jns_phone);
				return array('sts'=>true,'msg'=>'Sukses process otp');;
        	}
        	else return array('sts'=>false,'msg'=>'No HP Anda belum terdaftar dengan benar');
        }
        else return array('sts'=>false,'msg'=>'Kegagalan system, coba beberapa saat lagi...');;
	}
	
	public function getHpMember($membercode)
	{
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $iResult ='';
		$this->db2->where('member_code', $membercode);
		$data = $this->db2->get('member_list');
		if ($data->num_rows() > 0)
		{
			$data = $data->row();
			$iResult = $data->member_mobile;
			
		}
		return $iResult;
	}
	
    public function doGetPrefix($prefix)
	{	
	    $iResult ='';
		$ssql = 'select provider_name from provider';	
		$ssql = $ssql . ' where provider_pref like \'%' . $prefix . '%\'';
		//echo $ssql;
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $data = $this->db2->query($ssql);
		if ($data->num_rows() > 0)
		{
			$data = $data->row();
			$iResult = $data->provider_name;
			
		}
		return $iResult;
	
	}
 	protected function getUserIp()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress ;
     }
}
