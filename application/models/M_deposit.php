<?php
class M_deposit extends CI_Model {

	private $table='castback';
	private $db2;
	private $ci;
	private $_username;
	
	public function __construct()
	{
		parent::__construct();

		$this->table = 'castback';
		$dt=$this->session->userdata('member');
		//die(json_encode($dt['eklanku_id']));//('eklanku_id').$this->session->userdata('email');
		$this->_mbrcode = $dt['eklanku_id'];
        $this->_username = $dt['email'];
        $this->ci = &get_instance();
        $this->db2 = $this->ci->load->database('pgsql', TRUE);
        $this->ci->load->library('egc');
	}
	private function cleaninput($data){
		$filter = trim(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
		return $filter;
    }
 	public function doInsertRecord($nominal)
	{
		$tz = 'Asia/Makassar';
		$timestamp = time();
		$dt = new DateTime("now", new DateTimeZone($tz)); 
		$dt->setTimestamp($timestamp); 
		
		$pem_code		= 'PNS' . $this->ci->egc->genRandomString(12);
		$ssql = 'select * from f_cal_proses_penarikan(';
		$ssql = $ssql . '\'' . $dt->format('Y-m-d H:i:s') . '\',';
		$ssql = $ssql . '\'' . $this->_mbrcode . '\',';
		$ssql = $ssql . '\'' . $pem_code . '\',';
		$ssql = $ssql . '\'' . $nominal . '\',';
		$ssql = $ssql . '\'' . $this->input->ip_address() . '\',';
		$ssql = $ssql . '\'' . $this->cleaninput($this->_username). '\'';
		$ssql = $ssql . ')';

		$query = $this->db2->query($ssql);

		return $pem_code;
	}
	
	public function doTopup($data)
	{
		$tz = 'Asia/Makassar';
		$timestamp = time();
		$dt = new DateTime("now", new DateTimeZone($tz)); 
		$dt->setTimestamp($timestamp); 
		$codeUnix = 0;//$this->ci->egc->genRandomNumber(3);
		$pem_code		= 'TPS' . $this->ci->egc->genRandomString(12);
		/*anbank character varying,
		bnk character varying,
		norec character varying,  
		interval_bayar character varying*/
		$ssql = 'select * from f_cal_proses_topup_member(';
		$ssql = $ssql . '\'' . $dt->format('Y-m-d H:i:s'). '\',';
		$ssql = $ssql . '\'' . $this->_mbrcode. '\',';
		$ssql = $ssql . '\'' . $pem_code. '\',';
		$ssql = $ssql . '\'' . $data['nominal']. '\',';
		$ssql = $ssql . '\'' . $this->input->ip_address() . '\',';
		$ssql = $ssql . '\'' . $this->cleaninput($this->_username). '\',';
		$ssql = $ssql . '\'' . $data['pesan']. '\',';
		$ssql = $ssql . '\'' . $codeUnix . '\',';
		$ssql = $ssql . '\'' . $data['anbank'] . '\',';
		$ssql = $ssql . '\'' . $data['bnk'] . '\',';
		$ssql = $ssql . '\'' . $data['norec'] . '\',';
		$ssql = $ssql . '\'' . $data['interval_bayar'] . '\'';
		$ssql = $ssql . ')';
		//die($ssql);
		$query = $this->db2->query($ssql);
		return $pem_code;
	}
	
	public function cekTopup($jml_tarik)
	{
		$this->db2->where('member_code', $this->_mbrcode);
        $this->db2->where("transaksi_status", 'Waiting');
		$this->db2->where("transaksi_amount", $jml_tarik);
		$data = $this->db2->get('transaksi');
		if ($data->num_rows() > 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function getDataRekening()
	{
		$iResult ='';
		$this->db2->where('member_code', $this->_mbrcode);
		$data = $this->db2->get('member_list');
		if ($data->num_rows() > 0)
		{
			$data = $data->row();
			$iResult = $data->member_bank_num."|".$data->member_bank_acc."|".$data->member_bank_name;
			
		}
		return $iResult;
	}
	
	public function cekPenarikan($jml_tarik)
	{
		$this->db2->where('member_code', $this->_mbrcode);
        $this->db2->where("penarikan_status", 'Waiting');
		$this->db2->where("penarikan_amount", $jml_tarik);
		$data = $this->db2->get('penarikan');
		if ($data->num_rows() > 0)
		{
			return false;
			
		}
		else
		{
			return true;
		}
	}
	
	
	//========member_list
	private $table2='member_list';
	
	public function getName()
	{
		$iResult ='';
		$this->db2->where('member_code', $this->_mbrcode);
		$data = $this->db2->get($this->table2);
		if ($data->num_rows() > 0)
		{
			$data = $data->row();
			$iResult = $data->member_name;
			
		}
		return $iResult;
	}
	
	//=========emlt
	private $table3='eml_outbox';
	
	public function doGetTemplate($emlCode)
	{	
		$ssql = 'select * from eml_template';	
		$ssql = $ssql . ' where tpl_code = \'' . $emlCode . '\'';
		$data = $this->db2->query($ssql);
		return $data;
	}
	
	public function doInsert($outFrom, $outRcpt, $outSubj, $outBody, $type_email)
	{	
		$this->db2->set("out_sender", $outFrom);
		$this->db2->set("out_recipient", $outRcpt);
		$this->db2->set("out_subject", $outSubj);
		$this->db2->set("out_body", $outBody);
		$this->db2->set("out_status", 'Waiting');
		$this->db2->set("out_date", date("Y-m-d H:i:s"));
		$this->db2->set("type_email", $type_email);
		if($this->db2->insert($this->table3)==TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function doSMSRWD($emlCode, $outRcpt, $nominal,$nameMember,$accUser)
	{	
		$data = $this->doGetTemplate($emlCode);
		if ($data->num_rows() > 0)
		{
			$data = $data->row();
			$outFrom = $data->tpl_sender;
			$outSubj = $data->tpl_subject;
			$outBody = $data->tpl_text;
			$outBody = str_replace("[membername]", $nameMember, $outBody);
			$outBody = str_replace("[mapamount]", $nominal, $outBody);
			$outBody = str_replace("[username]", $accUser, $outBody);
			$this->doInsert($outFrom, $outRcpt, $outSubj, $outBody, "sms");		}
	}
	
	public function isValidPIN($pin)
	{
		//$pengacak       = 993524;
		$dataPin        = $pin;
		$this->db2->where('account_pin', ''.$dataPin);
		$this->db2->where('member_code', $this->_mbrcode);
		$data = $this->db2->get('member_list');
		//die($this->db2->last_query());
		if ($data->num_rows() > 0)
		{
			
			return TRUE;	
			
		}
		else
		{
			return FALSE;
		}	
	}
	
	public function getPointMember($code)
	{
		$iResult='0';
		If(!empty($code)){

    		$data = $this->db2->get("f_get_lst_point('$code')");
			if ($data->num_rows() > 0)
			{
				$data = $data->row();
				$iResult = $data->rpoint_member;
				
			}else $iResult='-';
		}
		return $iResult;		
	}
	
	public function getPointGroup($code)
	{
		$iResult='0';
		If(!empty($code)){
			$data = $this->db2->get("f_get_lst_pointgroup('$code')");
			if ($data->num_rows() > 0)
			{
				$data = $data->row();
				$iResult = $data->rpoint_group;
				
			}else $iResult='-';
		}
		return $iResult;		
	}
}