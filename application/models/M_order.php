<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_order extends CI_Model
{
  protected $table_name = 'cart_order';
  protected $table_daily_order = 'daily_order';

	public function __construct()
	{
		$this->load->database();
    }

	public function save_cart_order($array_data)
    {
		return $this->db->insert($this->table_name, $array_data);
	}
  
	public function update_cart_order($data)
	{
		if(isset($data['id']))$this->db->where('id', $data['id']);
		//$this->db->where('user_id', $data['user_id']);
		$this->db->where('status', $data['status']);
		
		return $this->db->update($this->table_name, array('payment_status'=>$data['payment_status']));
	}
  
	public function get_all_items($data)
	{
		$this->db->select("*");
		$this->db->from($this->table_name);
		
		if(isset($data['id']))$this->db->where('id', $data['id']);
		$this->db->where('user_id', $data['user_id']);
		$this->db->where('status', $data['status']);
		
		$query = $this->db->get(); //print_r($this->db->last_query()); die();
		$array = $query->result_array();

		$query->free_result();
		return $array;
	}
		
	public function get_one_items($id)
	{
		$this->db->select("*");
		$this->db->from($this->table_name);
		$this->db->where('id', $id);
		$this->db->order_by("id", "ASC");
		$query = $this->db->get()->row(); //print_r($this->db->last_query());
		return $query;
	}
	
	public function get_payment_status($data)
	{
		$filter ="";
		$filter .=!empty($data['id'])?" AND a.id='".$data['id']."'":"";
		$filter .=!empty($data['user_id'])?" AND a.user_id='".$data['user_id']."'":"";
		$filter .=!empty($data['payment_method'])?" AND a.payment_method='".$data['payment_method']."'":"";
		$filter .=!empty($data['payment_status'])?" AND a.payment_status='".$data['payment_status']."'":"";
		
		$sql="SELECT a.id, a.user_id, a.invoice_code, a.invoice_date, a.receive_name, a.receive_address, a.receive_phone, c.province_name, c.type, c.name, c.postal_code, 
					d.norek_asal, d.nama_nasabah, d.bank_tujuan_id, e.bill_name, d.norek_tujuan, d.nominal_transfer, a.payment_method, a.key_transfer, d.tgl_transfer,
					a.total AS total_belanja,  a.payment_status, b.pay_name, a.user_addr_id
				FROM cart_order AS a
					LEFT JOIN payment_status AS b ON b.id=a.payment_status
					LEFT JOIN regency AS c ON c.province_id=a.receive_province_id AND c.regency_id=a.receive_regency_id
					LEFT JOIN cart_order_status AS d ON d.cart_order_id=a.id
					LEFT JOIN billing_account AS e ON e.id=d.bank_tujuan_id
						WHERE a.status=1 $filter
					ORDER BY a.invoice_date DESC";
		$query = $this->db->query($sql);
		$array = $query->result_array();
		$query->free_result();
		return $array;
				
	}
	  
	public function get_active_items($data)
	{ 
		$this->db->select("*");
		$this->db->from($this->table_name);
		$this->db->where('order_invoice IS NULL');
		$this->db->where('user_id', $data['user_id']);
		$this->db->where('status', $data['status']);
		
		$query = $this->db->get(); 
		$array = $query->result_array();

		$query->free_result();
		return $array;
	}
	
	public function _query_review_order($data){
		$filter ="";
		$filter .=(isset($data['id']) && $data['id'] !="")?" AND a.id='".$data['id']."'":"";
		$filter .=(isset($data['user_id']) && $data['user_id'] !="")?" AND a.user_id='".$data['user_id']."'":"";
		$filter .=(isset($data['payment_method']) && $data['payment_method'] !="")?" AND a.payment_method='".$data['payment_method']."'":"";
		$filter .=(isset($data['payment_status']) && $data['payment_status'] !="")?" AND a.payment_status='".$data['payment_status']."'":"";
		
		$sql="SELECT a.id AS id_order, a.user_id, e.name AS user_name, e.phone AS user_phone, e.email AS user_email,
					   a.receive_name, a.receive_address, a.receive_province_id, a.receive_regency_id, a.receive_phone, 
					   a.user_addr_id, a.total_price, a.total_tax, a.total_cart, 
					   a.total, a.date_created, a.date_updated, 
					   a.invoice_code, a.invoice_date, a.key_transfer, a.payment_method, a.payment_status, a.status,
					   b.id AS id_cart, b.product_id, b.quantity, c.product_code, c.name AS product_name, c.price, c.weight, c.description, c.created_by,
					   d.province_name, d.type AS regency_type, d.name AS regency_name, d.postal_code
				FROM cart_order AS a
				LEFT JOIN cart AS b ON b.order_id=a.id AND b.user_id=a.user_id AND b.status=1
				LEFT JOIN product AS c ON c.id=b.product_id
				LEFT JOIN regency AS d ON d.regency_id=a.receive_regency_id AND d.province_id=a.receive_province_id
				LEFT JOIN user AS e ON e.id=a.user_id
				WHERE a.status=1 $filter ORDER BY a.user_id, a.id, b.id ASC;";					
			$query = $this->db->query($sql);
			return $query;
	}
	
	public function get_review_order_items($data)
	{
		$query = $this->_query_review_order($data);
			$dataperuser    = array(); 
			$datapernoorder = array(); 
			$datadetil      = array();
			if($query->num_rows() > 0){			
				$cur_user    = '#'; 
				$cur_noorder = '#'; 
				$counter    = 0;
				$this->load->model('m_cart');
				$this->load->model('m_user_kurir');
		    					
				foreach($query->result() as $row){
					$counter++;
					if($cur_user != $row->user_id){
						$cur_user = $row->user_id;
						$dataperuser[$cur_user]['user_id']    = isset($row->user_id)? $row->user_id:'';
						$dataperuser[$cur_user]['user_name']  = isset($row->user_name)? $row->user_name:'';
						$dataperuser[$cur_user]['user_phone'] = isset($row->user_phone)? $row->user_phone:'';
						$dataperuser[$cur_user]['user_email'] = isset($row->user_email)? $row->user_email:'';
						$dataperuser[$cur_user]['subtotaluser'] = 1;
						$cur_noorder = '#';
						$datapernoorder      = array(); 
					}else{
						$dataperuser[$cur_user]['subtotaluser']++;
					}
					if($cur_noorder != $row->id_order){
						$cur_noorder = $row->id_order; 
						$datapernoorder[$cur_noorder]['id_order']       = isset($row->id_order)? $row->id_order:'';
						$datapernoorder[$cur_noorder]['invoice_code']   = isset($row->invoice_code)? $row->invoice_code:'';
						$datapernoorder[$cur_noorder]['invoice_date']   = isset($row->invoice_date)? $row->invoice_date:'';
						$datapernoorder[$cur_noorder]['key_transfer']   = isset($row->key_transfer)? $row->key_transfer:'';
						$datapernoorder[$cur_noorder]['payment_method'] = isset($row->payment_method)? $row->payment_method:'';
						$datapernoorder[$cur_noorder]['payment_status'] = isset($row->payment_status)? $row->payment_status:'';
						$datapernoorder[$cur_noorder]['user_addr_id'] = isset($row->user_addr_id)? $row->user_addr_id:'';
						$arrCart=array();
						$carts = $this->m_cart->get_all_items($row->user_id,$row->id_order);
						foreach ($carts as $key => $cart) {
							$arrCart[]=array(
								'cart_id'=>$cart['cart_id'],
								'product_id'=>$cart['id'],
								'qty'=>$cart['quantity'],
							);				
						}
											
						$delivery_desc='';
						$delivery_rate=0;
						foreach($arrCart as $_cart){
							$_cart['user_addr_id']=$row->user_addr_id;
							$get_all_items =$this->m_user_kurir->get_all_items($_cart);
									if(!empty($get_all_items)){
										foreach ($get_all_items as $user_address){
											
											$delivery_rate+= $user_address['delivery_rate']*$_cart['qty'];
											$delivery_desc.= $user_address['delivery_desc'];
										}
									}
						}
						$datapernoorder[$cur_noorder]['delivery_rate']  = isset($delivery_rate)? $delivery_rate:'';
						$datapernoorder[$cur_noorder]['delivery_desc']  = isset($delivery_desc)? $delivery_desc:'';
						/*$datapernoorder[$cur_noorder]['delivery_rate']  = isset($row->delivery_rate)?$row->delivery_rate:'';
						*/
						$datapernoorder[$cur_noorder]['total_tax'] 	    = isset($row->total_tax)?$row->total_tax:'';
						$datapernoorder[$cur_noorder]['total']       	= isset($row->total)?$row->total:'';
						$datapernoorder[$cur_noorder]['receive_name']   = isset($row->receive_name)? $row->receive_name:'';
						$datapernoorder[$cur_noorder]['receive_address']= isset($row->receive_address)? $row->receive_address:'';
						$datapernoorder[$cur_noorder]['province_name']  = isset($row->province_name)? $row->province_name:'';
						$datapernoorder[$cur_noorder]['regency_type']   = isset($row->regency_type)? $row->regency_type:'';
						$datapernoorder[$cur_noorder]['regency_name']   = isset($row->regency_name)? $row->regency_name:'';
						$datapernoorder[$cur_noorder]['postal_code']    = isset($row->postal_code)? $row->postal_code:'';
						$datapernoorder[$cur_noorder]['receive_phone']  = isset($row->receive_phone)? $row->receive_phone:'';
						$datapernoorder[$cur_noorder]['subtotalorder']= 1;
						$datadetil           = array('no_urut'=>0);
					}else{
						$datapernoorder[$cur_noorder]['subtotalorder']++;  
					}
						$datadetil['no_urut']++;
						$datadetil['product_id'] 	= isset($row->product_id)?$row->product_id:'';
						$datadetil['product_name'] 	= isset($row->product_name)?$row->product_name:'';
						$datadetil['description'] 	= isset($row->description)?$row->description:'';
						$datadetil['price'] 	    = isset($row->price)?$row->price:'';
						$datadetil['weight'] 	    = isset($row->weight)?$row->weight:'';
						$datadetil['quantity'] 	    = isset($row->quantity)?$row->quantity:'';
						$datadetil['total_price'] 	= isset($row->total_price)?$row->total_price:'';
						$datadetil['total_cart'] 	= isset($row->total_cart)?$row->total_cart:'';
						$datadetil['total']     	= isset($row->total)?$row->total:'';
							
						$datapernoorder[$cur_noorder]['detil'][$datadetil['no_urut']]  = $datadetil;
						$dataperuser[$row->user_id]['dataperuser']= $datapernoorder;
				}
			
			}
			return $dataperuser;
	} 
	
	public function save_daily_order($array_data)
	{
		return $this->db->insert($this->table_daily_order, $array_data);
	}
	
	public function get_lastId_daily_order(){
		$today=date('Y-m-d');
		$sql="SELECT IFNULL(MAX(key_transfer_code)+1,1) AS nomor FROM daily_order
			  WHERE date_created='".$today."'";
		$resQuery  = $this->db->query($sql)->row();
		$_nomor    = $resQuery->nomor;
		$lastNomor = sprintf("%03d",$_nomor);
		return $lastNomor;	  
	}
	
	public function check_key_random($key){
		$sql="SELECT COUNT(key_transfer) AS jumlah 
				FROM cart_order 
					WHERE payment_method='transfer' AND payment_status ='4' 
						AND key_transfer='".$key."';";
		$query  = $this->db->query($sql)->row();
		$jumlah = $query->jumlah;
		return $jumlah;
	}
	
	public function get_last_top_delivery($id,$limit=3)
	{
		$sql="SELECT a.id,user_id, receive_name,receive_address,receive_province_id, receive_regency_id,receive_phone,
					b.province_name, b.type, b.name, b.postal_code
				FROM cart_order as a
					LEFT JOIN regency AS b ON b.province_id=a.receive_province_id AND b.regency_id=a.receive_regency_id
						WHERE user_id='".$id."'
							ORDER BY a.id DESC LIMIT $limit;";
		$query = $this->db->query($sql);
		$array = $query->result_array();

		$query->free_result();
		return $array;	  
	} 
	
}

