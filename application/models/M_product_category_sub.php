<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_product_category_sub extends CI_Model
{
  protected $table_name = 'product_category_sub';

  public function __construct()
  {
    $this->load->database();
  }

  public function save($name, $category_id)
  {
    $data = array(
      'name'         => $name,
      'category_id'  => $category_id,
      'date_created' => date('Y-m-d H:i:s')
    );

    return $this->db->insert($this->table_name, $data);
  }

  public function get_all_items_by_category_id($category_id) 
  {
    $this->db->from($this->table_name);
    $this->db->where('status', '1');
    $this->db->where('category_id', $category_id); 
    $this->db->order_by('name ASC');

    $query = $this->db->get();
    $array = $query->result_array();

    $query->free_result();
    unset($query);

    return $array;
  }

  public function get_all_items() 
  {
    $this->db->from($this->table_name);
    $this->db->where('status', '1');
    $this->db->order_by('name ASC');

    $query = $this->db->get();
    $array = $query->result_array();

    $query->free_result();
    unset($query);

    return $array;
  }

  public function delete($id)
  {
    $this->db->where('id', $id);
    return $this->db->update($this->table_name, array('status' => '0'));
  }
}
