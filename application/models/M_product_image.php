<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_product_image extends CI_Model
{
  protected $table_name = 'product_image';

  public function __construct()
  {
    $this->load->database();
  }

  public function get_all_items_by_product_code($user_id, $product_code)
  {
    $this->db->from($this->table_name);
    $this->db->where('product_code', $product_code);
    $this->db->where('created_by', $user_id);

    $query = $this->db->get();
    $array = $query->result_array();

    $query->free_result();
    return $array;
  }

  public function get_show_image($user_id, $product_code)
  {
    $this->db->from($this->table_name);
    $this->db->where('created_by', $user_id);
    $this->db->where('product_code', $product_code);

    $query = $this->db->get();
    $array = $query->row_array();

    $query->free_result();
    return $array;
  }

  public function save($image_name, $product_code, $user_id)
  {
    $this->db->where('product_code', $product_code);
    $this->db->where('created_by', $user_id);

    $total = $this->db->count_all_results($this->table_name);

    if ($total > 0) {
      $is_show = '0';
    } else {
      $is_show = '1';
    }

    return $this->db->insert($this->table_name, array(
      'image'        => $image_name,
      'product_code' => $product_code,
      'date_created' => date('Y-m-d H:i:s'),
      'is_show'      => $is_show,
      'created_by'   => $user_id
    ));
  }

  public function reset_primary_image($user_id, $product_code)
  {
    $this->db->where('created_by', $user_id);
    $this->db->where('product_code', $product_code);

    return $this->db->update($this->table_name, array(
      'is_show' => '0'
    ));
  }

  public function update_primary_image($image_id, $user_id, $product_code)
  {
    $this->db->where('created_by', $user_id);
    $this->db->where('product_code', $product_code);
    $this->db->where('id', $image_id);

    return $this->db->update($this->table_name, array(
      'is_show' => '1'
    ));
  }

  public function check_image_name($image_name)
  {
    $this->db->where('image', $image_name);
    $total = $this->db->count_all_results($this->table_name);

    if ($total > 0) {
      return true;
    }

    return false;
  }

  public function delete($id, $user_id)
  {
    $this->db->where('created_by', $user_id);
    $this->db->where('id', $id);
    return $this->db->delete($this->table_name);
  }
}
