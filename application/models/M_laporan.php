<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_laporan extends CI_Model {

	private $table;
	private $tabledata;
	private $db2;
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'vw_data_cashback';
		$this->tabledata = 'vw_transaksi_member_payment';
		//$this->mbrcode = $this->session->userdata('mbr_code');
		$dt=$this->session->userdata('member');
		//die(json_encode($dt['eklanku_id']));//('eklanku_id').$this->session->userdata('email');
		$this->mbrcode = $dt['eklanku_id'];
        //$this->_username = $dt['email'];
        $ci = &get_instance();
        $this->db2 = $ci->load->database('pgsql', TRUE);
        
	}
	public function record_count() {
		$this->db2->where('member_code', $this->mbrcode);
		$query = $this->db2->get($this->table);
		return $query->num_rows();
    }
	public function record_count_cari($cari) {
		$sql = "select * from ".$this->table." where member_code = '".$this->mbrcode."' and downline like '%".$cari."%'  order by castback_date desc";
        $query = $this->db2->query($sql);
		return $query->num_rows();
    }
	
	function get_department_list($limit, $start)
    {
        $sql = "select * from ".$this->table." where member_code = '".$this->mbrcode."' order by castback_date desc OFFSET " . $start . " limit " . $limit;
        $query = $this->db2->query($sql);
        return $query->result();
    }
	function getData()
    {
        $sql = "select * from ".$this->table." where member_code = '".$this->mbrcode."' order by castback_date desc  ";
        $query = $this->db2->query($sql);
        return $query;
    }
	function get_department_list_cari($cari,$limit, $start)
    {
        $sql = "select * from ".$this->table." where member_code = '".$this->mbrcode."' and downline like '%".$cari."%'  order by castback_date desc OFFSET " . $start . " limit " . $limit;
        $query = $this->db2->query($sql);
        return $query->result();
    }
	
	//Payment
	public function record_count_payment() {
		$this->db2->where('member_code', $this->mbrcode);
		$query = $this->db2->get($this->tabledata);
		return $query->num_rows();
    }
	public function record_count_caripayment($cari) {
		$sql = "select * from ".$this->tabledata." where member_code = '".$this->mbrcode."' and transaksi_code like '%".$cari."%'  order by transaksi_date desc";
        $query = $this->db2->query($sql);
		return $query->num_rows();
    }
	
	function get_department_listpayment($limit, $start)
    {
        $sql = "select * from ".$this->tabledata." where member_code = '".$this->mbrcode."' order by transaksi_date desc OFFSET " . $start . " limit " . $limit;
        $query = $this->db2->query($sql);
        return $query->result();
    }
    function get_department_listpayment_api($mbrcode, $limit, $start)
    {
        $sql = "select * from $this->tabledata where member_code = '$mbrcode' order by transaksi_date desc OFFSET $start limit $limit";
        $query = $this->db2->query($sql);
        return $query->result();
    }
	function getDatapayment()
    {
        $sql = "select * from ".$this->tabledata." where member_code = '".$this->mbrcode."' order by transaksi_date desc  ";
        $query = $this->db2->query($sql);
        return $query;
    }
	function get_department_list_caripayment($cari,$limit, $start)
    {
        $sql = "select * from ".$this->tabledata." where member_code = '".$this->mbrcode."' and transaksi_code like '%".$cari."%'  order by transaksi_date desc OFFSET " . $start . " limit " . $limit;
        $query = $this->db2->query($sql);
        return $query->result();
    }
    function get_datatrx($cari)
    {
        $sql = "select * from ".$this->tabledata." where member_code = '".$this->mbrcode."' and transaksi_code = '".$cari."'";
        $query = $this->db2->query($sql);
        return $query->result();
    }
}