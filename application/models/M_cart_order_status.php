<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_cart_order_status extends CI_Model
{
  protected $table_name = 'cart_order_status';

  public function __construct()
  {
    $this->load->database();
  }

  public function save_order_status($array_data)
  {
    return $this->db->insert($this->table_name, $array_data);
  }
  
  public function update_order_status($data)
  {
    $this->db->where('id', $data['id']);
	$this->db->where('cart_order_id', $data['cart_order_id']);
	
	$this->db->update($this->table_name, $data);
  }
  
	public function get_all_items($data)
	{
		$this->db->select("*");
		$this->db->from($this->table_name);
		
		if(isset($data['id']))$this->db->where('id', $data['id']);
		if(isset($data['cart_order_id']))$this->db->where('cart_order_id', $data['cart_order_id']);
		$this->db->where('status','1'); 
		$query = $this->db->get(); print_r($this->db->last_query()); die();
		$array = $query->result_array();

		$query->free_result();
		return $array;
	}
   
	public function validate_cart_order_status($params)
	{
		extract($params);
		$errors = array();
		$norek_asal=trim(str_replace('-','',$norek_asal));
		$nominal_transfer=trim(str_replace(',','',$nominal_transfer));
		
		if (empty($nama_nasabah)) {
		  $errors['nama_nasabah'] = 'Nama Bank /Pengirim Transfer harus diisi.';
		} 
		
		if (empty($norek_asal)) {
		  $errors['norek_asal'] = 'Bank Asal/Nasabah Transfer harus diisi.';
		} 
		
		if (empty($bank_id)) {
		  $errors['bank_id'] = 'Bank Asal Transfer harus diisi.';
		} 
		if (empty($bank_tujuan_id)) {
		  $errors['bank_tujuan_id'] = 'Bank Tujuan Transfer harus diisi.';
		} 
		
		if (empty($nominal_transfer)) {
		  $errors['nominal_transfer'] = 'Nilai nominal Transfer harus diisi.';
		} elseif ($nominal_transfer < 1000) {
		  $errors['nominal_transfer'] = 'Nilai transfer minimal Rp 1.000';
		}
		
		if (empty($tgl_transfer)) {
		  $errors['tgl_transfer'] = 'Tanggal Transfer harus diisi.';
		} 
		
		return $errors;
	}
	
}

