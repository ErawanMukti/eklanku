<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_iklan extends CI_Model {

	private $table;
	private $db2;
	private $ci;
	
	public function __construct()
	{
		parent::__construct();

		$this->table = 'iklan_member';
		//$this->mbrcode = $this->session->userdata('mbr_code');
		$dt=$this->session->userdata('member');
		$this->mbrcode = $dt['eklanku_id'];
        $ci = &get_instance();
        $this->db2 = $ci->load->database('pgsql', TRUE);
        
        $this->ci = &get_instance();
        $this->ci->load->library('egc');
		
	}
	public function getDesKrip($codeIklan)
	{
		$iResult = "";
		$where = "iklan_status = 'Active' and link_iklan = '".$codeIklan."' ";
		$this->db2->where($where);		
		$data = $this->db2->get('vw_iklan_detail');

		foreach ($data->result() as $row)
		{
		     $iResult = $row->iklan_code."|".$row->iklan_title."|".$row->iklan_image."|".$row->iklan_deskripsi."|".$row->iklan_kategory."|".$row->iklan_price."|".$row->provinsi."|".$row->view_iklan."|".$row->email."|".$row->member_mobile."|".$row->rate_iklan."|".$row->iklan_review."|".$row->link_iklan."|".$row->category_name;
		}						
		return $iResult;				
	}

    public function getIklanShare()
	{
		$iResult = "";
		$data = $this->db2->get('vw_iklan_share');	
        foreach ($data->result() as $row)
		{
		     $iResult = $row->iklan_image."|".$row->iklan_isi;
		}						
		return $iResult;		
				
	}
	
	 public function record_count($code) {
	$this->db2->where('iklan_status', 'Active');
	if(!empty($code))
		$this->db2->where('iklan_kategory', $code);
    $query = $this->db2->get($this->table);
	return $query->num_rows();
    }
    
	function get_department_list($limit, $start, $code)
    {
	$sql = "select * from iklan_member where iklan_kategory = '".$code."' and iklan_status = 'Active' order by iklan_date desc OFFSET " . $start . " limit " . $limit;
    $query = $this->db2->query($sql);
        return $query->result();
    }
    public function record_countSearchBy($code) {   
        $code=empty($code)?'':"(iklan_kategory like '%".$code."%' or iklan_title like '%".$code."%' )and ";
    	$sql = "select * from vw_iklan_detail where ".$code." iklan_status = 'Active' order by iklan_date desc";
        $query = $this->db2->query($sql);
        return $query->num_rows();
    }
    
	public function doCsearchBy($code,$_order='',$_dir='',$limit, $start)
	{
			$code=empty($code)?'':"(iklan_kategory like '%".$code."%' or iklan_title like '%".$code."%' )and ";
    		
			$pos='';
					if($_order=='price')
						$pos='iklan_price '.(empty($_dir)?'desc':$_dir);
					else if ($_order=='name')
						$pos='trim(iklan_title) '.(empty($_dir)?'desc':$_dir);
					else
						$pos='iklan_date '.(empty($_dir)?'desc':$_dir);
			
			$pos.=empty($pos)?'':',';
			//$page=" OFFSET " . $start . " limit " . $limit;
				
			$where = "iklan_status = 'Active' and type_iklan = 'Reguler' order by $pos view_iklan desc OFFSET " . $start . " limit " . $limit;
		    $where=empty($code)?$where:$code." ".$where;
		   // die($where);
		    $this->db2->where($where);
			$data = $this->db2->get('vw_iklan_detail');
			return $data;
	}

	public function getCounter()
	{
		$iResult = 0;
		$this->db2->select_max('iklan_counter','max_counter');
		$query = $this->db2->get($this->table);	
		foreach ($query->result() as $row)
		{
		    $iResult = $row->max_counter;
		}						
		return $iResult;
	}
	public function getCodeIklan($kode)
	{
		$this->db2->where('link_iklan', $kode);
		$data = $this->db2->get($this->table);
		if ($data->num_rows() > 0)
		{
			return TRUE;	
			
		}
		else
		{
			return FALSE;
		}	
	}
	public function getCodeNamaPosting($koment_name,$iklancode)
	{
		$this->db2->where('member_name', $koment_name);
		$this->db2->where('link_iklan', $iklancode);
		$data = $this->db2->get('vw_iklan_detail');
		if ($data->num_rows() > 0)
		{
			return False;	
			
		}
		else
		{
			return True;
		}	
	}
	

	public function getCodeNamaEmail($koment_name,$iklancode)
	{
		$this->db2->where('email', $koment_name);
		$this->db2->where('link_iklan', $iklancode);
		$data = $this->db2->get('vw_iklan_detail');
		if ($data->num_rows() > 0)
		{
			return False;	
			
		}
		else
		{
			return True;
		}	
	}
	public function getNameComent($kode,$name)
	{
		$this->db2->where('iklan_code', $kode);
		$this->db2->where('koment_name', $name);
		$data = $this->db2->get('koment_iklan');
		if ($data->num_rows() > 0)
		{
			return False;	
			
		}
		else
		{
			return True;
		}	
	}
	
    public function doSubmit()
	{
		$komentKode			= $this->input->post("kodeIklan");
		$namePosting		= $this->input->post("nama");
		$emailPosting		= $this->input->post("email");
		$komentarPosting	= $this->input->post("komentar");
		$ratingPosting		= $this->input->post("rating");
		if($namePosting == null){
			return "Masukan Nama Anda terlebih dahulu.";
		}else if($emailPosting == null){
			return "Masukan Email Anda terlebih dahulu.";
		}else if($komentarPosting == null){
			return "Masukan Komentar Anda terlebih dahulu.";
		}else {
			if($this->getCodeIklan($komentKode) == true)
			{
				if($this->getNameComent($komentKode,$namePosting) == true)
				{
					if($this->getCodeNamaPosting($namePosting,$komentKode) == true)
					{
						if($this->getCodeNamaEmail($emailPosting,$komentKode) == true)
						{
						
							$koment_kode	= 'CK' . $this->egc->genRandomString(8);
							$koment_date	= date("Y-m-d H:i:s");		
												
							$this->db2->set("koment_code", $koment_kode);
							$this->db2->set("iklan_code", $komentKode);
							$this->db2->set("koment_text", $komentarPosting);
							$this->db2->set("koment_date", $koment_date);
							$this->db2->set("koment_rating", $ratingPosting);
							$this->db2->set("koment_name", $namePosting);
							if($this->db2->insert('koment_iklan')==TRUE)
							{
								return "Sukses Simpan";
							}
							else
							{
								return "Submit Komentar failed.";
							}
						}else{
							return "Email terdaftar sebagai pemilik iklan silakan inputkan email yang lain.";
							
						}
					}else{
						return "Pemilik iklan di larang menulis komentar iklanya sendiri, silakan login untuk membalas komentar.";
						
					}
				}else{
					return "Anda sudah pernah mengomentari iklan ini, silakan kontak nomer HP/email untuk melanjutkan transaksi.";
				}
			}else{
				return "Iklan tersebut tidak terdaftar di Sistem kami!";
			}
		}
	}
		
	 
	
	
	public function doCreate()
	{
		$ikltitle		= $this->input->post("title");
		$iklkategory   	= $this->input->post("kategory");
		$ikldeskripsi   = $this->input->post("deskripsi");
		$iklProve   	= $this->input->post("daerah");
        $harga   	    = $this->input->post("harga");
        $gbr=array();
        $gbr[]=$this->input->post("gambar1");$gbr[]=$this->input->post("gambar2");
        $gbr[]=$this->input->post("gambar3");$gbr[]=$this->input->post("gambar4");
        $gbr[]=$this->input->post("gambar5");
		
		$iklCode		= 'IKL' . $this->ci->egc->genRandomString(13);				
					
					
		$this->db2->set("iklan_code", $iklCode);
		$this->db2->set("iklan_title", $ikltitle);
		$this->db2->set("iklan_kategory", $iklkategory);
		$this->db2->set("member_code", $this->mbrcode);
		$this->db2->set("iklan_deskripsi", $ikldeskripsi);
		$this->db2->set("iklan_price", $harga);
		$this->db2->set("iklan_image", $gbr[0]);
		$this->db2->set("iklan_image2", $gbr[1]);
		$this->db2->set("iklan_image3", $gbr[2]);
		$this->db2->set("iklan_image4", $gbr[3]);
		$this->db2->set("iklan_image5", $gbr[4]);
		$this->db2->set("type_iklan", 'Reguler');
		$this->db2->set("iklan_date",date("Y-m-d H:i:s"));
		$this->db2->set("view_iklan",0);
		$this->db2->set("link_iklan",$iklCode);
		$this->db2->set("rate_iklan",4);
		$this->db2->set("iklan_review",0);
		$this->db2->set("provinsi",strtoupper($iklProve));
		$this->db2->set("iklan_status", 'Reserved');		
		if($this->db2->insert($this->table)==TRUE)
		{
			$dirSrc=dirname($_SERVER["SCRIPT_FILENAME"]).'/assets/uploader/tmp/';
			$dirDes=dirname($_SERVER["SCRIPT_FILENAME"]).'/assets/receipt/';
			for($i=0;$i<count($gbr);$i++){
				if(!empty($gbr[$i])){	
					$arrGbr=explode('.',$gbr[$i]);
					$_gbr=$arrGbr[0].'.'.$arrGbr[1];
					$this->egc->rcopy($dirSrc.$_gbr,$dirDes.$_gbr);
					//$_gbr=$arrGbr[0].'150x200.'.$arrGbr[1];
					//$this->egc->rcopy($dirSrc.$_gbr,$dirDes.$_gbr);
					//$_gbr=$arrGbr[0].'750x1000.'.$arrGbr[1];
					//$this->egc->rcopy($dirSrc.$_gbr,$dirDes.$_gbr);
				}
			}
			return TRUE;
		}					
		else
		{
			return FALSE;
		}
				
	}
	
	public function doReadCategory()
	{
		$data = $this->db2->get('category_iklan');
		return $data;
	}
	public function doReadCategoryByCode($kodecata)
	{
		$name_cata="";
		$this->db2->where('category_code', $kodecata);
		$data = $this->db2->get('category_iklan');
		if ($data->num_rows() > 0)
		{
			$data = $data->row();
			$name_cata = $data->category_name;
			
		}	
		return $name_cata;
	}
	public function doReadComent($kodecata)
	{
		$this->db2->where('link_iklan', $kodecata);
		$data = $this->db2->get('vw_koment_detail');
		return $data;
	}
	
	
	public function doViewCategory($codeIklan)
	{
		$where = "iklan_status = 'Active' and type_iklan = 'Reguler' and category_code = '".$codeIklan."' ";
		$this->db2->where($where);		
		$data = $this->db2->get('vw_iklan_detail');
		return $data;
	}
	public function doViewDetailCategory($codeIklan)
	{
		$where = "iklan_status = 'Active' and link_iklan = '".$codeIklan."' ";
		$this->db2->where($where);		
		$data = $this->db2->get('vw_iklan_detail');
		return $data;
	}
	
	
	public function doRead()
	{
		$where = "iklan_status = 'Active' and type_iklan = 'Reguler' order by iklan_date desc limit 50 ";
		$this->db2->where($where);
		$data = $this->db2->get('vw_iklan_detail');
		return $data;
	}
	public function doReadSaya()
	{
		
		$this->db2->where('member_code', $this->mbrcode);
		$data = $this->db2->get('vw_iklan_detail');
		return $data;
	}
	public function doReadMy($reff)
	{
		//$member_code = $this->getReff($reff);
		//$this->db2->where('member_code', $member_code);
		//$this->db2->where('type_iklan', 'Reguler');
		//$this->db2->where('iklan_status', 'Active');
		//$data = $this->db2->get('vw_iklan_detail');
		//if ($data->num_rows() > 0)
		//{
		//	return $data;
		//}
		//else
		//{
			$where = "iklan_status = 'Active' and type_iklan = 'Reguler' order by view_iklan desc limit 5 ";
		    $where=empty($reff)?$where:"iklan_kategory = '".$reff."' and ".$where;
		    $this->db2->where($where);
			$data = $this->db2->get('vw_iklan_detail');
			return $data;
		//}
	}
	
	public function doCcategory($category,$_order='',$_dir='',$limit, $start)
	{
			$pos='';
			if($category=='populer')
				$category='';
			elseif($category=='default'){
				$category='';$pos='iklan_date '.(empty($_dir)?'desc':$_dir);
			}
			else{
				if($_order=='price')
					$pos='iklan_price '.(empty($_dir)?'desc':$_dir);
				else if ($_order=='name')
					$pos='trim(iklan_title) '.(empty($_dir)?'desc':$_dir);
				else
					$pos='iklan_date '.(empty($_dir)?'desc':$_dir);
			}
			
			$pos.=empty($pos)?'':',';
			//$page=" OFFSET " . $start . " limit " . $limit;
				
			$where = "iklan_status = 'Active' and type_iklan = 'Reguler' order by $pos view_iklan desc OFFSET " . $start . " limit " . $limit;
		    $where=empty($category)?$where:"iklan_kategory = '".$category."' and ".$where;
		   // die($where);
		    $this->db2->where($where);
			$data = $this->db2->get('vw_iklan_detail');
			return $data;
	}
	
	public function getReff($usrName)
	{
		$iResult ='MBR0000058';
		$this->db2->where('member_reff', str_replace("'","",$usrName));
		$data = $this->db2->get('member_list');
		if ($data->num_rows() > 0)
		{
			$data = $data->row();
			$iResult = $data->member_code;
			
		}
		return $iResult;
	}
	
	public function doViewEdit($codeIklan)
	{
		$this->db2->where("acc_code",$this->acccode);
		$this->db2->where("iklan_code",$codeIklan);
		$data = $this->db2->get('vw_iklan_detail');
		return $data;
	}
	public function doReadHistory()
	{
		$where = "acc_code = '" . $this->acccode . "' and iklan_status <> 'Reserved' ";
		$this->db2->where($where);		
		$data = $this->db2->get('vw_iklan_detail');
		
		return $data;
	}
	public function doViewIklan()
	{
		$where = "iklan_status = 'Active' and type_iklan = 'Reguler' order by iklan_date desc limit 16 ";
		$this->db2->where($where);		
		$data = $this->db2->get('vw_iklan_detail');
		
		return $data;
	}
	
	public function doViewIklanAll($code)
	{
		//$this->db2->limit($limit, $start);
		$where = "iklan_status = 'Active' and type_iklan = 'Reguler' and iklan_kategory = '".$code."' ";
		$this->db2->where($where);		
		$data = $this->db2->get('vw_iklan_detail');
        return $data;
        
	}
	public function doViewIklanPrenium()
	{
		$where = "iklan_status = 'Active' and type_iklan = 'Prenium' ";
		$this->db2->where($where);		
		$data = $this->db2->get('iklan_member');
		
		return $data;
	}
	
	
	
	public function doViewIklanPremiumAndro(){
		$sql = "select iklan_image,iklan_title,link_iklan,iklan_price from iklan_member where iklan_status = 'Active' and type_iklan = 'Prenium'";
        $query = $this->db2->query($sql);
     
        return array("result"=>$query->result());
	}
	
	public function doReadMyAndro($reff)
	{
			$where = "iklan_status = 'Active' and type_iklan = 'Reguler' order by view_iklan desc limit 6 ";
		    $where=empty($reff)?$where:"iklan_kategory = '".$reff."' and ".$where;
		    //die($where);
			$this->db2->where($where);
			$data = $this->db2->get('vw_iklan_detail');
			return array("result"=>$data->result());
	}
	public function doReadAndro($reff)
	{
		$where = "iklan_status = 'Active' and type_iklan = 'Reguler' order by iklan_date desc limit 50 ";
		$where=empty($reff)?$where:"iklan_kategory = '".$reff."' and ".$where;
		$this->db2->where($where);
		$data = $this->db2->get('vw_iklan_detail');
		return array("result"=>$data->result());
	}
	
	public function doReadAndroDetail($reff)
	{
		$where = "iklan_status = 'Active' and type_iklan = 'Reguler' order by iklan_date desc limit 50 ";
		$where=empty($reff)?$where:"link_iklan = '".$reff."' and ".$where;
		$this->db2->where($where);
		$data = $this->db2->get('vw_iklan_detail');
		$res=$data->result();
		$_res=$res[0];
		//die($_res->iklan_image);
		$_res->image=array($_res->iklan_image//,"0bbe4d85f7ed789e1f4cda79aca442f81.jpg","0ab13ea0d490f7bf8d0322561bc8bdca1.jpg"
		);
		$_res->review=array($_res->iklan_review);		
		$_res->iklan_price=$this->egc->accounting_format($_res->iklan_price);		
		$_res->rate_iklan=round($_res->rate_iklan);
		return $_res;		
	}
///sampe disini
	public function doUpdate()
	{
		$typ_code		= $this->input->post("typ_code");
		$ikltitle		= $this->input->post("title");
		$iklkategory   	= $this->input->post("kategory");
		$iklcontact   	= $this->input->post("contact");
		$email       	= $this->input->post("email");
		$name       	= $this->input->post("name");
		$ikldeskripsi   = $this->input->post("deskripsi");
		$iklProve   	= $this->input->post("provinsi");
		
		

		if(($ikltitle != null) && ($typ_code!=null) && ($iklkategory != null) && ($iklcontact != null) && ($email != null) && ($name != null) && ($ikldeskripsi != null) && ($iklProve != null)){
			$config['file_name']=$typ_code;
			$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"]).'/assets/receipt/';
			$config['upload_url'] = base_url()."assets/receipt/";
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['overwrite'] = TRUE;
			$config['max_size']	= '350';
			$config['max_width']  = '1024';
			$config['max_height']  = '768';

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload())
			{
					
				return FALSE;
			}
				
			else
			{
				
				$cnfFil 		= '';
					
				foreach ($this->upload->data() as $item => $value)
				{
					if ($item == 'file_name')
					{
						$cnfFil = $value;	
					}				
				}
				
				$this->db2->set("email", $email);
				$this->db2->set("name", $name);
				$this->db2->set("prove_code", $iklProve);
				$this->db2->set("iklan_date",date("Y-m-d H:i:s"));
				$this->db2->set("iklan_title", $ikltitle);
				$this->db2->set("iklan_kategory", $iklkategory);
				$this->db2->set("iklan_contact", $iklcontact);
				$this->db2->set("iklan_deskripsi", $ikldeskripsi);
				$this->db2->set("iklan_image", $cnfFil);
				$this->db2->set("iklan_date",date("Y-m-d H:i:s"));
				$this->db2->set("iklan_status", 'Reserved');	
				
				$this->db2->where("iklan_code", $typ_code);
				if($this->db2->update($this->table)==TRUE)
				{
					return TRUE;
				}
				else
				{
					return FALSE;
				}	
			}	
		}else{
			return FALSE;
		}
	}
   public function doReadIklanCek($judul,$kategory)
	{
		$this->db2->where("member_code",$this->mbrcode);
		$this->db2->where("iklan_title",$judul);
		$this->db2->where("iklan_kategory",$kategory);
		
		$data = $this->db2->get($this->table);
		if ($data->num_rows() > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
    
	public function doVieW($iklanCode,$jmlView)
	{
		
		$this->db2->set("view_iklan",$jmlView + 1);
		$this->db2->where("link_iklan", $iklanCode);
		if($this->db2->update($this->table)==TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function doActivation($accNumber)
	{
		$this->db2->set("iklan_status", 'Active');
		$this->db2->where("iklan_code", $accNumber);
		if($this->db2->update($this->table)==TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
 
        public function record_countSaya() {
	$this->db2->where('member_code', $this->mbrcode);
    $query = $this->db2->get('vw_iklan_detail');
	return $query->num_rows();
    }
	
	function get_department_listSaya($limit, $start)
    {
        $sql = "select * from vw_iklan_detail where member_code = '".$this->mbrcode."' order by iklan_date desc OFFSET " . $start . " limit " . $limit;
        $query = $this->db2->query($sql);
        return $query->result();
    }

	public function doDelete($idiklan)
	{
		$this->db2->where("iklan_code",$idiklan);
		if($this->db2->delete($this->table)==TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}			
	}
}
