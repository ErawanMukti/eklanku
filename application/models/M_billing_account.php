<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_billing_account extends CI_Model
{
  protected $table_name = 'billing_account';

  public function __construct()
  {
    $this->load->database();
  }

  public function get_all_items($data='')
  {
    $this->db->from($this->table_name);
	if(!empty($data)){
		foreach($data as $key=>$val)
			$this->db->where($key,$val);
	}
    $query = $this->db->get();
    $array = $query->result_array();

    $query->free_result();

    return $array;
  }
  
  
	function getBillingOneField($id,$field)
	{
		if(empty($id))return '';
		$this->db->select($field);
		$this->db->where('id', $id);
		$row = $this->db->get($this->table_name)->row();
		return isset($row->$field)?($row->$field):$field;
	}

}
