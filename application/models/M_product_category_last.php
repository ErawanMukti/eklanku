<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_product_category_last extends CI_Model
{
  protected $table_name = 'product_category_last';

  public function __construct()
  {
    $this->load->database();
  }

  public function save($name, $category_id)
  {
    $data = array(
      'name'         => $name,
      'category_id'  => $category_id,
      'date_created' => date('Y-m-d H:i:s')
    );

    return $this->db->insert($this->table_name, $data);
  }

  public function get_all_items_by_category_id($category_id) 
  {
    $this->db->from($this->table_name);
    $this->db->where('status', '1');
    $this->db->where('category_id', $category_id); 
    $this->db->order_by('name ASC');

    $query = $this->db->get();
    $array = $query->result_array();

    $query->free_result();
    unset($query);

    return $array;
  }

  public function get_all_items() 
  {
    $this->db->from($this->table_name);
    $this->db->where('status', '1');
    $this->db->order_by('name ASC');

    $query = $this->db->get();
    $array = $query->result_array();

    $query->free_result();
    unset($query);

    return $array;
  }

  public function delete($id)
  {
    $this->db->where('id', $id);
    return $this->db->update($this->table_name, array('status' => '0'));
  }
  
  public function getBreadcrumbs_by_category($id){
	$sql="SELECT DISTINCT c.name AS cat, b.name AS sub_cat,  a.name AS last_cat 
				FROM product_category_last AS a 
					LEFT JOIN product_category_sub AS b ON b.id=a.category_id
					LEFT JOIN product_category AS c ON c.id=b.category_id
						WHERE c.id='".$id."'";
	$query = $this->db->query($sql)->row();
	return $query;	
  }
  
  public function getBreadcrumbs_by_sub_category($id){
	$sql="SELECT DISTINCT c.id as cat_id, c.name AS cat, b.name AS sub_cat,  a.name AS last_cat 
				FROM product_category_last AS a 
					LEFT JOIN product_category_sub AS b ON b.id=a.category_id
					LEFT JOIN product_category AS c ON c.id=b.category_id
						WHERE b.id='".$id."'";
	$query = $this->db->query($sql)->row();
	return $query;	
  }
  
  public function getBreadcrumbs_by_last_category($id){
	$sql="SELECT c.id as cat_id, c.name AS cat, b.id as sub_cat_id, b.name AS sub_cat,  a.name AS last_cat 
				FROM product_category_last AS a 
					LEFT JOIN product_category_sub AS b ON b.id=a.category_id
					LEFT JOIN product_category AS c ON c.id=b.category_id
						WHERE a.id='".$id."'";
	$query = $this->db->query($sql)->row();
	return $query;	
  }
}
