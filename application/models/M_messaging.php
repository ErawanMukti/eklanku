<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_messaging extends CI_Model
{
  protected $table_name = 'messaging';

	public function __construct()
	{
		$this->load->database();
    }

	public function save_messaging($data)
    {
		return $this->db->insert($this->table_name, $data);
	}
    
	public function update_messaging($id, $data)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->table_name, $data);
    }
	
	public function get_all_items($data)
	{
		$this->db->select("*");
		$this->db->from($this->table_name);
		 
		if(isset($data['id']))$this->db->where('id', $data['id']);
		if(isset($data['msg_to']))$this->db->where('msg_to', $data['msg_to']);
		if(isset($data['msg_from']))$this->db->where('msg_from', $data['msg_from']);
		
		$query = $this->db->get();
		$array = $query->result_array();

		$query->free_result();
		return $array;
	}
	
	public function get_view_items($id)
	{
		$this->db->select("*");
		$this->db->from($this->table_name);
		 
		$this->db->where("id='".$id."' OR msg_refid='".$id."'");
		$this->db->order_by("id", "ASC");
		$query = $this->db->get();
		$array = $query->result_array();

		$query->free_result();
		return $array;
	}
	
	public function get_one_items($id)
	{
		$this->db->select("*");
		$this->db->from($this->table_name);
		 
		$this->db->where('id', $id);
		$this->db->order_by("id", "ASC");
		$query = $this->db->get()->row();
		return $query;
	}
	
	public function get_inbox_message($email)
	{
		$this->db->select("*");
		$this->db->from($this->table_name);
		 
		$this->db->where("msg_from='".$email."' OR msg_to='".$email."'");
		$this->db->order_by("id", "ASC");
		$query = $this->db->get(); //print_r($this->db->last_query()); die();
		$array = $query->result_array();

		$query->free_result();
		return $array;
	}
	
	public function get_inbox_message_group_by_refid($email){
		$sql="SELECT * FROM messaging WHERE msg_from='".$email."' OR msg_to='".$email."'
				GROUP BY msg_refid ORDER BY id ASC;";
		$query = $this->db->query($sql);
		$array = $query->result_array();

		$query->free_result();
		return $array;
	}
	
 	public function count_new_inbox(){
		$member       = $this->session->member;
		$sql="SELECT count(msg_read) AS jumlah FROM messaging WHERE msg_to='".$member['email']."' AND msg_read=0";
		$query = $this->db->query($sql)->row();
		return  $query->jumlah; 
	} 	
	
	
	
}

