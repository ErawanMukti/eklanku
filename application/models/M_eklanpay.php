<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_eklanpay extends CI_Model
{
  protected $table_name = 'eklanpay';
  protected $db2;
  
	public function __construct()
	{
		$this->load->database();
    }

	public function do_save($data)
    {
		return $this->db->insert($this->table_name, $data);
	}
    
	public function do_update($id, $data)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->table_name, $data);
    }
	
	public function get_all_items($data)
	{ 
		$filter ="";
		$filter .=(!empty($data['id']))?" AND id = '".$data['id']."' ":"";
		$sql = "SELECT * FROM eklanpay WHERE status = '1' $filter ORDER BY id ASC;"; 
		$query = $this->db->query($sql);
		$data  = $query->result_array();
		$query->free_result();
		return $data;
	}
	
	public function get_payment_status($data)
	{
		$filter ="";
		$filter .=!empty($data['id'])?" AND a.id='".$data['id']."'":"";
		$filter .=!empty($data['user_id'])?" AND a.user_id='".$data['user_id']."'":"";
		//$filter .=!empty($data['payment_method'])?" AND a.payment_method='".$data['payment_method']."'":"";
		$filter .=!empty($data['payment_status'])?" AND a.payment_status='".$data['payment_status']."'":"";
		
		$sql="SELECT a.id, a.user_id, a.invoice_code, a.invoice_date, a.nominal, a.key_transfer, a.nominal+a.key_transfer as total ,a.pem_code,
				a.payment_status, b.pay_name,c.norek_tujuan,c.norek_tujuan, e.bill_name,
				sum(c.nominal_transfer) total_transfer, max(c.tgl_transfer) as tgl_transfer, count(c.id) as banyak_transfer, 
				c.norek_asal, c.nama_nasabah, c.bank_id as bank_asal, 
				e.bill_name,e.bill_number,u.eklanku_id,u.`name`,u.phone
				FROM eklanpay_order AS a
					LEFT JOIN payment_status AS b ON b.id=a.payment_status
					LEFT JOIN eklanpay_confirm AS c ON c.eklanpay_order_id=a.id and c.user_id=a.user_id
					LEFT JOIN eklanpay AS d ON d.id=a.eklanpay_id
					LEFT JOIN billing_account AS e ON e.id=c.bank_tujuan_id
					left join user as u on u.id=a.user_id
						WHERE a.status=1 $filter
				group by a.id
					ORDER BY c.tgl_transfer,a.invoice_date DESC";
		$query = $this->db->query($sql);
		$array = $query->result_array();
		$query->free_result();
		return $array;
				
	}
	
	public function get_one_items($id)
	{
		$this->db->select("*");
		$this->db->from($this->table_name);
		$this->db->where('id', $id);
		$this->db->order_by("id", "ASC");
		$query = $this->db->get()->row(); //print_r($this->db->last_query());
		return $query;
	}
	
	public function do_delete($data)
	{
		$this->db->where('id', $data['id']);
		return $this->db->delete($this->table_name);
	}
	
	
	public function doTopup($nominal, $pesan,$pem_code)
	{
		$tz = 'Asia/Makassar';
		$timestamp = time();
		$dt = new DateTime("now", new DateTimeZone($tz)); 
		$dt->setTimestamp($timestamp); 
		//$codeUnix = $this->egc->genRandomNumber(3);
		//$pem_code		= 'TP' . $this->egc->genRandomString(13);
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $ssql = 'select * from f_cal_proses_topup(';
		$ssql = $ssql . '\'' . $dt->format('Y-m-d H:i:s'). '\',';
		$ssql = $ssql . '\'' . $this->mbrcode. '\',';
		$ssql = $ssql . '\'' . $pem_code. '\',';
		$ssql = $ssql . '\'' . $nominal. '\',';
		$ssql = $ssql . '\'' . $this->input->ip_address() . '\',';
		$ssql = $ssql . '\'' .  $this->cleaninput($this->username). '\',';
		$ssql = $ssql . '\'' . $pesan. '\',';
		$ssql = $ssql . '\'' . $codeUnix . '\'';
		$ssql = $ssql . ')';

		$query = $this->db2->query($ssql);

		return true;
	}
	
	public function cekPenarikan($jml_tarik)
	{
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $this->db2->where('member_code', $this->mbrcode);
        $this->db2->where("penarikan_status", 'Waiting');
		$this->db2->where("penarikan_amount", $jml_tarik);
		$data = $this->db2->get('penarikan');
		if ($data->num_rows() > 0)
		{
			return false;
		}
		else
		{
			return true;
		}

	}
	
	public function getDataRekening()
	{
		$iResult ='';
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $this->db2->where('member_code', $this->mbrcode);
		$data = $this->db2->get('member_list');
		if ($data->num_rows() > 0)
		{
			$data = $data->row();
			$iResult = $data->member_bank_num."|".$data->member_bank_acc."|".$data->member_bank_name;
			
		}
		return $iResult;
	}
	
	public function doInsertRecord($nominal,$pem_code)
	{
		$tz = 'Asia/Makassar';
		$timestamp = time();
		$dt = new DateTime("now", new DateTimeZone($tz)); 
		$dt->setTimestamp($timestamp); 
		
		//$pem_code		= 'PN' . $this->egc->genRandomString(13);
		$ssql = 'select * from f_cal_proses_penarikan(';
		$ssql = $ssql . '\'' . $dt->format('Y-m-d H:i:s') . '\',';
		$ssql = $ssql . '\'' . $this->mbrcode . '\',';
		$ssql = $ssql . '\'' . $pem_code . '\',';
		$ssql = $ssql . '\'' . $nominal . '\',';
		$ssql = $ssql . '\'' . $this->input->ip_address() . '\',';
		$ssql = $ssql . '\'' . $this->cleaninput($this->username). '\'';
		$ssql = $ssql . ')';

		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $query = $this->db2->query($ssql);

		return true;
	
		
	}
	
	public function doSMSRWD($emlCode, $outRcpt, $nominal,$nameMember,$accUser)
	{	
		$data = $this->doGetTemplate($emlCode);
		if ($data->num_rows() > 0)
		{
			$data = $data->row();
			$outFrom = $data->tpl_sender;
			$outSubj = $data->tpl_subject;
			$outBody = $data->tpl_text;
			$outBody = str_replace("[membername]", $nameMember, $outBody);
			$outBody = str_replace("[mapamount]", $nominal, $outBody);
			$outBody = str_replace("[username]", $accUser, $outBody);
			$this->doInsert($outFrom, $outRcpt, $outSubj, $outBody, "sms");		}
	}
	
	public function doInsert($outFrom, $outRcpt, $outSubj, $outBody, $type_email)
	{	
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $this->db2->set("out_sender", $outFrom);
		$this->db2->set("out_recipient", $outRcpt);
		$this->db2->set("out_subject", $outSubj);
		$this->db2->set("out_body", $outBody);
		$this->db2->set("out_status", 'Waiting');
		$this->db2->set("out_date", date("Y-m-d H:i:s"));
		$this->db2->set("type_email", $type_email);
		if($this->db2->insert($this->table)==TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function doGetTemplate($emlCode)
	{	
		$ssql = 'select * from eml_template';	
		$ssql = $ssql . ' where tpl_code = \'' . $emlCode . '\'';
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $data = $this->db2->query($ssql);
		return $data;
	}

	public function cekTopup($jml_tarik)
	{
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $this->db2->where('member_code', $this->mbrcode);
        $this->db2->where("transaksi_status", 'Waiting');
		$this->db2->where("transaksi_amount", $jml_tarik);
		$data = $this->db2->get('transaksi');
		if ($data->num_rows() > 0)
		{
			return false;
			
		}
		else
		{
			return true;
		}

	}
	public function getStatusTopup($data)
	{
		$CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        foreach($data as $key=>$val)
        	$this->db2->where($key, $val);
       // $this->db2->where("transaksi_status", 'Waiting');
	//	$this->db2->where("transaksi_amount", $jml_tarik);
		$iResult='';
		$data = $this->db2->get('transaksi');
		if ($data->num_rows() > 0)
		{
			$data = $data->row();
			$iResult = $data->transaksi_status;
			
		}
		
		return $iResult;
	}
}

