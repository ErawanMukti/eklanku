<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_product_category extends CI_Model
{
  protected $table_name = 'product_category';

  public function __construct()
  {
    $this->load->database();
  }

  public function save($name, $alias_name)
  {
    $data = array(
      'name'         => $name,
      'alias_name'   => clear_html($name, true, true),
      'date_created' => date('Y-m-d H:i:s')
    );

    return $this->db->insert($this->table_name, $data);
  }

  public function update($id, $name)
  {
    $data = array(
      'name'         => $name,
      'date_updated' => date('Y-m-d H:i:s')
    );

    $this->db->where('id', $id);
    return $this->db->insert($this->table_name, $data);
  }

  public function get_all_items() 
  {
    $this->db->from($this->table_name);
    $this->db->where('status', '1');
    $this->db->order_by('name ASC');

    $query = $this->db->get();
    $array = $query->result_array();

    $query->free_result();
    return $array;
  }

  public function get_item_by_id($id) 
  {
    $this->db->from($this->table_name);
    $this->db->where('status', '1');
    $this->db->where('id', $id);

    $query = $this->db->get();
    $array = $query->row_array();

    $query->free_result();
    return $array;
  }

  public function delete($id)
  {
    $this->db->where('id', $id);
    return $this->db->update($this->table_name, array('status' => '0'));
  }
}
