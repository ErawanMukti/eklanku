<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_payment_status extends CI_Model
{
  protected $table_name = 'payment_status';

  public function __construct()
  {
    $this->load->database();
  }

  public function get_all_items($data="")
  {
    $this->db->from($this->table_name);
	$this->db->where('status', '1');
	if(isset($data['id']) && $data['id'] !="")$this->db->where("id", $data['id']);
	if(isset($data['showlist_in_admin']) && $data['showlist_in_admin'] !="")$this->db->where("showlist_in_admin", $data['showlist_in_admin']);
	if(isset($data['showlist_in_seller']) && $data['showlist_in_seller'] !="")$this->db->where("showlist_in_seller", $data['showlist_in_seller']);
	if(isset($data['showlist_in_buyer']) && $data['showlist_in_buyer'] !="")$this->db->where("showlist_in_buyer", $data['showlist_in_buyer']);
    $query = $this->db->get();
    $array = $query->result_array();

    $query->free_result();

    return $array;
  }
  
  public function get_paystatus_by_id($id)
  {
    $this->db->from($this->table_name);
    if(is_array($id)){
    	foreach($id as $key=>$val)
    		$this->db->or_where_in('id', $val);
    }else
    	$this->db->where('id', $id);
    $this->db->where('status', '1');
    $query = $this->db->get();
    $array = $query->result_array();

    $query->free_result();

    return $array;
  }
  
	public function get_paystatus_field($id,$field='')
	{
		$iResult ='';
		$this->db->where('id', $id);
		//$this->db->where('product_status', 'Active');
    	$this->db->where('status', '1');
		$data = $this->db->get($this->table_name);
		if ($data->num_rows() > 0)
		{
			$data = $data->row();
			$iResult = empty($field)?$data:$data->$field;
		}//die($iResult);
		return $iResult;
	}
  
    public function getrow_paystatus_byid($id)
   {
		$this->db->from($this->table_name);
		$this->db->where('id', $id);
		$this->db->where('status', '1');
		$query = $this->db->get()->row();
		return $query;
   }

   function getPayStatusName($Info)
	{
		if(empty($Info))return '';
		$this->db->select('pay_name');
		$this->db->where('id', $Info);
		$row = $this->db->get('payment_status')->row();
		return isset($row->pay_name)?($row->pay_name):'-';
    }

}



