<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_promosi_user extends CI_Model
{
  protected $table_name = 'promosi_user';

	public function __construct()
	{
		$this->load->database();
    }

	public function do_save($data)
    {
		return $this->db->insert($this->table_name, $data);
	}
    
	public function do_update($id, $data)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->table_name, $data);
    }
	
	public function get_all_items($data)
	{ 
		$filter ="";
		$filter .=!empty($data['id'])?" AND id = '".$data['id']."' ":"";
		$filter .=!empty($data['user_id'])?" AND user_id = '".$data['user_id']."' ":"";
		$filter .=!empty($data['payment_status'])?" AND payment_status = '".$data['payment_status']."' ":"";
		$sql = "SELECT a.id,promosi_id, a.user_id, a.startdate_contract, a.enddate_contract, a.payment_metod, 
					   a.payment_status, a.invoice_code, a.invoice_date, a.status,
					   b.nama_paket, b.harga_paket, b.jumlah_produk, b.durasi_kontrak
					FROM promosi_user AS a
					LEFT JOIN promosi AS b ON b.id=a.promosi_id
					WHERE a.status=1 $filter ORDER BY id ASC;";
		$query = $this->db->query($sql);
		$data  = $query->result_array();
		$query->free_result();
		return $data;
	}
	
	
	public function get_one_items($id)
	{
		$this->db->select("*");
		$this->db->from($this->table_name);
		$this->db->where('id', $id);
		$this->db->order_by("id", "ASC");
		$query = $this->db->get()->row(); //print_r($this->db->last_query());
		return $query;
	}
	
	public function do_delete($data)
	{
		$this->db->where('id', $data['id']);
		return $this->db->delete($this->table_name);
	}
	
	public function get_review_order_items($promo)
	{
		$query = $this->get_one_items($promo['promo_id']);
		$dataperuser    = array(); 
		$datapernoorder = array(); 
		$datadetil      = array();
		$member= $promo['member'];
			if($query->num_rows() > 0){	
				$counter    = 0;
				foreach($query->result() as $row){
					
						$cur_user    = $member['id']; 
						$counter++;
						$dataperuser[$cur_user]['user_id']    = $member['id'];
						$dataperuser[$cur_user]['user_name']  = $member['name'];
						$dataperuser[$cur_user]['user_phone'] = $member['phone'];
						$dataperuser[$cur_user]['user_email'] = $member['email'];
						$dataperuser[$cur_user]['subtotaluser'] = 1;
						$cur_noorder = $data['promo_id'];
						$datapernoorder[$cur_noorder]['id_order']       = $cur_noorder;
						$datapernoorder[$cur_noorder]['invoice_code']   = isset($row->invoice_code)? $row->invoice_code:'';
						$datapernoorder[$cur_noorder]['invoice_date']   = isset($row->invoice_date)? $row->invoice_date:'';
						$datapernoorder[$cur_noorder]['key_transfer']   = isset($row->key_transfer)? $row->key_transfer:'';
						$datapernoorder[$cur_noorder]['payment_method'] = isset($row->payment_metod)? $row->payment_metod:'';
						$datapernoorder[$cur_noorder]['payment_status'] = isset($row->payment_status)? $row->payment_status:'';
						$datapernoorder[$cur_noorder]['total']       	= isset($row->total)?$row->total:'';
						$datapernoorder[$cur_noorder]['subtotalorder']= 1;
						$datadetil           = array('no_urut'=>1);
			
				
						$datadetil['product_id'] 	= $data['promo_id'];
						$datadetil['product_name'] 	= isset($row->product_name)?$row->product_name:'';
						$datadetil['description'] 	= isset($row->description)?$row->description:'';
						$datadetil['price'] 	    = isset($row->price)?$row->price:'';
						$datadetil['weight'] 	    = isset($row->weight)?$row->weight:'';
						$datadetil['quantity'] 	    = isset($row->quantity)?$row->quantity:'';
						$datadetil['total_price'] 	= isset($row->total_price)?$row->total_price:'';
						$datadetil['total_cart'] 	= isset($row->total_cart)?$row->total_cart:'';
						$datadetil['total']     	= isset($row->total)?$row->total:'';
					
						$datapernoorder[$cur_noorder]['detil'][$datadetil['no_urut']]  = $datadetil;
						$dataperuser[$member['id']]['dataperuser']= $datapernoorder;
						
			}		
		}
		return $dataperuser;
	} 
	
}

