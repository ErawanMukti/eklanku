<?php defined('BASEPATH') OR exit('No direct script access allowed');
class M_contact_center extends CI_Model
{
  protected $table_name = 'contact_center';

  public function __construct()
  {
    $this->load->database();
  }

  public function get_all_items($code="")
  {
    $this->db->from($this->table_name);
    $this->db->where('status', '1');
	if($code !="")$this->db->where('cntctr_code',$code);
	
    $query = $this->db->get();
    $array = $query->result_array();
    $query->free_result();
    return $array;
  }
}
