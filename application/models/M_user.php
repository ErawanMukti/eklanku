<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_user extends CI_Model
{
  protected $table_name     = 'user';
  protected $table_province = 'province';
  protected $table_regency  = 'regency';

  protected $secret_key = 'eKlanku.Com';

  protected $min_name_length             = 3;
  protected $max_name_length             = 100;
  protected $max_phone_length            = 50;
  protected $max_email_length            = 255;
  protected $min_password_length         = 6;
  protected $max_password_length         = 16;
  protected $max_shop_name_length        = 100;
  protected $min_shop_name_length        = 3;
  protected $max_shop_description_length = 1000;
  protected $db2;

  public function __construct()
  {
    $this->load->database();
  }

  public function login($email,$password='')
  {
    $this->db->select("{$this->table_name}.*, {$this->table_province}.name AS shop_province_name, {$this->table_regency}.name AS shop_regency_name");
    $this->db->from($this->table_name);
    $this->db->join($this->table_province, "{$this->table_name}.shop_province_id = {$this->table_province}.id", 'left');
    $this->db->join($this->table_regency, "{$this->table_name}.shop_regency_id = {$this->table_regency}.id", 'left');
    $this->db->where('email', $email);
    // $this->db->where('password', sha1($password . $this->secret_key));

    $query = $this->db->get();
    $array = $query->row_array();

    $query->free_result();
    return $array;
  }

  public function get_item_by_id($id)
  {
    $this->db->select("{$this->table_name}.*, {$this->table_province}.name AS shop_province_name, {$this->table_regency}.name AS shop_regency_name");
    $this->db->from($this->table_name);
    $this->db->join($this->table_province, "{$this->table_name}.shop_province_id = {$this->table_province}.id", 'left');
    $this->db->join($this->table_regency, "{$this->table_name}.shop_regency_id = {$this->table_regency}.id", 'left');
    $this->db->where("{$this->table_name}.id", $id);
//	$this->db->where("{$this->table_name}.shop_status", 1);
//	$this->db->where("{$this->table_name}.shop_startactivedate <=", date('Y-m-d'));
//	$this->db->where("{$this->table_name}.shop_endactivedate >=", date('Y-m-d'));
//$this->db->where('sell_date BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
    $query = $this->db->get();
    $array = $query->row_array();

    $query->free_result();
    return $array;
  }

  public function get_item_by_eklanku_id($eklanku_id)
  {
    $this->db->select("{$this->table_name}.*, {$this->table_province}.name AS shop_province_name, {$this->table_regency}.name AS shop_regency_name");
    $this->db->from($this->table_name);
    $this->db->join($this->table_province, "{$this->table_name}.shop_province_id = {$this->table_province}.id", 'left');
    $this->db->join($this->table_regency, "{$this->table_name}.shop_regency_id = {$this->table_regency}.id", 'left');
    $this->db->where("{$this->table_name}.eklanku_id", $eklanku_id);

    $query = $this->db->get();
    $array = $query->row_array();

    $query->free_result();
    return $array;
  }

  public function get_all_items($data="")
  {
    $this->db->select("{$this->table_name}.*, {$this->table_province}.name AS shop_province_name, {$this->table_regency}.type AS shop_regency_type, {$this->table_regency}.name AS shop_regency_name, table_referral.name AS referral_name");
    $this->db->from($this->table_name);
    $this->db->join($this->table_province, "{$this->table_name}.shop_province_id = {$this->table_province}.id", 'left');
    $this->db->join($this->table_regency, "{$this->table_name}.shop_regency_id = {$this->table_regency}.id", 'left');
    $this->db->join("{$this->table_name} AS table_referral", "{$this->table_name}.referral_id = table_referral.id", 'left');
	if(isset($data['shop_status']) and $data['shop_status'] !="") $this->db->where("{$this->table_name}.shop_status", $data['shop_status']);
    $query = $this->db->get();//print_r($this->db->last_query());
    $array = $query->result_array();

    $query->free_result();
    return $array;
  }

  public function get_items()
  {
    return $this->get_all_items();
  }

  public function add($params)
  {
    $errors = $this->validate($params);

    if (empty($errors)) {
      $this->db->insert($this->table_name, array(
        'name'         => $params['name'],
        'phone'        => $params['phone'],
        'email'        => $params['email'],
        'password'     => sha1($params['password'] . $this->secret_key),
        'agreement'    => $params['agreement'],
        'date_created' => date('Y-m-d H:i:s'),
        'referral_id'  => (!empty($params['referral_id'])) ? $params['referral_id'] : null,
        'eklanku_id'   => $params['member_id']
      ));
    }

    return $errors;
  }

  public function add_all_data($data)
  {
    $errors = $this->validate($data);

    if (empty($errors)) {
      $this->db->where("eklanku_id",$data['eklanku_id']);
      $total = $this->db->count_all_results($this->table_name);
 
     
      if ($total > 0) {
        $this->db->where("eklanku_id",$data['eklanku_id']);
        $this->db->update($this->table_name, array(
          'name'         => $data['name'],
          'phone'        => $data['phone'],
          'email'        => $data['email'],
          'password'     => sha1($data['password'] . $this->secret_key),
          'date_updated' => date('Y-m-d H:i:s'),
          'wallet'       => $data['wallet']
        ));
      } else {
        $this->db->insert($this->table_name, array(
          'name'         => $data['name'],
          'phone'        => $data['phone'],
          'email'        => $data['email'],
          'password'     => sha1($data['password'] . $this->secret_key),
          'agreement'    => $data['agreement'],
          'date_created' => date('Y-m-d H:i:s'),
          'referral_id'  => (!empty($data['referral_id'])) ? $data['referral_id'] : null,
          'eklanku_id'   => $data['eklanku_id'],
          'wallet'       => $data['wallet']
        ));
      }
    
      
    }

    return $errors;
  }

  public function update_all_data($id, $data)
  {
    $errors = $this->validate($data, true);

    if (empty($errors)) {
      $this->db->where('id', $id);
      $this->db->update($this->table_name, array(
        'name'         => $data['name'],
        'phone'        => $data['phone'],
        'email'        => $data['email'],
        'password'     => sha1($data['password'] . $this->secret_key),
        'date_updated' => date('Y-m-d H:i:s'),
        'wallet'       => $data['wallet']
      ));
    }

    return $errors;
  }
	
	public function update_by_id($id, $data)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->table_name, $data); 
	}
	
  public function update_member_id($id, $member_id)
  {
    $this->db->where('id', $id);
    $this->db->update($this->table_name, array(
      'eklanku_id' => $member_id
    ));
  }

  public function add_product_code($id, $increase_point = 1)
  {
    $this->db->set('product_code', "product_code+{$increase_point}", false);
    $this->db->where('id', $id);
    return $this->db->update($this->table_name);
  }

  public function validate($params, $is_update = false)
  {
    extract($params);
    $errors = array();

    if (empty($name)) {
      $errors['name'] = 'Nama harus diisi.';
    } elseif (strlen($name) < $this->min_name_length) {
      $errors['name'] = 'Panjang minimal nama adalah ' . $this->min_name_length . ' karakter.';
    } elseif (strlen($name) > $this->max_name_length) {
      $errors['name'] = 'Panjang maksimal nama adalah ' . $this->max_name_length . ' karakter.';
    }

    if (empty($phone)) {
      $errors['phone'] = 'No. telepon harus diisi.';
    } elseif (strlen($phone) > $this->max_phone_length) {
      $errors['phone'] = 'Panjang maksimal no. telepon adalah ' . $this->max_phone_length . ' karakter.';
    } elseif (!preg_match('/^[\+0-9\-\(\)\s]*$/', $phone)) {
      $errors['phone'] = 'Format no. telepon salah.';
    }

    if (empty($email)) {
      $errors['email'] = 'Alamat email harus diisi.';
    } elseif (strlen($email) > $this->max_email_length) {
      $errors['email'] = 'Panjang maksimal alamat email adalah ' . $this->max_email_length . ' karakter.';
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $errors['email'] = 'Format email salah.';
    } elseif ($this->_is_email_duplicate($email) && !$is_update) {
      $errors['email'] = 'Sudah ada user yang menggunakan email ini.';
    }

    if (empty($password)) {
      $errors['password'] = 'Kata sandi harus diisi.';
    } elseif (strlen($password) < $this->min_password_length) {
      $errors['password'] = 'Panjang minimal kata sandi adalah ' . $this->min_password_length . ' karakter.';
    } elseif (strlen($password) > $this->max_password_length) {
      $errors['password'] = 'Panjang maksimal kata sandi adalah ' . $this->max_password_length . ' karakter.';
    }

    if (empty($re_password)) {
      $errors['re_password'] = 'Ketik ulang kata sandi harus diisi.';
    } elseif ($password !== $re_password) {
      $errors['re_password'] = 'Ketik ulang Kata sandi berbeda dengan kata sandi.';
    }

    if ((empty($agreement) || $agreement !== '1') && !$is_update) {
      $errors['agreement'] = 'Syarat dan ketentuan harus dicentang.';
    }

    return $errors;
  }

  public function validate_shop_name($name, $id)
  {
    $message = array();

    if (empty($name)) {
      $message['errors'] = 'Nama toko harus diisi.';
    } elseif (strlen($name) < $this->min_shop_name_length) {
      $message['errors'] = 'Panjang minimal nama toko adalah ' . $this->min_shop_name_length . ' karakter.';
    } elseif (strlen($name) > $this->max_shop_name_length) {
      $message['errors'] = 'Panjang maksimal nama toko adalah ' . $this->max_shop_name_length . ' karakter.';
    } else {
      $this->db->where('shop_id', $id);
      $total = $this->db->count_all_results($this->table_name);

      if ($total > 0) {
        $message['errors'] = 'Nama toko yang anda pilih sudah terpakai.';
      } else {
        $message['success'] = 'Saat ini nama toko yang anda pilih tersedia.';
      }
    }

    return $message;
  }

  public function update_shop_image($user_id, $image)
  {
    $this->db->where('id', $user_id);
    return $this->db->update($this->table_name, array(
      'shop_image' => $image
    ));
  }

	public function update_shop($user_id, $data, $is_update = false)
	{
		$errors = $this->validate_shop($data, $is_update);
		if (empty($errors)) {
		  $this->db->where('id', $user_id);
		  $this->db->update($this->table_name, $data);
		}  

		return $errors;
	}

	public function validate_shop($data, $is_update = false)
	{
		extract($data);

		$errors = array();

		if (!$is_update) {
		  $name_validate = $this->validate_shop_name($shop_name, clear_html($shop_name, true, true));

		  if (!empty($name_validate['errors'])) {
			$errors['name'] = $name_validate['errors'];
		  }
		}

		if (!empty($shop_description) && strlen($shop_description) > $this->max_shop_description_length) {
		  $errors['description'] = 'Panjang maksimal catatan toko adalah ' . $this->max_shop_description_length . ' karakter.';
		}
		
		if (empty($shop_province_id)) {
		  $errors['province_id'] = 'Pilih provinsi dari option yang sudah disediakan.';
		}

		if (empty($shop_regency_id)) {
		  $errors['regency_id'] = 'Pilih kota dari option yang sudah disediakan.';
		}

		return $errors;
	}

	public function update_wallet($id, $wallet)
	  {
	    $this->db->where('id', $id);
	    
	    return $this->db->update($this->table_name, array(
	      'wallet' => $wallet
	    ));
	  }
  
	public function update_db2wallet($id, $wallet)
	  {
	    $CI = &get_instance();
        $this->db2 = $CI->load->database('pgsql', TRUE);
        $this->db2->where('member_code', $id);
		//$this->db->where('id', $id);
	    return $this->db2->update('member_list', array(
	      'member_amount' => $wallet
	    ));
	  }

  protected function _is_email_duplicate($email)
  {
    $this->db->where('email', $email);
    $total = $this->db->count_all_results($this->table_name);

    if ($total > 0) {
      return true;
    }
  }
}
