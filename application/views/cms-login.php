<!DOCTYPE html>
<html lang="id">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="CMS Login">
    <meta name="author" content="">
    <title>CMS Login</title>
    <link href="<?php echo asset_uri('css/bootstrap.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_uri('css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_uri('cms/metisMenu/metisMenu.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_uri('cms/css/sb-admin-2.css') ?>" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo asset_uri('js/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset_uri('cms/metisMenu/metisMenu.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset_uri('cms/js/jquery.md5.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset_uri('cms/js/sb-admin-2.js') ?>"></script>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <div class="login-panel panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Please Sign In</h3>
            </div>
            <div class="panel-body">
              <?php $attributes = array('role' => 'form') ?>
              <?php echo form_open(base_url('cms-admin/login'), $attributes) ?>
                <?php if (!empty($errors)) : ?>
                  <div class="alert alert-danger">
                    <?php echo $errors ?>
                  </div>
                <?php endif ?>
                <fieldset>
                  <div class="form-group">
                    <input class="form-control" placeholder="Username" name="username" type="text" value="<?php echo set_value('username') ?>" autofocus>
                  </div>
                  <div class="form-group">
                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                  </div>
                  <!-- Change this to a button or input when using this as a form -->
                  <button type="submit" class="btn btn-lg btn-info btn-block">Login</button>
                </fieldset>
              <?php echo form_close() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script>
      $('form').submit(function() {
        var $password = $(this).find('[name=password]');
        $password.val($.md5($password.val()));
      });
    </script>
  </body>
</html>
