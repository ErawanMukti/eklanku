<?php 
	$CI=&get_instance();
	$CI->load->library('egc');
?>
<main id="dashboard">
  <div class="container">
    <div class="breadcrumbs">
      <ul class="items">
        <li class="item"><a href="">Home</a></li>
        <li class="item"><a href="">Confirm Transfer</a></li>
        <li class="item current">Ubah Status</li>
      </ul>
    </div>
    <div class="row">
      <div class="col-sm-4 col-md-3">
        <?php $this->load->view('layout/user-sidebar')?>
      </div>
      <div class="col-sm-8 col-md-9">
			
				<div class="card-box">
				  <div class="card-header text-center">
					<h2>Konfirmasi Pembayaran</h2>
				  </div>
					<?php $attributes = array('role' => 'form','class' => 'general-form','id' => 'fUpdateStatusBayar','name' => 'fUpdateStatusBayar') ?>
					<?php echo form_open($form_url, $attributes) ?>	
						<div class="card-body">
							<div class="text-center">
								<div class="">
									<div style='    
	color: red;
    font: 20px bold;
    background-color: #ffdddc;'>
									 <?php if(isset($msg))echo $msg; ?>
									 <?php if(isset($msg_tiga_digit_transfer))echo $msg_tiga_digit_transfer; ?>
									 <?php if(isset($msg_nominal_transfer_tdk_sama))echo $msg_nominal_transfer_tdk_sama; ?>
									 </div>
									<div class="promotion-header">
									  <h3>Nomor Tagihan</h3>
									  <input type="hidden" class="form-control" id="order_id" name="order_id" value="<?php echo (!empty($id_order)?$id_order:'');?>">
									  <div class="promotion-price">
										<span class="promo-value"><?php echo empty($invoice_code)?'':"$invoice_code";?></span>
									  </div>
									</div>
									<div class="promotion-body">
									<?php if(!empty($id_order)){?>
									  <p>Silahkan isi form berikut untuk mengkonfirmasi pembayaran Anda. Pembayaran maksimal akan dikonfirmasi 2x24 jam</p>
										<table class="table">
											<tr>
											  <td>Nama Nasabah</td>
											  <td>
												<input type="text" id="nama_nasabah" name="nama_nasabah" value="<?php echo empty($nama_nasabah)?'':$nama_nasabah?>" class="form-control">
												<?php if(isset($msg_nama_nasabah))echo "<span class='label label-danger'>$msg_nama_nasabah</span>"; ?>
											  </td>
											</tr>
											<tr>
											  <td>
												No Rekening Nasabah
											  </td>
											  <td>
												<input type="text" id="norek_asal" name="norek_asal" value="<?php echo empty($norek_asal)?'':$norek_asal?>" class="form-control">
												<?php if(isset($msg_norek_asal))echo "<span class='label label-danger'>$msg_norek_asal</span>"; ?>
											  </td>
											</tr>
											<tr>
											  <td>Nama Bank</td>
											  <td>
											  <?php $arrBank=array(
											  	'BCA'=>'BANK BCA','BNI'=>'BANK BNI',
											  	'BRI'=>'BANK BRI','CIMB'=>'BANK CIMB',
											  	'DANAMON'=>'BANK DANAMON','MANDIRI'=>'BANK MANDIRI',
											  	'OCBC'=>'BANK OCBC NISP')?>
												<select id="bank_id" name="bank_id" class="form-control select2me">
													<div class="input-icon right">
														<option value="">---</option>
														<?php 
														foreach($arrBank as $key=>$val){
															if(!empty($bank_id))$is_select=($key==$bank_id?'selected':'');
															echo "<option value=\"$key\" ".$is_select.">$val</option>";
														}
														?>
													</div>
												</select>
												<?php if(isset($msg_bank_id))echo "<span class='label label-danger'>$msg_bank_id</span>"; ?>
											  </td>
											</tr>
											<tr>
											  <td>Bank Tujuan</td>
											  <td>
												<select id="bank_tujuan_id" name="bank_tujuan_id" class="form-control">
													<?php
														if (!empty($billing_account)){
															foreach ($billing_account as $bank){
																$is_select='';
																if(!empty($bank_tujuan_id))$is_select=($key==$bank_tujuan_id?'selected':'');
																echo "<option value='".$bank['id']."' ".$is_select.">".$bank['bill_name']."</option>";
															}
														}
													?>
												</select>
												<?php if(isset($msg_bank_tujuan_id))echo "<span class='label label-danger'>$msg_bank_tujuan_id</span>"; ?>
											  </td>
											</tr>
											<tr>
											  <td>
												Nominal transfer
											  </td>
											  <td>
												<input type="text" id="nominal_transfer" name="nominal_transfer" value="<?php echo empty($nominal_transfer)?'':$CI->egc->accounting_format($nominal_transfer);?>" class="form-control" readonly>
												<?php if(isset($msg_nominal_transfer))echo "<span class='label label-danger'>$msg_nominal_transfer</span>"; ?>
											  </td>
											</tr>
											<tr>
											  <td>
												Tanggal transfer
											  </td>
											  <td>
												<input type="text" id="tgl_transfer" name="tgl_transfer" value="" class="form-control" readonly>
												<?php if(isset($msg_tgl_transfer))echo "<span class='label label-danger'>$msg_tgl_transfer</span>"; ?>
											  </td>
											</tr>
										</table>
									<?php }
									else 
										echo "<p>Konfirmasi Anda akan segera kami followup dalam 2x24jam.</p>";
									?>
									</div> 
									
									<div class="alert bg-warning">
									<button type="button" class="btn btn-warning" onclick="window.location='<?php echo base_url('user/confirm_transfer');?>'">
						                  <i class="fa fa-fw fa-arrow-left"></i>Kembali
						            </button>			
                					<input type="submit"  id="btnSubmit" class="btn btn-primary <?php echo empty($id_order)?'disabled':'';?>" value="Kirim Konfirmasi"/>									 
									</div>
								</div>
							</div>
						</div>
					</form>		
				</div>
			
		</div>
    </div>
  </div>
</main>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="/assets/js/jquery.mask.js"></script>
	<script type="text/javascript">
	/* $(function(){
		var base_url = '<?php echo base_url();?>';
        $("#fUpdateStatusBayar").submit(function(event){
            event.preventDefault();
            $.ajax({
                    url:base_url+'index.php/activity/saveJobActivity',
                    type:'POST',
                    data:$(this).serialize(),
                    success:function(result){
						alert("Success!");
						//alert(result);
                    }

            });
        });
    }); */
    $(function() {
  		
    	  $( "#tgl_transfer" ).datepicker();
    	  var strDate = "<?php echo empty($tgl_transfer)?date("Y-m-d"):$tgl_transfer; ?>";
    	  var dateParts = strDate.split("-");
    	  var date = new Date(dateParts[0], (dateParts[1] - 1), dateParts[2]);
    	  $( "#tgl_transfer" ).datepicker("option", "dateFormat", 'yy-mm-dd').datepicker("setDate", date);
    	  
      	//maskFormat('norek_asal','','-');
      	//maskFormat('nominal_transfer','Rp ','.');  

      	//var nmask='9999-9999-9999-9999-9999';
  		masking('norek_asal','9999-9999-9999-9999-9999');
  		//masking('nominal_transfer','9999.9999-9999-9999-9999');
  		
      	// $('#nominal_transfer').val("<?php echo empty($nominal_transfer)?'':$nominal_transfer;?>").trigger('keyup');  	
    });
/*
    function maskFormat(name,prefix,separator){
    	$('#'+name).inputmask("numeric", {
      	    radixPoint: ",",
      	    groupSeparator: separator,
      	    digits: 0,
      	    autoGroup: true,
      	    prefix: prefix, //Space after $, this will not truncate the first character.
      	    rightAlign: false,
      	    oncleared: function () { self.Value(''); }
      	});
      	$('#'+name).inputmask({removeMaskOnSubmit: true});
    }*/

    function masking(name,mask){
  	  //mask="(999) 999-9999";
  	  
  	 // alert('name:'+name+' mask:'+mask);
  	  $("#"+name).mask(mask);
  	  $("#"+name).on("blur", function() {
  	      var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );

  	      if( last.length == 5 ) {
  	          var move = $(this).val().substr( $(this).val().indexOf("-") + 1, 1 );

  	          var lastfour = last.substr(1,4);

  	          var first = $(this).val().substr( 0, 9 );

  	          $(this).val( first + move + '-' + lastfour );
  	      }
  	  });
  	}; 
</script>
