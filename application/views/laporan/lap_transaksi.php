<main id="dashboard">
  <div class="container">
    <div class="breadcrumbs">
      <ul class="items">
        <li class="item"><a href="">Home</a></li>
        <li class="item"><a href=""><?php echo $member['name'];?></a></li>
        <li class="item current">Laporan Transaksi</li>
      </ul>
    </div>
    <div class="row">
		<div class="col-sm-4 col-md-3">
			 <?php $this->load->view('layout/user-sidebar'); ?>
		</div>
		<div class="col-sm-8 col-md-9">
			


<div class="row">
            
<div class="col-lg-12 col-xs-12 col-sm-12">
    <div class="row">
          <div class="col-sm-6">
            <div class="card-box gain-height">
            	<h2 class="title home title-icon">Informasi Pribadi</h2>
			    <span class="category-icon home">
			       <span class="title-icon">
			       <i class="fa fa-fw fa-user-secret"></i>
			       </span>
			    </span>
              <div class="card-body">
                <p><?php echo h($member['name']) ?></p>
                <p><?php echo h($member['email']) ?></p>
                <p>
                  <?php echo h($member['phone']) ?>
                  <!--<i class="initialism text-success fa fa-fw fa-check" title="Verified"></i>-->
                  <!-- <a class="btn btn-xs btn-default" href="">Verifikasi</a> -->
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="card-box gain-height">
            	<h2 class="title food title-icon">Alamat Pribadi</h2>
			    <span class="category-icon food">
			       <span class="title-icon">
			       <i class="fa fa-fw fa-address-card"></i>
			       </span>
			    </span>
			    
              <div class="card-body">
                <?php if(!empty($member_personal_address)){ ?>
					<p>Nurman Zakaria</p>
					<p>Jl. Tukad Citarum Gg. DD no 999</p>
					<p>Bali - Kota Denpasar - Denpasar Selatan</p>
					<p>082144017270</p>
                <?php }else{ ?>
                  <div class="alert alert-warning">
                    <i class="fa fa-fw fa-exclamation-circle"></i>
                    Belum ada data.
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
	<div class="portlet light">
		<div class="portlet light bordered" >
			<div class="portlet-title tabbable-line" >
				<h2 class="title food title-icon">Laporan Transaksi</h2>
			    <span class="category-icon food">
			       <span class="title-icon">
			       <i class="fa fa-fw fa-bar-chart"></i>
			       </span>
			    </span>
				
			</div>
		<div class="portlet-body">
				<?php 
			$CI=&get_instance();
			$CI->load->library('egc');
				
if(count($content_data)>0)
{  
?>	
			<div class="row">
              
              <div class="col-md-12 col-sm-12">
			   <form class="search-form" action="<?php echo site_url('laporan/transaksi')?>" method="POST">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Masukan ID Transaksi" name="key_pencarian">
                        <span class="input-group-btn">
							<button type="submit" class="btn green" name="sbt_invoice">Cari</button>
                        </span>
                     </div>
               </form>
			
                
              </div>
            </div>
<?php } ?>
		<?php 
if(count($content_data)>0)
{  
?>		
	<link href="<?php echo $jualbeli;?>assets/global/css/components-rounded.min.css" rel="stylesheet" type="text/css">            
	<div class="table-scrollable">		
	
	
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
												
					<th>
						<b>No</b>
					</th>
					<th>
						<b>Print</b>
					</th>
					<th>
						<b>Tanggal Transaksi</b>
					</th>
					<th>
						<b>ID Transaksi</b>
					</th>					
					<th>
						<b>Jumlah</b>
					</th>				
                    <th>
						<b>Kode Product</b>
					</th>
					<th>
						<b>Tujuan</b>
					</th>
					<th>
						<b>Keterangan</b>
					</th>
					<th>
						<b>Status</b>
					</th>
				</tr>
			</thead>
	
			<tbody>
<?php
$no = $page;
	for ($i = 0; $i < count($content_data); ++$i) 
	{
		$no = $no+1;
		$date = $content_data[$i]->transaksi_date;
		$date = strtotime($date);
		$trndate = 	date('d-m-Y', $date);	
        $jumlah = $content_data[$i]->transaksi_amount;		
		$cash = $content_data[$i]->transaksi_type;
		$idtrx = $content_data[$i]->transaksi_code;
		$rsp = $content_data[$i]->resp;
		if($cash == 'PPOB'){
		    $cash = $content_data[$i]->opr;
		}
		
?>	
			<tr class="odd gradeX">	
          			
				<td><?php echo $no; ?></td>
				<td> 
					<?php if($content_data[$i]->transaksi_status == "Active" and $rsp != "" and !empty($rsp))
					{
					   
					 echo "<a class='btn green' onclick=\"printOrder('cetak/$idtrx/$cash');\">
								Print <i class='fa fa-print'></i>
								</a>";
					}else{
						echo '-';
					}
					?>	
				</td>
				<td> 
					<?php echo isset($content_data[$i]->transaksi_date) ? $trndate : ''; ?>	
				</td>
				<td>
				 <?php echo isset($content_data[$i]->transaksi_code) ? $content_data[$i]->transaksi_code : ''; ?>			 
				</td>
				<td align="right">
					<?php echo isset($jumlah) ? $CI->egc->accounting_format($jumlah) : ''; ?>		
				</td>
				<td> 
					<?php echo isset($content_data[$i]->opr) ? $content_data[$i]->opr : ''; ?>	
				</td>
				<td> 
				<?php echo isset($content_data[$i]->ip_addrs) ? $content_data[$i]->ip_addrs : ''; ?>
				</td>
				
				<td> 
				<?php echo isset($content_data[$i]->transaksi_desc) ? $content_data[$i]->transaksi_desc : ''; ?>	
				</td>
				
				<td align="center"> 
				
					<?php
					if($content_data[$i]->transaksi_status == "Locked")
					{
						echo "<span style='color: #EFC80D; font-size: 12px;' >Pending</span>";
					}
					else if($content_data[$i]->transaksi_status == "Active")
					{
						echo "<span style='color: #26C281; font-size: 12px;' >Sukses</span>";
					}
					else if($content_data[$i]->transaksi_status == "Waiting")
					{
						echo "<span style='color: #E43A45; font-size: 12px;' >Waiting</span>";
					}
					else if($content_data[$i]->transaksi_status == "Onproses")
					{
						echo "<span style='color: #26C281; font-size: 12px;' >Onproses</span>";
					}
					else if($content_data[$i]->transaksi_status == "Approve")
					{
						echo "<span style='color: #26C281; font-size: 12px;' >Approve</span>";
					}else{
						echo"<span style='color: #E43A45; font-size: 12px;' >Gagal</span>";
					}
				?>	
								
				</td>				
			</tr>
		
			
<?php

	}
	?>
	
	
		</tbody>
		
	</table>
	</div>
<?php
}else{

	if($proses_pencarian_data == true)
	{
		?>
			<br>
		<div class="alert alert-warning"><button data-dismiss="alert" class="close" style="top:5px;"></button>Data yang Anda cari tidak di temukan </div>
		<?php
			echo anchor('laporan', 'View All', array('class'=>'btn blue'));
		?>
	<?php	
	}
	else
	{
?>	
<div class="alert alert-warning"><button data-dismiss="alert" class="close" style="top:5px;"></button>Tidak ada riwayat transaksi silahkan melakukan pembelian <a href = "<?php echo base_url();?>payment"> Sekarang </a> </div>
<?php
	}
}
?>
<?php
			if (count($content_data)>0)
			{
				$rc = $page+count($content_data);
				?>
            <div class="row">
                <div class="col-md-4 col-sm-4 items-info">Items <?php echo $page+1;?> to <?php echo $rc;?> of <?php echo $TotalNum;?>total</div>
              <div class="col-md-8 col-sm-8">
			   <?php 
				$this->load->view('pager/pager');
			?>
               
              </div>
			
            </div>
	
			<?php
			}
			?>
	</div>
	
	</div>
     </div>
</div>	
<script language="JavaScript">
function printOrder(path){
		var prwin = window.open(path, "Print Data Order Confirm", "left=125, top=150, height=425 ,width=600, status=yes, scrollbars=no");
	}
	</script>
</div>


		</div>
    </div>
  </div>
</main>