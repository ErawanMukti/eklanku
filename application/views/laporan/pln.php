<?php	
if(count($content_data)>0)
{  
  for ($i = 0; $i < count($content_data); ++$i) 
  {
	   $idtrx = $content_data[$i]->transaksi_code;
	   $rsp = $content_data[$i]->resp;
	   $vtype = $content_data[$i]->opr;
	   $data = explode("#",$rsp);
	   //TRANSAKSI SUKSES#523040522248#(2)#ANTON SUTIKNO#201709#620343#2500#622843#https://202.43.173.234/struk/?id=t42BUzgR2%2BfV%2BB6z0%2BMtik%2FQW1BYCXGniPdZZgHZ2r4KFMp1YDoX8bmPPA1Vbs05L00UfgLEih0ry3EcBo14%2BA%3D%3D
	   $plgid = $data[1];
	   $admin = $data[6];
	   $nama = $data[3];
	   $bulan = $data[4];
	   $tag = $data[5];
	   $rpnya = $admin + $tag;
	   $detail = $data[8];;
	   $date = date('d-m-Y H:i:s');
	   $date = strtotime($date);
	   $trndate = 	date('d-m-Y H:i:s', $date);
  }
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Print Transaksi: <?php echo $idtrx;?></title>
<style type="text/css">
body{
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
}


#tabletop td {
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
}

#table01 th {
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
	background-color: #ccc;
	padding-top: 5px;
	padding-bottom: 5px;
	padding-left: 5px;
	padding-right: 5px;
	color: #000;
	font-weight: bold;
	border-bottom: 2px solid #0f0f0f;
	border-top: 1.5px solid #0f0f0f;
}

#table01 td {
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
	padding-top: 5px;
	padding-bottom: 5px;
	padding-left: 5px;
	padding-right: 5px;
	color: #000;
	border-bottom: 1.5px solid #ccc;
}

#table02 th {
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
	background-color: #ccc;
	padding-top: 5px;
	padding-bottom: 5px;
	padding-left: 5px;
	padding-right: 5px;
	font-weight: bold;
	border-bottom: 2px solid #0f0f0f;
	border-top: 1.5px solid #0f0f0f;
}

#table02 td {
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
	padding-top: 5px;
	padding-bottom: 5px;
	padding-left: 5px;
	padding-right: 5px;
	border-bottom: 1.5px solid #ccc;
}

</style>
<script type="text/javascript">
	function doPrint(){
		alert("Anda dapat melakukan print dgn menekan tombol 'Ctrl+P' dari keyboard.");
	}
</script>

</head>
<body onload="doPrint()">
<table id="tabletop" width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="2" align="right">Cetak Ulang (CU) | <?php echo $trndate; ?><td></tr>
<tr><td width="10%"><img alt="EKLANKU PAYMENT" src="<?php echo base_url();?>assets/admin/img/2.png" class="img-responsive" /></td><td></td><tr>
<tr><td colspan="2"><br /><td></tr>
<tr><td colspan="2" align="center"><b>STRUK PEMBAYARAN TAGIHAN LISTRIK PASCA BAYAR</b><td></tr>
</table>

<br /><table id="table01" width="100%" cellpadding="0" cellspacing="0" >
	<tr><th colspan="4" align="left"><b>Detail Data Transaksi</b></th></tr>
	<tr>
		<td>IDPEL</td><td>: <?php echo $plgid; ?></td>
		<td>BLN/THN</td><td>: <?php echo $bulan; ?></td>
	</tr>
	<tr>
		<td>NAMA</td><td>: <?php echo $nama; ?></td>
	    <td>RP TAG PLN</td><td>: <?php echo "Rp. ".number_format($tag,0,',','.').",-"; ?></td>
	</tr>
	<tr>
		<td>ADMIN BANK</td><td>: <?php echo "Rp. ".number_format($admin,0,',','.').",-"; ?></td>
			<td>TOTAL BAYAR</td><td>: <?php echo "Rp. ".number_format($rpnya,0,',','.').",-"; ?></td>
	</tr>

</table><br>
<p align="center">PLN menyatakan struk ini sebagai bukti pembayaran yang sah & mohon disimpan.</p>
<p align="center">Info lebih lanjut silahkan akses www.pln.co.id atau Kantor PLN</p>


</body>
</html>