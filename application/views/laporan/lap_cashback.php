	<?php

$bonus = 	0;			
if ($content_total->num_rows() > 0)
{
	foreach ($content_total->result() as $r) 
	{
		$bonus = $r->total_uang;

	}
}
																		
?>	
<?php

$outmoney = 	0;			
if ($content_out->num_rows() > 0)
{
	foreach ($content_out->result() as $r) 
	{
		$outmoney = $r->uang_terpakai;

	}
}
																		
?>	
<div class="col-lg-12 col-xs-12 col-sm-12">
	<div class="portlet light">
		<div class="portlet light bordered" >
			<div class="portlet-title tabbable-line" >
				<div class="caption" >
				<i class="icon-bubbles font-dark hide"></i>
				   <span class="caption-subject font-dark bold uppercase">Laporan Cashback
				</div>
			</div>
		<div class="portlet-body">
		<div class="table-scrollable">
								<table class="table table-striped table-bordered table-advance table-hover">
								<thead>
								<tr>
									<th colspan="2">
										My Account <?php echo $this->session->userdata('mbr_code'); ?>																																			</th>
								  </tr>
								</thead>
								<tbody>
								<tr>
									<td class="highlight"> Sisa Cashback </td>
									<td class="highlight"> Rp. <?php echo isset($bonus) ? $this->egc->accounting_format($bonus) : ''; ?></td>
								  </tr>
								<tr>
									<td class="highlight"> Cashback Out </td>
									<td class="highlight"> Rp. <?php echo isset($outmoney) ? $this->egc->accounting_format($outmoney) : ''; ?></td>
								  </tr>  
								
								</tbody>
								</table>
							</div>
		
		
						<?php 
if(count($content_data)>0)
{  
?>	
			<div class="row">
              
              <div class="col-md-12 col-sm-12">
			   <form class="search-form" action="<?php echo site_url('laporan/cashback')?>" method="POST">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Masukan ID Downline" name="key_pencarian">
                        <span class="input-group-btn">
							<button type="submit" class="btn green" name="sbt_invoice">Cari</button>
                        </span>
                     </div>
               </form>
			
                
              </div>
            </div>
			<?php } ?>
		<?php 
if(count($content_data)>0)
{  
?>		
	<div class="table-scrollable">		
	
	
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
												
					<th>
						<b>No</b>
					</th>
					<th>
						<b>Release Date</b>
					</th>
					<th>
						<b>Nomor Invoice</b>
					</th>
<th>
						<b>Jenis Cashback</b>
					</th>					
					<th>
						<b>Jumlah</b>
					</th>				
                    <th>
						<b>ID Downline</b>
					</th>					
					<th>
						<b>Nama Downline</b>
					</th>	
					<th>
						<b>Username</b>
					</th>
					<th>
						<b>Status</b>
					</th>
				</tr>
			</thead>
	
			<tbody>
<?php
$no = $page;
	for ($i = 0; $i < count($content_data); ++$i) 
	{
		$no = $no+1;
		$date = $content_data[$i]->castback_date;
		$date = strtotime($date);
		$trndate = 	date('d-m-Y', $date);	
        $jumlah = $content_data[$i]->castback_amount;		

		$cash = $content_data[$i]->type_castback;
		if($cash == "CASTBACK"){$cash = "Cashback";}
?>	
			<tr class="odd gradeX">	
          			
				<td><?php echo $no; ?></td>
				<td >
								
				<b><span style='color: #F74C0B; font-size: 12px;' ><?php
					$tgl1 = $content_data[$i]->castback_date;
					$tgl2 = date('Y-m-d', strtotime('+30 days', strtotime($tgl1)));
				    echo $tgl2; ?></span></b>
				</td>
				<td>
				 <?php echo isset($content_data[$i]->pembelian_code) ? $content_data[$i]->pembelian_code : ''; ?>	
				 <br>
             	  <?php
					echo anchor('member/doInvoice/'.$content_data[$i]->pembelian_code, 'View Detail', array('class'=>'btn blue'));
				?>			 
				</td>
				<td> 
				<?php 
				echo isset($cash) ? $cash : ''; ?>	
				</td>
				<td align="right">
					<?php echo isset($jumlah) ? $this->egc->accounting_format($jumlah) : ''; ?>		
				</td>
				<td> 
				<?php echo isset($content_data[$i]->downline) ? $content_data[$i]->downline : ''; ?>	
				</td>
				<td> 
				<?php echo isset($content_data[$i]->downline_name) ? $content_data[$i]->downline_name : ''; ?>	
				</td>
				<td> 
				<?php echo isset($content_data[$i]->downline_username) ? $content_data[$i]->downline_username : ''; ?>	
				</td>
				<td align="center">
				
					<?php
					if($content_data[$i]->castback_status == "Locked")
					{
						echo "<span style='color: #EFC80D; font-size: 12px;' >Pending</span>";
					}
					else if($content_data[$i]->castback_status == "Active")
					{
						echo "<span style='color: #26C281; font-size: 12px;' >Active</span>";
					}
					else if($content_data[$i]->castback_status == "Waiting")
					{
						echo "<span style='color: #E43A45; font-size: 12px;' >Waiting</span>";
					}
					else if($content_data[$i]->castback_status == "Frezed")
					{
						echo "<span style='color: #E43A45; font-size: 12px;' >Pending</span>";
					}
					else if($content_data[$i]->castback_status == "Transfered")
					{
						echo "<span style='color: #26C281; font-size: 12px;' >Approve</span>";
					}
					else if($content_data[$i]->castback_status == "Approve")
					{
						echo "<span style='color: #26C281; font-size: 12px;' >Approve</span>";
					}else{
						echo"<span style='color: #E43A45; font-size: 12px;' >Rejected</span>";
					}
				?>	
								
				</td>				
			</tr>
<?php

	}
	?>
		</tbody>
		
	</table>
	</div>
<?php
}else{

	if($proses_pencarian_data == true)
	{
		?>
			<br>
		<div class="alert alert-warning"><button data-dismiss="alert" class="close" style="top:5px;"></button>Data yang Anda cari tidak di temukan </div>
		<?php
			echo anchor('laporan/cashback', 'View All', array('class'=>'btn blue'));
		?>
	<?php	
	}
	else
	{
?>	
<div class="alert alert-warning"><button data-dismiss="alert" class="close" style="top:5px;"></button>Tidak ada riwayat cashback silahkan melakukan pembelanjaan <a href = "<?php echo base_url();?>belanja"> Sekarang </a> </div>
<?php
	}
}
?>
<?php
			if (count($content_data)>0)
			{
				$rc = $page+count($content_data);
				?>
            <div class="row">
                <div class="col-md-4 col-sm-4 items-info">Items <?php echo $page+1;?> to <?php echo $rc;?> of <?php echo $TotalNum;?>total</div>
              <div class="col-md-8 col-sm-8">
			   <?php 
				$this->load->view('pager/pager');
			?>
               
              </div>
			
            </div>
	
			<?php
			}
			?>
	</div>
	
	</div>
     </div>
</div>	

