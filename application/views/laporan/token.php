<?php	
if(count($content_data)>0)
{  
  for ($i = 0; $i < count($content_data); ++$i) 
  {
	   $idtrx = $content_data[$i]->transaksi_code;
	   $rsp = $content_data[$i]->resp;
	   $vtype = $content_data[$i]->opr;
	   $data = explode("#",$rsp);
	   //"32104044378#I GST AGUS GUNA HERMAWAN#0#0#2000#13.5#22000#R1M/900#1819#2612-1116-8354-3482-2330#2017-07-06 20:52:51"
	   $plgid = $data[0];
	   $admin = $data[4];
	   $namapel = $data[1];
	   $materai = $data[3];
	   $td = $data[7];
	   $ppn = $data[2];
	   $rpbayar = $data[6];
	   $ppj = $data[8];
	   $ref = "-";
	   $angsuran = 0;
	   $token = $data[9];
	   $kwh = $data[5];
	   $date = date('d-m-Y H:i:s');
	   $date = strtotime($date);
	   $trndate = 	date('d-m-Y H:i:s', $date);
  }
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Print Transaksi: <?php echo $idtrx;?></title>
<style type="text/css">
body{
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
}


#tabletop td {
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
}

#table01 th {
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
	background-color: #ccc;
	padding-top: 5px;
	padding-bottom: 5px;
	padding-left: 5px;
	padding-right: 5px;
	color: #000;
	font-weight: bold;
	border-bottom: 2px solid #0f0f0f;
	border-top: 1.5px solid #0f0f0f;
}

#table01 td {
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
	padding-top: 5px;
	padding-bottom: 5px;
	padding-left: 5px;
	padding-right: 5px;
	color: #000;
	border-bottom: 1.5px solid #ccc;
}

#table02 th {
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
	background-color: #ccc;
	padding-top: 5px;
	padding-bottom: 5px;
	padding-left: 5px;
	padding-right: 5px;
	font-weight: bold;
	border-bottom: 2px solid #0f0f0f;
	border-top: 1.5px solid #0f0f0f;
}

#table02 td {
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
	padding-top: 5px;
	padding-bottom: 5px;
	padding-left: 5px;
	padding-right: 5px;
	border-bottom: 1.5px solid #ccc;
}

</style>
<script type="text/javascript">
	function doPrint(){
		alert("Anda dapat melakukan print dgn menekan tombol 'Ctrl+P' dari keyboard.");
	}
</script>

</head>
<body onload="doPrint()">
<table id="tabletop" width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="2" align="right">Cetak Ulang (CU) | <?php echo $trndate; ?><td></tr>
<tr><td width="10%"><img alt="EKLANKU PAYMENT" src="<?php echo base_url();?>assets/admin/img/2.png" class="img-responsive" /></td><td></td><tr>
<tr><td colspan="2"><br /><td></tr>
<tr><td colspan="2" align="center"><b>STRUK PEMBAYARAN TAGIHAN LISTRIK PRA BAYAR</b><td></tr>
</table>

<br /><table id="table01" width="100%" cellpadding="0" cellspacing="0" >
	<tr><th colspan="4" align="left"><b>Detail Data Transaksi</b></th></tr>
	<tr>
		<td>NO METER</td><td>: <?php echo $plgid; ?></td>
		<td>ADMIN</td><td>: <?php echo $admin; ?></td>
	</tr>
	<tr>
		<td>NAMA</td><td>: <?php echo $namapel; ?></td>
		<td>MATERAI</td><td>: <?php echo $materai; ?></td>
	</tr>
	<tr>
		<td>TARIF/DAYA</td><td>: <?php echo $td; ?></td>
		<td>PPn</td><td>: <?php echo $ppn; ?></td>
	</tr>
	<tr>
		<td>RP BAYAR</td><td>: <?php echo $rpbayar; ?></td>
		<td>PPJ</td><td>: <?php echo $ppj; ?></td>
	</tr>
	<tr>
		<td>JML KWH</td><td>: <?php echo $kwh; ?></td>
		<td>ANGSURAN</td><td>: <?php echo $angsuran; ?></td>
	</tr>
	
	<tr>
		<td>TOKEN</td><td>:<font size='3'><b><?php echo $token; ?></b></font></td>
		<td>&nbsp;</td><td>&nbsp;</td>
	</tr>
</table><br />

<p align="center">Informasi Hubungi Call Center 123 atau Hub PLN Terdekat</p>
<div align="center"><a href="#" onclick="window.print('print')" title="Cetak Dokumen" ;>Cetak</a>&nbsp;|&nbsp;<a href="javascript:window.close();">Tutup</a></div>
<!-- <div align="center"><a href="#" onclick="window.print('print')" title="Cetak Dokumen" ;><img src="../../images/btn-print.gif" alt="Cetak Dokumen" width="151" height="33" border="0" /></a>&nbsp;&nbsp;&nbsp;<a href="javascript:window.close();"><img src="../../images/btn_close.gif" alt="Tutup Dokumen" width="151" height="33" border="0" /></a></div> -->
</body>
</html>