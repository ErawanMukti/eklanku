<?php	
if(count($content_data)>0)
{  
  for ($i = 0; $i < count($content_data); ++$i) 
  {
	   $idtrx = $content_data[$i]->transaksi_code;
	   $rsp = $content_data[$i]->resp;
	   $vtype = $content_data[$i]->opr;
	   $data = explode("#",$rsp);
	   //"087832265725#66510707590060#2017-07-07 18:14:50"
	   $tujuan = $data[0];
	   $datavsn = $data[1];
	   $tgltrx = $data[2];
	   
	   $date = date('d-m-Y H:i:s');
	   $date = strtotime($date);
	   $trndate = 	date('d-m-Y H:i:s', $date);
  }
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Print Transaksi : <?php echo $idtrx;?></title>
<style type="text/css">
body{
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
}


#tabletop td {
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
}

#table01 th {
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
	background-color: #ccc;
	padding-top: 5px;
	padding-bottom: 5px;
	padding-left: 5px;
	padding-right: 5px;
	color: #000;
	font-weight: bold;
	border-bottom: 2px solid #0f0f0f;
	border-top: 1.5px solid #0f0f0f;
}

#table01 td {
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
	padding-top: 5px;
	padding-bottom: 5px;
	padding-left: 5px;
	padding-right: 5px;
	color: #000;
	border-bottom: 1.5px solid #ccc;
}

#table02 th {
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
	background-color: #ccc;
	padding-top: 5px;
	padding-bottom: 5px;
	padding-left: 5px;
	padding-right: 5px;
	font-weight: bold;
	border-bottom: 2px solid #0f0f0f;
	border-top: 1.5px solid #0f0f0f;
}

#table02 td {
	font-family: Tahoma, sans-serif; 
	font-size: 11px;
	padding-top: 5px;
	padding-bottom: 5px;
	padding-left: 5px;
	padding-right: 5px;
	border-bottom: 1.5px solid #ccc;
}

</style>
<script type="text/javascript">
	function doPrint(){
		alert("Anda dapat melakukan print dgn menekan tombol 'Ctrl+P' dari keyboard.");
	}
</script>

</head>
<body onload="doPrint()">
<table id="tabletop" width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="2" align="right">Cetak Ulang (CU) | <?php echo $trndate; ?><td></tr>
<tr><td width="10%"><img alt="EKLANKU PAYMENT" src="<?php echo base_url();?>assets/admin/img/2.png" class="img-responsive" /></td><td></td><tr>
<tr><td colspan="2"><br /><td></tr>
<tr><td colspan="2" align="center"><b>STRUK PEMBAYARAN VOUCHER PULSA ELEKTRIK</b><td></tr>
</table>

<br /><table id="table01" width="100%" cellpadding="0" cellspacing="0" >
	<tr><th colspan="2" align="left"><b>Detail Data Transaksi</b></th></tr>
	<tr>
		<td>Tgl Transaksi</td><td>: <?php echo $tgltrx; ?></td>
	</tr>
	<tr>
		<td>Voucher</td><td>: <?php echo $vtype; ?></td>
	</tr>
	<tr>
		<td>VSN</td><td>: <?php echo $datavsn; ?></td>
	</tr>
	<tr>
		<td>No. HP/Pelanggan</td><td>: <?php echo $tujuan; ?></td>
	</tr>
</table><br />
<p align="center">Kami menyatakan struk ini sebagai bukti pembayaran yang sah & mohon disimpan.</p>
<div align="center"><a href="#" onclick="window.print('print')" title="Cetak Dokumen" ;>Cetak</a>&nbsp;|&nbsp;<a href="javascript:window.close();">Tutup</a></div>
</body>
</html>