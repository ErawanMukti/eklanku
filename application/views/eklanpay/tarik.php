<main id="dashboard">
  <div class="container">
    <div class="breadcrumbs">
      <ul class="items">
        <li class="item"><a href="">Home</a></li>
        <li class="item"><a href="<?php echo base_url('user');?>"><?php echo h($member['name']); ?></a></li>
        <li class="item current">Penarikan Deposit</li>
      </ul>
    </div>
    <div class="row">
      <div class="col-sm-4 col-md-3">
        <?php $this->load->view('layout/user-sidebar') ?>
      </div>
      <div class="col-sm-8 col-md-9">
        <div class="card-box">
          <h2 class="title home title-icon">Penarikan Deposit</h2>
			    <span class="category-icon home">
			       <span class="title-icon">
			       <i class="fa fa-fw fa-money"></i>
			       </span>
			    </span>		
	<?php 
	//echo json_encode($errors);
	if(!empty($errors))
	foreach($errors as $key=>$val){
		$$key=$val;
	}
	$attributes = array('role' => 'form','class' => 'general-form') ?>
					<?php echo form_open($form_url, $attributes) ?>	
		 	<div class="form-body">
		 		<?php if($this->session->flashdata('message_info')):?>
					<div class="alert alert-danger">
						<p class='flashMsg flashError'> <?=$this->session->flashdata('message_info')?> </p>
					</div>
				<?endif?>
				<?php if($this->session->flashdata('message_error')):?>
					<div class="alert alert-danger">
						<p class='flashMsg flashError'> <?=$this->session->flashdata('message_error')?> </p>
					</div>
				<?endif?>
				<div class="row form-group">
					<label class="control-label col-md-3">
						<span class="caption-subject font-dark bold uppercase">Jumlah Penarikan</span>	
					</label>
					<div class="col-md-8">
						<div class="input-icon right">
							<input type="text" class="form-control" id="jml_ditarik" name="jml_ditarik" value="<?php echo empty($jml_ditarik)?'':$jml_ditarik?>">
						</div>
						<?php if($this->session->flashdata('msg_jml_ditarik'))echo '<span class="label label-danger">'.$this->session->flashdata('msg_jml_ditarik').'</span>'; ?>
					</div>
				</div>
				
				<div class="row form-group">
						<label class="control-label col-md-3"><span class="caption-subject font-dark bold uppercase">PIN</span>
						</label>
					<div class="col-md-8">
						<div class="input-icon right">
							<input name="pin_trx" type="password" class="form-control" value="">
						</div>
						<?php if($this->session->flashdata('msg_pin_trx'))echo '<span class="label label-danger">'.$this->session->flashdata('msg_pin_trx').'</span>'; ?>
					</div>
				</div>
				<div class="row alert text-center">
					<button type="submit" class="btn btn-primary"><i class="fa fa-money"></i> Proses Penarikan Deposit</button>
				</div>
			</div>
				
		<?php echo form_close() ?>
		
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.2.6/jquery.inputmask.bundle.min.js"></script>
		<script>
		$(function() {
	      	maskFormat('jml_ditarik','Rp ','.');    	
	    });

	    function maskFormat(name,prefix,separator){
	    	$('#'+name).inputmask("numeric", {
	      	    radixPoint: ",",
	      	    groupSeparator: separator,
	      	    digits: 0,
	      	    autoGroup: true,
	      	    prefix: prefix, //Space after $, this will not truncate the first character.
	      	    rightAlign: false,
	      	    oncleared: function () { self.Value(''); }
	      	});
	      	$('#'+name).inputmask({removeMaskOnSubmit: true});
	    }
		</script>
			</div>
			</div>
		</div>
	</div>
</main>
		