<main id="dashboard">
  <div class="container">
    <div class="breadcrumbs">
      <ul class="items">
        <li class="item"><a href="">Home</a></li>
        <li class="item"><a href=""><?php echo h($member['name']) ?></a></li>
        <li class="item current">Daftar Paket Eklanpay</li>
      </ul>
    </div>
    <div class="row">
      <div class="col-sm-4 col-md-3">
        <?php $this->load->view('layout/user-sidebar') ?>
      </div>
      <div class="col-sm-8 col-md-9">
        <?php $this->load->view('eklanpay/nav'); ?>
        <div class="card-box">
          <h2 class="title home title-icon">Daftar Paket TopUp</h2>
			    <span class="category-icon home">
			       <span class="title-icon">
			       <i class="fa fa-fw fa-money"></i>
			       </span>
			    </span>
		<?php /*<div class="card-header">
            <h2>Daftar Paket Eklanpay</h2>
          </div>*/?>
          <div class="card-body">
            <div class="row">
				
						<?php
							if(!empty($eklanpay_list)){
								foreach ($eklanpay_list as $promo){
						?>
									<div class="col-sm-4">
										<div class="promotion-table" style='border:1px dotted grey;margin:5px'>
											<div class="promotion-header">
												<h3><?php echo $promo['nama_paket'];?></h3>
												<div class="promotion-price">
													<span class="promo-start">paket</span>
													<span class="promo-value"><?php echo number_format_id($promo['harga_paket']);?></span>
													<span class="promo-currency">Rupiah</span>
												</div>
											</div>
											<div class="promotion-body">
												<ul>
													<li><button class="btn btn-primary btn-block" onclick="window.location.href ='<?php echo base_url('eklanpay/billing?eklanpay_id='.$promo['id']) ?>'">Pilih</button></li>
												</ul>
											</div>
										</div>
									</div>
						<?php
								}
							}
						?>
						
				
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
