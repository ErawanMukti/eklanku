			<div class="card-box">
				<?php /*<div class="card-header">
					<h2>Konfirmasi Tagihan Paket Eklanpay</h2> 
				</div>*/?>
				<h2 class="title promoted title-icon">Tagihan Topup Deposit</h2>
			    <span class="category-icon promoted">
			       <span class="title-icon">
			       <i class="fa fa-fw fa-shopping-bag"></i>
			       </span>
			    </span>
					<div class="card-body">
						<?php if(isset($msg))echo $msg; ?>
						<table class="table table-hover table-striped">
							<thead>
								<th>No.</th>
								<th>No. Tagihan</th>
								<th>Tgl Tagihan</th>
								<th>Paket</th>
								<th>Harga Paket (Rp)</th>
								<th></th>
							</thead>
							<tbody>
								  <?php $no=0;?> 
								  <?php foreach($cofirm_eklanpay as $promo){ ?>
								   <?php $no++;?>
											<tr>
												<td>
													<?php echo $no;?>.
												</td>
												<td><?php echo $promo['invoice_code'];?></td>
												<td><?php echo convertDate($promo['invoice_date']);?></td>
												<td><?php echo $promo['nama_paket'];?></td>
												<td><?php echo number_format_id($promo['harga_paket']);?></td>
												<td> 
													<a class="btn btn-default" href="<?php echo base_url('eklanpay/detail/'.$promo['id']);?>"><i class="fa fa-fw fa-eye"></i>Lihat</a>
													<a class="btn btn-primary" href="<?php echo base_url('eklanpay/confirm/'.$promo['id']);?>"><i class="fa fa-fw fa-money"></i>Bayar</a>
												</td>
											</tr>
								  <?php } ?>
							</tbody>
						</table>
					</div>
			</div>