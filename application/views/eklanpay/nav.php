
        <style>
.board .nav-tabs {
	position: relative;
	/* border-bottom: 0; */
	/* width: 80%; */
	margin: 20px auto;
	/*margin-bottom: 0;*/
	box-sizing: border-box;

}

.board > div.board-inner > .nav-tabs {
	border: none;
}

p.narrow{
	width: 60%;
	margin: 10px auto;
}

.liner{
	height: 2px;
	background: #ddd;
	position: absolute;
	width: 80%;
	margin: 0 auto;
	left: 0;
	right: 0;
	top: 40%;
	z-index: 1;
}

.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
	color: #555555;
	cursor: default;
	/* background-color: #ffffff; */
	border: 0;
	border-bottom-color: transparent;
	outline: 0;
}

span.round-tabs{
	width: 40px;
	height: 40px;
	line-height: 40px;
	display: inline-block;
	border-radius: 100px;
	background: gainsboro;
	z-index: 2;
	position: absolute;
	left: 0;
	text-align: center;
	font-size: 20px;
}

span.round-tabs.one{
	border: 2px solid #ddd;
	color: #fff;
}

li.active span.round-tabs.one{
	background: #ff5c00 !important;
	color: #fff;
}

.nav-tabs > li.active > a span.round-tabs{
	background: #fafafa;
}
.nav-tabs > li {
	width: 25%;
}

.nav-tabs > li a{
	width: 40px;
	height: 40px;
	margin: 10px auto;
	border-radius: 100%;
	padding: 0;
}

.nav-tabs > li a:hover{
	background: transparent;
}

.tab-content{
}
.tab-pane{
	position: relative;
	padding-top: 50px;
}

.btn-outline-rounded{
	padding: 10px 40px;
	margin: 20px 0;
	border: 2px solid transparent;
	border-radius: 25px;
}

.btn.green{
	background-color:#69cb95;
	/*border: 2px solid #5cb85c;*/
	color: #ffffff;
}

	.nav-text {
		font-size:12px;
	}
@media( max-width : 585px ){

	.board {
		width: 90%;
		height:auto !important;
	}
	span.round-tabs {
		font-size:16px;
		width: 50px;
		height: 50px;
		line-height: 50px;
	}
	.tab-content .head{
		font-size:20px;
	}
	.nav-tabs > li a {
		width: 50px;
		height: 50px;
		line-height:50px;
	}

	li.active:after {
		content: " ";
		position: absolute;
		left: 35%;
	}

	.btn-outline-rounded {
		padding:12px 20px;
	}
}
</style>
 <?php	/*	<nav>
          <ol class="board cd-multi-steps promo">
       

for($i=0;count($nav['data'])>$i;$i++){
	if($nav['idx']>=$i)
		$txt="<li class='current'><a href=#>".$nav['data'][$i]."</a></li>";
	else 
		$txt="<li><em>".$nav['data'][$i]."</em></li>";
	echo $txt;
}

	</ol>
	</nav>*/
	?>
<div class="board">
   <div class="board-inner">
      <ul class="board nav nav-tabs" id="myTab">
    		<div class="liner"></div>
       <?php 

for($i=0;count($nav['data'])>$i;$i++){
	if($nav['idx']>=$i){
		$isActive="class='active'";
	}
	else{
		$isActive='';
	}
	
	$txt="
		<li $isActive style=\"text-align: center;\">
			<a href=#   title=\"".$nav['data'][$i]."\">
				<span class=\"round-tabs one\">
                   ".($i+1)."
                 </span>
            </a>
            <div class=\"nav-text\">".$nav['data'][$i]."</div>
		</li>";
	echo $txt;
}
?>

        </ul>
    </div>
</div>
