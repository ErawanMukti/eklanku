<main id="dashboard">
  <div class="container">
  
   <!-- form itself -->
    <form id="verify-form" class="white-popup-block mfp-hide">
      <div class="form-header">
        <img alt="" src="<?php echo asset_uri('images/logo.png') ?>">
        <h1>Welcome To Eklanku</h1>
      </div>
      <div class="form-body">
        <div class="row">
          <div class="col-sm-2">
            &nbsp;
          </div>
          <div class="col-sm-8">
             <p id="verify_head"></p>
	         <div style="color: red;
    background-color: gold;
    padding: 1px 10px;
    margin: 10px 0px;
    border: 1px dotted;
    border-radius: 20px 0px 20px 0px;
    " id="verify_status"></div>
	         <div class="row" id="verify_form">
	               
	         </div>
	         <div class="form-group" style="display: none;" id="verify_loader">
	         	<img style="height: 11px;" src="/assets/images/loading.gif">
	         </div>     
          </div>
          <div class="col-sm-2">
            &nbsp;
          </div>
        </div>
      </div>
    </form>
    
    <div class="breadcrumbs">
      <ul class="items">
        <li class="item"><a href="">Home</a></li>
        <li class="item"><a href=""><?php echo h($member['name']) ?></a></li>
        <li class="item current">Detail Penagihan</li>
      </ul>
    </div>
    <div class="row">
      <div class="col-sm-4 col-md-3">
         <?php $this->load->view('layout/user-sidebar') ?>
      </div>
      <div class="col-sm-8 col-md-9">
        <?php $this->load->view('eklanpay/nav'); ?>        
        <div class="card-box">
          <div class="card-header text-center">
            <h2>Detail Penagihan</h2>
          </div>
          <div class="card-body">
            <div class="text-center">
               	<div>
						<div class="promotion-header">
							<h3><?php echo (isset($promotion_row->nama_paket))?$promotion_row->nama_paket:"";?></h3>
							
							<input type="hidden" class="form-control" name="promosi_id" id="promosi_id" value="<?php echo $promotion_row->id;?>">
							
							<div class="promotion-price">
								<span class="promo-start">Dari</span>
								<span class="promo-value"><?php echo (isset($promotion_row->harga_paket))?number_format_id($promotion_row->harga_paket):"";?></span>
								<span class="promo-currency">Rupiah</span>
							</div>
						</div>
						<div class="promotion-body">
							<table class="table">
								<tr>
								  <td>Nama User:</td>
								  <td><?php echo h($member['name']) ?></td>
								</tr>
								<tr>
								  <td>Nama Paket:</td>
								  <td>Eklanpay</td>
								</tr>
								<tr>
									<td>
										Metode Pembayaran
									</td>
									<td>
					 <ul class="payment-method-list" id="accordion">
						<li class="payment-body" style="background-color:transparent;">
						  <div id="transfer-body" class="panel-collapse collapse in"><br>
							  	Silahkan transfer melalui pilihan rekening bank kami dibawah ini:
								<div class="row payment-bank">
								<?php
										if(!empty($billing_account)){
											foreach ($billing_account as $bill_acc){
									?>
										<div class="col-sm-6">
											<img alt="" style="max-width: none;" src="<?php echo asset_uri('images/'.$bill_acc['bill_logo']) ?>" class="bank-img">
												<dl>
													<dd><?php echo $bill_acc['bill_account'];?>, <?php echo $bill_acc['bill_city'];?></dd>
													<dt><?php echo $bill_acc['bill_number'];?></dt>
													<dd><?php echo $bill_acc['bill_name'];?></dd>
												</dl>
										</div>
									<?php										
											}
										}

									?>	
								</div>
						  </div>
						</li>
					  </ul>
								<?php  /*?>		<div class="radio">
										  <label>
											<input type="radio" name="payment_metod" id="payment_metod1" value="wallet" onclick="javascript:promoPaymentMethod(this.value);" />
											Wallet
										  </label>
										</div>
										<div class="radio">
										  <label>
											<input type="radio" name="payment_metod" id="payment_metod2" value="transfer" onclick="javascript:promoPaymentMethod(this.value);" />
											Direct Transfer
										  </label>
										</div> 
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<div class="form-group">
										  <input class="form-control" name="passwordeklanku" id="passwordeklanku" placeholder="Masukkan PIN Eklankku" type="password">
										  <!-- Disable kalo pilih direct transfer -->
										</div>
									</td>*/?>
								</tr>
							</table>	
							<div class="promotion-footer">
								<input type="hidden" class="form-control" name="metode_bayar" id="metode_bayar" value="transfer">
								<button type="button" id= "billing" class="popup-verify-form btn btn-primary btn-block" href="#verify-form">Bayar</button>
							</div>
						</div>
						 
						 
					</div>
				
			  
				<script>

					

					
					$('.popup-verify-form').magnificPopup({
					    type: 'inline',
					    preloader: false,
					//    focus: '#name',

					    // When elemened is focused, some mobile browsers in some cases zoom in
					    // It looks not nice, so we disable it:
					    callbacks: {
					      beforeOpen: function() {
					    	//  var mp = $.magnificPopup.instance, 
					    //	  nm=mp.st.el.attr('id'),
					    	  vrf='#verify_',
					    	  head=$(vrf+"head"),
					    	  form=$(vrf+"form"),
					    	  load=$(vrf+"loader"),
					    	  sts=$(vrf+"status");
					    	  
					    	  var form_data = {
					    			metode_bayar: $('#metode_bayar').val(),
					    			promotion_id: $('#promosi_id').val()
				          	    };
					    	  	head.html('Dalam Process');
				    		    form.html('');
				    		    load.show();
				    		    sts.html('');
				          	    $.ajax({
				          	        url: '<?php echo site_url('eklanpay/verifyPin'); ?>',
				          	        type: 'POST',
				          	        data: form_data,
				          	      	cache: false,
				        		    success: function(msg) {
				            		  console.log(msg);
				        		    	if(msg.status=='0'){
				            		    	//head.html(msg.head);
					            		    if(!(msg.id == null || msg.id.length === 0)){
					            		    	//setTimeout(function() {
					            		    		window.location.href = '<?php echo base_url('eklanpay/detail');?>/'+msg.id;
					    						//}, 20000);
					            		    }
				            		    }
				            		    else{
				            		      	head.html('Info Gagal');
				                    		sts.html('<h4>'+msg.head+'</h4>');
				                    		load.hide();
				                    		form.html(msg.form);
				                    	}
				        		    	
				        		    	//form.show();
				                    	
				          	        },
				          	    });
					      },
					    }
					  });
					            	

				$('.popup-with-form').magnificPopup({
					type: 'inline',
					preloader: false,
					focus: '#name',
					
					// When elemened is focused, some mobile browsers in some cases zoom in
					// It looks not nice, so we disable it:
					callbacks: {
						beforeOpen: function() {
							if($(window).width() < 700) {
								this.st.focus = false;
							} else {
								this.st.focus = '#name';
							}
						},
						open: function() {
							$.magnificPopup.instance.close = function() {
								// do something here
								
								$.magnificPopup.proto.close.call(this);
								
							};
						}
					}
				});
				
				</script>
			  
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
