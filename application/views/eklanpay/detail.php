<main id="dashboard">
  <div class="container">
  
   <!-- form itself -->
    <form id="verify-form" class="white-popup-block mfp-hide">
      <div class="form-header">
        <img alt="" src="<?php echo asset_uri('images/logo.png') ?>">
        <h1>Welcome To Eklanku</h1>
      </div>
      <div class="form-body">
        <div class="row">
          <div class="col-sm-2">
            &nbsp;
          </div>
          <div class="col-sm-8">
             <p id="verify_head"></p>
	         <div style="color: red;
    background-color: gold;
    padding: 1px 10px;
    margin: 10px 0px;
    border: 1px dotted;
    border-radius: 20px 0px 20px 0px;
    " id="verify_status"></div>
	         <div class="row" id="verify_form">
	               
	         </div>
	         <div class="form-group" style="display: none;" id="verify_loader">
	         	<img style="height: 11px;" src="/assets/images/loading.gif">
	         </div>     
          </div>
          <div class="col-sm-2">
            &nbsp;
          </div>
        </div>
      </div>
    </form>
    
    <div class="breadcrumbs">
      <ul class="items">
        <li class="item"><a href="">Home</a></li>
        <li class="item"><a href=""><?php echo h($member['name']) ?></a></li>
        <li class="item current">Detail Penagihan</li>
      </ul>
    </div>
    <div class="row">
      <div class="col-sm-4 col-md-3">
         <?php $this->load->view('layout/user-sidebar') ?>
      </div>
      <div class="col-sm-8 col-md-9">
        <?php $this->load->view('eklanpay/nav'); ?>        
        <div class="card-box">
          <div class="card-header text-center">
            <h2>Detail Penagihan</h2>
          </div>
          <div class="card-body">
          
<div class="text-center mt-25">
		<p class="bold fs-18">Mohon Segera Selesaikan Pembayaran</p>
		
		<p>Transfer dana anda sebelum tanggal <strong><?php 
			$date = new DateTime($order_row->invoice_date);
			$date->add(new DateInterval('PT2H'));
        	setlocale(LC_TIME, 'fr_ID');
			echo $date->format('d F Y H:i:s')?> WIB</strong></p>
		<style>
			.countdown{
				margin-top: 15px;
			    margin-bottom: 10px;
			    letter-spacing: 1.5px;
			    font-weight: 300;
			    font-size: 28px;
			}
			.inline-block{
				display:inline-block;
			}
			.time-caption{
				color: #999;
			    text-transform: uppercase;
			    font-weight: 400;
			    font-size: 10px;
			}
			.v-top{
			 	vertical-align: top;
			}
			.mt25{
				margin-top: 25px;
			}
			.mt15{
				margin-top: 15px;
			}
			.message-background {
			    background-color: #fdf8e2;
			    padding: 15px;
			}
			.mb-20 {
			    margin-bottom: 20px;
			}
			.bold, .semi-bold {
			    font-weight: 600;
			}
			.fs-16 {
			    font-size: 16px;
			}
			.payment-nominal__pulsa-payment-transfer {
			    color: #ff5722;
			}
			.unique-digit-box {
			    border: 1px solid #ec8f05;
			    padding: 0 1px;
			    position: relative;
			}
			.v-middle {
			    vertical-align: middle!important;
			}
			.u-inline-block {
			    display: inline-block;
			}
			.bold, .semi-bold {
			    font-weight: 600;
			}
			.pb-10 {
			    padding-bottom: 10px!important;
			}
			.fs-13 {
			    font-size: 13px;
			}
			.m-auto {
			    margin: 0 auto;
			}
			.transfer-payment {
			    float: right;
			    width: 50%;
			}
			.transfer-info__pulsa-payment-transfer {
			    background-color: #ec8f05;
			    padding: 15px;
			    border-radius: 3px;
			    color: #fff;
			    position: relative;
			}
			.u-center {
			    text-align: center;
			}
			.mb-10 {
			    margin-bottom: 10px;
			}
			.mt-5 {
			    margin-top: 5px;
			}
			.content-center {
			    margin: 0 auto;
			    max-width: 460px;
			}
			.u-italic {
			    font-style: italic;
			}
			.fs-12 {
			    font-size: 12px;
			}
			.cursor-pointer {
			    cursor: pointer;
			}
			.green {
			    color: #ec8f05;
			}
			
			:after, :before {
			    -webkit-box-sizing: border-box;
			    -moz-box-sizing: border-box;
			    box-sizing: border-box;
			}
			.unique-digit-box::before {
			    position: absolute;
			    display: block;
			    border-style: solid;
			    content: '';
			    border-color: transparent transparent #ec8f05;
			    top: 10px;
			    right: 0;
			    border-width: 15px;
			}
		</style>
        <div class="countdown">
            <div class="inline-block">
                <div id="countdown-hour">00</div>
                <div class="time-caption">Jam</div>
            </div>
            <span class="v-top">:</span>
            <div class="inline-block">
                <div id="countdown-minute">00</div>
                <div class="time-caption">Menit</div>
            </div>
            <span class="v-top">:</span>
            <div class="inline-block">
                <div id="countdown-second">00</div>
                <div class="time-caption">Detik</div>
            </div>
        </div>

		<?php 
			$CI=&get_instance();
			$CI->load->library('egc');
		?>
		<hr class="mt-15">
        <div class="message-background">
            <table class="text-left m-auto mb-20 fs-13 trx-detail">
                <tbody>
                    <tr class="trx-detail-hide none" colspan="2" style="display: table-row;">
                        <td>
                            <div class="v-middle u-inline-block"> Total Tagihan <?php echo (isset($pay_row->nama_paket))?$pay_row->nama_paket:"";?></div>
                            <div class="pb-10 text-right u-inline-block u-right transfer-payment"> <?php echo (isset($pay_row->harga_paket))?$CI->egc->accounting_format($pay_row->harga_paket):"";?> </div>
                        </td>
                    </tr>
                    <tr class="trx-detail-hide none" colspan="2" style="display: table-row;">
                        <td>
                            <div class="v-middle u-inline-block"> Kode Unik </div>
                            <div class="pb-10 text-right u-inline-block u-right transfer-payment"> <?php 
                            $uniq=(isset($order_row->key_transfer))?$order_row->key_transfer:"0";
                            $nominal=(isset($order_row->nominal))?$order_row->nominal:"0";
                            echo $CI->egc->accounting_format($uniq);
                            $total=$uniq+$nominal;
                            ?> </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" width="400">
                            <div class="bold v-middle u-inline-block"> Jumlah yang harus dibayar </div>
                            <div class="pb-10 text-right u-inline-block u-right transfer-payment">
                                <div class="bold fs-16 payment-nominal__pulsa-payment-transfer">
                                    Rp <?php echo $CI->egc->accounting_format(substr($total,0,-3))?>,
                                    <span class="unique-digit-box" id="unique-price"><?php echo substr($total,-3)?></span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="content-center u-center mb-10 mt-5 transfer-info__pulsa-payment-transfer">
                <p class="u-bold fs-14"> Harap transfer sesuai jumlah pembayaran hingga tiga digit terakhir. </p>
                <p class="fs-12 u-italic"> Jika jumlah yang ditransfer tidak sesuai, proses verifikasi pembayaran Anda dapat terhambat. </p>
            </div>
        <?php /*   
            <span class="green cursor-pointer fs-12" id="trigger-trx-detail">Buka detail pembayaran <i class="icon-chevron-tipis-down"></i> </span>
       	*/?> 
        </div>

        <hr class="mb-20">
        <div class="content-center">
            				<div id="transfer-body" class="panel-collapse collapse in"><br>
							  	Silahkan transfer melalui pilihan rekening bank kami dibawah ini:
								<div class="row payment-bank">
								<?php
										if(!empty($billing_account)){
											$num=count($billing_account)>3?4:count($billing_account);
											$num=round(12/$num);
											foreach ($billing_account as $bill_acc){
									?>
										<div class="col-sm-<?php echo $num?>">
											<img alt="" style="max-width: none;" src="<?php echo asset_uri('images/'.$bill_acc['bill_logo']) ?>" class="bank-img">
												<dl>
													<dd><?php echo $bill_acc['bill_account'];?>, <?php echo $bill_acc['bill_city'];?></dd>
													<dt><?php echo $bill_acc['bill_number'];?></dt>
													<dd><?php echo $bill_acc['bill_name'];?></dd>
												</dl>
										</div>
									<?php										
											}
										}

									?>	
								</div>
						  </div>
            <a href="<?php echo base_url('user/confirm_transfer')?>"> 
                <button class="btn btn-action mt-20 mb-20 upload-btn__pulsa-payment-transfer">
                    Cek Data Tagihan
                </button> 
            </a>
            <a href="<?php echo base_url('eklanpay/confirm/'.$order_row->id)?>"> 
                <button class="btn btn-primary mt-20 mb-20 upload-btn__pulsa-payment-transfer">
                    Konfirmasi Bayar
                </button> 
            </a>
        </div>
        <script type="text/javascript">
        	function initializeClock(endtime){
        	  var jam = $("#countdown-hour");
        	  var min = $("#countdown-minute");
        	  var sec = $("#countdown-second");
        	  var timeinterval = setInterval(function(){
        	    var t = getTimeRemaining(endtime);
        	    jam.html(t.hours);
        	    min.html(t.minutes);
        	    sec.html(t.seconds);
        	    if(t.total<=0){
        	      clearInterval(timeinterval);
        	    }
        	  },1000);
        	}

        	function getTimeRemaining(endtime){
        		  var t = Date.parse(endtime) - Date.parse(new Date());
        		  var seconds = Math.floor( (t/1000) % 60 );
        		  var minutes = Math.floor( (t/1000/60) % 60 );
        		  var hours = Math.floor( (t/(1000*60*60)) % 24 );
        		  var days = Math.floor( t/(1000*60*60*24) );
        		  return {
        		    'total': t,
        		    'days': days,
        		    'hours': hours,
        		    'minutes': minutes,
        		    'seconds': seconds
        		  };
        		}
        	var deadline = '<?php 
        	echo $date->format('Y-m-d H:i:s')?>';
        	initializeClock(deadline);       	
        	
        </script>
	</div>
	
	
        </div>
      </div>
    </div>
  </div>
</main>