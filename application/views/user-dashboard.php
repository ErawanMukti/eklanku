<main id="dashboard">
  <div class="container">
    <div class="breadcrumbs">
      <ul class="items">
        <li class="item"><a href="<?php echo h(base_url('user')) ?>">Home</a></li>
        <li class="item current"><?php echo h($member['name']) ?></li>
      </ul>
    </div>
    <div class="row">
      <div class="col-sm-4 col-md-3">
        <?php $this->load->view('layout/user-sidebar') ?>
      </div>
      <div class="col-sm-8 col-md-9">
        <h1 class="dash-title">Panel Akun</h1>
        <div class="row">
          <div class="col-sm-6">
            <div class="card-box gain-height">
            	<h2 class="title home title-icon">Informasi Pribadi</h2>
			    <span class="category-icon home">
			       <span class="title-icon">
			       <i class="fa fa-fw fa-user-secret"></i>
			       </span>
			    </span>
			    <?php /*
				<div class="card-header clearfix">
					<div class="pull-left">
					  <h2>Informasi Pribadi</h2>
					  
					</div>
					<!--<div class="pull-right">
					  <a href="#<?php //echo base_url('user/edit') ?>">
						<i class="fa fa-fw fa-pencil"></i>
						Edit
					  </a>  
					</div> -->
				</div>*/?>
              <div class="card-body">
                <p><?php echo h($member['name']) ?></p>
                <p><?php echo h($member['email']) ?></p>
                <p>
                  <?php echo h($member['phone']) ?>
                  <!--<i class="initialism text-success fa fa-fw fa-check" title="Verified"></i>-->
                  <!-- <a class="btn btn-xs btn-default" href="">Verifikasi</a> -->
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="card-box gain-height">
            	<h2 class="title food title-icon">Alamat Pribadi</h2>
			    <span class="category-icon food">
			       <span class="title-icon">
			       <i class="fa fa-fw fa-address-card"></i>
			       </span>
			    </span>
			    <?php /*
              <div class="card-header clearfix">
                <div class="pull-left">
                  <h2>Alamat Pribadi</h2>
                </div>
              <!--  <div class="pull-right">
                  <a href="<?php echo base_url('user/edit/1') ?>">
                    <i class="fa fa-fw fa-pencil"></i>
                    Edit
                  </a>  
                </div> -->
              </div>*/?>
              <div class="card-body">
                <?php if(!empty($member_personal_address)){ ?>
					<p>Nurman Zakaria</p>
					<p>Jl. Tukad Citarum Gg. DD no 999</p>
					<p>Bali - Kota Denpasar - Denpasar Selatan</p>
					<p>082144017270</p>
                <?php }else{ ?>
                  <div class="alert alert-warning">
                    <i class="fa fa-fw fa-exclamation-circle"></i>
                    Belum ada data.
                  </div>
                <?php } ?>
              </div>
            </div>
          </div><?php /*
          <div class="col-sm-4">
            <div class="card-box gain-height">
              <h2 class="title home title-icon">Alamat Penagihan</h2>
			    <span class="category-icon home">
			       <span class="title-icon">
			       <i class="fa fa-fw fa-home"></i>
			       </span>
			    </span>
			    <div class="card-header clearfix">
                <div class="pull-left">
                  <h2>Alamat Penagihan</h2>
                </div>
            <!--    <div class="pull-right">
                  <a href="edit-informasi.html"><i class="fa fa-fw fa-pencil"></i>Edit</a>  
                </div> -->
              </div>
              <div class="card-body">
                <?php if (!empty($member_billing_address)) : ?>
                  <p>Nurman Zakaria</p>
                  <p>Jl. Tukad Citarum Gg. DD no 999</p>
                  <p>Bali - Kota Denpasar - Denpasar Selatan</p>
                  <p>082144017270</p>
                <?php else : ?>
                  <div class="alert alert-warning">
                    <i class="fa fa-fw fa-exclamation-circle"></i>
                    Belum ada data.
                  </div>
                <?php endif ?>
              </div>
            </div>
          </div>*/?>
        </div>
        <?php $this->load->view('layout/product-payment');?>
		<?php $this->load->view('eklanpay/list');?>
        <!--
        <div class="row">
          <div class="col-sm-6">
            <div class="card-box gain-height_2">
              <div class="card-header">
                <div class="clearfix">
                  <div class="pull-left">
                    <h2>Vendor List</h2>
                  </div>
                  <div class="pull-right">
                    <a class="btn btn-xs btn-primary" href="">Lihat Semua</a>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <table class="table table-hover table-striped">
                  <thead>
                    <tr>
                      <th>Member Name</th>
                      <th>Last Login</th>
                      <th class="action-product">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><a href="">noezack store</a></td>
                      <td>13 Dec 2016</td>
                      <td>
                        <select class="form-control">
                          <option>Pending</option>
                          <option>Active</option>
                          <option>Disabled</option>
                          <option>Hidden</option>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td><a href="">noezack store</a></td>
                      <td>13 Dec 2016</td>
                      <td>
                        <select class="form-control">
                          <option>Pending</option>
                          <option>Active</option>
                          <option>Disabled</option>
                          <option>Hidden</option>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td><a href="">noezack store</a></td>
                      <td>13 Dec 2016</td>
                      <td>
                        <select class="form-control">
                          <option>Pending</option>
                          <option>Active</option>
                          <option>Disabled</option>
                          <option>Hidden</option>
                        </select>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="card-box gain-height_2">
              <div class="card-header">
                <div class="clearfix">
                  <div class="pull-left">
                    <h2>Order By Status</h2>
                  </div>
                  <div class="pull-right">
                    <a class="btn btn-xs btn-primary" href="">Lihat Semua</a>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <table class="table table-hover table-striped">
                  <thead>
                    <tr>
                      <th>Status</th>
                      <th>Qty</th>
                      <th class="text-right">Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><a href="">Complete</a></td>
                      <td>3</td>
                      <td class="text-right">IDR 10.000.000</td>
                    </tr>
                    <tr>
                      <td><a href="">Open</a></td>
                      <td>3</td>
                      <td class="text-right">IDR 10.000.000</td>
                    </tr>
                    <tr>
                      <td><a href="">Process</a></td>
                      <td>3</td>
                      <td class="text-right">IDR 10.000.000</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="card-box">
          <div class="card-header">
            <h2>Penjualan</h2>
          </div>
          <div class="card-body">
            <div class="clearfix">
              <div class="pull-left">
                <div class="checkbox-inline">
                  <label>
                    <input type="checkbox" value="">
                    Pilih Semua
                  </label>
                </div>
                <a href="#delete-modal" data-toggle="modal" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></a>
              </div>
              <div class="pull-right">
                <div class="form-group">
                  <input type="text" class="form-control search" placeholder="&#xF002; Cari Produk...">
                </div>
              </div>
            </div>
            <table class="table table-hover table-striped">
              <tbody>
                <tr>
                  <td>
                    <input type="checkbox" value="">
                  </td>
                  <td>
                    <a href="">Order ID #0001</a>
                  </td>
                  <td>
                    Status: Konfirmasi Pembayaran<br>
                    <a href="">Ubah Status</a>
                    <select class="form-control">
                      <option>Proccessed</option>
                      <option>Complete</option>
                      <option>Open</option>
                      <option>Failed</option>
                      <option>Declined</option>
                      <option>Backordered</option>
                      <option>Canceled</option>
                      <option>Awaiting Call</option>
                    </select>
                  </td>
                  <td>20/01/2017 14:22 WIB</td>
                  <td><a href="">Nurman Zakaria</a></td>
                  <td>
                    noezack@gmail.com<br>
                    082144017270
                  </td>
                  <td class="action-product">
                    <a class="btn btn-default" href=""><i class="fa fa-fw fa-eye"></i>Lihat</a>
                  </td>
                </tr>
                <tr>
                  <td>
                    <input type="checkbox" value="">
                  </td>
                  <td>
                    <a href="">Order ID #0001</a>
                  </td>
                  <td>
                    Status: Konfirmasi Pembayaran<br>
                    <a href="">Ubah Status</a>
                    <select class="form-control">
                      <option>Proccessed</option>
                      <option>Complete</option>
                      <option>Open</option>
                      <option>Failed</option>
                      <option>Declined</option>
                      <option>Backordered</option>
                      <option>Canceled</option>
                      <option>Awaiting Call</option>
                    </select>
                  </td>
                  <td>20/01/2017 14:22 WIB</td>
                  <td><a href="">Nurman Zakaria</a></td>
                  <td>
                    noezack@gmail.com<br>
                    082144017270
                  </td>
                  <td class="action-product">
                    <a class="btn btn-default" href=""><i class="fa fa-fw fa-eye"></i>Lihat</a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-box">
          <div class="card-header clearfix">
            <div class="pull-left">
              <h2>Daftar Produk</h2>
            </div>
            <div class="pull-right">
              <a href="<?php echo base_url('user/add-product') ?>"><i class="fa fa-fw fa-plus"></i>Tambah Produk</a>
            </div>
          </div>
          <div class="card-body">
            <div class="clearfix">
              <?php if (!empty($products)) : ?>
                <div class="pull-left">
                  <div class="checkbox-inline">
                    <label>
                      <input type="checkbox" value="">
                      Pilih Semua
                    </label>
                  </div>
                  <a href="#delete-modal" data-toggle="modal" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></a>
                </div>
                <div class="pull-right">
                  <div class="form-group">
                    <input type="text" class="form-control search" placeholder="&#xF002; Cari Produk...">
                  </div>
                </div>
              <?php endif ?>
            </div>
            <?php if (!empty($product_update)) : ?>
              <div class="alert alert-success">
                <?php echo h($product_update) ?>
              </div>
            <?php endif ?>
            <?php if (!empty($products)) : ?>
              <table class="table table-hover table-striped">
                <tbody>
                  <?php foreach ($products as $product) : ?>
                  <tr>
                    <td>
                      <input type="checkbox" value="">
                    </td>
                    <td>
                      <img alt="" src="<?php echo base_url($upload_dir_product_thumb . '/' . $product['image']) ?>" width="46">
                      <a href=""><?php echo h($product['name']) ?></a>
                    </td>
                    <td width="200">
                      <div class="form-group">
                        <input type="text" class="form-control" value="100.000">  
                      </div>
                      <div class="form-group">
                        <select class="form-control">
                          <option>Published</option>
                          <option>Unpublished</option>
                        </select>
                      </div>
                      <a href="#discount-modal" data-toggle="modal">Set Diskon</a>
                    </td>
                    <td>
                      <p>
                        Etalase: Mainan<br>
                        <span>Stok: </span><input type="text" class="form-control" value="1" style="width:34px; display:inline-block"><br>
                        Status: Stok Tersedia
                      </p>
                      <a href="">Set Kosong</a>
                    </td>
                    <td class="action-product">
                      <a class="btn btn-default" href=""><i class="fa fa-fw fa-pencil"></i>Ubah</a>
                      <a class="btn btn-default" href=""><i class="fa fa-fw fa-retweet"></i>Duplikat</a><br>
                      <a class="btn btn-danger" href=""><i class="fa fa-fw fa-trash"></i>Hapus</a>
                    </td>
                  </tr>
                  <?php endforeach ?>
                  <?php /*
                  <tr>
                    <td>
                      <input type="checkbox" value="">
                    </td>
                    <td>
                      <img alt="" src="<?php echo asset_uri('images/product2.jpg') ?>" width="46">
                      <a href="">Gundam XYZ_1</a>
                    </td>
                    <td width="200">
                      <div class="form-group">
                        <input type="text" class="form-control" value="150.000" disabled>
                        <p class="more-info text-muted">Harga Telah Didiskon sebesar <b>20%</b> menjadi <b>100.000</b>. Harga tidak dapat dirubah setelah didiskon</p>
                      </div>
                      <div class="form-group">
                        <select class="form-control">
                          <option>Unpublished</option>
                          <option>Published</option>
                        </select>
                      </div>
                      <!--
                      <a href="#discount-modal" data-toggle="modal">Set Diskon</a>
                      -->
                    </td>
                    <td>
                      <p>
                        Etalase: Mainan<br>
                        <!--
                        <span>Stok: </span><input type="text" class="form-control" value="0" style="width:34px; display:inline-block"><br>
                        -->
                        Status: Stok Kosong
                      </p>
                      <a href="">Set Tersedia</a>
                    </td>

                    <td class="action-product">
                      <a class="btn btn-default" href=""><i class="fa fa-fw fa-pencil"></i>Ubah</a>
                      <a class="btn btn-default" href=""><i class="fa fa-fw fa-retweet"></i>Duplikat</a><br>
                      <a class="btn btn-danger" href=""><i class="fa fa-fw fa-trash"></i>Hapus</a>
                    </td>
                  </tr>
                  */ ?>
                </tbody>
              </table>
            <?php else : ?>
              <div class="alert alert-warning">
                Product masih kosong.
              </div>
            <?php endif ?>
            <div class="modal fade" id="discount-modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Set Diskon</h4>
                  </div>
                  <div class="modal-body">
                    <div class="clearfix">
                      <div class="pull-left">
                        <img alt="" src="<?php echo asset_uri('images/product1.jpg') ?>">
                        <div class="discount-holder">
                          <h5 class="product-title">Gundam XYZ</h5>
                          <p class="price">Rp. 50.000</p>  
                        </div>  
                      </div>
                      <div class="pull-right">
                        <div class="form-group">
                          <label>Diskon (%)</label>
                          <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                          <label>Harga Setelah Diskon</label>
                          <input type="text" class="form-control" disabled>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <h5 class="title">Tanggal Berlaku Diskon</h5>
                    <p class="alert alert-warning">Anda dapat mengajukan diskon antara 07 Februari 2017 sampai dengan 13 Februari 2017. Sistem akan menampilkan setiap barang yang disetujui diskonnya selama 7 hari sejak tanggal yang diajukan. <br><b>Harga yang telah didiskon tidak dapat dirubah hingga masa diskon selesai</b></p>
                    <label>Mulai:</label>
                    <div class="row">
                      <div class="col-sm-4">
                        <select class="form-control">
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                        </select>
                      </div>
                      <div class="col-sm-4">
                        <select class="form-control">
                          <option>Januari</option>
                          <option>Februari</option>
                          <option>Maret</option>
                          <option>April</option>
                        </select>
                      </div>
                      <div class="col-sm-4">
                        <select class="form-control">
                          <option>2017</option>
                          <option>2018</option>
                          <option>2019</option>
                          <option>2020</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        -->
      </div>
    </div>
  </div>
</main>
