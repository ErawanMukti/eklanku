<main>
  <div class="register-img">
    <img src="<?php echo asset_uri('images/logo.png') ?>">
  </div>
  <div class="register-box">
    <h1>Silakan masuk ke dalam akun kamu</h1>
    <?php if (!empty($update_email_message)) : ?>
      <div class="alert alert-success">
        <i class="fa fa-fw fa-exclamation-circle"></i>
        <?php echo h($update_email_message) ?>
      </div>
    <?php endif ?>
    <?php if (!empty($errors)) : ?>
      <div class="alert alert-danger">
        <i class="fa fa-fw fa-exclamation-circle"></i>
        <?php echo h($errors) ?>
      </div>
    <?php endif ?>
    <?php $attributes = array('role' => 'form') ?>
    <?php echo form_open(base_url('login'), $attributes) ?>
      <div class="form-group">
        <label class="sr-only">Alamat Email</label>
        <input class="form-control" type="text" placeholder="Email" name="email">
      </div>
      <div class="form-group">
        <label class="sr-only">Kata Sandi</label>
        <input class="form-control" type="password" placeholder="Kata Sandi" name="password">
      </div>
      <?php if (!empty($callback_url)) : ?>
        <input type="hidden" name="callback_url" value="<?php echo $callback_url ?>">
      <?php endif ?>
      <button type="submit" class="btn btn-block btn-primary">Login</button>
      <div class="register-separator">
        Atau
      </div>
      <?php /*
      <button type="button" class="btn btn-block btn-facebook"><i class="fa fa-fw fa-facebook"></i>Login Dengan Facebook</button>
      <button type="button" class="btn btn-block btn-google"><i class="fa fa-fw fa-google-plus"></i>Login Dengan Google</button>
      */ ?>
      <p class="reg-size">Belum punya akun? <a href="<?php echo h(base_url('register')) ?>">Daftar Sekarang</a></p>
    <?php echo form_close() ?>
  </div>
</main>
