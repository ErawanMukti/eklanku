<main id="dashboard">
  <div class="container">
    <div class="breadcrumbs">
      <ul class="items">
        <li class="item"><a href="">Home</a></li>
        <li class="item"><a href=""><?php echo $member['name'];?></a></li>
        <li class="item current">Confirm Transfer</li>
      </ul>
    </div>
    <div class="row">
		<div class="col-sm-4 col-md-3">
			 <?php $this->load->view('layout/user-sidebar') /*?>
			<div class="sidebar">
				<div class="card-box">
					<div class="card-header">
					  <img alt="" src="<?php echo asset_uri('images/avatar.jpeg') ?>">
					  <div class="card-name"><?php //print_r($member);?>
						<h2><?php echo $member['name'];?></h2>
						<p><b>User</b> - <?php echo $member['shop_province_name']." ".$member['shop_regency_name'];?></p>
					  </div>
					</div>
					<div class="card-info"> 
					  <p>URL: <a href="<?php echo base_url($user_data['shop_id']) ?>" target="_blank"><?php echo base_url($user_data['shop_id']) ?></a></p>
						Bagikan:
							<a class="btn btn-fb" href=""><i class="fa fa-fw fa-facebook"></i></a>
							<a class="btn btn-twitter" href=""><i class="fa fa-fw fa-twitter"></i></a>
							<a class="btn btn-gp" href=""><i class="fa fa-fw fa-google-plus"></i></a>
					</div>
					<div class="card-body"> 
						<ul class="menu-item">
							<li><a href="">Pemesanan</a></li>
							<li><a href="<?php echo base_url()."wishlist/index/";?>">Wishlist</a></li>
							<li class="separator"></li>
							<li><a href="<?php echo base_url()."user/inbox_message/";?>">Kotak Masuk <?php echo $this->m_messaging->count_new_inbox();?></a></li>
							<li><a href="<?php echo base_url()."user/discussion/".h($prod['id']);?>">Diskusi Produk</a></li>
							<li><a href="">Ulasan Produk</a></li>
							<li class="separator"></li>
							<li><a href="">Voucher</a></li>
							<li><a href="">Dompet / Wallet</a></li>
							<li><a href="<?php echo base_url()."user/help_center/";?>">Help Center</a></li>
						</ul>
					</div>
				</div>
				<!--
				<div class="card-box" id="">
					<div class="card-header">
					  <h2>Notifikasi</h2>
					</div>
					<div class="card-body">
					  <ul class="list-group">
						<li class="list-group-item">
						  Orders (status change): Complete -> Open<br>
						  <a href="">Order #97</a><br>
						  <span class="more-info text-muted">06/02/2017, 10:24</span>
						</li>
						<li class="list-group-item">
						  Orders (status change): Complete -> Open<br>
						  <a href="">Order #97</a><br>
						  <span class="more-info text-muted">06/02/2017, 10:24</span>
						</li>
						<li class="list-group-item">
						  Orders (status change): Complete -> Open<br>
						  <a href="">Order #97</a><br>
						  <span class="more-info text-muted">06/02/2017, 10:24</span>
						</li>
						<li class="list-group-item">
						  Orders (status change): Complete -> Open<br>
						  <a href="">Order #97</a><br>
						  <span class="more-info text-muted">06/02/2017, 10:24</span>
						</li>
						<li class="list-group-item">
						  Orders (status change): Complete -> Open<br>
						  <a href="">Order #97</a><br>
						  <span class="more-info text-muted">06/02/2017, 10:24</span>
						</li>
					  </ul>
					</div>
				</div>
				-->
			</div> */?>
		</div>
		<div class="col-sm-8 col-md-9">
			<?php $this->load->view('eklanpay/list');?>
		</div>
    </div>
  </div>
</main>
