<main id="dashboard">
  <div class="container">
    <div class="breadcrumbs">
      <ul class="items">
        <li class="item"><a href="">Home</a></li>
        <li class="item"><a href=""><?php echo $member['name'];?></a></li>
        <li class="item current">Iklan Saya</li>
      </ul>
    </div>
    <div class="row">
		<div class="col-sm-4 col-md-3">
			 <?php $this->load->view('layout/user-sidebar'); ?>
		</div>
		<div class="col-sm-8 col-md-9">
			


<div class="row">
<link href="<?php echo $jualbeli;?>assets/global/css/components-rounded.min.css" rel="stylesheet" type="text/css">            
<div class="col-lg-12 col-xs-12 col-sm-12">
    <div class="row">
          <div class="col-sm-6">
            <div class="card-box gain-height">
            	<h2 class="title home title-icon">Informasi Pribadi</h2>
			    <span class="category-icon home">
			       <span class="title-icon">
			       <i class="fa fa-fw fa-user-secret"></i>
			       </span>
			    </span>
              <div class="card-body">
                <p><?php echo h($member['name']) ?></p>
                <p><?php echo h($member['email']) ?></p>
                <p>
                  <?php echo h($member['phone']) ?>
                  <!--<i class="initialism text-success fa fa-fw fa-check" title="Verified"></i>-->
                  <!-- <a class="btn btn-xs btn-default" href="">Verifikasi</a> -->
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="card-box gain-height">
            	<h2 class="title food title-icon">Alamat Pribadi</h2>
			    <span class="category-icon food">
			       <span class="title-icon">
			       <i class="fa fa-fw fa-address-card"></i>
			       </span>
			    </span>
			    
              <div class="card-body">
                <?php if(!empty($member_personal_address)){ ?>
					<p>Nurman Zakaria</p>
					<p>Jl. Tukad Citarum Gg. DD no 999</p>
					<p>Bali - Kota Denpasar - Denpasar Selatan</p>
					<p>082144017270</p>
                <?php }else{ ?>
                  <div class="alert alert-warning">
                    <i class="fa fa-fw fa-exclamation-circle"></i>
                    Belum ada data.
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
	<div class="portlet light">
		<div class="portlet light bordered" >
			<div class="portlet-title tabbable-line" >
				<h2 class="title food title-icon"><?php echo $view_title;?></h2>
			    <span class="category-icon food">
			       <span class="title-icon">
			       <i class="fa fa-fw fa-bar-chart"></i>
			       </span>
			    </span>
				
			</div>
		<div class="portlet-body">
			<div class="row">

    <div class="col-lg-12 col-xs-12 col-sm-12">
       <div class="portlet light bordered">
            <div class="portlet-body">
            <div class="tab-content">
			<div class="tab-pane active" id="Active">
<?php
                $message_error = $this->session->flashdata('message_error');
                if(!empty($message_error))
                {
                        echo '<div class="alert alert-danger"><button data-dismiss="alert" class="close" style="top:5px;"></button>'.$message_error.'</div>';
                }
                ?>

<?php
                $message_info = $this->session->flashdata('message_info');
                if(!empty($message_info))
                {
                        echo '<div class="alert alert-info"><button data-dismiss="alert" class="close" style="top:5px;"></button>'.$message_info.'</div>';
                }
                ?>				
<?php 
if(count($deptlist)>0)
{  
     for ($i = 0; $i < count($deptlist); ++$i) 
	{
	$date = $deptlist[$i]->iklan_date;
	$date = strtotime($date);
	$trndate = 	date('d-m-Y', $date);
	
?>	
					
    <!-- BEGIN: Comments -->
    <div class="mt-comments">
		<div class="mt-comment">
			<div class="mt-comment-img">
				<img src="<?php echo base_url();?>assets/receipt/<?php echo $deptlist[$i]->iklan_image; ?>" class="img-responsive"/> 
			</div>
			<div class="mt-comment-body">
				<div class="mt-comment-info">
					<span class="mt-comment-author">
					<?php
					if($deptlist[$i]->iklan_status == "Active")
					{
						$link = "?reff=".$this->session->userdata('mbr_link');
					?>
					<a href = "<?php echo $jualbeli;//base_url();?>homepage/cdetail/<?php echo $deptlist[$i]->link_iklan.$link;?>" target = "_blank"><?php echo isset($deptlist[$i]->iklan_title) ? $deptlist[$i]->iklan_title : ''; ?></a>
					<?php								
					}
					else{
						echo isset($deptlist[$i]->iklan_title) ? $deptlist[$i]->iklan_title : '';
					}
					?>
					</span>
					<span class="mt-comment-date"><?php echo isset($deptlist[$i]->castback_date) ? $trndate : ''; ?></span>
				</div>
				<div class="mt-comment-text"> 
					<?php echo isset($deptlist[$i]->iklan_deskripsi) ? $deptlist[$i]->iklan_deskripsi : ''; ?> 
				</div>
				<div class="mt-comment-details">
				<?php
					if($deptlist[$i]->iklan_status == "Reserved")
					{
						echo '<span class="mt-comment-status mt-comment-status-pending">Pending</span>';
					}else if($deptlist[$i]->iklan_status == "Active")
					{
						echo ' <span class="mt-comment-status mt-comment-status-approved">Approved</span>';
					}else{
						echo'<span class="mt-comment-status mt-comment-status-rejected">Rejected</span>';
					}
				?>
				<ul class="mt-comment-actions">
				    <?php
					if($deptlist[$i]->iklan_status == "Active")
					{
						$link = "?reff=".$this->session->userdata('mbr_link');
					?>
					
					<li>
						<a href = "<?php echo $jualbeli;//base_url();?>homepage/cdetail/<?php echo $deptlist[$i]->link_iklan.$link;?>" target = "_blank">View</a>
					</li>
					<li>
						<a href="<?php echo base_url();?>iklan/doDelete/<?php echo $deptlist[$i]->link_iklan;?>">Delete</a>
					</li>
					
					<?php								
					}
					else{
				    ?>
					
						<li>
						<a href="<?php echo base_url();?>iklan/doDelete/<?php echo $deptlist[$i]->link_iklan;?>">Delete</a>
					</li>
					
                    <?php					
					}
					?>
					
					
				</ul>
				</div>
			</div>
		</div>
    </div>

<?php
	}
}else{
	?>
	
	<div class="alert alert-warning"><button data-dismiss="alert" class="close" style="top:5px;"></button>Anda belum memiliki iklan, silahkan pasang iklan Anda <a href = "<?php echo base_url();?>iklan">disini.</a></div>
	<?php
}
?>
<?php
			if (count($deptlist)>0)
			{
				?>
            <div class="row">
			
              <div class="col-md-4 col-sm-4 items-info">Items <?php echo $page+1;?> to <?php echo $total;?> of <?php echo $total;?> total</div>
              <div class="col-md-8 col-sm-8">
                <?php echo $pagination; ?>
              </div>
            </div>
			<?php
			}
			?>
</div>
    </div>
    </div>
    </div>
    </div>
</div>

		</div>
		</div>
		</div>
		</div>			
		</div>
		</div>
		</div>
	</div>
</main>
			
				
