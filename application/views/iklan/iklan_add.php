<main id="dashboard">
  <div class="container">
    <div class="breadcrumbs">
      <ul class="items">
        <li class="item"><a href="">Home</a></li>
        <li class="item"><a href=""><?php echo $member['name'];?></a></li>
        <li class="item current">Pasang Iklan</li>
      </ul>
    </div>
    <div class="row">
		<div class="col-sm-4 col-md-3">
			 <?php $this->load->view('layout/user-sidebar'); ?>
		</div>
		<div class="col-sm-8 col-md-9">
			


<div class="row">
            
<div class="col-lg-12 col-xs-12 col-sm-12">
    <div class="row">
          <div class="col-sm-6">
            <div class="card-box gain-height">
            	<h2 class="title home title-icon">Informasi Pribadi</h2>
			    <span class="category-icon home">
			       <span class="title-icon">
			       <i class="fa fa-fw fa-user-secret"></i>
			       </span>
			    </span>
              <div class="card-body">
                <p><?php echo h($member['name']) ?></p>
                <p><?php echo h($member['email']) ?></p>
                <p>
                  <?php echo h($member['phone']) ?>
                  <!--<i class="initialism text-success fa fa-fw fa-check" title="Verified"></i>-->
                  <!-- <a class="btn btn-xs btn-default" href="">Verifikasi</a> -->
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="card-box gain-height">
            	<h2 class="title food title-icon">Alamat Pribadi</h2>
			    <span class="category-icon food">
			       <span class="title-icon">
			       <i class="fa fa-fw fa-address-card"></i>
			       </span>
			    </span>
			    
              <div class="card-body">
                <?php if(!empty($member_personal_address)){ ?>
					<p>Nurman Zakaria</p>
					<p>Jl. Tukad Citarum Gg. DD no 999</p>
					<p>Bali - Kota Denpasar - Denpasar Selatan</p>
					<p>082144017270</p>
                <?php }else{ ?>
                  <div class="alert alert-warning">
                    <i class="fa fa-fw fa-exclamation-circle"></i>
                    Belum ada data.
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
	<div class="portlet light">
		<div class="portlet light bordered" >
			<div class="portlet-title tabbable-line" >
				<h2 class="title food title-icon"><?php echo $view_title;?></h2>
			    <span class="category-icon food">
			       <span class="title-icon">
			       <i class="fa fa-fw fa-bar-chart"></i>
			       </span>
			    </span>
				
			</div>
		<div class="portlet-body">
				<div class="row">

    <div class="col-lg-12 col-xs-12 col-sm-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					 <div class="portlet light bordered">
					 
                <?php
                $message_error = $this->session->flashdata('message_error');
                if(!empty($message_error))
                {
                        echo '<div class="alert alert-danger"><button data-dismiss="alert" class="close" style="top:5px;"></button>'.$message_error.'</div>';
                }
                ?>
                						
						<div class="portlet-body form">
						   <div class="form-group">
									<label class="col-md-3" style="text-align:right">Upload Foto</label>
										<div class="col-md-9">
													<!-- start uploader -->
															<?php 
															$number=5;
															$this->load->view('iklan/editfoto',array('number'=>$number));
															?>								
													<!-- End Uploader -->
										
										</div>
								</div>
						   <form class="form-horizontal" action="<?php echo site_url('iklan/doCreate')?>" method="POST" enctype="multipart/form-data">
								<input name="cmd_save" type="hidden" value="T">
								
								<div class="alert alert-danger hide">
									<button class="close" data-close="alert"></button>
									Register iklan gagal, data harus lengkap!
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Judul Iklan<span class="required"> * </span></label>
										<div class="col-md-9">
											<input name="title" type="text" class="form-control" maxlength="50">
										</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Harga<span class="required"> * </span></label>
										<div class="col-md-9">
											<input name="harga" type="text" class="form-control" maxlength="50">
										</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Kategory<span class="required"> * </span></label>
										<div class="col-md-9">
											<select name="kategory" class="form-control select2me">
												<option value="">Select...</option>
												<?php 
												if ($content_data_catagory->num_rows() > 0)
													{
														foreach ($content_data_catagory->result() as $row) 
														{

														echo '
														<option value="'.$row->category_code.'">'.$row->category_name.'</option>
														';
													}
												}
												?>
												
											</select>
										</div>
								</div>	
								<div class="form-group">
									<label class="col-md-3 control-label">Deskripsi iklan<span class="required"> * </span></label>
										<div class="col-md-9">
											<textarea name="deskripsi" class="form-control"></textarea>
										</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Daerah<span class="required"> * </span></label>
										<div class="col-md-9">
											<input name="daerah" type="text" class="form-control" maxlength="50">
										</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label"></label>
										<div class="col-md-9">
											<input type="checkbox" name="TOC" value="T" class="form-control" onClick="EnableSubmit(this)">Saya setuju dengan <a href="<?php echo base_url();?>homepage/privacy" target="_BLANK">Term and Condition</a>										</div>
								</div>								
								<div class="form-group">
									<label class="col-md-3 control-label"></label>
										<div class="col-md-9">
											<?php 
											for($i=1;$i<=$number;$i++)
											echo '<input name="gambar'.$i.'" type="hidden">';
											?>
															    
											<button type="submit" id="Accept" class="btn green" disabled>Submit</button>
											<?php
											echo anchor('iklan/doList', 'Cancel', array('class'=>'btn default'));
											?>
										</div>
								</div>
							</form>
						</div>		
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>				
			</div>						


			<script>
			EnableSubmit = function(val)
			{
			    var sbmt = document.getElementById("Accept");
			    if (val.checked == true)
			    {
			        sbmt.disabled = false;
			    }
			    else
			    {
			        sbmt.disabled = true;
			    }
			} 
			</script>
			

		</div>
		</div>
		</div>
		</div>			
		</div>
		</div>
		</div>
	</div>
</main>