<main>
  <div class="container">
    <div class="breadcrumbs">
      <ul class="items">
        <li class="item"><a href="<?php echo base_url();?>">Home</a></li>
        <!--li class="item"><a href=""><?php //echo $breadcrumbs->cat;?></a></li>
        <li class="item"><?php //echo $breadcrumbs->sub_cat;?></li>-->
		<?php echo $breadcrumbs;?>
      </ul>
    </div>
    <div class="row">
      	<div class="col-sm-3">
        <?php $this->load->view('layout/product-sidebar') ?>
      	</div>
	  	<div class="col-sm-9">
			<h2 class="product-list-title"><?php echo $breadcrumbs_title;?></h2>
			<div class="product-toolbar clearfix">
      		400 Bad Request
      		</div>
      	</div>
    </div>
   </div>
</main>
