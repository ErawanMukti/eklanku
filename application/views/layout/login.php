<!DOCTYPE html>
<html lang="id">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=1">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo asset_uri('images/favicon.png') ?>">
    <title>Eklanku.com</title>

    <!-- CSS -->
    <link href="<?php echo asset_uri('css/font-awesome.min.css') ?>" rel="stylesheet" type='text/css'>
    <link href="<?php echo asset_uri('css/flaticon.css') ?>" rel="stylesheet" type='text/css'>
    <link href="<?php echo asset_uri('css/bootstrap.css') ?>" rel="stylesheet" type='text/css'>
    <link href="<?php echo asset_uri('css/style.css') ?>" rel="stylesheet" type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') ?>"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js') ?>"></script>
    <![endif]-->

    <!-- Fonts -->
    <link href="//fonts.googleapis.com/css?family=Nunito:400,700,900|Roboto:400,700,900" rel="stylesheet">

    <!-- jQuery -->
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
 <?php /*   <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyAfzmFeIYKB7UTrKPaVHD6DX8mxRPCVrp8&amp;sensor=false"></script>
*/?>
    <!-- JavaScript -->
    <script type="text/javascript" src="<?php echo asset_uri('js/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset_uri('js/sakarioka.cookie.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset_uri('js/sakarioka.function.js') ?>"></script>
  </head>
  <body>
    <?php echo $main_html ?>
    <footer id="footer">
      <div class="footer-bottom">
        <div class="container">
          <div class="text-center">
            <p>Eklanku &copy;<?php echo date('Y') ?>. All Right Reserved. Designed By <a href="">PT. INDONESIA SEHATI CEMERLANG</a></p>
            <img alt="payment" src="<?php echo asset_uri('images/payment.png') ?>">
          </div>
        </div>
      </div>
    </footer>

    <script>
      if ($(document).height() <= $(window).height()) {
        $('body').attr('id', 'register');
      }
    </script>
  </body>
</html>