<?php 
if(empty($isSide)||!$isSide){
	$colsm3='col-sm-3';
	$colsm6='col-sm-6';
	$colsm9='col-sm-9';
	$style='background-color: #fcf8e3;';
}
else{
	$colsm3='';
	$colsm6='';
	$colsm9='';
	$style='margin:0;';
}
?>
<!-- form itself -->
    <form id="verify-form" class="white-popup-block mfp-hide">
      <div class="form-header">
        <img alt="" src="<?php echo asset_uri('images/logo.png') ?>">
        <h1>Pembayaran/Pembelian</h1>
      </div>
      <div class="form-body">
        <div class="row">
          <div class="col-sm-2">
            &nbsp;
          </div>
          <div class="col-sm-8">
             <p id="verify_head"></p>
	         <div style="color: red;
    background-color: gold;
    padding: 1px 10px;
    margin: 10px 0px;
    border: 1px dotted;
    border-radius: 20px 0px 20px 0px;
    " id="verify_status"></div>
	         <div class="row" id="verify_form">
	               
	         </div>
	         <div class="form-group" style="display: none;" id="verify_loader">
	         	<img style="height: 11px;" src="assets/images/loading.gif">
	         </div>     
          </div>
          <div class="col-sm-2">
            &nbsp;
          </div>
        </div>
      </div>
    </form>

    <div role="tabpanel" class="tabpanel" id="ppob" style="<?php echo $style;?> /*#f8a722;box-shadow: 0 1px 2px rgba(0,0,0,0.4);*/">
      <!-- Nav tabs -->
        <?php /*
      <ul class="nav nav-tabs hidden-xs" role="tablist">
        <li role="presentation" class="active">
          <a href="#listrik" aria-controls="listrik" role="tab" data-toggle="tab">Listrik</a>
        </li>
        <li role="presentation">
          <a href="#pulsatab" aria-controls="pulsatab" role="tab" data-toggle="tab">Pulsa</a>
        </li>
        <li role="presentation">
          <a href="#voucher-game" aria-controls="voucher-game" role="tab" data-toggle="tab">Voucher Game</a>
        </li>
        <li role="presentation">
          <a href="#pdam" aria-controls="pdam" role="tab" data-toggle="tab">Air PDAM</a>
        </li>
        <li role="presentation">
          <a href="#pasca" aria-controls="pasca" role="tab" data-toggle="tab">Tagihan Pasca Bayar</a>
        </li>
         <li role="presentation">
          <a href="#bpjs" aria-controls="bpjs" role="tab" data-toggle="tab">BPJS</a>
        </li>
       
        <li role="presentation">
          <a href="#kredit" aria-controls="kredit" role="tab" data-toggle="tab">Angsuran Kredit</a>
        </li>
        <li role="presentation">
          <a href="#telkom" aria-controls="telkom" role="tab" data-toggle="tab">Telkom</a>
        </li>
        <li role="presentation">
          <a href="#paket-data" aria-controls="paket-data" role="tab" data-toggle="tab">Paket Data</a>
        </li>
      </ul>*/?>
      <div class="" style="background-color: white;background: rgba(253, 253, 253, 0.65);">
        <ul class="nav nav-tabs " role="tablist" id="tabs">
          <li role="presentation" class="active text-center">
            <a href="#listrik" aria-controls="listrik" role="tab" data-toggle="tab">
            	<i class="fa fa-flash" style="display: block;font-size: xx-large;color: lightsalmon;"></i>
            	Listrik
            </a>
          </li>
          <li role="presentation" class="text-center">
            <a href="#pulsatab" aria-controls="pulsatab" role="tab" data-toggle="tab">
            	<i class="fa fa-mobile" style="color: crimson;display: block;font-size: xx-large;"></i>
            	Pulsa
            </a>
          </li>
          <li role="presentation" class="text-center">
            <a href="#voucher-game" aria-controls="voucher-game" role="tab" data-toggle="tab">
            	<i class="fa fa-gamepad" style="color: dodgerblue;display: block;font-size: xx-large;"></i>
            	Voucher</a>
          </li>
          <li role="presentation" class="text-center">
            <a href="#pdam" aria-controls="pdam" role="tab" data-toggle="tab">
            	<i class="fa fa-bath" style="color: #ebb92c;display: block;font-size: xx-large;"></i>
            	Air PDAM</a>
          </li>
          <li role="presentation" class="text-center">
            <a href="#pasca" aria-controls="pasca" role="tab" data-toggle="tab">
            	<i class="fa fa-money" style="color: darkgreen;display: block;font-size: xx-large;"></i>
            	Tagihan/Bayar</a>
          </li>
          <li role="presentation" class="text-center">
            <a href="#bpjs" aria-controls="bpjs" role="tab" data-toggle="tab">
            <i class="fa fa-user-plus" style="color: cadetblue;display: block;font-size: xx-large;"></i>
            	BPJS</a>
          </li>          
          <li role="presentation" class="text-center">
            <a href="#televisi" aria-controls="televisi" role="tab" data-toggle="tab">
            <i class="fa fa-plug" style="color: #ba397e;display: block;font-size: xx-large;"></i>
            	TV Kabel</a>
          </li>
         <?php /* 
          <li role="presentation">
            <a href="#kredit" aria-controls="kredit" role="tab" data-toggle="tab">Angsuran Kredit</a>
          </li>
          */?>
          <li role="presentation" class="text-center">
            <a href="#telkom" aria-controls="telkom" role="tab" data-toggle="tab">
            	<i class="fa fa-phone" style="color: darkorchid;display: block;font-size: xx-large;"></i>
            	Telkom
            </a>
          </li>
          <li role="presentation" class="text-center">
            <a href="#paket-data" aria-controls="paket-data" role="tab" data-toggle="tab">
            	<i class="fa fa-cloud" style="color: limegreen;display: block;font-size: xx-large;"></i>
            	Paket Data</a>
          </li>
          <li role="presentation" id="lastTab" class="text-center">
            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="fa fa-ellipsis-v" style="color:grey;display: block;font-size: xx-large;"></i>
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" id="collapsed" style="left:-90px;">
              
            </ul>
          </li>
        </ul>
      </div>
      <script type="text/javascript">
      var autocollapse = function() {
    	  
    	  var tabs = $('#tabs');
    	  var tabsHeight = tabs.innerHeight();
    	 // alert('1-'+tabsHeight);
    	  if (tabsHeight >= 75) {
    	    while(tabsHeight > 75) {
    	      //console.log("new"+tabsHeight);
    	      
    	      var children = tabs.children('li:not(:last-child)');
    	      var count = children.size();
    	      $(children[count-1]).prependTo('#collapsed');
    	      
    	      tabsHeight = tabs.innerHeight();
    	      var collapsed = $('#collapsed').children('li');
    	      if(collapsed.size()>0)
    	    	  $('#lastTab').attr('style','');
    	//     alert('2-'+collapsed.size());
        	  
    	    }
    	  }
    	  else {
    	  	while(tabsHeight < 75 && (tabs.children('li').size()>0)) {
    	      
    	      var collapsed = $('#collapsed').children('li');
    	      var count = collapsed.size();
    	      if(count>0){
	    	      $(collapsed[0]).insertBefore(tabs.children('li:last-child'));
	    	      tabsHeight = tabs.innerHeight();
    	      }
    	      else{
    	    	  $('#lastTab').attr('style','display:none');
        	      break;
    	    //  if(collapsed.size()==0)
    	   // 	  $('#lastTab').attr('style','display:none');
    	    //  alert('3-'+collapsed.size());
    	      }
    	    }
    	    if (tabsHeight>75) { // double chk height again
    	   // 	 alert('11-'+tabsHeight);
    	    	  
    	    	autocollapse();
    	   // 	alert('12-'+tabsHeight);
    	    }
    	  }
    	  
    	};

    	$(function() {
    	  
    	  	autocollapse(); // when document first loads

    		$(window).on('resize', autocollapse); // when window is resized
    		$('.nav-tabs a').on('shown.bs.tab', function(event){
    		    var x = $(event.target).text();         // active tab
				//alert(x);
    		    var collapsed = $('#collapsed').children('li');
    		    var count = collapsed.size();
    		    var i=0;
    		    while(i<count){
        		    var y=$(collapsed[i]).children('a').text();
        		    i++;
					if(y==x){//alert(x);
						continue;
					}
					$(collapsed[i-1]).removeClass('active');
    		    }
    		});
    	});
      </script>
      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="listrik">
          <div class="tab-body" style="padding:10px 15px 0px 15px">
            <div class="row">
              <div class="<?php echo $colsm3;?> form-group">
                <label>Beli Token / Bayar Tagihan</label>
               
                	<select onchange="selectNominal('token',this.options[this.selectedIndex].value)" class="form-control select2me" id="token_jns_produk" name="jns_produk">
									<option value="">Pilih</option>
									<option value="PLN">Token Listrik</option>
									<option value="PLNPOST">Tagihan Listrik</option>	
	 </select>
              </div>
              <div class="<?php echo $colsm3;?> form-group">
                <label>No. Meter/ID. Pelanggan</label>
                <input type="text" class="form-control" id="token_id_pel" name = "id_pel" placeholder="Contoh 1122334455">
              </div> 
             <div class="<?php echo $colsm3;?> form-group">
	             <div id="token_nominal" style="display: none;">
	                <label>Nominal</label>
	                <select class="form-control" name="nominal" id="token_nominal_dd">
	                </select>
	              </div>
	              <div id="token_nominal_loader" style="display: none;height:11px;width:16px;"><img src="assets/images/loading.gif"></div>
              </div> 
              <div class="<?php echo $colsm3;?> form-group">
                <label class="invisible">submit</label>
                <button type="button" id= "token" class="popup-verify-form btn btn-primary btn-block" href="#verify-form">Beli</button>
              </div>
            </div>
            
          </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="pulsatab">
          <div class="tab-body">
            <div class="row">
              <div class="<?php echo $colsm3;?> form-group">
                   <label>Nomor Telepon</label>
                    <input id="pulsa_id_pel"  type="text" name="no_hp" class="form-control">
                  </div>
                  <div class="<?php echo $colsm3;?> form-group">
                    <label>Jenis Kartu</label>
              			<select id="pulsa_jns_produk"  onchange="selectNominal('pulsa',this.options[this.selectedIndex].value)" class="form-control select2me" name="jns_produk">
							<option value="">Pilih</option>
							<option value="X">XL</option>
							<option value="SK">Telkomsel</option>
							<option value="T">Three</option>
							<option value="SF">SmartFren</option>
							<option value="I">Indosat</option> 
						</select>
                  </div>          
              <div class="<?php echo $colsm3;?> form-group">
	             <div id="pulsa_nominal" style="display: none;">
	                <label>Nominal</label>
	                <select class="form-control" name="nominal" id="pulsa_nominal_dd">
	                </select>
	              </div>
	              <div id="pulsa_nominal_loader" style="display: none;height:11px;width:16px;"><img src="assets/images/loading.gif"></div>
              </div> 
              <div class="<?php echo $colsm3;?> form-group">
                <label class="invisible">submit</label>
                <button type="button" id= "pulsa" class="popup-verify-form btn btn-primary btn-block" href="#verify-form">Beli</button>
              </div>
            </div>
	      </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="voucher-game">
          <div class="tab-body">
            <div class="row">
              <div class="<?php echo $colsm3;?> form-group">
                    <label>Jenis Voucher</label>
                    <select  onchange="selectNominal('game',this.options[this.selectedIndex].value)" class="form-control select2me" id="game_jns_produk">
                      <option value="">Pilih</option>
                      <option value="GS">Gemscool</option>
                      <option value="MX">Megaxus</option>
                      <option value="LT">Lyto</option>
                      <option value="WG">Wavegame</option>
                      <option value="Q">Qeon</option>
                      <option value="GR">Garena</option>
                      <option value="ZY">ZYNGA</option>
                      <option value="GPUS">Googleplay US</option>
                      <option value="SID">Steam Wallet IDR</option>
                      <option value="SUS">Steam Wallet US</option>
                      <option value="VCR">Cherry Credit</option>
                      <option value="PSID">PSN Indonesia</option>
                    </select>                  
              </div> 
              <div class="<?php echo $colsm3;?> form-group">
                   <label>Nomor Telepon</label>
                    <input id="game_id_pel"  type="text" class="form-control">
              </div>
              <div class="<?php echo $colsm3;?> form-group">
	             <div id="game_nominal" style="display: none;">
	                <label>Nominal</label>
	                <select class="form-control" name="nominal" id="game_nominal_dd">
	                </select>
	              </div>    
	          	  <div id="game_nominal_loader" style="display: none;height:11px;width:16px;"><img src="assets/images/loading.gif"></div>
              </div>
              <div class="<?php echo $colsm3;?> form-group">
                <label class="invisible">submit</label>
                  <button type="button" id= "game" class="popup-verify-form btn btn-primary btn-block" href="#verify-form">Beli</button>
              </div>
            </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="pdam">
          <div class="tab-body">
            <div class="row">
              <div class="<?php echo $colsm6;?>">
                <div class="row">
                  <div class="<?php echo $colsm6;?> form-group">
                    <label>Wilayah</label>
                    <select  onchange="selectNominal('pdambyr',this.options[this.selectedIndex].value)" class="form-control select2me" id="pdambyr_jns_produk">
                      <option value="">Pilih</option>
<option value="PDAM_AETRA">PDAM AETRA JAKARTA</option>
<option value="PDAM_BONDO">PDAM BONDOWOSO</option>
<option value="PDAM_BALANGAN">PDAM KAB. BALANGAN</option>
<option value="PDAM_BNKLN">PDAM KAB BANGKALAN</option>
<option value="PDAM_WABATANG">PDAM KAB. BATANG</option>
<option value="PDAM_WABJN">PDAM KAB. BOJONEGORO</option>
<option value="PDAM_GRBG">PDAM KAB GROBOGAN</option>
<option value="PDAM_JMBR">PDAM KAB JEMBER</option>
<option value="PDAM_WAKUBURAYA">PDAM KAB. KUBU RAYA</option>
<option value="PDAM_MLG">PDAM KAB MALANG</option>
<option value="PDAM_MJKRT">PDAM KAB MOJOKERTO</option>
<option value="PDAM_WAPASU">PDAM KAB. PASURUAN</option>
<option value="PDAM_WASAMPANG">PDAM KAB. SAMPANG</option>
<option value="PDAM_SIDO">PDAM KAB SIDOARJO</option>
<option value="PDAM_STUBN">PDAM KAB SITUBONDO</option>
<option value="PDAM_WATAPIN">PDAM KAB. TAPIN</option>
<option value="PDAM_LPG">PDAM KOTA BANDAR LAMPUNG</option>
<option value="PDAM_BDG">PDAM KOTA BANDUNG</option>
<option value="PDAM_WAIBANJAR">PDAM KOTA BANJARBARU</option>
<option value="PDAM_KOBGR">PDAM KOTA BOGOR</option>
<option value="PDAM_DENPASAR">PDAM KOTA DENPASAR</option>
<option value="PDAM_WAJAMBI">PDAM KOTA JAMBI</option>
<option value="PDAM_WAMANADO">PDAM KOTA MANADO</option>
<option value="PDAM_WAGIRIMM">PDAM KOTA MATARAM</option>
<option value="PDAM_WAPLMBNG">PDAM KOTA PALEMBANG</option>
<option value="PDAM_WAKOPASU">PDAM KOTA PASURUAN</option>
<option value="PDAM_PURRJ">PDAM KOTA PURWOREJO</option>
<option value="PDAM_SURKT">PDAM KOTA SURAKARTA</option>
<option value="PDAM_WAGROGOT">PDAM KOTA TANAH GROGOT</option>
<option value="PDAM_PLYJA">PDAM PALYJA JAKARTA</option>
<option value="PDAM_PON">PDAM PONTIANAK</option>
<option value="PDAM_SBY">PDAM SURABAYA</option>
                    </select>
                  </div>
	              <div class="<?php echo $colsm6;?> form-group">
	                    <label>Nomor Pelanggan</label>
	                    <input  id="pdambyr_id_pel" type="text" class="form-control" placeholder="Contoh 123456789">
	              </div>
                </div>
              </div>
              <div class="<?php echo $colsm6;?> form-group">
                <label class="invisible">Nomor Pelanggan</label>
                  <button type="button" id= "pdambyr" class="popup-verify-form btn btn-primary btn-block" href="#verify-form">Beli</button>
              </div>
            </div>
          </div>
        </div>
        <?php /*
        */?>
        <div role="tabpanel" class="tab-pane" id="pasca">
          <div class="tab-body">
            <div class="row">
              <div class="<?php echo $colsm9;?>">
                <div class="row">
                  <div class="<?php echo $colsm6;?> form-group">
                    <label>Pilih Operator</label>
                    <select onchange="selectNominal('pascabyr',this.options[this.selectedIndex].value)" class="form-control select2me" id="pascabyr_jns_produk" name="jns_produk">
	        		  <option value>Pilih</option>
	        		  <option value="TSELPOST">Telkomsel - Halo</option>
                      <option value="ISATPOST">Indosat - Matrix</option>
                      <option value="XLPOST">XL Pasca</option>
                      <option value="THREEPOST">THREE Pasca</option>
                      <option value="SMARTPOST">SMARTFREN Pasca</option>
                      <option value="FRENPOST">FREN/MOBI/HEPI Pasca</option>
                      <option value="ESIAPOST">ESIA Pasca</option>
                    </select>
                  </div>
                  <div class="<?php echo $colsm6;?> form-group">
                    <label>Nomor Telepon</label>
                    <input id="pascabyr_id_pel" type="text" class="form-control" placeholder="Contoh 081234567890">
                  </div>
                </div>
              </div>
              <div class="<?php echo $colsm3;?> form-group">
                <label class="invisible">submit</label>
                <button type="button" id="pascabyr" class="popup-verify-form btn btn-primary btn-block" href="#verify-form">Beli</button>
              </div>
            </div>
          </div>
        </div>
       <div role="tabpanel" class="tab-pane" id="bpjs">
          <div class="tab-body">
            <div class="row">
              <div class="<?php echo $colsm9;?>">
                	<div class="row">
	                <div class="<?php echo $colsm6;?> form-group">
		                <label>Bayar BPJS</label>
		                <select  onchange="selectNominal('bpjsbyr',this.options[this.selectedIndex].value)" class="form-control select2me" id="bpjsbyr_jns_produk" name="jns_produk">
		                  <option value>Pilih</option>
			        	  <option value="BPJSKES">BPJS Kesehatan</option>
		               <?php /*   <option value>Ketenagakerjaan</option>*/?>
		                </select>
	                </div>
                
		              <div class="<?php echo $colsm6;?> form-group">
		                <label>Nomor Kepesertaan BPJS</label>
		                <input id="bpjsbyr_id_pel" type="text" class="form-control" placeholder="Contoh 1223243322">
		              </div>
	              </div>
              </div>
              <?php /*
              <div class="col-sm-3 form-group">
                <label>Bayar Hingga</label>
                <select class="form-control">
                  <option>Pilih</option>
                  <option>April 2017</option>
                  <option>Mei 2017</option>
                  <option>Juni 2017</option>
                  <option>Juli 2017</option>
                  <option>Agustus 2017</option>
                </select>
              </div>*/?>
              <div class="<?php echo $colsm3;?> form-group">
                <label class="invisible">submit</label>
                <button type="button" id="bpjsbyr" class="popup-verify-form btn btn-primary btn-block" href="#verify-form">Beli</button>
              </div>
            </div>
          </div>
        </div>
        
        <div role="tabpanel" class="tab-pane" id="televisi">
          <div class="tab-body">
            <div class="row">
              <div class="<?php echo $colsm9;?>">
                	<div class="row">
	                <div class="<?php echo $colsm3;?> form-group">
		                <label>Jenis Pembayaran</label>
		                <select  onchange="selectGroup('tvbyr',this.options[this.selectedIndex].value)" class="form-control select2me" id="tvbyr_group_produk">
			                <option value>Pilih</option>
				        	<option value="TV">TAGIHAN TV</option>
							<option value="VOC">VOUCHER TV</option>
		                </select>
	                </div>
	                <div class="<?php echo $colsm6;?> form-group">
		                <div id="tvbyr_jns">
				            <label>Bayar TV Kabel</label>
			                <select  onchange="selectNominal('tvbyr',this.options[this.selectedIndex].value)" class="form-control select2me" id="tvbyr_jns_produk" name="jns_produk">
				              	<option value>Pilih Jenis Produk</option>
              					<?php /*  
					        	<option value="TVINDVS">Indovision</option>
								<option value="TVTOP">Top TV</option>
								<option value="TVOKE">Okevision</option>
								<option value="TVTLKMV">Telkomvision</option>
								<option value="TVTTNSVIONS">Transvision</option>
								<option value="TVYES">Yes TV</option>
								<option value="TVNEX">Nex Media</option>
								<option value="TVTOPAS">Topas TV</option>
								<option value="TVORANGE">Orange TV</option>
								<option value="TVBIG">BIG TV</option>
								<option value="TVINNOV">INNOVATE TV</option>
								<option value="VOCKV">K VISION</option>*/?>
			                </select>
			            </div>
			            <div id="tvbyr_jns_loader" style="display: none;height:11px;width:16px;"><img src="assets/images/loading.gif"></div>
		            </div>
                
		              <div class="<?php echo $colsm3;?> form-group">
		                <label>Nomor ID TV</label>
		                <input id="tvbyr_id_pel" type="text" class="form-control" placeholder="Contoh 1223243322">
		              </div>
		             <?php /* 
		             <div class="<?php echo $colsm3;?> form-group">
			             <div id="tvbyr_nominal" style="display: none;">
			                <label>Nominal Voucher</label>
			                <select class="form-control" name="nominal" id="tvbyr_nominal_dd">
			                </select>
			              </div>
			              <div id="tvbyr_nominal_loader" style="display: none;height:11px;width:16px;"><img src="assets/images/loading.gif"></div>
		              </div> */?>
	              </div>
              </div>
              <div class="<?php echo $colsm3;?> form-group">
                <label class="invisible">submit</label>
                <button type="button" id="tvbyr" class="popup-verify-form btn btn-primary btn-block" href="#verify-form">Beli</button>
              </div>
            </div>
          </div>
        </div>
         <?php /*
        <div role="tabpanel" class="tab-pane" id="kredit">
          <div class="tab-body">
            <div class="row">
              <div class="col-sm-9">
                <div class="row">
                  <div class="col-sm-6 form-group">
                    <label>Pilih Penyedia Pinjaman</label>
                    <select class="form-control">
                      <option>Adira Finance</option>
                      <option>BAF Finance</option>
                    </select>
                  </div>
                  <div class="col-sm-6 form-group">
                    <label>Nomor Kontrak</label>
                    <input type="text" class="form-control" placeholder="Masukkan Nomor Kontrak Anda">
                  </div>
                </div>
              </div>
              <div class="col-sm-3 form-group">
                <label class="invisible">submit</label>
                <button type="submit" class="btn btn-primary btn-block">Beli</button>
              </div>
            </div>
          </div>
        </div>
        */?>
        <div role="tabpanel" class="tab-pane" id="telkom">
          <div class="tab-body">
            <div class="row">
              <div class="<?php echo $colsm9;?>">
                	<div class="row">
	                <div class="<?php echo $colsm6;?> form-group">
		            <label>Pilih Jenis Tagihan</label>
                    <select   onchange="selectNominal('telkombyr',this.options[this.selectedIndex].value)" class="form-control select2me" id="telkombyr_jns_produk" name="jns_produk">
		              <option value>Pilih Tagihan Telkom</option>
                      <option value="TELKOM">Speedy,Vision,Telpon</option>
                      <?php /*?>
                      <option>Telkom Vision</option>
                      <option>Tagihan Telpon Rumah</option>*/?>
                    </select>
                    </div>
                
		              <div class="<?php echo $colsm6;?> form-group">
		            <label>Nomor Pelanggan</label>
                    <input  id="telkombyr_id_pel" type="text" class="form-control" placeholder="Contoh 02134567890">
                  </div>
                </div>
              </div>
             <div class="<?php echo $colsm3;?> form-group">
                 <label class="invisible">submit</label>
                <button id="telkombyr" type="submit"  class="popup-verify-form btn btn-primary btn-block" href="#verify-form">Beli</button>
              </div>
            </div>
          </div>
        </div>
        <?php /*
        */?>
        <div role="tabpanel" class="tab-pane" id="paket-data">
          <div class="tab-body">
            <div class="row">
              <div class="<?php echo $colsm3;?> form-group">
                   <label>Nomor Telepon</label>
                    <input id="paket_id_pel"  type="text" name="no_hp" class="form-control">
                  </div>
                  <div class="<?php echo $colsm3;?> form-group">
                    <label>Jenis Paket</label>
              			<select id="paket_jns_produk"  onchange="selectNominal('paket',this.options[this.selectedIndex].value)" class="form-control select2me" name="jns_produk">
							<option value="">Pilih</option>
							<option value="AXD">Axis Data</option>
							<option value="BXD">Axis Bronet</option>
							<option value="BOLT">BOLT Data</option>
							<option value="XH">XL Data</option>
							<option value="XCD">XL Combo</option>
							<option value="SMD">SmartFren Data</option>
							<option value="TD">Three Data</option>
							<option value="SD">TSel Data</option>
							<option value="SS">TSel SMS</option>
							<option value="IND">ISat Data</option>
							<option value="INC">ISat Combo</option>
							<option value="INE">ISat Extra</option>
						</select>
                  </div>          
              <div class="<?php echo $colsm3;?> form-group">
	             <div id="paket_nominal" style="display: none;">
	                <label>Nama Paket</label>
	                <select class="form-control" name="nominal" id="paket_nominal_dd">
	                </select>
	              </div>
	              <div id="paket_nominal_loader" style="display: none;height:11px;width:16px;"><img src="assets/images/loading.gif"></div>
              </div> 
              <div class="<?php echo $colsm3;?> form-group">
                <label class="invisible">submit</label>
                <button type="button" id= "paket" class="popup-verify-form btn btn-primary btn-block" href="#verify-form">Beli</button>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    
	</div>
    <script type="text/javascript" src="assets/js/jquery.mask.js"></script>
	<script type="text/javascript">
            	function selectGroup(name,idx_jenis){
                  var namedd='#'+name+'_jns_product';
            	  if(idx_jenis!=""){
            	    loadJenis(name,idx_jenis);//alert(name);
            	    $(namedd).html("<option value=''>Pilih Jenis Produk</option>");
            	  }else{
            	    $(namedd).html("<option value=''>Pilih Jenis Produk</option>");
            	  }
            	}

            	function loadJenis(nameType,loadId){
              	  var loadType=nameType+'_jns';
          		  var dataString = 'loadType='+ loadType +'&loadId='+ loadId;
          		  $("#"+loadType).hide();
                //    if(loadId.indexOf('VOC') == -1&&loadId.indexOf('TV')==-1){
	            		  $("#"+loadType+"_loader").show();
	            		  $.ajax({
		            		    type: "POST",
		            		    url: "home/loadData",
		            		    data: dataString,
		            		    cache: false,
		            		    success: function(result){
			            		//    alert(result);
			            //		console.log(result);
		                		  $("#"+loadType+"_loader").hide();
		            		      $("#"+loadType+"_produk").html("<option value=''>Pilih Jenis Produk</option>");
		            		      $("#"+loadType+"_produk").append(result);
          		  	  		  	  $("#"+loadType).show();
		                		  $("#"+nameType+"_nominal_dd").html("<option value=''>Pilih Harga</option>");
		            		    }
	            			});
                //    }   
          		}

            	function selectNominal(name,idx_jenis){
                    var namedd='#'+name+'_nominal_dd';
              	  if(idx_jenis!=""){
              	    loadData(name,idx_jenis);//alert(name);
              	    $(namedd).html("<option value=''>Pilih Harga</option>");
              	  }else{
              	    $(namedd).html("<option value=''>Pilih Harga</option>");
              	  }
              	}

            	function loadData(nameType,loadId){
                	  var loadType=nameType+'_nominal';
            		  var dataString = 'loadType='+ loadType +'&loadId='+ loadId;
            		//  $("#"+nameType).html('Beli');
            		  $("#"+loadType).hide();
            		 // alert(loadId.indexOf('BPJS'));
                      if(loadId.indexOf('POST') == -1&&loadId.indexOf('BPJS')==-1&&loadId.indexOf('TELKOM')==-1&&loadId.indexOf('PDAM')==-1&&loadId.indexOf('TV')==-1&&loadId.indexOf('VOC')==-1){
	            		  $("#"+loadType+"_loader").show();
	            	//	  alert(loadId);
						  //if($("#"+nameType+"_jns_produk option:selected").index>0)
                    	  	$("#"+nameType).html('Beli '+$("#"+nameType+"_jns_produk option:selected").text());  
                    	  
	            		//  $("#"+loadType+"_loader").fadeIn(400).html('<img src="assets/images/loading.gif" />');
	            		  $.ajax({
		            		    type: "POST",
		            		    url: "home/loadData",
		            		    data: dataString,
		            		    cache: false,
		            		    success: function(result){
			            		//    alert(result);
		                		  $("#"+loadType+"_loader").hide();
		            		      $("#"+loadType+"_dd").html("<option value=''>Pilih Harga</option>");
		            		      $("#"+loadType+"_dd").append(result);
            		  	  		  $("#"+loadType).show();
		            		    }
	            			});
                      }
                      else //if(loadId=='TAG')
                      {
                    	  //alert(nameType);
                    	  $("#"+nameType).html('Cek '+$("#"+nameType+"_jns_produk option:selected").text());      
                      }     
            		}

            	$('.popup-verify-form').magnificPopup({
            	    type: 'inline',
            	    preloader: false,
            	//    focus: '#name',

            	    // When elemened is focused, some mobile browsers in some cases zoom in
            	    // It looks not nice, so we disable it:
            	    callbacks: {
            	      beforeOpen: function() {
            	    	  var mp = $.magnificPopup.instance, 
            	    	  nm=mp.st.el.attr('id'),
            	    	  vrf='#verify_',
            	    	  head=$(vrf+"head"),
            	    	  form=$(vrf+"form"),
            	    	  load=$(vrf+"loader"),
            	    	  sts=$(vrf+"status");
            	    	  
            	    	  var form_data = {
                      	        nominal: $('#'+nm+'_nominal_dd').val(),
                      	        jenis: $('#'+nm+'_jns_produk').val(),
                      	        id_pel: $('#'+nm+'_id_pel').val()
                      	    };
            	    	  	head.html('Dalam Process');
	            		    form.html('');
	            		    load.show();
	            		    sts.html('');
            	    	  //console.log($('#'+nm+'_nominal_dd').val());
                      	    $.ajax({
                      	        url: '<?php echo site_url('payment/verifyPin'); ?>/'+nm,
                      	        type: 'POST',
                      	        data: form_data,
                      	      	cache: false,
		            		    success: function(msg) {
			            		   console.log(msg);
		            		    	if(msg.status=='0'){
				            		    //sts.hide();
			            		    	head.html(msg.head);
				            		 //   form.html(msg.form);
			            		    }
			            		    else{
			            		      	//sts.show();
			            		      	head.html('Info Gagal');
			                    		sts.html('<h4>'+msg.head+'</h4>');
			                    	}
		            		    	form.html(msg.form);
		            		    	form.show();
			                    	load.hide();
                      	        }, 
                      	  <?php  /*   	complete: function (response){
 								 if(window.console){
 								  console.log(response.responseText);
 								 }
                      	      	} */?>
                      	    });
            	      },
            	      <?php  	/*      open: function() {
            	    	  var mp = $.magnificPopup.instance,
            	          t = $(mp.currItem.el[0]);

            	      		console.log( t.data('custom') );
            	      }*/?>
            	    }
            	  });
            	            	
  <?php /*?>   
  $('.popup-with-form').magnificPopup({
    type: 'inline',
    preloader: false,
    focus: '#name',

    // When elemened is focused, some mobile browsers in some cases zoom in
    // It looks not nice, so we disable it:
    callbacks: {
      beforeOpen: function() {
        if($(window).width() < 700) {
          this.st.focus = false;
        } else {
          this.st.focus = '#name';
        }
      },
      open: function() {
        $.magnificPopup.instance.close = function() {
          // do something here
          if ($('#show-today-pop-up').is(':checked')) {
            set_cookie_tomorrow('eklanku_news', 'close');
          } else {
            set_cookie_minutes('eklanku_news', 'close', 10);
          }

          $.magnificPopup.proto.close.call(this);

        };
      }
    }
  });
*/?>
  function masking(name,mask){
	  //mask="(999) 999-9999";
	  
	 // alert('name:'+name+' mask:'+mask);
	  $("#"+name).mask(mask);
	  $("#"+name).on("blur", function() {
	      var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );

	      if( last.length == 5 ) {
	          var move = $(this).val().substr( $(this).val().indexOf("-") + 1, 1 );

	          var lastfour = last.substr(1,4);

	          var first = $(this).val().substr( 0, 9 );

	          $(this).val( first + move + '-' + lastfour );
	      }
	  });
	  /*$("#"+name).mask(mask)
	  .on("change", function() {

	      var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );

	      if( last.length == 3 ) {
	          var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );
	          var lastfour = move + last;
	          var first = $(this).val().substr( 0, 8 ); // Change 9 to 8 if you prefer mask without space: (99)9999?9-9999

	          $(this).val( first + '-' + lastfour );
	      }
	  })
	  .change();*/
	}; 
	$(function(){
		var nmask='9999-9999-9999-9999-9999';
		masking('pulsa_id_pel',nmask);
		masking('game_id_pel',nmask);
		masking('token_id_pel',nmask);
		masking('pascabyr_id_pel',nmask);
		masking('bpjsbyr_id_pel',nmask);
		masking('tvbyr_id_pel',nmask);
		masking('pdambyr_id_pel',nmask);
		masking('telkombyr_id_pel',nmask);
		masking('paket_id_pel',nmask);
	});
</script>