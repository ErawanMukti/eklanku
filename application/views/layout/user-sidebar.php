<div class="sidebar">
  <div class="card-box">
    <div class="card-header">
      <div class="row bg-warning alert" style="border-radius: 20px;">
        <div class="col-xs-3">
          <img alt="<?php echo h($member['name']) ?>" src="<?php echo asset_uri('images/avatar.jpeg') ?>">
        </div>
        <div class="col-xs-9">
          <div class="card-name">
            <h2><?php echo h($member['name']) ?></h2>
            <p><strong>User Shop</strong>: <?php echo ($member['shop_province_name']) . ' - ' . h($member['shop_regency_name']) ?></p>
            <p><strong>Saldo: IDR <?php echo h(number_format_id($member['wallet'])) ?></strong>
            </p>
            <div class="mt10">
			<?php if (empty($member['shop_name'])) : /*?>
                <button style="display: inline-block;margin-top: 5px;" class="btn btn-primary btn-xs" onclick="window.location='<?php echo base_url('user/open-shop') ?>'">
                  <i class="fa fa-fw fa-shopping-bag"></i>
                  Buka Toko
                </button>
            <?php else : ?>
                <button  type=button style="display: inline-block;margin-top: 5px;" class="btn btn-default"  onclick="window.location='<?php echo base_url('user/shop-dashboard') ?>'">
                  <i class="fa fa-fw fa-shopping-bag"></i>
                  Toko
                </button>
                <?php 
                <div class="modal fade" id="modal-confirm">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Maintenance</h4>
                      </div>
                      <div class="modal-body">
                        <div class="text-center">
                          Untuk sementara fitur 'Top Up' pada Eklanku Shopping belum dapat digunakan. Silahkan melakukan Top Up pada Member Area di
                          <a href="https://jual-beli.eklanku.com/" class="btn btn-primary">Jual Beli Eklanku</a><br>
                          Untuk menuju halaman login, silahkan klik link di bawah ini.<br>
                          <a href="https://jual-beli.eklanku.com/login" class="btn btn-primary mt10">Login Jual Beli Eklanku</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                */?>
            <?php endif; ?>
            </div>
          </div>
        </div>
        
      </div>
       <div class="mt10 md10" style="text-align:center;">
      		   <button type=button class="btn btn-info" onclick="window.location='<?php echo base_url('eklanpay/order');//#modal-confirm data-toggle="modal"?>'" >
                  <i class="fa fa-fw fa-money"></i>
                  Topup
                </button>
                <button type=button class="btn btn-warning" onclick="window.location='<?php echo base_url('eklanpay/tarik');//#modal-confirm data-toggle="modal"?>'" >
                  <i class="fa fa-fw fa-dollar"></i>
                  Tarik
                </button>
                
                
              </div>
    </div>

    <?php 
    if(!empty($member['eklanku_id'])):
	    $CI=&get_instance();
	    $CI->load->model('m_deposit');
	    $pointMbr=$CI->m_deposit->getPointMember($member['eklanku_id']);
	    $pointGrp=$CI->m_deposit->getPointGroup($member['eklanku_id']);
    ?>
    <div class="card-header">
      <div class="alert text-center" style="border: 1px red dashed;">
      	<div style=display:inline-block;>
      		<b>ID Member </b>: 
      		<a href="javascript:void(0);"><?php echo h($member['eklanku_id']);?></a>
      	</div>
      	<br>
 	<?php /*	<div style=display:inline-block;>
 			Point: <br>
 			<i class="fa fa-user"> <b>Point Pribadi:</b></i> 	
 			<span class="label label-success"> <?php echo h(number_format_id($pointMbr));?></span><br>
 			<i class="fa fa-users"> <b>Point Group:</b></i> 	
 			<span class="label label-primary"><?php echo h(number_format_id($pointGrp));?></span>
 			
 		</div>*/?>
      </div>
      Bagikan:
      <a class="btn btn-fb" href=""><i class="fa fa-fw fa-facebook"></i></a>
      <a class="btn btn-twitter" href=""><i class="fa fa-fw fa-twitter"></i></a>
      <a class="btn btn-gp" href=""><i class="fa fa-fw fa-google-plus"></i></a>
    </div>
    <?php endif;?>
    <div class="card-body hidden-xs">
		<ul class="menu-item">
			<li class="user-submenu-item"><a href="<?php echo base_url('user/confirm_transfer');?>"><i class="fa fa-check" aria-hidden="true"></i> Konfirmasi Transfer<!-- <span class="amount">999</span> --></a></li>
			<li class="user-submenu-item"><a href="<?php echo base_url('iklan/detail')?>"><i class="fa fa-bar-chart-o" aria-hidden="true"></i>Iklan Saya</a></li>
			<li class="user-submenu-item"><a href="<?php echo base_url('laporan')?>"><i class="fa fa-bar-chart-o" aria-hidden="true"></i>Laporan Transaksi</a></li>
			<li class="separator"></li>
            <?php /*
			<li><a href="<?php echo base_url()."user/discussion/".h(empty($prod['id'])?'':$prod['id']);?>">Diskusi Produk</a></li>
			<li><a href="<?php echo base_url('eklanpay/order') ?>">Top Up Deposit</a></li>
			<li><a href="<?php echo base_url()."user/help_center/";?>">Help Center</a></li>*/?>
		</ul>
    </div>
  </div>
  <?php /*
  <div class="card-box">
    <div class="card-header">
      <h2>Notifikasi</h2>
    </div>
    <div class="card-body">
      <ul class="list-group">
        <li class="list-group-item">
          Orders (status change): Complete -> Open<br>
          <a href="">Order #97</a><br>
          <span class="more-info text-muted">06/02/2017, 10:24</span>
        </li>
        <li class="list-group-item">
          Orders (status change): Complete -> Open<br>
          <a href="">Order #97</a><br>
          <span class="more-info text-muted">06/02/2017, 10:24</span>
        </li>
        <li class="list-group-item">
          Orders (status change): Complete -> Open<br>
          <a href="">Order #97</a><br>
          <span class="more-info text-muted">06/02/2017, 10:24</span>
        </li>
        <li class="list-group-item">
          Orders (status change): Complete -> Open<br>
          <a href="">Order #97</a><br>
          <span class="more-info text-muted">06/02/2017, 10:24</span>
        </li>
        <li class="list-group-item">
          Orders (status change): Complete -> Open<br>
          <a href="">Order #97</a><br>
          <span class="more-info text-muted">06/02/2017, 10:24</span>
        </li>
      </ul>
    </div>
  </div>
  */ ?>
</div>
