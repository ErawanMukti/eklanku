<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=1">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon"  href="<?php echo asset_uri('images/favicon.png') ?>">
     
    <title>Eklanku.com</title>

    <?php if (!empty($og_facebook)) : ?>
      <!-- Facebook Share -->
      <meta property="fb:app_id" content="1301072049981066">
      <meta property="og:type" content="Article">
      <meta property="og:site_name" content="eklanku.com">
      <meta property="og:title" content="<?php echo h($og_facebook['title']) ?>">
      <meta property="og:url" content="<?php echo current_url() ?>">
      <meta property="og:description" content="<?php echo h($og_facebook['description']) ?>">
      <meta property="og:image" content="<?php echo h($og_facebook['image']) ?>">
      <meta property="article:author" content="https://www.facebook.com/Eklankucom-1207022706005939/">
      <meta property="article:publisher" content="https://www.facebook.com/Eklankucom-1207022706005939/">
    <?php endif ?>

    <!-- CSS -->
    <link href="<?php echo asset_uri('css/font-awesome.min.css') ?>" rel="stylesheet" type='text/css'>
    <link href="<?php echo asset_uri('css/flaticon.css') ?>" rel="stylesheet" type='text/css'>
    <link href="<?php echo asset_uri('css/flexslider.css') ?>" rel="stylesheet" type='text/css'>
    <link href="<?php echo asset_uri('css/owl.carousel.css') ?>" rel="stylesheet" type='text/css'>
    <link href="<?php echo asset_uri('css/owl.theme.default.css') ?>" rel="stylesheet" type='text/css'>
    <link href="<?php echo asset_uri('css/magnific-popup.css') ?>" rel="stylesheet" type='text/css'>
    <link href="<?php echo asset_uri('css/bootstrap.css') ?>" rel="stylesheet" type='text/css'>
    <link href="<?php echo asset_uri('css/style.css') ?>" rel="stylesheet" type='text/css'>

    <!-- Product List -->
    <link href="<?php echo asset_uri('css/nouislider.css') ?>" rel="stylesheet" type='text/css'>

    <!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?53MAxaGoShIIgVJ3mDiIohWSIsveHHmr";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') ?>"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js') ?>"></script>
    <![endif]-->
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?53MAxaGoShIIgVJ3mDiIohWSIsveHHmr";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
    <!-- Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Nunito:400,700,900|Roboto:400,700,900" rel="stylesheet">

    <!-- jQuery -->
    <script type="text/javascript" src="<?php echo asset_uri('js/jquery.min.js') ?>"></script>
 <?php /*   <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyAfzmFeIYKB7UTrKPaVHD6DX8mxRPCVrp8&amp;sensor=false"></script>
    <script type="text/javascript" src="<?php echo asset_uri('js/jquery.countdown.min.js') ?>"></script>
*/?>
    <!-- JavaScript -->
    <script type="text/javascript" src="<?php echo asset_uri('js/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset_uri('js/jquery.flexslider-min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset_uri('js/owl.carousel.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset_uri('js/jquery.magnific-popup.min.js') ?>"></script>

    <!-- Product List -->
    <script type="text/javascript" src="<?php echo asset_uri('js/nouislider.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset_uri('js/sakarioka.cookie.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset_uri('js/sakarioka.function.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset_uri('js/jquery.dotdotdot.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset_uri('js/jquery.simplemasonry.min.js') ?>"></script>
  </head>
  <body ng-app="eklankuapp">
    <header id="header" class="hidden-xs">
    <?php /*  <!--
      <div id="navbar-top">
        <ul class="navbar-contain">
          <li>
            <button class="btn-link dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              Indonesian 
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li><a href="#">Indonesian</a></li>
              <li><a href="#">Japanese</a></li>
              <li><a href="#">Spanish</a></li>
            </ul>
          </li>
          <li>
            <button class="btn-link dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              IDR - Rupiah
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li><a href="#">USD - Dollars</a></li>
              <li><a href="#">JPY - Yen</a></li>
              <li><a href="#">EUR - Euro</a></li>
            </ul>
          </li>
          <li><a href="<?php echo h($company['facebook']) ?>" target="_blank"><i class="fa fa-fw fa-facebook"></i>Facebook</a></li>
          <li><a href="<?php echo h($company['twitter']) ?>" target="_blank"><i class="fa fa-fw fa-twitter"></i>Twitter</a></li>
          <li><a href="<?php echo h($company['instagram']) ?>" target="_blank"><i class="fa fa-fw fa-instagram"></i>Instagram</a></li>
          <li><i class="fa fa-fw fa-whatsapp"></i>Whatsapp</li>
          <li><a href="<?php echo base_url('about-us') ?>">About Us</a></li>
          <li><a href="<?php echo base_url('advanced-search') ?>">Advanced Search</a></li>
          <li><a href="<?php echo base_url('order-return') ?>">Order and Returns</a></li>
          <?php if (empty($is_member)) : ?>
            <li><a class="bold-link" href="<?php echo base_url('login') ?>">Sign In</a> or <a class="bold-link" href="<?php echo base_url('register') ?>">Create Account</a></li>
          <?php else : ?>
            <li class="username">
              Halo, <a class="bold-link" href="<?php echo base_url('user') ?>"><?php echo h($member['name']) ?></a>
              <div class="user-submenu-container">
                <ul class="user-submenu">
                  <li class="user-submenu-item avatar-container">
                    <img alt="<?php echo h($member['name']) ?>" src="<?php echo asset_uri('images/avatar.jpeg') ?>">
                  </li>
                  <li class="user-submenu-item"><a href="<?php echo base_url('user') ?>">Panel Akun</a></li>
                  <li class="user-submenu-item"><a href="">Keranjang<span class="amount">999</span></a></li>
                  <li class="user-submenu-item"><a href="">Pembelian<span class="amount">999</span></a></li>
                  <li class="user-submenu-item"><a href="<?php echo base_url('wishlist') ?>">Wishlist<span class="amount">999</span></a></li>
                  <li class="user-submenu-item user-submenu-separator"></li>
                  <li class="user-submenu-item"><a href="">Pengaturan</a></li>
                  <li class="user-submenu-item"><a href="<?php echo base_url('logout') ?>">Logout</a></li>
                </ul>
              </div>
            </li>
          <?php endif ?>
        </ul>
      </div>
      --> */?>
      <div id="navbar-primary">
        <div class="container">
          <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo base_url('home') ?>">
              <img alt="" src="<?php echo asset_uri('images/logo.png') ?>">
            </a>
          </div>
    <!--        <div class="input-group">
              <input type="text" class="form-control" placeholder="Cari Produk">
              <span class="input-group-btn">
                <button class="btn btn-primary" type="button"><i class="fa fa-fw fa-search"></i></button>
              </span>
            </div>-->
            <?php //$this->load->view("cyber/search");?>
           <div class="navbar-cart pull-right">
            <ul class="navbar navbar-nav">
              <!-- <li><a href="<?php echo base_url('wishlist') ?>"><i class="fa fa-fw fa-heart"></i>Wishlist</a></li> -->
               <li>
                <a href="<?php echo base_url('cart') ?>"><i class="fa fa-fw fa-shopping-cart"></i>
                  Cart <?php if (!empty($total_cart)) echo '(' . h($total_cart) . ')' ?>
                </a>
              </li>
              <?php if (empty($is_member)) : ?>
                <li>
                	<div class="pull-right nav">
                	<a class="bold-link" href="<?php echo base_url('register') ?>">Daftar</a> atau 
                	
						<link href="<?php echo asset_uri('css/styles-hmcs.css')?>" rel="stylesheet">
                	    <a href="#" class="bold-link quick-nav" data-toggle="popover" id="loginOrRegister" data-placement="bottom"><i class="fa fa-user"></i>&nbsp;Masuk</a>
	                    <div id="loginOrRegisterContent" class="hidden">
	                        <?php $attributes = array('role' => 'form','id'=>'fpoplogin');
              					echo form_open(base_url('login'), $attributes); ?>
			                  
			        			<div class="form-group">
	                                <input type="email" name="email" class="form-control" placeholder="Masukkan Email" required />
	                            </div>
	                            <input type="hidden" id="ceklanku" name="" value="" />
	                            <div class="form-group">
	                                <div class="input-group">
	                                    <input type="password" name="password" class="form-control" placeholder="Password" autocomplete="off" required />
	                                    <span class="input-group-btn">
	                                        <input id="btnlogin" type="submit" class="btn btn-primary" value="Login" disabled />
	                                    </span>
	                                </div>
	                            </div> 
	                            <label class="checkbox-inline">
	                                <input type="checkbox" name="rememberme" /> Remember Me &bull; <a href="/pwreset.php">Lupa Password?</a>
	                            </label> 
	                        <?php echo form_close() ?>
	                        <strong>Belum terdaftar?</strong> <a href="<?php echo base_url('register') ?>">Click disini untuk daftar...</a>
	              		</div>
	              </div>
	              
					<script src="<?php echo asset_uri('js/whmcs.js')?>"></script>
                </li>
                	
              <?php else : ?>
              <li class="username">
                Halo,
                <a class="bold-link" href="<?php echo base_url('user') ?>" title="<?php echo h($member['name']) ?>">
                  <?php if (isset($member['short_name'])) : ?>
                    <?php echo h($member['short_name']) ?>
                  <?php else : ?>
                    <?php echo h($member['name']) ?>
                  <?php endif ?>
                </a>
                <div class="user-submenu-container">
                  <ul class="user-submenu">
                    <li class="user-submenu-item avatar-container">
                      <img alt="<?php echo h($member['name']) ?>" src="<?php echo asset_uri('images/avatar.jpeg') ?>">
                    </li>
                    <li class="user-submenu-item"><a href="<?php echo base_url('user') ?>"><i class="fa fa-user" aria-hidden="true"></i> Personal Data</a></li>
                    
					<li class="user-submenu-item"><a href="<?php echo base_url('user/confirm_transfer');?>"><i class="fa fa-check" aria-hidden="true"></i> Konfirmasi Transfer<!-- <span class="amount">999</span> --></a></li>
                    <li class="user-submenu-item user-submenu-separator"></li>
                    <?php /*<li class="user-submenu-item"><a href="">Pengaturan</a></li>*/?>
                    <li class="user-submenu-item"><a href="<?php echo base_url('logout') ?>"><i class="fa fa-sign-out" aria-hidden="true"></i> Keluar</a></li>
                  </ul>
                </div>
              </li>
            <?php endif ?>
            </ul>
          </div>
        </div>
      </div>
      <div id="navbar-bottom">
        <div class="container">
          <?php /*<ul class="navbar-contain">
            <li class="menu-content-sidebar">
              <a href="<?php echo base_url('product') ?>"><i class="fa fa-fw fa-tasks"></i>Semua Kategori</a>
				<ul class="new-main-menu">
					<!--//get_items_by_categoryId get_items_by_subcategoryId get_items_by_lastcategoryId-->
					<?php foreach ($top_categories as $top_category) : ?>
					<li class="first-categories">
						<a href="<?php echo base_url()."product/item_by_category/".h($top_category['id']);?>"><?php echo h($top_category['name']) ?></a>
						<ul class="second-categories-holder">
							<div class="nav-submenu-left">
							<?php foreach ($top_category['sub_categories'] as $sub_category) : ?>
								<li class="second-categories">
									<a href="<?php echo base_url()."product/item_by_subcategory/".h($sub_category['id']);?>"><?php echo h($sub_category['name']) ?></a>
									<ul class="third-categories-holder">
									  <?php foreach ($sub_category['last_categories'] as $last_category) : ?>
										<li class="third-categories">
										  <a href="<?php echo base_url()."product/item_by_lastcategory/".h($last_category['id']);?>"><?php echo h($last_category['name']) ?></a>
										</li>
									  <?php endforeach ?>
									</ul>
								</li>
							<?php endforeach ?>
							</div>
						</ul>
					</li>
					<?php endforeach ?>
				</ul>
				
            </li>
            <li><a href="https://jual-beli.eklanku.com" target="_blank"><i class="fa fa-fw fa-shopping-basket"></i>Jual Beli</a></li>
          </ul>*/?>
        </div>
      </div>
    </header>
    <header id="mobile-header" class="visible-xs">
      <div class="mob-menu">
        <div class="icon-close">
          <i class="fa fa-fw fa-close"></i>
        </div>
        <div class="user-submenu-container" id="user-submenu">
          <ul class="user-submenu">
            <?php if (empty($is_member)) : ?>
              <li class="user-submenu-item"><a href="<?php echo base_url('home') ?>">Beranda</a></li>
              <li class="user-submenu-item"><a href="<?php echo base_url('register') ?>">Daftar</a></li>
              <li class="user-submenu-item"><a href="<?php echo base_url('login') ?>">Masuk</a></li>
              <li class="user-submenu-item"><a href="https://jual-beli.eklanku.com" target="_blank">Jual Beli</a></li>
            <?php else : ?>
              <li class="user-submenu-item avatar-container">
                <img alt="<?php if (!empty($member['name'])) echo h($member['name']) ?>" src="<?php echo asset_uri('images/avatar.jpeg') ?>">
                <p class="mob-username">Halo,
                <a class="bold-link" href="<?php echo base_url('user') ?>" title="<?php echo h($member['name']) ?>">
                  <?php if (isset($member['short_name'])) : ?>
                    <?php echo h($member['short_name']) ?>
                  <?php else : ?>
                    <?php echo h($member['name']) ?>
                  <?php endif ?>
                </a></p>
              </li>
              <li class="user-submenu-item"><a href="<?php echo base_url('home') ?>"><i class="fa fa-home" aria-hidden="true"></i> Beranda</a></li>
                    <li class="user-submenu-item"><a href="<?php echo base_url('user') ?>"><i class="fa fa-user" aria-hidden="true"></i> Personal Data</a></li>
                    <li class="user-submenu-item"><a href="<?php echo base_url('user/confirm_transfer');?>"><i class="fa fa-check" aria-hidden="true"></i> Konfirmasi Transfer<!-- <span class="amount">999</span> --></a></li>
                    
					<?php /*<li class="user-submenu-item"><a href="<?php echo base_url('wishlist/index');?>">Wishlist</a></li>
                    <li><a href="<?php echo base_url()."user/inbox_message/";?>">Kotak Masuk 
					<?php 
			        	$num=$this->m_messaging->count_new_inbox();
			        	echo $num>0?"<span class=\"badge\">$num</span>":"";
			        ?></a></li>
               <li class="user-submenu-item"><a href="">Pemesanan</a></li>
              <li class="user-submenu-item"><a href="">Pengaturan</a></li>
			  <li class="user-submenu-item"><a href="<?php echo base_url('user/help_center') ?>">Help Center</a></li>
              <li class="user-submenu-item"><a href="<?php echo base_url('eklanpay/order') ?>">Top Up</a></li>
              <li><a href="<?php echo base_url()."user/discussion/".h(empty($prod['id'])?'':$prod['id']);?>">Diskusi Produk</a></li>
			  <li><a href="<?php echo base_url('user/review/0/0/'.$member['id']);?>">Ulasan Produk</a></li>*/?>
              <li class="user-submenu-item"><a href="<?php echo base_url('laporan') ?>"><i class="fa fa-th" aria-hidden="true"></i> Laporan Transaksi</a></li>
              <!-- Seller sampe disini -->
              <li class="user-submenu-item"><a href="<?php echo base_url('logout') ?>"><i class="fa fa-sign-out" aria-hidden="true"></i>  Keluar</a></li>
            <?php endif ?>
          </ul>
        </div>
      </div>
      <div class="mob-main-header">
        <div class="container">
          <div class="row">
            <div class="col-xs-2 text-left">
              <a href="javascript:void(0)" class="icon-menu" id="icon-menu">
                <i class="fa fa-fw fa-bars"></i>
              </a>
            </div>
            <div class="col-xs-8 text-center">
              <a href="">
                <img alt="" src="<?php echo asset_uri('images/logo-white.png') ?>" class="mob-logo">
              </a>
            </div>
            <!--
            <div class="col-xs-2 text-right">
              <a href="javascript:void(0)" class="icon-menu" id="account-menu">
                <i class="fa fa-fw fa-ellipsis-v"></i>
              </a>
            </div>
            <div class="mob user-submenu-container" id="user-submenu">
              <ul class="user-submenu">
                <?php if (empty($is_member)) : ?>
                  <li class="user-submenu-item"><a href="<?php echo base_url('login') ?>">Sign In</a></li>
                  <li class="user-submenu-item"><a href="<?php echo base_url('register') ?>">Register</a></li>
                <?php else : ?>
                  <li class="user-submenu-item avatar-container">
                    <img alt="<?php if (!empty($member['name'])) echo h($member['name']) ?>" src="<?php echo asset_uri('images/avatar.jpeg') ?>">
                    <p class="mob-username">Halo,
                    <a class="bold-link" href="<?php echo base_url('user') ?>" title="<?php echo h($member['name']) ?>">
                      <?php if (isset($member['short_name'])) : ?>
                        <?php echo h($member['short_name']) ?>
                      <?php else : ?>
                        <?php echo h($member['name']) ?>
                      <?php endif ?>
                    </a></p>
                  </li>
                  <li class="user-submenu-item"><a href="<?php echo base_url('user') ?>">Panel Akun</a></li>
                  <li class="user-submenu-item"><a href="<?php echo base_url('logout') ?>">Logout</a></li>
                <?php endif ?>
              </ul>
            </div>
            -->
          </div>
        </div>
      </div>
      <div class="mob-search-header">
        <div class="container"><?php /*
          <div class="navbar-search">
            <div class="input-group">
              <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">All Categories<span class="caret"></span></button>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li><a href="#">Separated link</a></li>
                </ul>
              </div><!-- /btn-group -->
              <input type="text" class="form-control" aria-label="..." placeholder="Cari produk">
              <span class="input-group-btn">
                <button class="btn btn-primary" type="button"><i class="fa fa-fw fa-search"></i></button>
              </span>
            </div>
          </div>*/
           //$this->load->view("cyber/search");?>
        </div>
      </div>
    </header>
    <style>
    	#dashboard .container {
    		background: #fff;
		    box-shadow: 0 1px 2px rgba(0,0,0,0.4);
		    margin-bottom: 15px;
		    padding: 15px 20px;
    	}
    	#dashboard .container .sidebar{
    		margin-right: 10px;
    	}
    	#dashboard .container .sidebar .card-box{
    		margin: 0px;
    		padding:0px;
    		border:none;
    	}
    	
    </style>
    <?php echo $main_html ?>
    <footer id="footer">
      <div class="footer-feature">
        <div class="container">
          <div class="row">
            <div class="col-sm-13">
              <i class="fa fa-fw fa-truck"></i>
              <div class="footer-feature-holder">
                <p class="footer-feature-title">Gratis Kirim</p>
                <p class="footer-feature-subtitle">*S&amp;K Berlaku</p>
              </div>
            </div>
            <div class="col-sm-13">
              <i class="fa fa-fw fa-money"></i>
              <div class="footer-feature-holder">
                <p class="footer-feature-title">Bayar Ditempat</p>
                <p class="footer-feature-subtitle">*S&amp;K Berlaku</p>
              </div>
            </div>
            <div class="col-sm-13">
              <i class="fa fa-fw fa-gift"></i>
              <div class="footer-feature-holder">
                <p class="footer-feature-title">Hadiah</p>
                <p class="footer-feature-subtitle">*S&amp;K Berlaku</p>
              </div>
            </div>
            <div class="col-sm-13">
              <i class="fa fa-fw fa-phone-square"></i>
              <div class="footer-feature-holder">
                <p class="footer-feature-title">Kontak Kami</p>
                <p class="footer-feature-subtitle">+6281239681111</p>
              </div>
            </div>
            <div class="col-sm-13">
              <i class="fa fa-fw fa-users"></i>
              <div class="footer-feature-holder">
                <p class="footer-feature-title">Referal</p>
                <p class="footer-feature-subtitle">*S&amp;K Berlaku</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-main">
        <div class="container">
          <div class="row">
            <div class="col-sm-13">
              
            </div>
            <div class="col-sm-13">
              
            </div>
            <div class="col-sm-13">
              
            </div>
            <div class="col-sm-13">
              
            </div>
            <div class="col-sm-13">
              <h2>Contact Us</h2>
              <table>
                <tr>
                  <td>
                    <span class="fa-stack fa-lg">
                      <i class="fa fa-square fa-stack-2x"></i>
                      <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
                    </span>
                  </td>
                  <td>
                    Bali: Jln. Merdeka II no 6B Renon - Denpasar, Bali, Indonesia<br>
                    Surabaya: Mandiri Tower 1 Lantai 7 room 701. Jln. Basuki Rahmad
                  </td>
                </tr>
                <tr>
                  <td>
                    <span class="fa-stack fa-lg">
                      <i class="fa fa-square fa-stack-2x"></i>
                      <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                    </span>
                  </td>
                  <td>
                    info@eklanku.com<br>
                    customercare@eklanku.com
                  </td>
                </tr>
                <tr>
                  <td>
                    <span class="fa-stack fa-lg">
                      <i class="fa fa-square fa-stack-2x"></i>
                      <i class="fa fa-mobile fa-stack-1x fa-inverse"></i>
                    </span>
                  </td>
                  <td>
                    +6281239681111
                  </td>
                </tr>
              </table>
            </div>
          </div>
          
        </div>
      </div>
      <div class="footer-bottom">
        <div class="container">
          <div class="text-center">
            <p>&copy;2017. All Right Reserved. Designed by Eklanku</p>
            <img alt="" src="<?php echo asset_uri('images/payment.png') ?>">
          </div>
        </div>
      </div>
    </footer>
    <script type="text/javascript">
      $("#account-menu").click(function(){
        $("#user-submenu").toggle();
      });
      var main = function() {
        /* Push the body and the nav over by 285px over */
        $('#icon-menu').click(function() {
          $('.mob-menu').animate({
            left: "0px"
          }, 200);

          $('body').animate({
            left: "285px"
          }, 200);
        });

        /* Then push them back */
        $('.icon-close').click(function() {
          $('.mob-menu').animate({
            left: "-285px"
          }, 200);

          $('body').animate({
            left: "0px"
          }, 200);
        });
      };

      $(document).ready(main);

      $('.categories-main li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).fadeIn(0);
      }, function() {
        $(this).find('.dropdown-menu').stop(true, true).fadeOut(0);
      });
      var bground = ["F6E4A2", "5BB863", "F9D820","C5E6E7","D8EFF2",
                  "E3D992","EC976B","F5A2BD","F09EC5","268984","8AD3DD","F6F3E1"];
      $(document).ready(function() {
        $('#banner').flexslider({
          animation: "slide",
          controlNav:true,
          after: function()
          {
	          var current = $('.box-banner #banner li.flex-active-slide');
	          var clr=bground[current.index('.slides li')-1];
	          $('#fluid-bg-box').css({'background-color': '#'+clr});
	         // alert(clr);
          }
        });
/*
        $('#lelang-super').flexslider({
          animation: "slide"
        });

        $('.owl-carousel').owlCarousel({
          loop:true,
          margin:10,
          nav:true,
          navText:[
            '<i class="fa fa-fw fa-chevron-left"></i>',
            '<i class="fa fa-fw fa-chevron-right"></i>'
          ],
          responsive : {
            0: {
              items:1
            },

            480: {
              items:4
            }
          }
        });*/

        $(".twolines").dotdotdot({
          height  : 30
        });

        $(".dropdown-menu").hover(function(){
          $(this).closest(".dropdown-submenu").addClass('hoverClass')}, 
          function(){
            $(this).closest(".dropdown-submenu").removeClass('hoverClass')
          });

        $('.masonry').simplemasonry()
      });
<?php /*
      function CountDown(date,id){
        dateNow = new Date();
        amount = date.getTime() - dateNow.getTime();
        delete dateNow;
        if(amount < 0){
          id.html("Now!");
        } else{
          days=0;hours=0;mins=0;secs=0;out="";
          amount = Math.floor(amount/1000);
          days=Math.floor(amount/86400);
          amount=amount%86400;
          hours=Math.floor(amount/3600);
          amount=amount%3600;
          mins=Math.floor(amount/60);
          amount=amount%60;
          secs=Math.floor(amount);
          out += "<div class='deals-time time-day'><div class='num-time'>"+ days +"</div><div class='title-time'>"+((days==1)?"Day":"Days")+"</div></div>";
          out += "<div class='deals-time time-hours'><div class='num-time'>"+ hours +"</div><div class='title-time'>"+((hours==1)?"Hour":"Hours")+"</div></div>";
          out += "<div class='deals-time time-mins'><div class='num-time'>"+ mins +"</div><div class='title-time'>"+((mins==1)?"Minute":"Minutes")+"</div></div>";
          out += "<div class='deals-time time-secs'><div class='num-time'>"+ secs +"</div><div class='title-time'>"+((secs==1)?"Second":"Seconds")+"</div></div>";
          out = out.substr(0,out.length-2);
          id.html(out);
          setTimeout(function(){CountDown(date,id)}, 1000);
        }
      }

      $( ".deals-countdown").each(function() {
        var timer = $(this).data('timer');
        var data = new Date(timer);
        CountDown(data,$(this));
      });
*/?>
      $(window).scroll(function(){
        var sticky = $('.sticky'),
            scroll = $(window).scrollTop();

        if (scroll >= 150) sticky.addClass('fixed');
        else sticky.removeClass('fixed');
      });

      // $(window).resize(function() {
      //   check_mobile();
      // });

      // check_mobile();

      // function check_mobile()
      // {
      //   var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;

      //   if (width < 768) {
      //     $('body').addClass('mobile');
      //   } else {
      //     $('body').removeClass('mobile');
      //   }
      // }
    </script>
   
  </body>
</html>