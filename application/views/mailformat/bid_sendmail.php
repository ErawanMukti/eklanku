<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width" />
  <title>Title</title>
  <link rel="stylesheet" href="<?php echo asset_uri('css/foundation-emails.css')?>" />
    <style>
      .header {
        padding: 10px;
        background-color: #fe0000 !important;
      }

      .header img {
        max-width: 200px;
      }

      .table-body {
        background-color: #ffffff !important;
      }

      .page-header {
        border-bottom: 3px solid #fe0000;
        color: #fe0000;
        font-weight: bold;
      }

      .bold {
        font-weight: bold;
      }

      .italic {
        font-style: italic;
      }

      .content td,
      .content th {
        font-size: 14px;
      }

      .content ol {
        margin:0;
      }

      .content-table tr,
      .table tr {
        border-bottom: 1px solid #dedede;
      }

      .padding th,
      .padding td {
        padding: 5px;
      }

      .table th {
        font-weight: bold;
      }

      .table th:last-child,
      .table td:last-child {
        text-align: right;
      }

      .float-right td {
        text-align: right;
      }

      .text-red {
        color: #fe0000;
      }

      .button {
        background-color: #fe0000;
        color: #fff;
        text-transform: uppercase;
        text-align: center !important;
      }

      .none {
        border: none !important;
      }
  </style>
</head>

<body>
  <!-- <style> -->
  <table class="body" data-made-with-foundation>
    <tr>
      <td class="float-center" align="center" valign="top">
        <center data-parsed>
          <table class="spacer">
            <tbody>
              <tr>
                <td height="20px" style="font-size:20px; line-height:20px"></td>
              </tr>
            </tbody>
          </table>
          <table class="container header">
            <tbody>
              <tr>
                <td>
                  <table class="row collapse">
                    <tbody>
                      <tr>
                        <th class="small-12 large-12 columns first last" style="padding: 1px 10px 10px 0px;">
                          <table>
                            <tbody>
                              <tr>
                                <th style="background-color: #fff3d9;padding: 5px;">
                                  <img alt="logo" src="<?php echo asset_uri('images/logo.png')?>">
                                </th>
                              </tr>
                            </tbody>
                          </table>
                        </th>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <table class="container table-body">
            <tbody>
              <tr>
                <td>
                  <table class="spacer">
                    <tbody>
                      <tr>
                        <td height="20px" style="font-size:20px; line-height:20px"></td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- Booking Details -->
                  <table class="row">
                    <tbody>
                      <tr>
                        <th class="small-12 large-12 first last">
                          <h5 class="page-header"><?php echo $title?> Details</h5>
                        </th>
                      </tr>
                    </tbody>
                  </table>
                  <table class="row content">
                    <tbody>
                      <tr>
                        <td class="small-6 large-6 columns first">
                          <table>
                            <tbody>
                              <tr>
                                <td class="bold"><?php echo $no_order?></td>
                              </tr>
                            </tbody>
                          </table>
                          <table class="spacer">
                            <tbody>
                              <tr>
                                <td height="10px" style="font-size:10px; line-height:10px"></td>
                              </tr>
                            </tbody>  
                          </table>
                          <table>
                            <tbody>
                              <tr>
                                <td class="bold italic"><?php echo $title?> Date</td>
                              </tr>
                              <tr>
                                <td class="bold"><?php echo $date_order?></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                        <td class="small-6 large-6 columns last">
                          <table class="row content-table padding">
                            <tbody>
                              <tr>
                                <th class="small-4 large-4">Name</th>
                                <td class="small-8 large-8"><?php echo $name_order?></td>
                              </tr>
                              <tr>
                                <th class="small-4 large-4">Email</th>
                                <td class="small-8 large-8"><?php echo $email_order?></td>
                              </tr>
                              <tr>
                                <th class="small-4 large-4">Phone</th>
                                <td class="small-8 large-8"><?php echo $phone_order?></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- End Booking Details -->
                  <?php if(!empty($participant)):?>
                  <!-- Participant Details -->
                  <table class="row">
                    <tbody>
                      <tr>
                        <th class="small-12 large-12 first last">
                          <h5 class="page-header">Participant Details</h5>
                        </th>
                      </tr>
                    </tbody>
                  </table>
                  <table class="row content">
                    <tbody>
                      <tr>
                        <td class="small-12 large-12 columns">
                          <ol><?php 
                            	foreach($participant as $_part):
                          		foreach($_part as $key=>$val)
                          				$$key=$val;?>
                            <li><?php echo "$name / $eklanku_id";?></li>
                            <?php endforeach;?>
                          </ol>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- End Participant Details -->
                  <?php 
                  	endif;
                   	if(!empty($list)):?>
                  <!-- Reservation Details -->
                  <table class="row">
                    <tbody>
                      <tr>
                        <th class="small-12 large-12 first last">
                          <h5 class="page-header"><?php echo $headlist;?> Details</h5>
                        </th>
                      </tr>
                    </tbody>
                  </table>
                  <table class="row content">
                    <tbody>
                      <tr>
                        <td class="small-12 large-12 first-last">
                          <table align="center" class="float-center table wrapper padding">
                            <thead>
                              <tr>
                                <th>Program</th>
                                <th>Date of Service</th>
                                <th>Time of Service</th>
                                <th></th>
                                <th></th>
                                <th>Amount</th>
                              </tr>
                            </thead>
                            <tbody>
                            <?php 
                            foreach($list as $_list):
                            	foreach($_list as $key=>$val)
                            		$$key=$val;
                            echo "
                              <tr>
                                <td>$kode</td>
                                <td>$date</td>
                                <td>$service</td>
                                <td>$price</td>
                              </tr>";
                            endforeach;
                             ?> 
                            </tbody>
                            <tr class="float-right none">
                              <td class="bold italic" colspan="5"><?php echo $title?> amount</td>
                              <td class="bold italic"><?php echo $total?></td>
                            </tr>
                            <tr class="float-right none">
                              <td class="bold italic text-red" colspan="5"><?php echo $title?> status</td>
                              <td class="bold italic button"><?php echo $status?></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- End Reservation Details -->
                   <?php endif;?>
                  <!-- Notes -->
                  <table class="row">
                    <tbody>
                      <tr>
                        <th class="small-12 large-12 first last">
                          <h5 class="page-header">Notes</h5>
                        </th>
                      </tr>
                    </tbody>
                  </table>
                  <table class="row content">
                    <tbody>
                      <tr>
                        <td class="small-12 large-12 columns">
                          <ol><?php foreach($notes as $key=>$val):?>
                            <li><?php echo $val?></li>
                            <?php endforeach;?>
                          </ol>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- End Notes -->
                  <!-- Cancelation Policy -->
                  <table class="row">
                    <tbody>
                      <tr>
                        <th class="small-12 large-12 first last">
                          <h5 class="page-header">Cancelation Policy</h5>
                        </th>
                      </tr>
                    </tbody>
                  </table>
                  <table class="row content">
                    <tbody>
                      <tr>
                        <td class="small-12 large-12 columns">
                          <ol>
                          <?php foreach($cancelation as $key=>$val):?>
                            <li><?php echo $val?></li>
                          <?php endforeach;?>
                          </ol>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- End Cancelation Policy -->
                </td>
              </tr>
            </tbody>
          </table>
          <table class="spacer">
            <tbody>
              <tr>
                <td height="20px" style="font-size:20px; line-height:20px"></td>
              </tr>
            </tbody>
          </table>
        </center>
      </td>
    </tr>
  </table>
</body>
</html>