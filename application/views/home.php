<main>

<div class="container-fluid" id="fluid-bg-box" style="background-color: #F6E4A2;
background: linear-gradient(160deg, rgba(255, 252, 248,0.65) 0%,rgba(244, 255, 178,0.65) 50%,rgba(251, 185, 150,0.65) 100%);
background: -webkit-linear-gradient(-70deg, rgba(255, 252, 248,0.65) 0%,rgba(244, 255, 178,0.65) 50%,rgba(251, 185, 150,0.65) 100%);
background: -o-linear-gradient(-70deg, rgba(255, 252, 248,0.65) 0%,rgba(244, 255, 178,0.65) 50%,rgba(251, 185, 150,0.65) 100%);
background: -moz-linear-gradient(-70deg, rgba(255, 252, 248,0.65) 0%,rgba(244, 255, 178,0.65) 50%,rgba(251, 185, 150,0.65) 100%);
">
  <div class="container" style="margin-bottom: 45px;">
    <div id="main-images">
      <div class="row box-banner">
        <div class="col-md-6">
          <div class="flexslider" id="banner">
            <ul class="slides">
              <?php foreach ($adv_slider as $key=>$val) : ?>
                <li>
                  <a href="">
                    <img class="mb12" src="<?php echo asset_uri('images/banner/'.$val) ?>">
                  </a>
                </li>
              <?php endforeach ?>
            </ul>
          </div>
        </div>
        <div class="col-md-6" id="cat_payment">
          <div class="row">
            <?php $this->load->view('layout/product-payment',array('isSide'=>true)); ?>
            
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
          
</main>
