/**
 * The parameters of the function above are the name of the cookie (cname),
 * the value of the cookie (cvalue), and the number of days until the cookie should expire (exdays).
 * The function sets a cookie by adding together the cookiename, the cookie value, and the expires string.
 */
function set_cookie_minutes(cname, cvalue, exminutes) {
  var d = new Date();
  d.setTime(d.getTime() + (exminutes * 60 * 1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
} 

function set_cookie_days(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
} 

function set_cookie_tomorrow(cname, cvalue) {
  var d = new Date();
  d.setDate(d.getDate() + 1);
  var expires = "expires="+ d.toUTCString();
  console.log(expires);
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

/**
 * Take the cookiename as parameter (cname).
 * Create a variable (name) with the text to search for (cname + "=").
 * Decode the cookie string, to handle cookies with special characters, e.g. '$'
 * Split document.cookie on semicolons into an array called ca (ca = decodedCookie.split(';')).
 * Loop through the ca array (i = 0; i < ca.length; i++), and read out each value c = ca[i]).
 * If the cookie is found (c.indexOf(name) == 0), return the value of the cookie (c.substring(name.length, c.length).
 * If the cookie is not found, return "".
 */
function get_cookie(cname) {
  var name = cname + "=";
  var decoded_cookie = decodeURIComponent(document.cookie);
  var ca = decoded_cookie.split(';');

  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];

    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }

    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }

  return "";
}

/**
 * If the cookie is set it will display a greeting.
 * If the cookie is not set, it will display a prompt box,
 * asking for the name of the user, and stores the username cookie for 365 days,
 * by calling the setCookie function:
 */
function check_cookie() {
  var username = get_cookie("username");

  if (username != "") {
    alert("Welcome again " + username);
  } else {
    username = prompt("Please enter your name:", "");

    if (username != "" && username != null) {
      setCookie("username", username, 365);
    }
  }
} 

/**
 * Source: https://www.w3schools.com/js/js_cookies.asp
 * Modify by Sakarioka.Com
 */