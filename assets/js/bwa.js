var BwaLogger = new function() {
	var a = 0;
	this.setLevel = function(b) {
		a = b
	};
	this.log = function(b) {
		if (a >= 1) {
			console.log(b)
		}
	};
	this.debug = function(b) {
		if (a >= 2) {
			console.debug(b)
		}
	};
	this.error = function(b) {
		if (a >= 3) {
			console.error(b)
		}
	}
};
var customParameterName = [ "source", "refid" ];
var fixParameterName = [ "utm_source", "utm_medium" ];
var AjaxRequest = function(q) {
	var h = "POST";
	var e;
	var d;
	var a = [];
	var p;
	var j;
	var k = {
		success : null,
		fail : null,
		always : null
	};
	var c;
	if (q) {
		h = q.type || "POST";
		e = q.url;
		d = q.data;
		a = q.headers || [];
		p = q.jsonp || null;
		j = q.jsonpCallback || null
	}
	this.always = function(i) {
		if (b(i)) {
			k.always = i
		} else {
			BwaLogger.error("[AjaxRequest] ERROR - func is not a function!")
		}
		return this
	};
	this.success = function(i) {
		if (b(i)) {
			k.success = i
		} else {
			BwaLogger.error("[AjaxRequest] ERROR - func is not a function!")
		}
		return this
	};
	this.error = function(i) {
		if (b(i)) {
			k.error = i
		} else {
			BwaLogger.debug("[AjaxRequest] ERROR - func is not a function!")
		}
		return this
	};
	var b = function(i) {
		return i && (typeof i) == "function"
	};
	var l = function(r) {
		var t = "";
		var s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		for ( var u = 0; u < r; u++) {
			t += s.charAt(Math.floor(Math.random() * s.length))
		}
		return t
	};
	if (p) {
		window.AjaxRequestJsonp = window.AjaxRequestJsonp || {};
		var n = l(10);
		while (window.AjaxRequestJsonp[n]) {
			n = l(10)
		}
		var m = document.createElement("script");
		var f = document.getElementsByTagName("head")[0];
		window.AjaxRequestJsonp[n] = function(i) {
			if (b(k.success)) {
				k.success(i)
			}
			if (b(k.always)) {
				k.always()
			}
			if (b(j)) {
				_jsonCallback(i)
			}
			f.removeChild(m)
		};
		n = "window.AjaxRequestJsonp." + n;
		var o = e + "&" + p + "=" + n;
		m.src = o;
		m.type = "application/javascript";
		m.async = true;
		f.insertBefore(m, f.firstChild)
	} else {
		c = null;
		if (window.XMLHttpRequest) {
			c = new XMLHttpRequest()
		} else {
			c = new ActiveXObject("Microsoft.XMLHTTP")
		}
		c.onreadystatechange = function() {
			if (c.readyState == 4) {
				if (c.status == 200 && b(k.success)) {
					k.success(c.responseText)
				} else {
					if (b(k.error)) {
						k.error(c.status)
					}
				}
				if (b(k.always)) {
					k.always()
				}
			}
		};
		c.open(h, e, true);
		for ( var g = 0; g < a.length; g++) {
			c.setRequestHeader(a[g][0], a[g][1])
		}
		if (h == "POST") {
			c.send(d)
		} else {
			c.send()
		}
	}
};
var CorsRequest = function(l) {
	var g = "POST";
	var e;
	var d;
	var a = [];
	var k;
	var h;
	var j = {
		success : null,
		fail : null,
		always : null
	};
	var c;
	if (l) {
		g = l.type || "POST";
		e = l.url;
		d = l.data;
		a = l.headers || [];
		k = l.jsonp || null;
		h = l.jsonpCallback || null
	}
	this.always = function(i) {
		if (b(i)) {
			j.always = i
		} else {
			BwaLogger.debug("[CorsRequest] ERROR - func is not a function!")
		}
		return this
	};
	this.success = function(i) {
		if (b(i)) {
			j.success = i
		} else {
			BwaLogger.debug("[CorsRequest] ERROR - func is not a function!")
		}
		return this
	};
	this.error = function(i) {
		if (b(i)) {
			j.error = i
		} else {
			BwaLogger.debug("[CorsRequest] ERROR - func is not a function!")
		}
		return this
	};
	var b = function(i) {
		return i && (typeof i) == "function"
	};
	c = new XMLHttpRequest();
	if ("withCredentials" in c) {
		c.open(g, e, true)
	} else {
		if (typeof XDomainRequest != "undefined") {
			c = new XDomainRequest();
			c.open(g, e)
		} else {
			c = null
		}
	}
	if (!c) {
		BwaLogger.debug("CORS not supported")
	} else {
		for ( var f = 0; f < a.length; f++) {
			c.setRequestHeader(a[f][0], a[f][1])
		}
		if (g == "POST") {
			c.send(d)
		} else {
			c.send()
		}
	}
};
var AjaxHelper = new function() {
	this.doGet = function(a, c) {
		var b = new AjaxRequest({
			type : "GET",
			url : a,
			headers : [ [ "Content-Type", c ] ]
		});
		return b
	};
	this.doPost = function(a, b) {
		var c = new AjaxRequest(
				{
					type : "POST",
					url : a,
					data : b,
					headers : [ [ "Content-Type",
							"application/x-www-form-urlencoded" ] ]
				});
		return c
	};
	this.doJsonp = function(a, d, b) {
		var c = new AjaxRequest({
			url : a,
			jsonp : d,
			jsonpCallback : b
		});
		return c
	}
};
var CorsHelper = new function() {
	this.doPost = function(a, c) {
		BwaLogger.debug(c);
		var b = new CorsRequest(
				{
					type : "POST",
					url : a,
					data : c,
					headers : [ [ "Content-Type",
							"application/x-www-form-urlencoded" ] ]
				});
		return b
	}
};
var TimedTrackOperationQueue = function(i) {
	var a = [];
	var b = i || {};
	var d = b.dataKey || "data";
	var f = b.waitInMs || 2000;
	var h = b.callback || (function() {
	});
	var c = true;
	var e = function() {
		while (a.length) {
			a.pop()
		}
	};
	var g = function() {
		var j = [];
		for ( var k = 0; k < a.length; k++) {
			j.push([ JSON.stringify(a[k]) ])
		}
		e();
		return j
	};
	this.addTrackOperation = function(k, j) {
		a.push({
			cmdName : k,
			parameters : j
		})
	};
	this.isAvailable = function() {
		return c
	};
	this.setAvailable = function(j) {
		c = j
	};
	setTimeout(function() {
		h(g());
		c = false
	}, f)
};
var bwa = function() {
	var f = "";
	var g = "https://bwa.blibli.com";
	var l = "__bwa_account_id";
	var e = "__bwa_user_id";
	var i = "__bwa_session_id";
	var c = "__bwa_session_action_sequence";
	var k = "__bwa_session_referrer_data";
	var m = "__bwa_session_search_engine_keyword_data";
	var q = "__bwa_session_utm_campaign";
	var o = "__bwa_session_utm_medium";
	var b = "__bwa_session_utm_source";
	var a = "__bwa_session_utm_content";
	var p = "__bwa_session_utm_term";
	var h = "__bwa_user_session_sequence";
	var j = new Date();
	var n = this;
	var d = new TimedTrackOperationQueue({
		callback : function(t) {
			BwaLogger.debug("start - bannerImpressionCallback");
			var r = "batch";
			if (t != null && t.length > 0) {
				var s = g + "/" + r;
				n._hitTrackerCors(s, "data=[" + t + "]")
			}
			BwaLogger.debug("stop - bannerImpressionCallback")
		}
	});
			this._getCurrentDevice = function() {
				var z, v, s, u;
				var B = new Array();
				z = window.device;
				window.device = {};
				s = window.navigator.userAgent.toLowerCase();
				device.ios = function() {
					return device.iphone() || device.ipod() || device.ipad()
				};
				device.iphone = function() {
					return v("iphone")
				};
				device.ipod = function() {
					return v("ipod")
				};
				device.ipad = function() {
					return v("ipad")
				};
				device.android = function() {
					return v("android")
				};
				device.macintosh = function() {
					return v("macintosh")
				};
				device.androidPhone = function() {
					return device.android() && v("mobile")
				};
				device.androidTablet = function() {
					return device.android() && !v("mobile")
				};
				device.blackberry = function() {
					return v("blackberry") || v("bb10") || v("rim")
				};
				device.blackberryPhone = function() {
					return device.blackberry() && !v("tablet")
				};
				device.blackberryTablet = function() {
					return device.blackberry() && v("tablet")
				};
				device.windows = function() {
					return v("windows")
				};
				device.windowsPhone = function() {
					return device.windows() && v("phone")
				};
				device.windowsTablet = function() {
					return device.windows() && v("touch")
				};
				device.mobile = function() {
					return device.androidPhone() || device.iphone()
							|| device.ipod() || device.windowsPhone()
							|| device.blackberryPhone() || device.fxosPhone()
							|| device.meego()
				};
				device.tablet = function() {
					return device.ipad() || device.androidTablet()
							|| device.blackberryTablet()
							|| device.windowsTablet() || device.fxosTablet()
				};
				device.noConflict = function() {
					window.device = z;
					return this
				};
				v = function(D) {
					return s.indexOf(D) !== -1
				};
				device.firefox = function() {
					return (/firefox[\/\s](\d+\.\d+)/.test(s))
				};
				device.IE = function() {
					return (/msie (\d+\.\d+);/.test(s))
				};
				device.Chrome = function() {
					return (/chrome[\/\s](\d+\.\d+)/.test(s))
				};
				device.Opera = function() {
					return (/opera[\/\s](\d+\.\d+)/.test(s))
				};
				device.Safari = function() {
					return (/safari[\/\s](\d+\.\d+)/.test(s))
				};
				if (device.ios()) {
					B[0] = "iOS";
					if (device.ipad()) {
						B[1] = "iPad";
						B[2] = "tablet"
					} else {
						if (device.iphone()) {
							B[1] = "iPhone";
							B[2] = "mobile"
						} else {
							if (device.ipod()) {
								B[1] = "iPod";
								B[2] = "mobile"
							}
						}
					}
				} else {
					if (device.android()) {
						B[0] = "Android";
						if (device.androidTablet()) {
							B[1] = "android";
							B[2] = "tablet"
						} else {
							B[1] = "android";
							B[2] = "mobile"
						}
					} else {
						if (device.blackberry()) {
							B[0] = "Blackberry";
							if (device.blackberryTablet()) {
								B[1] = "blackberry";
								B[2] = "tablet"
							} else {
								B[1] = "blackberry";
								B[2] = "mobile"
							}
						} else {
							if (device.windows()) {
								B[0] = "Windows";
								if (device.windowsTablet()) {
									B[1] = "windows";
									B[2] = "tablet"
								} else {
									if (device.windowsPhone()) {
										B[1] = "windows";
										B[2] = "mobile"
									} else {
										B[1] = "windows";
										B[2] = "desktop"
									}
								}
							} else {
								if (device.macintosh()) {
									B[0] = "Mac OS";
									B[1] = "macintosh";
									B[2] = "desktop"
								} else {
									B[0] = "Others OS";
									if (v("tablet")) {
										B[1] = "other";
										B[2] = "tablet"
									} else {
										if (v("mobile")) {
											B[1] = "other";
											B[2] = "mobile"
										} else {
											B[1] = "other";
											B[2] = "desktop"
										}
									}
								}
							}
						}
					}
				}
				if (device.firefox()) {
					u = /firefox[\/\s](\d+\.\d+)/.test(s);
					var w = RegExp.$1;
					B[3] = "Firefox";
					B[4] = w
				} else {
					if (device.IE()) {
						u = /msie (\d+\.\d+);/.test(s);
						var A = RegExp.$1;
						B[3] = "IE";
						B[4] = A
					} else {
						if (device.Chrome()) {
							u = /chrome[\/\s](\d+\.\d+)/.test(s);
							var t = RegExp.$1;
							B[3] = "Chrome";
							B[4] = t
						} else {
							if (device.Opera()) {
								u = /opera[\/\s](\d+\.\d+)/.test(s);
								var r = RegExp.$1;
								B[3] = "Opera";
								B[4] = r
							} else {
								if (device.Safari()) {
									u = /safari[\/\s](\d+\.\d+)/.test(s);
									var C = RegExp.$1;
									B[3] = "Safari";
									B[4] = C
								} else {
									if (!u) {
										B[3] = "Other Browser";
										B[4] = "N/A"
									}
								}
							}
						}
					}
				}
				BwaLogger.debug(B);
				return B
			},
			this._getCookie = function(t) {
				var s = (document.cookie + ";").match(new RegExp(t + "=.*;"));
				var r = null;
				if (s != null) {
					r = s[0].split(/=|;/)[1]
				}
				return r
			},
			this._setCookie = function(r, u, t) {
				var s = r + "=" + u + ";expires=" + t.toGMTString() + ";path=/";
				document.cookie = s
			},
			this._deleteCookie = function(r) {
				var s = this._getCookieExpireTimeInMinute(-1);
				this._setCookie(r, "", s)
			},
			this._getHashCode = function(u) {
				var r = 0;
				if (u.length == 0) {
					return r
				}
				for ( var t = 0; t < u.length; t++) {
					var s = u.charCodeAt(t);
					r = ((r << 5) - r) + s;
					r = r & r
				}
				return Math.abs(r)
			},
			this._getRandomNumber = function() {
				return Math.floor((Math.random() * (10000000000000000)) + 1)
			},
			this._getCurrentTimeStamp = function() {
				return Math.round(new Date().getTime() / 1000)
			},
			this._generateTrackerUrl = function(w, t) {
				BwaLogger.debug("start - bwa._generateTrackerUrl");
				var v;
				var u;
				if (arguments.length == 4) {
					v = arguments[2];
					u = arguments[3]
				} else {
					if (arguments.length == 3) {
						v = arguments[2].map(function(z) {
							return z[0]
						});
						u = arguments[2].map(function(z) {
							return z[1]
						})
					} else {
						throw "invalid number of arguments : "
								+ arguments.length
					}
				}
				var s = w + "/" + t;
				for ( var r = 0; r < v.length; r++) {
					if (r == 0) {
						s += "?" + v[r] + "=" + encodeURIComponent(u[r])
					} else {
						if (v[r] !== undefined) {
							s += "&" + v[r] + "=" + encodeURIComponent(u[r])
						}
					}
				}
				BwaLogger.debug("bwa._generateTrackerUrl - trackerUrl : " + s);
				BwaLogger.debug("end - bwa._generateTrackerUrl");
				return s
			},
			this._hitTrackerGet = function(r, u, t) {
				BwaLogger.debug("start - _hitTrackerGet");
				var s = AjaxHelper.doJsonp(r, "callback").success(function() {
					BwaLogger.debug("bwa tracker hit success")
				});
				if (typeof t == "function") {
					s.always(function() {
						t()
					})
				}
				BwaLogger.debug("end - _hitTrackerGet");
				return s
			},
			this._hitTrackerCors = function(r, t, u) {
				BwaLogger.debug("start - _hitTrackerCors");
				var s = CorsHelper.doPost(r, t).success(function() {
					BwaLogger.debug("bwa tracker hit success")
				});
				BwaLogger.debug("end - _hitTrackerCors");
				return s
			},
			this._getReferrer = function(v) {
				BwaLogger.debug("start - bwa._getReferrer");
				var u = v.document.location.href;
				BwaLogger.debug("bwa._getReferrer - currentUrl : " + u);
				BwaLogger.debug("bwa._getReferrer - referrer : "
						+ v.document.referrer);
				var t = "&rf=";
				var w = u.indexOf(t, 0);
				if (w != -1) {
					w += t.length;
					prevUrl = u.substring(w, u.length)
				} else {
					prevUrl = v.document.referrer
				}
				if (prevUrl == f) {
					prevUrl = "direct"
				} else {
					var s = prevUrl.split("?");
					prevUrl = s
				}
				var r = prevUrl.length;
				while (true) {
					prevUrl = decodeURIComponent(prevUrl);
					if (r == prevUrl.length) {
						break
					}
					r = prevUrl.length
				}
				BwaLogger.debug("end - bwa._getReferrer");
				return prevUrl
			},
			this._getPreviousDomain = function(t) {
				BwaLogger.debug("start - bwa._getPreviousDomain");
				var u = null;
				if (t == f || t == "direct") {
					u = "direct"
				} else {
					var v = t;
					var s = v.indexOf("http://");
					var w = v.indexOf("https://");
					var r = v.indexOf("/", 9);
					if (w == 0) {
						if (r == -1) {
							u = v.substring(s + 9)
						} else {
							u = v.substring(s + 9, r)
						}
					} else {
						if (s == 0) {
							if (r == -1) {
								u = v.substring(s + 7)
							} else {
								u = v.substring(s + 7, r)
							}
						}
					}
				}
				BwaLogger.debug("bwa._getPreviousDomain - prevDomain : " + u);
				BwaLogger.debug("end - bwa._getPreviousDomain");
				if (u == null) {
					u = ""
				}
				return u
			},
			this._getSearchEngineKeyword = function(t) {
				BwaLogger.debug("start - bwa._getSearchEngineKeyword");
				BwaLogger.debug("bwa._getSearchEngineKeyword - prevUrl : " + t);
				var r = null;
				if (this._getPreviousDomain(t).indexOf("yahoo") != -1) {
					r = "p="
				}
				if (this._getPreviousDomain(t).indexOf("google") != -1) {
					r = "q="
				}
				if (this._getPreviousDomain(t).indexOf("bing") != -1) {
					r = "q="
				}
				var u = null;
				var v = t.indexOf(r);
				BwaLogger.debug("bwa._getSearchEngineKeyword - index start : "
						+ v);
				var s = t.indexOf("&");
				BwaLogger.debug("bwa._getSearchEngineKeyword - index end : "
						+ s);
				if (v != -1) {
					if (s == -1) {
						u = t.substring(v + 2);
						BwaLogger.debug("bwa._getSearchEngineKeyword - q1 : "
								+ u)
					} else {
						u = t.substring(v + 2, s);
						BwaLogger.debug("bwa._getSearchEngineKeyword - q2 : "
								+ u)
					}
				}
				if (u == null || u == undefined) {
					u = f
				}
				BwaLogger.debug("bwa._getSearchEngineKeyword - q : " + u);
				BwaLogger.debug("end - bwa._getSearchEngineKeyword");
				return u
			},
			this._getParameterByName = function(A, s) {
				BwaLogger.debug("start - bwa._getParameterByName");
				s = s.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
				var z = this._getMappingParameterName(s);
				var v = f;
				for ( var u = 0; u < z.length; u++) {
					s = z[u];
					var r = "[\\?&]" + s + "=([^&#]*)";
					var w = new RegExp(r);
					var t = w.exec(A.location.href);
					if (t == null) {
						v = f
					} else {
						v = decodeURIComponent(t[1].replace(/\+/g, " "))
					}
					if (v != f) {
						break
					}
				}
				BwaLogger.debug("end - bwa._getParameterByName");
				return v
			},
			this._getMappingParameterName = function(r) {
				var t = [ r ];
				for ( var s = 0; s < fixParameterName.length; s++) {
					if (r == fixParameterName[s]) {
						t.push(customParameterName[s])
					}
				}
				return t
			},
			this._getCookieExpireTimeInMonth = function(s) {
				var r = new Date();
				r.setTime(r.getTime() + (s * 30 * 24 * 60 * 60 * 1000));
				return r
			},
			this._getCookieExpireTimeInDay = function(r) {
				var s = new Date();
				s.setTime(s.getTime() + (days * 24 * 60 * 60 * 1000));
				return s
			},
			this._getCookieExpireTimeInHour = function(r) {
				var s = new Date();
				s.setTime(s.getTime() + (r * 60 * 60 * 1000));
				return s
			},
			this._getCookieExpireTimeInMinute = function(s) {
				var r = new Date();
				r.setTime(r.getTime() + (s * 60 * 1000));
				return r
			},
			this._getCookieExpireTimeInMs = function(s) {
				var r = new Date(j);
				r.setTime(r.getTime() + (s));
				return r
			},
			this._setBwaSessionActionSequence = function(t, r) {
				var s = r * 60000;
				var v = this._getDiffMsBeforeMidnight();
				var u = this._getCookieExpireTimeInMs(Math.min(s, v));
				this._setCookie(c, t, u)
			},
			this._getBwaSessionActionSequence = function() {
				var r = this._getCookie(c);
				if (r == undefined) {
					r = 0
				}
				return r
			},
			this._incrementByOneAndGetBwaSessionActionSequence = function() {
				var r = this._getBwaSessionActionSequence();
				r = parseInt(r) + 1;
				this._setBwaSessionActionSequence(r, 30);
				return r
			},
			this._setBwaUserSessionSequence = function(r, t) {
				var s = this._getCookieExpireTimeInMonth(t);
				this._setCookie(h, r, s)
			},
			this._getBwaUserSessionSequence = function() {
				var r = this._getCookie(h);
				if (r == undefined) {
					r = 0
				}
				return r
			},
			this._incrementByOneAndGetBwaUserSessionSequence = function() {
				var r = this._getBwaUserSessionSequence();
				r = parseInt(r) + 1;
				this._setBwaUserSessionSequence(r, 24);
				return r
			},
			this._setBwaSessionId = function(t, r) {
				var s = r * 60000;
				var v = this._getDiffMsBeforeMidnight();
				var u = this._getCookieExpireTimeInMs(Math.min(s, v));
				this._setCookie(i, t, u)
			},
			this._getBwaSessionId = function() {
				var r = this._getCookie(i);
				if (r == undefined) {
					r = f
				}
				return r
			},
			this._setBwaUserId = function(r, t) {
				var s = this._getCookieExpireTimeInMonth(t);
				this._setCookie(e, r, s)
			},
			this._getBwaUserId = function() {
				var r = this._getCookie(e);
				if (r == undefined) {
					r = f
				}
				return r
			},
			this._setBwaSessionReferrerData = function(t, r) {
				var s = r * 60000;
				var v = this._getDiffMsBeforeMidnight();
				var u = this._getCookieExpireTimeInMs(Math.min(s, v));
				this._setCookie(k, t, u)
			},
			this._getBwaSessionReferrerData = function() {
				var r = this._getCookie(k);
				return r
			},
			this._setBwaSessionSearchEngineKeywordData = function(u, r) {
				var s = r * 60000;
				var v = this._getDiffMsBeforeMidnight();
				var t = this._getCookieExpireTimeInMs(Math.min(s, v));
				this._setCookie(m, u, t)
			},
			this._getBwaSessionSearchEngineKeywordData = function() {
				var r = this._getCookie(m);
				if (r == undefined) {
					r = f
				}
				return r
			},
			this._setBwaSessionUtmData = function(u, s, r) {
				var t = r * 60000;
				var w = this._getDiffMsBeforeMidnight();
				var v = this._getCookieExpireTimeInMs(Math.min(t, w));
				this._setCookie(u, s, v)
			},
			this._getBwaSessionUtmCampaignData = function() {
				var r = this._getCookie(q);
				return r
			},
			this._getBwaSessionUtmMediumData = function() {
				var r = this._getCookie(o);
				return r
			},
			this._getBwaSessionUtmSourceData = function() {
				var r = this._getCookie(b);
				return r
			},
			this._getBwaSessionUtmContentData = function() {
				var r = this._getCookie(a);
				return r
			},
			this._getBwaSessionUtmTermData = function() {
				var r = this._getCookie(p);
				return r
			},
			this._generateUniqueId = function(r, s) {
				return this._getHashCode(s) + "." + r + "."
						+ this._getRandomNumber() + "."
						+ this._getCurrentTimeStamp()
			},
			this._generateBwaUserId = function() {
				var r = this._getAccount();
				return this._generateUniqueId("U", r)
			},
			this._generateBwaSessionId = function() {
				var r = this._getAccount();
				return this._generateUniqueId("S", r)
			},
			this._setAccount = function() {
				BwaLogger.debug("start - bwa._setAccount");
				var r = 30 * 60000;
				var u = this._getDiffMsBeforeMidnight();
				var s = this._getCookieExpireTimeInMs(Math.min(r, u));
				var t = this._getAccount();
				if (t == f || t == "blibli") {
					t = arguments[0];
					this._setCookie(l, t, s)
				}
				BwaLogger.debug("bwa._setAccount - accountId : " + t);
				BwaLogger.debug("end - bwa._setAccount")
			},
			this._getAccount = function() {
				var r = this._getCookie(l);
				if (r == undefined) {
					r = f
				}
				BwaLogger.debug("bwa._getAccount - accountId : " + r);
				return r
			},
			this._trackSession = function(t, s) {
				BwaLogger.debug("start - bwa._trackSession");
				var r = "session";
				this._track(r);
				BwaLogger.debug("end - bwa._trackSession")
			},
			this._trackPageView = function() {
				BwaLogger.debug("start - bwa._trackPageView");
				var r = "page";
				this._track(r, [ "pagetype", arguments[0] ], [
						"clientmemberid", arguments[1] ], [ "clientmembertype",
						arguments[2] ], [ "siteid", arguments[3] ], [
						"optpageurl", arguments[4] ]);
				BwaLogger.debug("end - bwa._trackPageView")
			},
			this._trackCategoryView = function() {
				BwaLogger.debug("start - bwa._trackCategoryView");
				var r = "category";
				this._track(r, [ "categoryid", arguments[0] ], [
						"topcategoryid", arguments[1] ], [ "clientmemberid",
						arguments[2] ]);
				BwaLogger.debug("end - bwa._trackCategoryView")
			},
			this._trackProductView = function() {
				BwaLogger.debug("start - bwa._trackProductView");
				var r = "product";
				this._track(r, [ "productid", arguments[0] ], [
						"clientmemberid", arguments[1] ]);
				BwaLogger.debug("end - bwa._trackProductView")
			},
			this._trackAddToCart = function() {
				BwaLogger.debug("start - bwa._trackAddToCart");
				var r = "addtocart";
				this._track(r, [ "itemid", arguments[0] ], [ "quantity",
						arguments[1] ], [ "clientmemberid", arguments[2] ]);
				BwaLogger.debug("end - bwa._trackAddToCart")
			},
			this._trackOrder = function() {
				BwaLogger.debug("start - bwa._trackOrder");
				var r = "order";
				this._track(r, [ "orderid", arguments[0] ], [ "clientmemberid",
						arguments[1] ]);
				BwaLogger.debug("end - bwa._trackOrder")
			},
			this._trackSearchInternalKeyword = function() {
				BwaLogger.debug("start - bwa._trackSearchInternalKeyword");
				var r = "searchinternalkeyword";
				if (arguments.length == 5) {
					this._track(r, [ "searchinternalkeyword", arguments[0] ], [
							"searchinternalcategoryid", arguments[1] ], [
							"searchinternalcategoryname", arguments[2] ], [
							"clientmemberid", arguments[3] ], [ "pagetype",
							arguments[4] ]);
					this._hitTrackerGet(trackerUrl, r)
				} else {
					this._track(r, [ "searchinternalkeyword", arguments[0] ], [
							"searchinternalcategoryid", arguments[1] ], [
							"searchinternalcategoryname", arguments[2] ], [
							"clientmemberid", arguments[3] ], [ "pagetype",
							arguments[4] ])
				}
				BwaLogger.debug("end - bwa._trackSearchInternalKeyword")
			},
			this._trackSearchInternalKeywordView = function() {
				BwaLogger.debug("start - bwa._trackSearchInternalKeywordView");
				var r = "searchinternalkeywordview";
				this._track(r, [ "searchinternalkeyword", arguments[0] ], [
						"searchinternalcategoryid", arguments[1] ], [
						"searchinternalcategoryname", arguments[2] ], [
						"clientmemberid", arguments[3] ], [ "searchid",
						arguments[4] ], [ "sequence", arguments[5] ], [
						"actualsearchinternalkeyword", arguments[6] ], [
						"searchresultcount", arguments[7] ], [ "pagenumber",
						arguments[8] ], [ "sortby", arguments[9] ]);
				BwaLogger.debug("end - bwa._trackSearchInternalKeywordView")
			},
			this._trackSearchInternalResultClick = function() {
				BwaLogger.debug("start - bwa._trackSearchInternalResultClick");
				var r = "searchinternalresultclick";
				this._track(r, [ "searchinternalkeyword", arguments[0] ], [
						"searchinternalcategoryid", arguments[1] ], [
						"searchinternalcategoryname", arguments[2] ], [
						"clientmemberid", arguments[3] ], [ "searchid",
						arguments[4] ], [ "sequence", arguments[5] ], [
						"actualsearchinternalkeyword", arguments[6] ], [
						"searchresultcount", arguments[7] ], [
						"resultrankclicked", arguments[8] ], [ "productid",
						arguments[9] ], [ "pagenumber", arguments[10] ], [
						"sortby", arguments[11] ]);
				BwaLogger.debug("end - bwa._trackSearchInternalResultClick")
			},
			this._trackBanner = function() {
				BwaLogger.debug("start - bwa._trackBanner");
				var t = "mediaevent";
				var v = [ [ "accountid", this._getAccount() ],
						[ "userid", this._getBwaUserId() ],
						[ "sessionid", this._getBwaSessionId() ],
						[ "mediaid", arguments[0] ],
						[ "mediatype", arguments[1] ],
						[ "event", arguments[2] ], [ "location", arguments[3] ] ];
				var w = arguments[4];
				if (w !== undefined && !(typeof w === "function") && w !== "") {
					v.push([ "clientmemberid", w ])
				}
				var u = arguments[2];
				if (u === "open" && d.isAvailable()) {
					d.addTrackOperation(t, v)
				} else {
					var r = this._generateTrackerUrl(g, t, v);
					var s = null;
					if (arguments.length >= 1
							&& typeof arguments[arguments.length - 1] === "function") {
						s = arguments[arguments.length - 1]
					}
					this._hitTrackerGet(r, t, s);
					BwaLogger.debug("end - bwa._trackBanner")
				}
			},
			this._trackSocialMediaShare = function() {
				BwaLogger.debug("start - bwa._trackSocialMediaShare");
				var r = "socialmediashare";
				this._track(r, [ "pageType", arguments[0] ], [
						"mediaShareType", arguments[1] ], [ "clientmemberid",
						arguments[2] ], [ "memberId", arguments[2] ]);
				BwaLogger.debug("end - bwa._trackSocialMediaShare")
			},
			this._trackRecommendationView = function() {
				BwaLogger.debug("start - bwa._trackRecommendationView");
				var r = "recommendationview";
				this._track(r, [ "trackType", arguments[0] ], [ "pageType",
						arguments[1] ], [ "categoryId", arguments[2] ], [
						"productId", arguments[3] ], [ "recommendedProducts",
						arguments[4] ], [ "recommendationType", arguments[5] ],
						[ "parameters", arguments[6] ], [ "clientMemberId",
								arguments[7] ]);
				BwaLogger.debug("end - bwa._trackRecommendationView")
			},
			this._trackRecommendationProduct = function() {
				BwaLogger.debug("start - bwa._trackRecommendationProduct");
				var r = "recommendationproduct";
				this._track(r, [ "trackType", arguments[0] ], [ "pageType",
						arguments[1] ], [ "categoryId", arguments[2] ], [
						"productId", arguments[3] ], [ "recommendedProducts",
						arguments[4] ], [ "recommendationType", arguments[5] ],
						[ "parameters", arguments[6] ], [ "clientMemberId",
								arguments[7] ]);
				BwaLogger.debug("end - bwa._trackRecommendationProduct")
			},
			this._trackProductSet = function() {
				BwaLogger.debug("start - bwa._trackProductSet");
				var r = "productset";
				this._track(r, [ "productsetid", arguments[0] ], [
						"productsetname", arguments[1] ], [ "pagetype",
						arguments[2] ], [ "event", arguments[3] ], [
						"clientmemberid", arguments[4] ]);
				BwaLogger.debug("end - bwa._trackProductSet")
			},
			this._trackProductSetResultClick = function() {
				BwaLogger.debug("start - bwa._trackProductSetResult");
				var r = "productsetresultclick";
				this._track(r, [ "productsetid", arguments[0] ], [
						"productsetname", arguments[1] ], [ "pagetype",
						arguments[2] ], [ "event", arguments[3] ], [
						"clientmemberid", arguments[4] ], [ "object",
						arguments[5] ], [ "productid", arguments[6] ]);
				BwaLogger.debug("end - bwa._trackProductSetResultClick")
			},
			this._trackBrandSet = function() {
				BwaLogger.debug("start - bwa._trackBrandSet");
				var r = "brandset";
				this._track(r, [ "brandsetid", arguments[0] ], [
						"brandsetname", arguments[1] ], [ "pagetype",
						arguments[2] ], [ "event", arguments[3] ], [
						"clientmemberid", arguments[4] ]);
				BwaLogger.debug("end - bwa._trackBrandSet")
			},
			this._trackBrandSetResultClick = function() {
				BwaLogger.debug("start - bwa._trackBrandSetResultClick");
				var r = "brandsetresultclick";
				this._track(r, [ "brandsetid", arguments[0] ], [
						"brandsetname", arguments[1] ], [ "pagetype",
						arguments[2] ], [ "event", arguments[3] ], [
						"clientmemberid", arguments[4] ], [ "object",
						arguments[5] ], [ "brandid", arguments[6] ]);
				BwaLogger.debug("end - bwa._trackBrandSetResultClick")
			},
			this._trackProductList = function() {
				BwaLogger.debug("start - bwa._trackProductList");
				var r = "productlist";
				this._track(r, [ "categoryid", arguments[0] ], [
						"categoryname", arguments[1] ], [ "sequence",
						arguments[2] ], [ "productcount", arguments[3] ], [
						"clientmemberid", arguments[4] ], [ "pagenumber",
						arguments[5] ], [ "sortby", arguments[6] ], [
						"filtermap", arguments[7] ]);
				BwaLogger.debug("end - bwa._trackProductList")
			},
			this._trackProductListResultClick = function() {
				BwaLogger.debug("start - bwa._trackProductListResultClick");
				var r = "productlistresultclick";
				this._track(r, [ "categoryid", arguments[0] ], [
						"categoryname", arguments[1] ], [ "sequence",
						arguments[2] ], [ "productcount", arguments[3] ], [
						"resultrankclicked", arguments[4] ], [ "productid",
						arguments[5] ], [ "clientmemberid", arguments[6] ], [
						"pagenumber", arguments[7] ],
						[ "sortby", arguments[8] ],
						[ "filtermap", arguments[9] ]);
				BwaLogger.debug("end - bwa._trackProductListResultClick")
			},
			this._trackCategoryClick = function() {
				BwaLogger.debug("start - bwa._trackCategoryClick");
				var r = "categoryClick";
				this
						._track(r, [ "clientmemberid", arguments[0] ], [
								"c1id", arguments[1] ],
								[ "c2id", arguments[2] ], [ "c3id",
										arguments[3] ], [ "categorytype",
										arguments[4] ], [ "pageurl",
										arguments[5] ], [ "pagetype",
										arguments[6] ]);
				BwaLogger.debug("end - bwa._trackCategoryClick")
			},
			this._trackMostPopularSearchClick = function() {
				BwaLogger.debug("start - bwa._trackMostPopularSearchClick");
				var r = "mostPopularSearchClick";
				this._track(r, [ "clientmemberid", arguments[0] ], [ "keyword",
						arguments[1] ], [ "rank", arguments[2] ], [ "pageurl",
						arguments[3] ], [ "pagetype", arguments[4] ]);
				BwaLogger.debug("end - bwa._trackMostPopularSearchClick")
			},
			this._trackRecentlyViewedClick = function() {
				BwaLogger.debug("start - bwa._trackRecentlyViewedClick");
				var r = "recentlyViewedClick";
				this._track(r, [ "clientmemberid", arguments[0] ], [
						"productid", arguments[1] ], [ "productidrecent",
						arguments[2] ], [ "rank", arguments[3] ]);
				BwaLogger.debug("end - bwa._trackRecentlyViewedClick")
			},
			this._trackChangeCartQuantity = function() {
				BwaLogger.debug("start - bwa._trackChangeCartQuantity");
				var r = "changeCartQuantity";
				this._track(r, [ "clientmemberid", arguments[0] ], [ "itemid",
						arguments[1] ], [ "quantitybefore", arguments[2] ], [
						"quantityafter", arguments[3] ], [ "amount",
						arguments[4] ]);
				BwaLogger.debug("end - bwa._trackChangeCartQuantity")
			},
			this._trackCheckoutCart = function() {
				BwaLogger.debug("start - bwa._trackCheckoutCart");
				var r = "checkoutCart";
				this._track(r, [ "clientmemberid", arguments[0] ], [
						"itemlist", arguments[1] ], [ "totalamount",
						arguments[2] ]);
				BwaLogger.debug("end - bwa._trackCheckoutCart")
			},
			this._trackScrollDepth = function() {
				BwaLogger.debug("start - bwa._trackScrollDepth");
				var r = "scrollDepth";
				this._track(r, [ "clientmemberid", arguments[0] ], [ "pageurl",
						arguments[1] ], [ "pagetype", arguments[2] ], [
						"scrolldepth", arguments[3] ]);
				BwaLogger.debug("end - bwa._trackScrollDepth")
			},
			this._trackProductListPage = function() {
				BwaLogger.debug("start - bwa._trackProductListPage");
				var r = "productListPage";
				this
						._track(r, [ "clientmemberid", arguments[0] ], [
								"pageurl", arguments[1] ], [ "pagenumber",
								arguments[2] ], [ "sortby", arguments[3] ], [
								"filtermap", arguments[4] ]);
				BwaLogger.debug("end - bwa._trackProductListPage")
			},
			this._trackCustomEvent = function() {
				BwaLogger.debug("start - bwa._trackCustomEvent");
				var t = "customevent";
				var v = [ [ "accountid", this._getAccount() ],
						[ "userid", this._getBwaUserId() ],
						[ "sessionid", this._getBwaSessionId() ] ];
				var s = arguments.length;
				for ( var u = 1; u < s; u++) {
					v.push([ arguments[u][0], arguments[u][1] ])
				}
				var r = this._generateTrackerUrl(g, t, v);
				r += "&customEventUri=" + arguments[0];
				this._hitTrackerGet(r, t);
				BwaLogger.debug("end - bwa._trackCustomEvent")
			},
			this._track = function() {
				BwaLogger.debug("start - bwa._track");
				var t = "track/" + arguments[0];
				var z = this._getCurrentDevice();
				var w = [ [ "accountid", this._getAccount() ],
						[ "userid", this._getBwaUserId() ],
						[ "sessionid", this._getBwaSessionId() ],
						[ "pageurl", window.document.location.href ],
						[ "device", z[1] ], [ "devicetype", z[2] ],
						[ "browser", z[3] ], [ "browserversion", z[4] ] ];
				if (arguments[0] === "page") {
					var v = this
							._incrementByOneAndGetBwaSessionActionSequence();
					w.push([ "sessionactionsequence", v ], [ "pagetitle",
							window.document.title ], [ "prevurl",
							window.document.referrer ])
				} else {
					if (arguments[0] === "order") {
						var v = this._getBwaSessionActionSequence();
						w.push([ "sessionactionsequence", v ])
					} else {
						if (arguments[0] === "session") {
							w
									.push(
											[ "os", z[0] ],
											[
													"referrer",
													decodeURIComponent(this
															._getBwaSessionReferrerData()) ],
											[
													"searchenginekeyword",
													this
															._getBwaSessionSearchEngineKeywordData() ],
											[
													"utm_campaign",
													this
															._getBwaSessionUtmCampaignData() ],
											[
													"utm_medium",
													this
															._getBwaSessionUtmMediumData() ],
											[
													"utm_source",
													this
															._getBwaSessionUtmSourceData() ],
											[
													"utm_content",
													this
															._getBwaSessionUtmContentData() ],
											[
													"utm_term",
													this
															._getBwaSessionUtmTermData() ],
											[
													"usersessionsequence",
													this
															._getBwaUserSessionSequence() ],
											[ "prevurl",
													window.document.referrer ])
						}
					}
				}
				var s = arguments.length;
				for ( var u = 1; u < s; u++) {
					w.push([ arguments[u][0], arguments[u][1] ])
				}
				var r = this._generateTrackerUrl(g, t, w);
				this._hitTrackerGet(r, t);
				BwaLogger.debug("end - bwa._track")
			},
			this._getDiffMinutesBeforeMidnight = function() {
				var t = new Date();
				var r = new Date();
				r = r.setHours(24, 0, 0, 0);
				var u = (r - t);
				var s = Math.round(u / 60000);
				return s
			},
			this._getDiffMsBeforeMidnight = function() {
				var s = new Date(j);
				var r = new Date();
				r = r.setHours(24, 0, 0, 0);
				var t = (r - s);
				return t
			},
			this._manageBwaUserId = function() {
				var r = this._getBwaUserId();
				if (r == f) {
					BwaLogger
							.debug("bwa._manageBwaUserId - generate new user id");
					r = this._generateBwaUserId()
				}
				BwaLogger.debug("bwa._manageBwaUserId - user id : " + r);
				this._setBwaUserId(r, 24)
			},
			this._manageBwaUserSessionSequence = function() {
				var r = this._getBwaUserSessionSequence();
				BwaLogger
						.debug("bwa._manageBwaUserSessionSequence - user session sequence : "
								+ r);
				this._setBwaUserSessionSequence(r, 24)
			},
			this._manageBwaSessionId = function() {
				var r = this._getBwaSessionId();
				if (r == f) {
					BwaLogger
							.log("bwa._manageBwaSessionId - generate new session id");
					r = this._generateBwaSessionId();
					bwaUserSessionSequence = this
							._incrementByOneAndGetBwaUserSessionSequence();
					BwaLogger
							.debug("bwa._manageBwaSessionId - bwaUserSessionSequence : "
									+ bwaUserSessionSequence);
					this._setBwaSessionId(r, 30);
					this._trackSession(r, bwaUserSessionSequence)
				}
				this._setBwaSessionId(r, 30);
				BwaLogger.debug("bwa._manageBwaSessionId - session id : " + r)
			}, this._manageBwaSessionReferrerData = function(t) {
				BwaLogger.debug("start - bwa._manageBwaSessionReferrerData");
				if (this._getBwaSessionId() == f) {
					var s = this._getReferrer(t);
					var r = encodeURIComponent(s);
					this._setBwaSessionReferrerData(r, 30);
					var u = this._getSearchEngineKeyword(s);
					if (u != f) {
						this._setBwaSessionSearchEngineKeywordData(u, 30)
					}
				}
				BwaLogger.debug("end - bwa._manageBwaSessionReferrerData")
			}, this._manageBwaSessionUtmData = function(w) {
				BwaLogger.debug("start - bwa._manageBwaSessionUtmData");
				var v = this._getParameterByName(w, "utm_campaign");
				var C = this._getParameterByName(w, "utm_medium");
				var r = this._getParameterByName(w, "utm_source");
				var D = this._getParameterByName(w, "utm_content");
				var s = this._getParameterByName(w, "utm_term");
				BwaLogger.debug("utm_campaign : " + v);
				BwaLogger.debug("utm_medium : " + C);
				BwaLogger.debug("utm_source : " + r);
				BwaLogger.debug("utm_content : " + D);
				BwaLogger.debug("utm_term : " + s);
				BwaLogger.debug("session: " + this._getBwaSessionId());
				if (this._getBwaSessionId() == f) {
					BwaLogger.debug("session is still empty");
					this._setBwaSessionUtmData(q, v, 30);
					this._setBwaSessionUtmData(o, C, 30);
					this._setBwaSessionUtmData(b, r, 30);
					this._setBwaSessionUtmData(a, D, 30);
					this._setBwaSessionUtmData(p, s, 30)
				} else {
					if (v || C || r || D || s) {
						BwaLogger.debug("some utm not empty");
						var B = this._getBwaSessionUtmCampaignData();
						var u = this._getBwaSessionUtmMediumData();
						var A = this._getBwaSessionUtmSourceData();
						var z = this._getBwaSessionUtmContentData();
						var t = this._getBwaSessionUtmTermData();
						if (B != v || u != C || A != r || z != D || t != s) {
							BwaLogger.debug("renew session");
							this._deleteCookie(i);
							this._setBwaSessionUtmData(q, v, 30);
							this._setBwaSessionUtmData(o, C, 30);
							this._setBwaSessionUtmData(b, r, 30);
							this._setBwaSessionUtmData(a, D, 30);
							this._setBwaSessionUtmData(p, s, 30)
						}
					}
				}
				BwaLogger.debug("session After: " + this._getBwaSessionId());
				BwaLogger.debug("end - bwa._manageBwaSessionUtmData")
			}, this.initData = function(r) {
				BwaLogger.debug("start - bwa.init");
				var s = true;
				if (r.bwaQueueAvailability != undefined) {
					s = bwaQueueAvailability
				}
				d.setAvailable(s);
				this._manageBwaSessionReferrerData(r);
				this._manageBwaSessionUtmData(r);
				this._manageBwaUserId();
				this._manageBwaUserSessionSequence();
				this._manageBwaSessionId();
				BwaLogger.debug("end - bwa.init")
			}, this.push = function() {
				BwaLogger.debug("start - bwa.push");
				for ( var r = 0; r < arguments.length; r++) {
					try {
						var s = arguments[r][0];
						var t = arguments[r].slice(1, arguments[r].length);
						var u = this[s];
						u.apply(this, t)
					} catch (v) {
						BwaLogger.debug(v.toString())
					}
				}
				BwaLogger.debug("end - bwa.push")
			}
};
var _exist_bwaq = window._bwaq;
window._bwaq = new bwa();
var _config_bwaq = [ "_setAccount" ];
var _tracker_bwaq = _exist_bwaq.slice(0);
var numOfConfigCmdBwaqFound = 0;
for ( var x = 0; x < _exist_bwaq.length; x++) {
	for ( var y = 0; y < _config_bwaq.length; y++) {
		if (_exist_bwaq[x][0] == _config_bwaq[y]) {
			window._bwaq.push.call(window._bwaq, _exist_bwaq[x]);
			_tracker_bwaq.splice(x - numOfConfigCmdBwaqFound, 1);
			numOfConfigCmdBwaqFound++;
			break
		}
	}
}
window._bwaq.initData(window);
window._bwaq.push.apply(window._bwaq, _tracker_bwaq);
